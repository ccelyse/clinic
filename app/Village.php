<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = "vilages";
    protected $fillable = ['id','CodeVillage','VillageName','codecell'];
}
