<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountantDeposit extends Model
{
    protected $table = "depositaccountant";
    protected $fillable = ['id','deposit_amount','deposit_date','depositcomment','deposited_by'];
}
