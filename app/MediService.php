<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediService extends Model
{
    protected $table = "mediservice";
    protected $fillable = ['id','medicinecode','medicinename','department_name_id','medicinequantity','insurance_medicine_id','medicinepriceinsurance'];
}
