<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DentalClinic extends Model
{
    protected $table = "DentalClinic";
    protected $fillable = ['id','consultation','Historyofpresentillness','PastMedicalhistory','Pastdentalhistory','ExtraoralExam','IntraoralExam','Examinationofteeth','clinicalfinding','Investigation','Diagnosis','Treatment','patient_id','patientrecoddateid','savedby'];
}
