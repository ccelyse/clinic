<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicineData extends Model
{
    protected $table = "medicine_table";
    protected $fillable = ['id','medicinecode','medicinename','medicinequantity','department_name_id'];
}
