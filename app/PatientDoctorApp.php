<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientDoctorApp extends Model
{
    protected $table = "patient_doctor_appointment";
    protected $fillable = ['id','patient_status','doctor_id','patient_id','nurse_id','patientrecoddateid','timeconfirm','dateconfirm'];
}
