<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicinePrice extends Model
{
    protected $table = "medicine_price";
    protected $fillable = ['id','medicine_id','medicine_department_id','medicine_insurance_id','medicinepriceprivate','medicinepriceinsurance'];
}
