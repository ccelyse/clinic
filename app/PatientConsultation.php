<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientConsultation extends Model
{
    protected $table = "patient_consultation";
    protected $fillable = ['id','consultation_comment','nurseid','patient_id','patientrecoddateid'];

}
