<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandName extends Model
{
    protected $table = "BrandName";
    protected $fillable = ['id','brandname','brandlogo'];
}
