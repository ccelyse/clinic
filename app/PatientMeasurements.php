<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientMeasurements extends Model
{
    protected $table = "patient_measurements";
    protected $fillable = ['id','patientWeight','patient_Diastolic','patient_Circumference','patient_Pulse','patient_Templocation','patient_Respiration','patient_OxygenSaturation','patient_BMI','patient_Temperature','patient_BMIstatus','patientrecoddateid','nurseid'];
}
