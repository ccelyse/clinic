<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceVerification extends Model
{
    protected $table = "InsuranceVerification";
    protected $fillable = ['id','patient_id','status','verified_by'];

}
