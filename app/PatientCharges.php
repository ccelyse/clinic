<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientCharges extends Model
{
    protected $table = "patientcharges";
    protected $fillable = ['id','activity_name','insurance_id','Medicine_service_name','medicine_service_price','patient_id','patientrecoddateid','nurseid','insurance_amount','patient_amount'];

}
