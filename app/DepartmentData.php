<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentData extends Model
{
    protected $table = "departmentdata";
    protected $fillable = ['id','departmentname'];
}
