<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomLevel extends Model
{
    protected $table = "roomlevels";
    protected $fillable = ['id','roomlevel','roomnumber','roomstatus','roomprice'];
}
