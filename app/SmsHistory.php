<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsHistory extends Model
{
    protected $table = "smshistory";
    protected $fillable = ['id','from','to','message','deliverystatus'];
}
