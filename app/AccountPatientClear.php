<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountPatientClear extends Model
{
    protected $table = "accountpatientclear";
    protected $fillable = ['id','insurance_due','amount_due','paymentcomment','chargesreasons','amount_paid','insurance_id','accountant_id','patientrecoddateid','patient_id','paymentmethod','remainingbalance'];
}
