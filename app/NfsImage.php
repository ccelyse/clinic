<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NfsImage extends Model
{
    protected $table = "nfsimage";
    protected $fillable = ['id','nfsimage','patient_id','patientrecoddateid','Laboratorytechnician'];
}
