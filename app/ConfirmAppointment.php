<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmAppointment extends Model
{
    protected $table = "confirm_appointments";
    protected $fillable = ['id','dateconfirm','patient_status','appointment_message','timeconfirm','patient_id','patientrecoddateid','nurse_id'];
}
