<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomPrice extends Model
{
    protected $table = "roomprice";
    protected $fillable = ['id','roomlevel','roomnumber','insurance_medicine','roomprice'];
}
