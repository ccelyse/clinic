<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = "patient";
    protected $fillable = ['id','patient_names','patient_country_id','insurance_patient_name','insurance_card_number','gender',
        'dateofbirth','patient_telephone_number','copay','copay_relationship','beneficiary_name','beneficiary_telephone_number','insured_location'];
}
