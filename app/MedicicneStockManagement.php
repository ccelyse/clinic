<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicicneStockManagement extends Model
{
    protected $table = "medicinestockmanagement";
    protected $fillable = ['id','productname','productpackage','productpiece','productunitprice','shipmentstatus','remainingpiece'];
}
