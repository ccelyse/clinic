<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaboratoryResultNfs extends Model
{
    protected $table = "laboratoryresultnfs";
    protected $fillable = ['id','laboratorytestname','laboratorytype','laboappoint_id','wbc','ly','mid','gra','rbc','hgb','hct','plt','ecbu','eap','fv_fu','mvc','mch','patient_id','patientrecoddateid','resultcategory','Labotechid'];
}
