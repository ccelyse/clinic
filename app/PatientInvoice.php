<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientInvoice extends Model
{
    protected $table = "patientinvoice";
    protected $fillable = ['id','accountpatientclear_id'];
}
