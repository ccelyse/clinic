<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentLaboratory extends Model
{
    protected $table = "appointmentlaboratory";
    protected $fillable = ['id','Laboratorytechnician','patient_id','lab_tech','nurse_id','patientrecoddateid','patient_status'];
}
