<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaboratoryInsurancePrice extends Model
{
    protected $table = "Laboratory_insurance_price";
    protected $fillable = ['id','labotest_id','departmentid','insuranceid','laboratoryprice'];
}
