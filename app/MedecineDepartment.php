<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedecineDepartment extends Model
{
    protected $table = "medicine_department_";
    protected $fillable = ['id','medicine_id','department_name'];
}
