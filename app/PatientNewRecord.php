<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientNewRecord extends Model
{
    protected $table = "patient_new_record";
    protected $fillable = ['id','patient_id','patientrecorddate'];
}
