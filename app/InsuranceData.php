<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceData extends Model
{
    protected $table = "insurancedata";
    protected $fillable = ['id','insurance_name'];
}
