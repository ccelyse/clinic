<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Request;
class ApiLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = "1234567dfghjiookjnrghnm";
        $validate = Request::header('token');
        $email = Request::header('email');
        $password = Request::header('password');

        if(!isset($validate)){
            return response()->json(['error' => 'token_required']);
        }
        if(!isset($email)){
            return response()->json(['error' => 'email_required']);
        }
        if(!isset($password)){
            return response()->json(['error' => 'password_required']);
        }


        $credentials = [
            'email' => $email,
            'password' => $password,
        ];


        if (Auth::attempt($credentials)  && $validate == $token) {

            return $next($request);
        }
        else{
            return response()->json(['error' => 'access-denied']);
        }
        return $next($request);
    }
}
