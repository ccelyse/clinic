<?php

namespace App\Http\Controllers;

use App\AccountantDeposit;
use App\AccountantExpense;
use App\AccountPatientClear;
use App\Cell;
use App\ConfirmAppointment;
use App\DentalClinic;
use App\DepartmentStockManagement;
use App\District;
use App\DoctorLabComment;
use App\HospitilazePatient;
use App\Http\Controllers\Controller;
use App\ApplyVisaFree;
use App\AppointmentLaboratory;
use App\Attractions;
use App\BrandModel;
use App\BrandName;
use App\Countries;
use App\DepartmentData;
use App\Hireaguide;
use App\InsuranceData;
use App\InsuranceVerification;
use App\JoinMember;
use App\LaboratoryInsurancePrice;
use App\LaboratoryOtherResults;
use App\LaboratoryResultNfs;
use App\LaboratoryTest;
use App\MedecineDepartment;
use App\MedicicneStockManagement;
use App\MedicineData;
use App\MedicineInsurance;
use App\MedicinePrice;
use App\Medicineshipment;
use App\MediService;
use App\NfsImage;
use App\Notifications\LaboratoryResults;
use Notifiable;
use App\Patient;
use App\PatientCharges;
use App\PatientConsultation;
use App\PatientDoctorApp;
use App\PatientInvoice;
use App\PatientMeasurements;
use App\PatientNewRecord;
use App\Province;
use App\RoomLevel;
use App\RoomPrice;
use App\Sector;
use App\SmsHistory;
use App\SOAP;
use App\User;
use App\Village;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Illuminate\Notifications\Notification;
use Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Excel;
use File;


require_once('./Class_MR_SMS_API.php');


class BackendController extends Controller
{
    public function SignIn_(Request $request){

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            $user_get_role = User::where('email', $request['email'])->value('role');

            switch ($user_get_role) {
                case "Admin":
                    return redirect()->intended('/Dashboard');
                    break;
                case "SuperAdmin":
                    return redirect()->intended('/Dashboard');
                    break;
                case "Doctor":
                    return redirect()->intended('/DoctorDashboard');
                    break;
                case "Doctor Dentist":
                    return redirect()->intended('/DoctorDentistDashboard');
                    break;
                case "Laboratory":
                    return redirect()->intended('/LaboratoryDashboard');
                    break;
                case "Accountant":
                    return redirect()->intended('/AccountantDashboard');
                    break;
                case "Reception":
                    return redirect()->intended('/ReceptionDashboard');
                    break;

                case "Insurance Verification":
                    return redirect()->intended('/InsuranceVerification');
                    break;

                case "Nurse":
                    return redirect()->intended('/NurseDashboard');
                    break;
                case "Hospitalization":
                    return redirect()->intended('/HospitalizationDashboard');
                    break;
                default:
                    return back();
                    break;
            }

        }
        else{
            return back()->with('success','your email or your password is not matching');
        }
    }
    public function getLogout(){
        Auth::logout();
        return redirect()->route('welcome');
    }

    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){

        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');
        return view('backend.Dashboard')->with(['insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);

    }
    public function CommunicationSendSms(){
        $listnumber = AccountPatientClear::join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->select('patient.*','accountpatientclear.created_at')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();
        return view('backend.CommunicationSendSms')->with(['listnumber'=>$listnumber]);
    }
    public function CommunicationSendSmsFilter(Request $request){
        $all = $request->all();
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];

        $listnumber = AccountPatientClear::whereBetween('accountpatientclear.created_at',[$fromdate,$todate])
            ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->select('patient.*','accountpatientclear.created_at')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();
        return view('backend.CommunicationSendSmsFilter')->with(['listnumber'=>$listnumber]);
//        return redirect()->route('backend.CommunicationSendSmsFilter')->with(['listnumber'=>$listnumber]);
    }

    public function FilterSendSMS(Request $request){
        $all = $request->all();
        $rsgamessage = $request['limitedtextarea'];
        $manyphonenumber = $request['manyphonenumber'];

        if($manyphonenumber == null){
            return back()->with('danger','you did not select phone number');
        }else{

            foreach ($manyphonenumber as $key => $manyphonenumber) {

                $api_key = 'SmR5T3Vnbj1GT0thSmhvR3Z5S20=';
                $from = 'NDENGERA';
                $destination = $request['manyphonenumber'][$key];
                $action='send-sms';
                $url = 'https://mistasms.com/sms/api';
                $sms = $rsgamessage;
                $unicode = 1; //For Plain Message
                $unicode = 0; //For Unicode Message
                $sms_body = array(
                    'api_key' => $api_key,
                    'to' => $destination,
                    'from' => $from,
                    'sms' => $sms,
                    'unicode' => $unicode,
                );
                $client = new \MRSMSAPI();
                $response = $client->send_sms($sms_body, $url);
                $response=json_decode($response);

                $savereport = new SmsHistory();
                $savereport->from = $from;
                $savereport->to = $destination;
                $savereport->message = $rsgamessage;
                $savereport->deliverystatus = $response->message;
                $savereport->save();

//                 echo 'Message: '.$response->message;
            }
            return redirect()->route('backend.CommunicationSendSms')->with('success','you have successfully sent your message');
//            if($response->code == 'ok'){
//
//            }else{
//                return back()->with('danger','you sms is not successfully sent');
//            }
            }
        }
        public function SmsList(){
        $firstdayoflastmonth = new Carbon('first day of last month');
        $lastdayoflastmonth = new Carbon('last day of last month');

        $firstdayofcurrentmonth = new Carbon('first day of this month');
        $lastdayofcurrentmonth = new Carbon('last day of this month');

        $smssentlastm = SmsHistory::whereBetween('created_at',[$firstdayoflastmonth,$lastdayoflastmonth])->select(DB::raw('count(id) as smssentlastm'))->value('smssentlastm');

        $smssentthism = SmsHistory::whereBetween('created_at',[$firstdayofcurrentmonth,$lastdayofcurrentmonth])->select(DB::raw('count(id) as smssentthism'))->value('smssentthism');

        $listsms = SmsHistory::orderBy('id', 'desc')->get();

        return view('backend.SmsList')->with(['listsms'=>$listsms,'smssentthism'=>$smssentthism,'smssentlastm'=>$smssentlastm]);
        }




    /**
     *
     * RECEPTION MODULE
     */

    public function ReceptionDashboard(){
        $accountnumber = User::where('role','Reception')->select(DB::raw('count(id) as accountnumber'))->value('accountnumber');
        $patientnumber = Patient::select(DB::raw('count(id) as patientnumber'))->value('patientnumber');
        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');

        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         * SUMMARY TABLE IN PATIENT
         */

        $patient_jan = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_feb = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_marc = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_apr = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_may = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_jun = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('patientnumber');


        $patient_jul = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_aug = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_sept = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_oct = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_nov = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_dec = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('patientnumber');

        return view('backend.ReceptionDashboard')->with(['patient_dec'=>$patient_dec,'patient_nov'=>$patient_nov,'patient_oct'=>$patient_oct,'patient_sept'=>$patient_sept,'patient_aug'=>$patient_aug,'patient_jul'=>$patient_jul,'patient_jun'=>$patient_jun,'patient_may'=>$patient_may,'patient_apr'=>$patient_apr,'patient_marc'=>$patient_marc,'patient_jan'=>$patient_jan,'patient_feb'=>$patient_feb,'accountnumber'=>$accountnumber,'patientnumber'=>$patientnumber,'insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);

    }
    public function ListReception(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->limit(20)
            ->get();
//        $users = User::where('votes', '>', 100)->paginate(15);
        $province = Province::all();

        return view('backend.ListReception')->with(['province'=>$province,'listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }
    public function ListReceptionReport(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $patientnumber = Patient::select(DB::raw('count(id) as patientnumber'))->value('patientnumber');
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->limit(20)
            ->get();
//        $users = User::where('votes', '>', 100)->paginate(15);
        $province = Province::all();

        return view('backend.ListReceptionReport')->with(['patientnumber'=>$patientnumber,'province'=>$province,'listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }
    public function ListReceptionReportFilter(Request $request){
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $patientnumber = Patient::whereBetween('patient.created_at',[$fromdate,$todate])->select(DB::raw('count(id) as patientnumber'))->value('patientnumber');
        $listpatient = Patient::whereBetween('patient.created_at',[$fromdate,$todate])->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        return view('backend.ListReceptionReport')->with(['patientnumber'=>$patientnumber,'listpatient'=>$listpatient]);

    }
    public function SearchReception(Request $request){
        $patientname = $request['patientname'];
        $listpatient = Patient::where('beneficiary_name', 'like', '%' . $patientname . '%')
            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('insurancedata.insurance_name','patient.*')
            ->get();


        foreach($listpatient as $patient){
            $record = route('backend.EditPatient',['id'=>$patient->id]);
            $NewRecordSearch = route('backend.NewRecordSearch',['id'=>$patient->id]);
            echo "<tr><td><a href='$NewRecordSearch' class='btn btn-login btn-min-width mr-1 mb-1'><i class='fas fa-edit'></i> New Record</a></td><td><a href='$record'>{$patient->beneficiary_name}</a></td><td>{$patient->gender}</td></td><td>{$patient->beneficiary_telephone_number}</td><td>{$patient->insurance_name}</td><td>{$patient->pid}</td></tr>";
        }

    }
    public function NewRecordSearch(Request $request){
        $id = $request['id'];
        return view('backend.NewRecordSearch')->with(['id'=>$id]);
    }
    public function Reception(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $province = Province::all();

        return view('backend.Reception')->with(['province'=>$province,'listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }

    public function GetDistrict(Request $request){
        $id = $request['province'];
        $getdistrict = District::where('provincecode',$id)->get();
        echo "<option value=\"\"></option>";
        foreach ($getdistrict as $district){
            echo "<option value='{$district->districtcode}'>{$district->namedistrict}</option>";
        }
    }

    public function GetSector(Request $request){
        $id = $request['district'];
        $getsector = Sector::where('districtcode',$id)->get();
        echo "<option value=\"\"></option>";
        foreach ($getsector as $sector){
            echo "<option value='{$sector->sectorcode}'>{$sector->namesector}</option>";
        }
    }
    public function GetCell(Request $request){
        $id = $request['sector'];
        $getcell = Cell::where('sectorcode',$id)->get();
        echo "<option value=\"\"></option>";
        foreach ($getcell as $cell){
            echo "<option value='{$cell->codecell}'>{$cell->nameCell}</option>";
        }
    }
    public function GetVillage(Request $request){
        $id = $request['cell'];
        $getvillage = Village::where('codecell',$id)->get();
        echo "<option value=\"\"></option>";
        foreach ($getvillage as $village){
            echo "<option value='{$village->CodeVillage}'>{$village->VillageName}</option>";
        }
    }
    public function Reception_(Request $request){
        $all = $request->all();
        $currentMonth = Carbon::now()->format('m');
        $code = 'PID'. mt_rand(10, 99). $currentMonth;

        $patient = new Patient();
        $patient->patient_names = $request['patient_names'];
        $patient->patient_country_id = $request['patient_country_id'];
        $patient->insurance_patient_name = $request['insurance_patient_name'];
        $patient->insurance_card_number = $request['insurance_card_number'];
        $patient->gender = $request['gender'];
        $patient->dateofbirth = $request['dateofbirth'];
        $patient->patient_telephone_number = $request['patient_telephone_number'];
        $patient->copay = $request['copay'];
        $patient->copay_relationship = $request['copay_relationship'];
        $patient->beneficiary_name = $request['beneficiary_name'];
        $patient->beneficiary_telephone_number = $request['beneficiary_telephone_number'];
        $patient->insured_location = $request['insured_location'];
        $patient->beneficiary_province = $request['beneficiary_province'];
        $patient->beneficiary_district = $request['beneficiary_district'];
        $patient->beneficiary_sector = $request['beneficiary_sector'];
        $patient->beneficiary_cell = $request['beneficiary_cell'];
        $patient->beneficiary_village = $request['beneficiary_village'];
        $patient->pid = $code;
        $patient->save();

        $newrecordpatient = new PatientNewRecord();
        $newrecordpatient->patient_id = $patient->id;
        $newrecordpatient->patientrecorddate = $patient->created_at;
        $newrecordpatient->save();

        $user_id = \Auth::user()->id;

        $insurancestatus  = new InsuranceVerification();
        $insurancestatus->patient_id = $patient->id;
        $insurancestatus->status = 'Not Verified Yet';
        $insurancestatus->verified_by = '';
        $insurancestatus->save();

        return back()->with('success','you have successfully created new patient data');

    }
    public function NewRecordHistory(Request $request){
        $allpatient = $request->all();
//        dd($allpatient);
        $newrecorddate = new PatientNewRecord();
        $newrecorddate->patient_id = $request['patient_id'];
        $newrecorddate->patientrecorddate = $request['newrecorddate'];
        $newrecorddate->save();
        return back()->with('success','you have successfully created new patient record date');
    }
    public function EditPatient(Request $request){
        $id = $request['id'];
        $listinsu= InsuranceData::all();
        $editPatient = Patient::where('patient.id',$id)
            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->join('provinces', 'provinces.provincecode', '=', 'patient.beneficiary_province')
            ->join('districts', 'districts.districtcode', '=', 'patient.beneficiary_district')
            ->join('sectors', 'sectors.sectorcode', '=', 'patient.beneficiary_sector')
            ->join('cells', 'cells.codecell', '=', 'patient.beneficiary_cell')
            ->join('vilages', 'vilages.CodeVillage', '=', 'patient.beneficiary_village')
            ->select('provinces.provincename','districts.namedistrict','sectors.namesector','cells.nameCell','vilages.VillageName','patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','insurancedata.insurance_name','patient.beneficiary_province','patient.beneficiary_district','patient.beneficiary_sector','patient.beneficiary_cell','patient.beneficiary_village')
            ->get();
        $province = Province::all();
        return view('backend.EditPatient')->with(['province'=>$province,'listinsu'=>$listinsu,'editPatient'=>$editPatient]);
    }
    public function EditPatient_(Request $request){
        $id = $request['id'];
        $updatepatient = Patient::find($id);
        $updatepatient->patient_names = $request['patient_names'];
        $updatepatient->patient_country_id = $request['patient_country_id'];
        $updatepatient->insurance_patient_name = $request['insurance_patient_name'];
        $updatepatient->insurance_card_number = $request['insurance_card_number'];
        $updatepatient->gender = $request['gender'];
        $updatepatient->dateofbirth = $request['dateofbirth'];
        $updatepatient->patient_telephone_number = $request['patient_telephone_number'];
        $updatepatient->copay = $request['copay'];
        $updatepatient->copay_relationship = $request['copay_relationship'];
        $updatepatient->beneficiary_name = $request['beneficiary_name'];
        $updatepatient->beneficiary_telephone_number = $request['beneficiary_telephone_number'];
        $updatepatient->insured_location = $request['insured_location'];
        $updatepatient->beneficiary_province = $request['beneficiary_province'];
        $updatepatient->beneficiary_district = $request['beneficiary_district'];
        $updatepatient->beneficiary_sector = $request['beneficiary_sector'];
        $updatepatient->beneficiary_cell = $request['beneficiary_cell'];
        $updatepatient->beneficiary_village = $request['beneficiary_village'];
        $updatepatient->save();
        return redirect()->route('backend.ListReception')->with('success','You have successful updated Patient Information');
    }
    public function DeletePatient(Request $request){
        $id = $request['id'];
        $DeletePatient = Patient::find($id);
        $DeletePatient->delete();
        return back()->with('success','you successfully deleted Patient Date');

    }
    public function ReceptionAccountPatient(Request $request){
        $id = $request['id'];
        $listusers = User::all();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();
        $listpatientcharges= PatientCharges::join('insurancedata', 'insurancedata.id', '=', 'patientcharges.insurance_id_')
            ->join('medicine_insurance', 'medicine_insurance.medicinepriceinsurance', '=', 'patientcharges.medicine_service_price')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('patientcharges.*','insurancedata.insurance_name','medicine_insurance.medicinepriceinsurance','medicine_table.medicinename')
            ->get();
//        dd($listpatientcharges);
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->join('users', 'users.id', '=', 'patient_doctor_appointment.doctor_id')
            ->select('patient_doctor_appointment.*','users.name','users.email')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('users', 'users.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','users.name','users.email')
            ->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        return view('backend.ReceptionAccountPatient')->with(['NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusers'=>$listusers,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp]);

    }
    public function ReceptionAccountant(){

        $insurance = InsuranceData::all();
        $invoicedetails = AccountPatientClear::where('chargesreasons','patientcharges')->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
            ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
            ->get();
        return view('backend.ReceptionAccountant')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

    }
    public function ReceptionAccountantFilter(Request $request){
        $all = $request->all();
        $insuranceid = $request['insurance'];
        $paymentmethod = $request['paymentmethod'];
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $insurance = InsuranceData::all();

        if($insuranceid == 'All' AND $paymentmethod == 'All'){

            $invoicedetails = AccountPatientClear::where('chargesreasons','patientcharges')->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();

            return view('backend.AccountantSalesFilter')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }
        elseif($insuranceid != 'All' AND $paymentmethod == 'All'){

            $invoicedetails = AccountPatientClear::where('chargesreasons','patientcharges')->where('accountpatientclear.insurance_id',$insuranceid)
                ->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
            return view('backend.AccountantSalesFilter')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }
        elseif($insuranceid !== 'All' AND $paymentmethod !='All'){

            $invoicedetails = AccountPatientClear::where('chargesreasons','patientcharges')->where('accountpatientclear.insurance_id',$insuranceid)
                ->where('accountpatientclear.paymentmethod',$paymentmethod)
                ->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
            return view('backend.ReceptionAccountantFilter')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }
    }
    public function ReceptionAppointment(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        return view('backend.ReceptionAppointment')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }
    public function ReceptionAppPatientHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->orderBy('id', 'desc')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.ReceptionAppPatientHistory')->with(['listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);
    }
    public function ReceptionDoctorAppointment(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->orWhere('role','Doctor Dentist')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();


        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();


        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->orderBy('id','desc')
            ->get();

        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        return view('backend.ReceptionDoctorAppointment')->with(['patient_amountpayed'=>$patient_amountpayed,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listinsu'=>$listinsu,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }

    /**
     *
     * END of RECEPTION MODULE
     */


    /**
     *
     * START of Insurance verification MODULE
     */
    public function InsuranceVerification(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        return view('backend.InsuranceVerification')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }
    public function VerifyPatient(Request $request){

        $user_id = \Auth::user()->id;
        $patient_id = $request['patient_id'];
        $checkstatus = InsuranceVerification::where('patient_id',$patient_id)->value('status');

        if($checkstatus == null){
            $insurancestatus  = new InsuranceVerification();
            $insurancestatus->patient_id = $request['patient_id'];
            $insurancestatus->status = $request['insurance_status'];
            $insurancestatus->verified_by =$user_id;
            $insurancestatus->save();
            return back()->with('success','you have changed confirmed patient insurance status');
        }else{
            return back()->with('success','there is status already, kindly remove status to update');
        }

    }



    public function DeleteVerifyPatient(Request $request){
        $id = $request['id'];
        $deletestatus = InsuranceVerification::find($id);
        $deletestatus->delete();
        return back()->with('success','you have successfully deleted patient insurance verification');
    }

    public function InsuranceVerificationDashboard(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        return view('backend.InsuranceVerificationDashboard')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }

    /**
     *
     * END of Insurance verification
     */


    /**
     *
     * START OF  NURSE MODULE
     */


    public function NurseDashboard(){
        $accountnumber = User::where('role','Reception')->select(DB::raw('count(id) as accountnumber'))->value('accountnumber');
        $patientnumber = Patient::select(DB::raw('count(id) as patientnumber'))->value('patientnumber');
        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');

        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         * SUMMARY TABLE IN PATIENT
         */

        $patient_jan = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_feb = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_marc = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_apr = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_may = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_jun = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('patientnumber');


        $patient_jul = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_aug = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_sept = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_oct = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_nov = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_dec = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('patientnumber');

        return view('backend.NurseDashboard')->with(['patient_dec'=>$patient_dec,'patient_nov'=>$patient_nov,'patient_oct'=>$patient_oct,'patient_sept'=>$patient_sept,'patient_aug'=>$patient_aug,'patient_jul'=>$patient_jul,'patient_jun'=>$patient_jun,'patient_may'=>$patient_may,'patient_apr'=>$patient_apr,'patient_marc'=>$patient_marc,'patient_jan'=>$patient_jan,'patient_feb'=>$patient_feb,'accountnumber'=>$accountnumber,'patientnumber'=>$patientnumber,'insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);


    }
    public function NurseConsultation(){
        $listinsu= InsuranceData::all();

        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        return view('backend.NurseConsultation')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }

    public function Nurse(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        return view('backend.Nurse')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }
    public function PatientHistoryRecord(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->orderBy('id', 'desc')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.PatientHistoryRecord')->with(['listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id]);

    }
    public function NurseOperationPatientRecordHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.NurseOperationPatientRecordHistory')->with(['id'=>$id,'listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }

    public function NurseConsultationPatient(Request $request){

        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;

        $listusersdoctors = User::where('role','Doctor')->get();
        $listusers = User::where('role','Doctor')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');

        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listconslutationcomment = PatientConsultation::where('patient_consultation.patient_id',$getpatientidfromdaterecord)
            ->join('users', 'users.id', '=', 'patient_consultation.nurseid')
            ->select('patient_consultation.*','users.name')
            ->get();

        $listmeasurements = PatientMeasurements::where('patient_measurements.patient_id',$getpatientidfromdaterecord)
            ->join('users', 'users.id', '=', 'patient_measurements.nurseid')
            ->select('patient_measurements.*','users.name')
            ->get();

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->join('users', 'users.id', '=', 'patient_doctor_appointment.doctor_id')
            ->select('patient_doctor_appointment.*','users.name','users.email')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $departmentdata = DepartmentData::all();



        $listlabotest = LaboratoryTest::all();
        return view('backend.NurseConsultationPatient')->with(['patient_amountpayed'=>$patient_amountpayed,'listmeasurements'=>$listmeasurements,'listconslutationcomment'=>$listconslutationcomment,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function AddPatientMeasurement(Request $request){
        $all = $request->all();

//        dd($all);
        $user_id = \Auth::user()->id;
        $AddMeasurements = new  PatientMeasurements();
        $AddMeasurements->patientWeight = $request['patientWeight'];
        $AddMeasurements->patient_Diastolic = $request['patient_Diastolic'];
        $AddMeasurements->patient_Circumference = $request['patient_Circumference'];
        $AddMeasurements->patient_Pulse = $request['patient_Pulse'];
        $AddMeasurements->patient_Templocation = $request['patient_Templocation'];
        $AddMeasurements->patient_Respiration = $request['patient_Respiration'];
        $AddMeasurements->patient_OxygenSaturation = $request['patient_OxygenSaturation'];
        $AddMeasurements->patient_BMI = $request['patient_BMI'];
        $AddMeasurements->patient_Temperature = $request['patient_Temperature'];
        $AddMeasurements->patient_BMIstatus = $request['patient_BMIstatus'];
        $AddMeasurements->patient_id = $request['patient_id'];
        $AddMeasurements->patientrecoddateid = $request['patientrecoddateid'];
        $AddMeasurements->nurseid = $user_id;
        $AddMeasurements->save();
        return back()->with('success','you have successfully added patient measurements');
    }
    public function EditPatientMeasurement(Request $request){
        $id = $request['id'];
        $EditPatientMeasurement = PatientMeasurements::find($id);
        $EditPatientMeasurement->patientWeight = $request['patientWeight'];
        $EditPatientMeasurement->patient_Diastolic = $request['patient_Diastolic'];
        $EditPatientMeasurement->patient_Circumference = $request['patient_Circumference'];
        $EditPatientMeasurement->patient_Pulse = $request['patient_Pulse'];
//        $EditPatientMeasurement->patient_Systoric = $request['patient_Systoric'];
        $EditPatientMeasurement->patient_Templocation = $request['patient_Templocation'];
        $EditPatientMeasurement->patient_Respiration = $request['patient_Respiration'];
        $EditPatientMeasurement->patient_OxygenSaturation = $request['patient_OxygenSaturation'];
        $EditPatientMeasurement->patient_BMI = $request['patient_BMI'];
        $EditPatientMeasurement->patient_Temperature = $request['patient_Temperature'];
        $EditPatientMeasurement->patient_BMIstatus = $request['patient_BMIstatus'];
        $EditPatientMeasurement->save();
        return back()->with('success','you have successfully edited patient measurements');
    }
    public function DeletePatientMeasurement(Request $request){
        $id = $request['id'];
        $DeletePatientMeasurement = PatientMeasurements::find($id);
        $DeletePatientMeasurement->delete();
        return back()->with('success','you have successfully delete patient measurements');

    }
    public function PatientOperations(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        return view('backend.PatientOperations')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }
    public function NursePatient(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->orWhere('role','Doctor Dentist')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();

        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();

        $getpatientlabo = MediService::where('department_name_id','11')->get();
        return view('backend.NursePatient')->with(['getpatientlabo'=>$getpatientlabo,'patient_amountpayed'=>$patient_amountpayed,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }

    public function PatientCharges(Request $request){
        $insurance_id = $request['insuranceid'];
//        $insurance_id = '5';
        $medicine = MedicineInsurance::where('insurance_medicine_id',$insurance_id)->join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.medicine_id','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        echo "<option value=\"\" selected>Select Medicine</option>";
        foreach ($medicine as $medi){
            echo "<option value='{$medi->id}'>{$medi->medicinename}</option>";
        }
    }

    public function PatientChargesNurse(Request $request){
        $activitynameid = $request['activitynameid'];
        $Insuranceidprice = $request['Insuranceidprice'];
        $getMediServMat = MediService::where('department_name_id',$activitynameid)->where('insurance_medicine_id',$Insuranceidprice)->get();

        echo "<option value=\"\"></option>";
        foreach ($getMediServMat as $medi){
            echo "<option value='{$medi->id}'>{$medi->medicinename}</option>";
        }
    }
    public function PatientChargeSelectMedicine(Request $request){
        $medicine = $request['Medicine_Service'];

        $insurance_id = $request['insurance_id'];
        $Insuranceidprice = $request['Insuranceidprice'];
        $activityname = $request['activityname'];

        $getmed_sername = MedicineData::where('id',$medicine)->value('medicinename');
        $quantitycheck = MediService::where('id',$medicine)->value('medicinequantity');

        $getprice = MediService::where('id',$medicine)->where('insurance_medicine_id',$Insuranceidprice)->value('medicinepriceinsurance');
        $getpricehospitalization = MediService::where('id',$medicine)->value('medicinepriceinsurance');
        $checkactivityname = DepartmentData::where('id',$activityname)->value('departmentname');

        if(empty($getprice)){
//                echo "<option value='noprice'>Medicine/Service not covered by your insurance</option>";
            echo "<div class=\"col-md-6\"> <div class=\"form-group\"> <label for=\"projectinput1\">Price</label> <select  class=\"form-control\" id=\"charges\" name=\"medicine_service_price\" required> <option value=\"noprice\">Medicine/Service not covered by your insurance</option> </select> </div></div>";
        }else{
           echo "<div class='row' style='width: 100%;padding: 15px;display: inline-block;'><div class='col-md-12'> <div class='form-group'> <label for='projectinput1'>Price</label> <select class='form-control' id='charges' name='medicine_service_price' required> <option value='$getprice' selected>$getprice</option> </select> </div></div><div class='col-md-12'> <div class='form-group'> <label for='projectinput1'>Quantity</label> <input type='text' id='stockitem' class='form-control' value='$quantitycheck' disabled> </div></div></div><div class='row'  style='width: 100%;display: inline-block;padding: 15px;'><div class='col-md-12'> <div class='form-group'> <label for='projectinput1'>Items</label> <input type='text' class='form-control' id='numberofitems' value='1' name='numberofitems required'> </div></div></div>";
//                 "<option value='$getprice'>$getprice</option>";
        }

    }

    public function PatientCharges_(Request $request){
        $medicine = $request['medicinelist'];
        $medicine = MedicineInsurance::where('medicine_insurance.id',$medicine)->join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        foreach ($medicine as $medi){
            echo "<option value='{$medi->medicinepriceinsurance}'>{$medi->medicinepriceinsurance}</option>";
        }
    }
    public function AddPatientCharges(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $numberofitemsnoqua = $request['numberofitems'];

        $copay = $request['copay'];
        $stockitem = $request['stockitem'];
        $insurance_id_ = $request['insurance_id'];
        $numberofitems = $request['numberofitems'];
        $unitprice = $request['charges'];
        $medicine_id = $request['medecineservices'];
        $charges = $request['charges'] * $numberofitems;
        $activity_name = $request['activity_name'];
        $medecineservices = $request['medecineservices'];
        $sumofinsurance = round(($charges * $copay)/100);
        $sumofpatient = round($charges - $sumofinsurance);

        $checkchargename = DepartmentData::where('id',$request['activity_name'])->value('departmentname');

        if($insurance_id_ == '6'){

            $addpatientcharges = new PatientCharges();
            $addpatientcharges->activity_name = $request['activity_name'];
            $addpatientcharges->Medicine_service_name = $request['medecineservices'];
            $addpatientcharges->medicine_service_price = round($charges);
            $addpatientcharges->patient_id = $request['patient_id'];
            $addpatientcharges->unitprice = round($unitprice);
            $addpatientcharges->numberofitems = $request['numberofitems'];
            $addpatientcharges->patientrecoddateid = $request['patientrecoddateid'];
            $addpatientcharges->nurseid = $user_id;
            $addpatientcharges->insurance_id = $request['insurance_id'];
            $addpatientcharges->insurance_amount = '0';
            $addpatientcharges->patient_amount = $charges;
            $addpatientcharges->chargename = $checkchargename;
            $addpatientcharges->save();

            $getnewbalanceid = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');

            if($getnewbalanceid == null) {
                $updatenewbalance = new AccountPatientClear();
                $updatenewbalance->patient_id = $request['patient_id'];
                $updatenewbalance->patientrecoddateid = $request['patientrecoddateid'];
                $updatenewbalance->amount_due = $charges;
                $updatenewbalance->insurance_due = '0';
                $updatenewbalance->amount_paid = '';
                $updatenewbalance->accountant_id = '';
                $updatenewbalance->paymentmethod = '';
                $updatenewbalance->insurance_id = '';
                $updatenewbalance->chargesreasons = '';
                $updatenewbalance->paymentcomment = '';
                $updatenewbalance->remainingbalance = $charges;
                $updatenewbalance->save();
//
            }else{
                if($insurance_id_ == '6'){
                    $getinsurancebalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');
                    $getremainingbalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('amount_due');

                    $newinsurancebalance = $getinsurancebalance + 0;
                    $newamaountdue = $getremainingbalance + $charges;
                    $updatenewbalance = AccountPatientClear::where('patient_id', $request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->update(['amount_due' => $newamaountdue, 'insurance_due'=>$newinsurancebalance, 'remainingbalance'=>$newamaountdue]);

                }else{
                    $getinsurancebalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');
                    $getremainingbalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('amount_due');

                    $newinsurancebalance = $getinsurancebalance + $sumofinsurance;
                    $newamaountdue = $getremainingbalance + $sumofpatient;
                    $updatenewbalance = AccountPatientClear::where('patient_id', $request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->update(['amount_due' => $newamaountdue, 'insurance_due'=>$newinsurancebalance, 'remainingbalance'=>$newamaountdue]);

                }

            }

            $patient_id = $request['patient_id'];
            $patientrecoddateid = $request['patientrecoddateid'];
            $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
                ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
                ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
                ->get();

            if($stockitem > 0){
                $checkcurrentquantity = MediService::where('id',$medicine_id)->value('medicinequantity');
                $newquantity = $checkcurrentquantity - $numberofitems;
                $updatequantity = MediService::where('id',$medicine_id)->update(['medicinequantity' =>$newquantity]);

            }


            foreach($listpatientcharges as $data){
                echo "<tr><td>{$data->departmentname}</td><td>{$data->medicinename}</td></td><td>{$data->medicine_service_price}</td></tr>";
            }
        }else{

            $addpatientcharges = new PatientCharges();
            $addpatientcharges->activity_name = $request['activity_name'];
            $addpatientcharges->Medicine_service_name = $request['medecineservices'];
            $addpatientcharges->medicine_service_price = round($charges);
            $addpatientcharges->patient_id = $request['patient_id'];
            $addpatientcharges->unitprice = $unitprice;
            $addpatientcharges->numberofitems = $request['numberofitems'];
            $addpatientcharges->patientrecoddateid = $request['patientrecoddateid'];
            $addpatientcharges->nurseid = $user_id;
            $addpatientcharges->insurance_id = $request['insurance_id'];
            $addpatientcharges->insurance_amount = round($sumofinsurance);
            $addpatientcharges->patient_amount = round($sumofpatient);
            $addpatientcharges->chargename = $checkchargename;
            $addpatientcharges->save();

            $getnewbalanceid = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');
            if($getnewbalanceid == null) {
                $updatenewbalance = new AccountPatientClear();
                $updatenewbalance->patient_id = $request['patient_id'];
                $updatenewbalance->patientrecoddateid = $request['patientrecoddateid'];
                $updatenewbalance->amount_due = $sumofpatient;
                $updatenewbalance->insurance_due = $sumofinsurance;
                $updatenewbalance->amount_paid = '';
                $updatenewbalance->accountant_id = '';
                $updatenewbalance->paymentmethod = '';
                $updatenewbalance->insurance_id = '';
                $updatenewbalance->chargesreasons = '';
                $updatenewbalance->paymentcomment = '';
                $updatenewbalance->remainingbalance = $sumofpatient;
                $updatenewbalance->save();
//
            }else{
                $getinsurancebalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');
                $getremainingbalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('amount_due');

                $newinsurancebalance = $getinsurancebalance + $sumofinsurance;
                $newamaountdue = $getremainingbalance + $sumofpatient;
                $updatenewbalance = AccountPatientClear::where('patient_id', $request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->update(['amount_due' => $newamaountdue, 'insurance_due'=>$newinsurancebalance, 'remainingbalance'=>$newamaountdue]);
            }

            $patient_id = $request['patient_id'];
            $patientrecoddateid = $request['patientrecoddateid'];
            $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
                ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
                ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
                ->get();


        if($stockitem > 0){
            $checkcurrentquantity = MediService::where('id',$medicine_id)->value('medicinequantity');
            $newquantity = $checkcurrentquantity - $numberofitems;
            $updatequantity = MediService::where('id',$medicine_id)->update(['medicinequantity' =>$newquantity]);

        }

            foreach($listpatientcharges as $data){
                echo "<tr><td>{$data->departmentname}</td><td>{$data->medicinename}</td></td><td>{$data->medicine_service_price}</td></tr>";
            }
        }


//        return back()->with('success','you have successfully added charges');
    }
    public function ClearAllCharges(Request $request){

        $all = $request->all();
        $patient_id = $request['patient_id'];
        $patientrecoddateid = $request['patientrecoddateid'];

        $findid = AccountPatientClear::where('patientrecoddateid',$patientrecoddateid)->where('patient_id',$patient_id)->value('amount_due');
//        dd($findid);
        $deletechargers = AccountPatientClear::where('patientrecoddateid',$patientrecoddateid)->where('patient_id',$patient_id)->delete();
        $deletechargers_ = PatientCharges::where('patientrecoddateid',$patientrecoddateid)->where('patient_id',$patient_id)->delete();
        return back()->with('success','you have successfully cleared all charges');

//        if(empty($findid)){
//            return back()->with('success','you do not have pending chargers at accounting database');
//        }else{
//            $deletechargers = AccountPatientClear::where('patientrecoddateid',$patientrecoddateid)->where('patient_id',$patient_id)->delete();
//            $deletechargers_ = PatientCharges::where('patientrecoddateid',$patientrecoddateid)->where('patient_id',$patient_id)->delete();
//            return back()->with('success','you have successfully cleared all charges');
//        }

    }
    public function PatientConsultaton(Request $request){
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        return view('backend.PatientConsultaton')->with(['id'=>$id,'user_id'=>$user_id]);
    }
    public function PatientConsultaton_(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $addconsult = new PatientConsultation();
        $addconsult->consultation_comment = $request['consultation_comment'];
        $addconsult->patient_id = $request['patient_id'];
        $addconsult->patientrecoddateid = $request['patientrecoddateid'];
        $addconsult->nurseid = $user_id;
        $addconsult->save();
        return back()->with('addedconsultation','you have successfully added consultation comment');

    }
    public function EditPatientConsultaton_(Request $request){
        $id = $request['id'];
        $all = $request->all();
        $user_id = \Auth::user()->id;

        $addconsult = PatientConsultation::find($id);
        $addconsult->consultation_comment = $request['consultation_comment'];
        $addconsult->patient_id = $request['patient_id'];
        $addconsult->patientrecoddateid = $request['patientrecoddateid'];
        $addconsult->nurseid = $user_id;
        $addconsult->save();

        return back()->with('addedconsultation_','you have successfully updated consultation comment');

    }
    public function DeletePatientConsultaton(Request $request){
        $id = $request['id'];
        $DeletePatientConsultaton = PatientConsultation::find($id);
        $DeletePatientConsultaton->delete();
        return back();
    }
    public function AddAppointmentDoctors(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $addapp = new PatientDoctorApp();

        $addapp->doctor_id = $request['doctor_id'];
        $addapp->patient_id = $request['patient_id'];
        $addapp->timeconfirm = $request['timeconfirm'];
        $addapp->dateconfirm = $request['dateconfirm'];
        $addapp->patient_id = $request['patient_id'];
        $addapp->patient_status = 'pending';
        $addapp->nurse_id = $user_id;
        $addapp->patientrecoddateid = $request['patientrecoddateid'];
        $addapp->save();
        return back()->with('addedconsultation_','you have successfully appointed patient to doctor');
    }
//    public function EditAddAppointmentDoctors(Request $request){
//        $all  = $request->all();
//        $id = $request['id'];
//        $user_id = \Auth::user()->id;
//        $updateappdoctor = PatientDoctorApp::find($id);
//        $updateappdoctor->doctor_id = $request['doctor_id'];
//        $updateappdoctor->patient_id = $request['patient_id'];
//        $updateappdoctor->nurse_id = $user_id;
//        $updateappdoctor->save();
//        return back()->with('addedconsultation_','you have successfully updated patient appointment');
//    }
    public function DeleteAddAppointmentDoctors(Request $request){
        $id = $request['id'];
        $DeletePatientapp = PatientDoctorApp::find($id);
        $DeletePatientapp->delete();
        return back();
    }


    public function AddAppointmentLabo(Request $request){
        $all = $request->all();
//        dd($all);
        $user_id = \Auth::user()->id;

        $lab_tech= $request['lab_tech'];

        foreach ($lab_tech as $key => $lab_tech) {
            $addapplabo = new AppointmentLaboratory();
            $addapplabo->lab_tech = $request['lab_tech'][$key];
            $addapplabo->patient_id = $request['patient_id'];
            $addapplabo->nurse_id = $user_id;
            $addapplabo->patient_status = 'pending';
            $addapplabo->patientrecoddateid = $request['patientrecoddateid'];
            $addapplabo->save();
//
//
//            $getestdetails = LaboratoryInsurancePrice::where('labotest_id',$lab_tech)->get();
//            foreach ($getestdetails as $getmoredetails) {
//                $laboratoryprice = $getmoredetails->laboratoryprice;
//                $departmentid = $getmoredetails->departmentid;
//
//            }
//
//            $user_id = \Auth::user()->id;
//            $addpatientcharges = new PatientCharges();
//            $addpatientcharges->activity_name = $departmentid;
//            $addpatientcharges->Medicine_service_name = $lab_tech;
//            $addpatientcharges->medicine_service_price = $laboratoryprice;
//            $addpatientcharges->patient_id = $request['patient_id'];
//            $addpatientcharges->patientrecoddateid = $request['patientrecoddateid'];
//            $addpatientcharges->nurseid = $user_id;
//            $addpatientcharges->insurance_id = $request['insurance_id'];
//            $addpatientcharges->save();

        }

        return back()->with('addedconsultation_','you have successfully appointed patient to doctor');

    }
//    public function EditAppointmentLabo(Request $request){
//        $all  = $request->all();
//        $id = $request['id'];
//        $user_id = \Auth::user()->id;
//        $updateapplabo = AppointmentLaboratory::find($id);
//        $updateapplabo->lab_tech = $request['lab_tech'];
//        $updateapplabo->patient_id = $request['patient_id'];
//        $updateapplabo->nurse_id = $user_id;
//        $updateapplabo->save();
//        return back()->with('addedconsultation_','you have successfully updated patient laboratory appointment');
//    }
    public function DeleteAddAppointmentLabo(Request $request){
        $id = $request['id'];
        $DeletePatientapp = AppointmentLaboratory::find($id);
        $DeletePatientapp->delete();
        return back();
    }
    public function EditPatientCharges(){

    }
    public function DeletePatientCharges(Request $request){
        $id = $request['id'];
        $getcurrentcharge = PatientCharges::where('id',$id)->get();
        foreach ($getcurrentcharge as $charges){
            $patientamount = $charges->patient_amount;
            $patient_id = $charges->patient_id;
            $patientrecoddateid = $charges->patientrecoddateid;
        }
        $getaccoutantcurrentamount = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_due');
        $newbalance = $getaccoutantcurrentamount - $patientamount;

        $updatenewbalance = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->update(['amount_due' => $newbalance,'remainingbalance'=>$newbalance]);

        $DeletePatientcharges = PatientCharges::find($id);
        $DeletePatientcharges->delete();
        return back()->with('addedconsultation_','you have successfully deleted patient charges');
    }
    public function MarkAsRead(){
        Auth::user()->unreadNotifications->markAsRead();
        return back();
    }
    public function Doctor(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        return view('backend.Doctor')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }
    public function DoctorPatient(Request $request){
        $id = $request['id'];
        $listusers = User::all();
        $listsoap = SOAP::where('patient_id',$id)->get();
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();
        $listpatientcharges= PatientCharges::join('insurancedata', 'insurancedata.id', '=', 'patientcharges.insurance_id_')
            ->join('medicine_insurance', 'medicine_insurance.medicinepriceinsurance', '=', 'patientcharges.medicine_service_price')
            ->select('patientcharges.*','insurancedata.insurance_name','medicine_insurance.medicinepriceinsurance')
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->join('users', 'users.id', '=', 'patient_doctor_appointment.doctor_id')
            ->select('patient_doctor_appointment.*','users.name','users.email')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();

        $listlabotest = LaboratoryTest::all();
        return view('backend.DoctorPatient')->with(['listappointdoctor'=>$listappointdoctor,'listsoap'=>$listsoap,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusers'=>$listusers,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp]);

    }
    public function SearchPatientData(Request $request){
        $patientname = $request['patientname'];
        $listpatient = Patient::where('beneficiary_name', 'like', '%' . $patientname . '%')
            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('insurancedata.insurance_name','patient.*')
            ->get();


        foreach($listpatient as $patient){
            $record = route('backend.DoctorPatientHistoryRecord',['id'=>$patient->id]);
            echo "<tr><td><a href='$record'>{$patient->beneficiary_name}</a></td><td>{$patient->gender}</td></td><td>{$patient->beneficiary_telephone_number}</td><td>{$patient->insurance_name}</td><td>{$patient->pid}</td></tr>";
        }

    }

    public function DoctorConsultation(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $appointmentlist = PatientDoctorApp::where('patient_doctor_appointment.doctor_id',$user_id)->where('patient_doctor_appointment.patient_status','pending')->where('patient_doctor_appointment.doctor_id',$user_id)
            ->join('patient', 'patient.id', '=', 'patient_doctor_appointment.patient_id')
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient.*','patient_doctor_appointment.dateconfirm','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

        $appointmentshift = ConfirmAppointment::where('confirm_appointments.patient_status','pending')->where('confirm_appointments.nurse_id',$user_id)
            ->join('patient', 'patient.id', '=', 'confirm_appointments.patient_id')
            ->join('users', 'users.id', '=', 'confirm_appointments.nurse_id')
            ->select('patient.*','confirm_appointments.appointment_message','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

//        dd($appointmentlist);
        return view('backend.DoctorConsultation')->with(['appointmentshift'=>$appointmentshift,'appointmentlist'=>$appointmentlist,'listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }
    public function DoctorPatientHistoryRecord(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.DoctorPatientHistoryRecord')->with(['id'=>$id,'listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }
    public function DoctorOperationPatientRecordHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.DoctorOperationPatientRecordHistory')->with(['id'=>$id,'listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }

    public function DoctorPatientHistoryOperation(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
//        $listusers = User::all();
        $listusers = User::where('role','Doctor')->get();
        $listsoap = SOAP::where('patient_id',$id)->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');

//        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
//            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
//            ->select('users.name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
//            ->get();
        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $listdoctorsapp = PatientDoctorApp::where('doctor_id',$user_id)->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $getpatientlabo = MediService::where('department_name_id','11')->get();

        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();

        return view('backend.DoctorPatientHistoryOperation')->with(['getpatientlabo'=>$getpatientlabo,'patient_amountpayed'=>$patient_amountpayed,'insurance_id'=>$insurance_id,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listpatientprice'=>$listpatientprice,'listappointdoctor'=>$listappointdoctor,'listsoap'=>$listsoap,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusers'=>$listusers,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp]);

    }
    public function AddLaboratoryService(Request $request){
        $all = $request->all();
//        dd($all);
        $user_id = \Auth::user()->id;

        $labodata = $request['labodata'];
//        $getprice = MediService::where('medicinename',$labodata)->value('medicinepriceinsurance');
        $patientcopay = Patient::where('id',$request['patient_id'])->value('copay');


        foreach ($labodata as $key => $labodata){

            $checkchargename = DepartmentData::where('id','11')->value('departmentname');

            $getprice = MediService::where('id',$labodata)->value('medicinepriceinsurance');
            $sumofinsurance = round(($getprice * $patientcopay)/100);
            $sumofpatient = round($getprice - $sumofinsurance);

            $addpatientcharges = new PatientCharges();
            $addpatientcharges->activity_name = '11';
            $addpatientcharges->Medicine_service_name = $labodata;
            $addpatientcharges->medicine_service_price = round($getprice);
            $addpatientcharges->patient_id = $request['patient_id'];
            $addpatientcharges->unitprice = $getprice;
            $addpatientcharges->numberofitems = '1';
            $addpatientcharges->patientrecoddateid = $request['patientrecoddateid'];
            $addpatientcharges->nurseid = $user_id;
            $addpatientcharges->insurance_id = $request['insurance_id'];
            $addpatientcharges->insurance_amount = round($sumofinsurance);
            $addpatientcharges->patient_amount = round($sumofpatient);
            $addpatientcharges->chargename = $checkchargename;
            $addpatientcharges->save();

            $getnewbalanceid = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');

            if($getnewbalanceid == null) {
                $updatenewbalance = new AccountPatientClear();
                $updatenewbalance->patient_id = $request['patient_id'];
                $updatenewbalance->patientrecoddateid = $request['patientrecoddateid'];
                $updatenewbalance->amount_due = round($sumofpatient);
                $updatenewbalance->insurance_due = round($sumofinsurance);
                $updatenewbalance->amount_paid = '';
                $updatenewbalance->accountant_id = '';
                $updatenewbalance->paymentmethod = '';
                $updatenewbalance->insurance_id = '';
                $updatenewbalance->chargesreasons = '';
                $updatenewbalance->paymentcomment = '';
                $updatenewbalance->remainingbalance = round($sumofpatient);
                $updatenewbalance->save();
            }else{
                $getinsurancebalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('insurance_due');
                $getremainingbalance = AccountPatientClear::where('patient_id',$request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->value('amount_due');

                $newinsurancebalance = round($getinsurancebalance + $sumofinsurance);
                $newamaountdue = round($getremainingbalance + $sumofpatient);
                $updatenewbalance = AccountPatientClear::where('patient_id', $request['patient_id'])->where('patientrecoddateid',$request['patientrecoddateid'])->update(['amount_due' => $newamaountdue, 'insurance_due'=>$newinsurancebalance, 'remainingbalance'=>$newamaountdue]);
            }
        }
        $addlaboap = new AppointmentLaboratory();

        $addlaboap->nurse_id = $user_id;
        $addlaboap->lab_tech = '';
        $addlaboap->patient_id = $request['patient_id'];
        $addlaboap->patientrecoddateid = $request['patientrecoddateid'];
        $addlaboap->patientrecoddateid = $request['patientrecoddateid'];
        $addlaboap->Laboratorytechnician = 'unknown';
        $addlaboap->patient_status = 'Pending';
        $addlaboap->save();


        return back()->with('success','you have successfully added laboratory test');

    }
    public function ConfirmPatientStatus(Request $request){
        $id  = $request['id'];
        $user_id = \Auth::user()->id;
        $getlabtec = User::where('id',$user_id)->value('name');
        $updatestatus = PatientDoctorApp::find($id);
        $updatestatus->patient_status = $request['patient_status'];
        $updatestatus->Operatedby = $getlabtec;
        $updatestatus->save();

        return back()->with('success','you have successfully changed Appointment status');
    }
    public function DoctorsReport(){
        $user_id = \Auth::user()->id;

        $listpatient =PatientDoctorApp::where('patient_doctor_appointment.doctor_id',$user_id)->where('patient_status','done')
            ->join('patient', 'patient.id', '=', 'patient_doctor_appointment.patient_id')
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.doctor_id')
            ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','patient_doctor_appointment.*')
            ->groupBy('patient_doctor_appointment.patientrecoddateid')
            ->get();


        $accountnumber =PatientDoctorApp::where('doctor_id',$user_id)->where('patient_status','done')
            ->select(DB::raw('count(id) as id'))->value('id');

        return view('backend.DoctorsReport')->with(['accountnumber'=>$accountnumber,'listpatient'=>$listpatient]);
    }
    public function DoctorsReportFilter(Request $request){
        $all = $request->all();
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];

        $user_id = \Auth::user()->id;
        $listpatient =PatientDoctorApp::whereBetween('patient_doctor_appointment.updated_at',[$fromdate,$todate])->where('patient_doctor_appointment.doctor_id',$user_id)->where('patient_status','done')
            ->join('patient', 'patient.id', '=', 'patient_doctor_appointment.patient_id')
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.doctor_id')
            ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','patient_doctor_appointment.*')
            ->groupBy('patient_doctor_appointment.patientrecoddateid')
            ->get();

        return view('backend.DoctorsReportFilter')->with(['listpatient'=>$listpatient]);

    }

    public function DoctorPatientHistoryConsult(Request $request){

        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listsoap = SOAP::where('patient_id',$id)->get();
        $listusersdoctors = User::where('role','Doctor')->get();
        $listusers = User::where('role','Doctor')->get();
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();

//        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
//            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
//            ->select('users.name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
//            ->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');


        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();
        $listmeasurements= PatientMeasurements::where('patient_id',$id)->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $listdental = DentalClinic::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        return view('backend.DoctorPatientHistoryConsult')->with(['patient_amountpayed'=>$patient_amountpayed,'listdental'=>$listdental,'insurance_id'=>$insurance_id,'patientrecoddateid'=>$patientrecoddateid,'listappointdoctor'=>$listappointdoctor,'listsoap'=>$listsoap,'listmeasurements'=>$listmeasurements,'listconslutationcomment'=>$listconslutationcomment,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }

    public function SOAP(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $soap = new SOAP();
        $soap->subjective = $request['subjective'];
        $soap->objective = $request['objective'];
        $soap->assessment = $request['assessment'];
        $soap->plan = $request['plan'];
        $soap->patient_id = $request['patient_id'];
        $soap->patientrecoddateid = $request['patientrecoddateid'];
        $soap->Labotechid = $user_id;
        $soap->save();
        return back()->with('success','You have successful updated Patient Information');

    }
    public function DoctorDentist(Request $request){
        $all= $request->all();
        $user_id = \Auth::user()->id;
        $adddentalclinic = new DentalClinic();
        $adddentalclinic->consultation = $request['consultation'];
        $adddentalclinic->Historyofpresentillness = $request['Historyofpresentillness'];
        $adddentalclinic->PastMedicalhistory = $request['PastMedicalhistory'];
        $adddentalclinic->Pastdentalhistory = $request['Pastdentalhistory'];
        $adddentalclinic->ExtraoralExam = $request['ExtraoralExam'];
        $adddentalclinic->IntraoralExam = $request['IntraoralExam'];
        $adddentalclinic->Examinationofteeth = $request['Examinationofteeth'];
        $adddentalclinic->clinicalfinding = $request['clinicalfinding'];
        $adddentalclinic->Investigation = $request['Investigation'];
        $adddentalclinic->Diagnosis = $request['Diagnosis'];
        $adddentalclinic->Treatment = $request['Treatment'];
        $adddentalclinic->patient_id = $request['patient_id'];
        $adddentalclinic->savedby = $user_id;
        $adddentalclinic->patientrecoddateid = $request['patientrecoddateid'];
        $adddentalclinic->save();
        return back()->with('success','you have successfully added Dental Information');

    }
    public function EditDoctorDentist(Request $request){
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        $editdental = DentalClinic::find($id);
        $editdental->consultation = $request['consultation'];
        $editdental->Historyofpresentillness = $request['Historyofpresentillness'];
        $editdental->PastMedicalhistory = $request['PastMedicalhistory'];
        $editdental->Pastdentalhistory = $request['Pastdentalhistory'];
        $editdental->ExtraoralExam = $request['ExtraoralExam'];
        $editdental->IntraoralExam = $request['IntraoralExam'];
        $editdental->Examinationofteeth = $request['Examinationofteeth'];
        $editdental->clinicalfinding = $request['clinicalfinding'];
        $editdental->Investigation = $request['Investigation'];
        $editdental->Diagnosis = $request['Diagnosis'];
        $editdental->Treatment = $request['Treatment'];
        $editdental->patient_id = $request['patient_id'];
        $editdental->savedby = $user_id;
        $editdental->patientrecoddateid = $request['patientrecoddateid'];
        $editdental->save();
        return back()->with('success','you have successfully edited Dental Information');
    }
    public function DeleteDoctorDentist(Request $request){
        $id = $request['id'];
        $deletedental = DentalClinic::find($id);
        $deletedental->delete();
        return back()->with('success','you have successfully deleted Dental information');

    }
    public function SOAPEdit(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $id = $request['id'];
        $updatesoap = SOAP::find($id);
        $updatesoap->subjective = $request['subjective'];
        $updatesoap->objective = $request['objective'];
        $updatesoap->assessment = $request['assessment'];
        $updatesoap->plan = $request['plan'];
        $updatesoap->patient_id = $request['patient_id'];
        $updatesoap->patientrecoddateid = $request['patientrecoddateid'];
        $updatesoap->Labotechid = $user_id;
        $updatesoap->save();
        return back()->with('success','You have successful updated Patient Information');

    }
    public function ConfirmAppointment(Request $request){
        $all = $request->all();
        $confirmapp = new ConfirmAppointment();
        $user_id = \Auth::user()->id;
        $confirmapp->dateconfirm = $request['dateconfirm'];
        $confirmapp->timeconfirm = $request['timeconfirm'];
        $confirmapp->appointment_message = $request['appointment_message'];

        $confirmapp->patient_id = $request['patient_id'];
        $confirmapp->patient_status = 'pending';
        $confirmapp->nurse_id = $user_id;
        $confirmapp->patientrecoddateid = $request['patientrecoddateid'];
        $confirmapp->save();

        return back()->with('success','You have successfully updated patient appointment');

    }
    public function ConfirmAppointmentEdit(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        $updatesoap = ConfirmAppointment::find($id);
        $updatesoap->patient_id = $request['patient_id'];
        $updatesoap->dateconfirm = $request['dateconfirm'];
        $updatesoap->timeconfirm = $request['timeconfirm'];
        $updatesoap->patient_id = $request['patient_id'];
        $updatesoap->nurse_id = $user_id;
        $updatesoap->patientrecoddateid = $request['patientrecoddateid'];
        $updatesoap->save();
        return back()->with('success','You have successfully updated patient appointment');
    }
    public function ConfirmAppointmentDelete(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $updatesoap = ConfirmAppointment::find($id);
        $updatesoap->delete();
        return back()->with('success','You have successfully delete patient appointment');
    }
    public function ClinicPatientProfile(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $appointmentlist = PatientDoctorApp::where('patient_doctor_appointment.doctor_id',$user_id)->where('patient_doctor_appointment.patient_status','pending')->where('patient_doctor_appointment.doctor_id',$user_id)
            ->join('patient', 'patient.id', '=', 'patient_doctor_appointment.patient_id')
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient.*','patient_doctor_appointment.dateconfirm','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

        $appointmentshift = ConfirmAppointment::where('confirm_appointments.patient_status','pending')->where('confirm_appointments.nurse_id',$user_id)
            ->join('patient', 'patient.id', '=', 'confirm_appointments.patient_id')
            ->join('users', 'users.id', '=', 'confirm_appointments.nurse_id')
            ->select('patient.*','confirm_appointments.appointment_message','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

//        dd($appointmentlist);
        return view('backend.ClinicPatientProfile')->with(['appointmentshift'=>$appointmentshift,'appointmentlist'=>$appointmentlist,'listinsu'=>$listinsu,'listpatient'=>$listpatient]);

    }
    public function ClinicPatientProfileRecord(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.ClinicPatientProfileRecord')->with(['id'=>$id,'listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }
    public function ClinicPatientProfileInfo(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listsoap = SOAP::where('patient_id',$id)->get();
        $listusersdoctors = User::where('role','Doctor')->get();
        $listusers = User::where('role','Doctor')->get();
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();

//        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
//            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
//            ->select('users.name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
//            ->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');


        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();
        $listmeasurements= PatientMeasurements::where('patient_id',$id)->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $listdental = DentalClinic::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();

        $listlaboratory =AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->where('patient_status','done')
            ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','appointmentlaboratory.*')
            ->groupBy('appointmentlaboratory.patientrecoddateid')
            ->get();

        $listlabochargers= PatientCharges::orWhere('patientcharges.activity_name','39')
            ->orWhere('patientcharges.activity_name','40')
            ->orWhere('patientcharges.activity_name','41')
            ->orWhere('patientcharges.activity_name','42')
            ->orWhere('patientcharges.activity_name','43')
            ->orWhere('patientcharges.activity_name','44')
            ->orWhere('patientcharges.activity_name','45')
            ->orWhere('patientcharges.activity_name','46')
            ->orWhere('patientcharges.activity_name','47')
            ->orWhere('patientcharges.activity_name','48')
            ->orWhere('patientcharges.activity_name','49')
            ->orWhere('patientcharges.activity_name','50')
            ->orWhere('patientcharges.activity_name','51')
            ->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listdental = DentalClinic::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        $listdoctorcomment = DoctorLabComment::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'doctorlabcomment.doneby')
            ->select('doctorlabcomment.*','users.name')
            ->get();

        return view('backend.ClinicPatientProfileInfo')->with(['listdoctorcomment'=>$listdoctorcomment,'listlaboratory'=>$listlaboratory,'patient_amountpayed'=>$patient_amountpayed,'listdental'=>$listdental,'insurance_id'=>$insurance_id,'patientrecoddateid'=>$patientrecoddateid,'listappointdoctor'=>$listappointdoctor,'listsoap'=>$listsoap,'listmeasurements'=>$listmeasurements,'listconslutationcomment'=>$listconslutationcomment,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    /**
     *
     * END OF  NURSE MODULE
     */



    /**
     *
     * START OF  LABORATORY MODULE
     */

    public function LaboratoryDashboard(){
        $accountnumber = User::where('role','Reception')->select(DB::raw('count(id) as accountnumber'))->value('accountnumber');
        $patientnumber = Patient::select(DB::raw('count(id) as patientnumber'))->value('patientnumber');
        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');

        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         * SUMMARY TABLE IN PATIENT
         */

        $patient_jan = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_feb = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_marc = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_apr = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_may = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_jun = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('patientnumber');


        $patient_jul = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_aug = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_sept = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_oct = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_nov = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_dec = AppointmentLaboratory::select(DB::raw('count(id) as patientnumber'))->where('patient_status','done')
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('patientnumber');

        return view('backend.LaboratoryDashboard')->with(['patient_dec'=>$patient_dec,'patient_nov'=>$patient_nov,'patient_oct'=>$patient_oct,'patient_sept'=>$patient_sept,'patient_aug'=>$patient_aug,'patient_jul'=>$patient_jul,'patient_jun'=>$patient_jun,'patient_may'=>$patient_may,'patient_apr'=>$patient_apr,'patient_marc'=>$patient_marc,'patient_jan'=>$patient_jan,'patient_feb'=>$patient_feb,'accountnumber'=>$accountnumber,'patientnumber'=>$patientnumber,'insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);


    }
    public function LaboratoryTech(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
//        $listlaboapp = AppointmentLaboratory::join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
//            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
//            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
//            ->select('users.name','users.email','patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name','appointmentlaboratory.lab_tech','appointmentlaboratory.patient_id','appointmentlaboratory.nurse_id','insurancedata.insurance_name')
//            ->get();

        $listlaboapp = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')

            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $appointmentlist = AppointmentLaboratory::where('appointmentlaboratory.patient_status','pending')
            ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('patient.*','appointmentlaboratory.created_at','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

        return view('backend.Laboratory')->with(['appointmentlist'=>$appointmentlist,'listinsu'=>$listinsu,'listpatient'=>$listpatient,'listlaboapp'=>$listlaboapp]);

    }
    public function LaboratoryAllPatient(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
//        $listlaboapp = AppointmentLaboratory::join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
//            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
//            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
//            ->select('users.name','users.email','patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name','appointmentlaboratory.lab_tech','appointmentlaboratory.patient_id','appointmentlaboratory.nurse_id','insurancedata.insurance_name')
//            ->get();

        $listlaboapp = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')

            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $appointmentlist = AppointmentLaboratory::where('appointmentlaboratory.patient_status','pending')
            ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('patient.*','appointmentlaboratory.created_at','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

        return view('backend.LaboratoryAllPatient')->with(['appointmentlist'=>$appointmentlist,'listinsu'=>$listinsu,'listpatient'=>$listpatient,'listlaboapp'=>$listlaboapp]);

    }
    public function LaboratoryPatientHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->orderBy('id', 'desc')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.LaboratoryPatientHistory')->with(['listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }

    public function LaboratoryTechTest(Request $request){

        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;

        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();


        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
//        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
//            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
//            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
//            ->get();

        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name','users.name')
            ->get();

        $listlabochargers= PatientCharges::whereIn('patientcharges.activity_name',['39','40','41','42','43','44','45','46','47','48','49','50','51'])
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listlabochar= PatientCharges::where('activity_name','11')
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('users.name','mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();

        $listlabotest = LaboratoryTest::all();
        return view('backend.LaboratoryTechTest')->with(['listlabochar'=>$listlabochar,'listlabochargers'=>$listlabochargers,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function ResultNotifyPatient(Request $request){
        $all = $request->all();
        $rsgamessage = $request['limitedtextarea'];
//        $rsgamessage = 'Testing new SMS';
//        $phonenumber = '0782384772';
        $phonenumber = $request['phonenumber'];
        $intnumber = '25'.$phonenumber;
//        dd($intnumber);
        $api_key = 'SmR5T3Vnbj1GT0thSmhvR3Z5S20=';
        $from = 'NDENGERA';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response=json_decode($response);

//        dd($response);
        $savereport = new SmsHistory();
        $savereport->from = $from;
        $savereport->to = $destination;
        $savereport->message = $rsgamessage;
        $savereport->deliverystatus = $response->message;
        $savereport->save();

        if($response->code == 'ok'){
            return back()->with('success','you have successfully sent your message');
        }else{
            return back()->with('danger','you have is not successfully sent');
        }
    }
    public function LabNotifyDoctor(Request $request){
        $all = $request->all();
        $rsgamessage = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $intnumber = '25'.$phonenumber;

        $api_key = 'SmR5T3Vnbj1GT0thSmhvR3Z5S20=';
        $from = 'NDENGERA';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response=json_decode($response);

        $savereport = new SmsHistory();
        $savereport->from = $from;
        $savereport->to = $destination;
        $savereport->message = $rsgamessage;
        $savereport->deliverystatus = $response->message;
        $savereport->save();

        if($response->code == 'ok'){
            return back()->with('success','you have successfully sent your message');
        }else{
            return back()->with('danger','you have is not successfully sent');
        }
    }
    public function LaboOtherResults(Request $request){
        $all = $request->all();
        $laboappid = $request['id'];
        $laboratorytype = PatientCharges::where('id',$laboappid)->value('activity_name');
        $laboratorytestname = PatientCharges::where('id',$laboappid)->value('Medicine_service_name');
        $getpatientrecordid = PatientCharges::where('id',$laboappid)->value('patientrecoddateid');
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$getpatientrecordid)->value('patient_id');
        $id = $getpatientidfromdaterecord;

        $listresult = LaboratoryOtherResults::where('laboappoint_id',$laboappid)->where('patient_id',$id)->where('patientrecoddateid',$getpatientrecordid)
            ->join('users', 'users.id', '=', 'Laboratoryotherresults.Labotechid')
            ->select('Laboratoryotherresults.*','users.name')
            ->get();
        return view('backend.LaboOtherResults')->with(['laboratorytestname'=>$laboratorytestname,'laboratorytype'=>$laboratorytype,'laboappid'=>$laboappid,'patientrecoddateid'=>$getpatientrecordid,'id'=>$id,'listresult'=>$listresult]);

    }

    public function LaboratoryNFS(Request $request){
        $all = $request->all();
        $laboappid = $request['id'];
        $laboratorytype = PatientCharges::where('id',$laboappid)->value('activity_name');
        $laboratorytestname = PatientCharges::where('id',$laboappid)->value('Medicine_service_name');
        $getpatientrecordid = PatientCharges::where('id',$laboappid)->value('patientrecoddateid');
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$getpatientrecordid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listresult = LaboratoryResultNfs::where('laboappoint_id',$laboappid)->where('patient_id',$id)->where('patientrecoddateid',$getpatientrecordid)
            ->join('users', 'users.id', '=', 'laboratoryresultnfs.Labotechid')
            ->select('laboratoryresultnfs.*','users.name')
            ->get();
        $listnfsimage = NfsImage::where('patient_id',$id)->where('patientrecoddateid',$getpatientrecordid)->get();
        return view('backend.LaboratoryNFS')->with(['listnfsimage'=>$listnfsimage,'laboratorytestname'=>$laboratorytestname,'laboratorytype'=>$laboratorytype,'laboappid'=>$laboappid,'patientrecoddateid'=>$getpatientrecordid,'id'=>$id,'listresult'=>$listresult]);
    }

    public function AddOtherResultsPatients(Request $request){
        $all = $request->all();
//        dd($all);
        $user_id = \Auth::user()->id;
        $AddOtherResultsPatients = new LaboratoryOtherResults();
        $AddOtherResultsPatients->laboratoryresults = $request['laboratoryresults'];
        $AddOtherResultsPatients->normalvalues = $request['normalvalues'];
        $AddOtherResultsPatients->patient_id = $request['patient_id'];
        $AddOtherResultsPatients->patientrecoddateid = $request['patientrecoddateid'];
        $AddOtherResultsPatients->resultcategory = 'Other Results';
        $AddOtherResultsPatients->laboappoint_id = $request['laboappoint_id'];
        $AddOtherResultsPatients->Labotechid = $user_id;
        $AddOtherResultsPatients->laboratorytype = $request['laboratorytype'];
        $AddOtherResultsPatients->laboratorytestname = $request['laboratorytestname'];
        $AddOtherResultsPatients->save();

        $patientrecoddateid = $request['patientrecoddateid'];

        $id = $request['patient_id'];

        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->get();

        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name','users.name')
            ->get();
        $listlabochargers= PatientCharges::whereIn('patientcharges.activity_name',['39','40','41','42','43','44','45','46','47','48','49','50','51'])
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listlabochar= PatientCharges::where('activity_name','11')
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();

        $listlabotest = LaboratoryTest::all();
//        return redirect()->intended('/Dashboard');
        return redirect()->route('backend.LaboratoryTechTest',['patientrecoddateid'=> $request['patientrecoddateid']])->with(['listlabochar'=>$listlabochar,'listlabochargers'=>$listlabochargers,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);
//        return view('backend.LaboratoryTechTest')->with(['listlabochar'=>$listlabochar,'listlabochargers'=>$listlabochargers,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function EditOtherResultsPatients(Request $request){

        $user_id = \Auth::user()->id;
        $id = $request['id'];
        $EditOtherResultsPatients = LaboratoryOtherResults::find($id);
        $EditOtherResultsPatients->laboratoryresults = $request['laboratoryresults'];
        $EditOtherResultsPatients->normalvalues = $request['normalvalues'];
        $EditOtherResultsPatients->patient_id = $request['patient_id'];
        $EditOtherResultsPatients->patientrecoddateid = $request['patientrecoddateid'];
        $EditOtherResultsPatients->resultcategory = 'Other Results';
        $EditOtherResultsPatients->Labotechid = $user_id;
        $EditOtherResultsPatients->save();

        $patientrecoddateid = $request['patientrecoddateid'];

        $id = $request['patient_id'];

        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();

        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();

        $listlabotest = LaboratoryTest::all();
        $listlabochargers= PatientCharges::whereIn('patientcharges.activity_name',['39','40','41','42','43','44','45','46','47','48','49','50','51'])
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        return view('backend.LaboratoryTechTest')->with(['listlabochargers'=>$listlabochargers,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function DeleteOtherResultsPatients(Request $request){
        $id = $request['id'];
        $deleteotherresults = LaboratoryOtherResults::find($id);
        $deleteotherresults->delete();
        return back()->with('success','you have successfully deleted laboratory results');
    }
    public function AddNfsImage(Request $request){
        $user_id = \Auth::user()->id;
        $addimage = new NfsImage();
        $addimage->patient_id = $request['patient_id'];
        $addimage->patientrecoddateid = $request['patientrecoddateid'];
        $addimage->Laboratorytechnician = $user_id;
        $image = $request->file('nfsimage');
        $addimage->nfsimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/nfsimage');
        $image->move($destinationPath, $imagename);
        $addimage->save();
        return back()->with('success','you have successfully added image');

    }
    public function AddOtherResultsPatientsNFS(Request $request){
        $user_id = \Auth::user()->id;
        $AddOtherResultsPatientsNFS = new LaboratoryResultNfs();
        $AddOtherResultsPatientsNFS->wbc = $request['wbc'];
        $AddOtherResultsPatientsNFS->ly = $request['ly'];
        $AddOtherResultsPatientsNFS->mid = $request['mid'];
        $AddOtherResultsPatientsNFS->gra = $request['gra'];
        $AddOtherResultsPatientsNFS->rbc = $request['rbc'];
        $AddOtherResultsPatientsNFS->hgb = $request['hgb'];
        $AddOtherResultsPatientsNFS->hct = $request['hct'];
        $AddOtherResultsPatientsNFS->plt = $request['plt'];
        $AddOtherResultsPatientsNFS->mvc = $request['mvc'];
        $AddOtherResultsPatientsNFS->mch = $request['mch'];
//        $AddOtherResultsPatientsNFS->ecbu = $request['ecbu'];
//        $AddOtherResultsPatientsNFS->eap = $request['eap'];
//        $AddOtherResultsPatientsNFS->fv_fu = $request['fv_fu'];
        $AddOtherResultsPatientsNFS->laboappoint_id = $request['laboappoint_id'];
        $AddOtherResultsPatientsNFS->patient_id = $request['patient_id'];
        $AddOtherResultsPatientsNFS->patientrecoddateid = $request['patientrecoddateid'];
        $AddOtherResultsPatientsNFS->resultcategory = 'NFS Results';
        $AddOtherResultsPatientsNFS->Labotechid = $user_id;
        $AddOtherResultsPatientsNFS->laboratorytype = $request['laboratorytype'];
        $AddOtherResultsPatientsNFS->laboratorytestname = $request['laboratorytestname'];
//        $AddOtherResultsPatientsNFS->patient_result_id = $;
        $AddOtherResultsPatientsNFS->save();

        $patientrecoddateid = $request['patientrecoddateid'];

        $id = $request['patient_id'];

        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name','users.name')
            ->get();
        $listlabochargers= PatientCharges::whereIn('patientcharges.activity_name',['39','40','41','42','43','44','45','46','47','48','49','50','51'])
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listlabochar= PatientCharges::where('activity_name','11')
            ->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->where('patientcharges.patient_id',$id)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('users.name','mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();


        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();

        $listlabotest = LaboratoryTest::all();
        return view('backend.LaboratoryTechTest')->with(['listlabochar'=>$listlabochar,'listlabochargers'=>$listlabochargers,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function EditOtherResultsPatientsNFS(Request $request){
        $user_id = \Auth::user()->id;
        $id = $request['id'];
        $EditOtherResultsPatientsNFS = LaboratoryResultNfs::find($id);
        $EditOtherResultsPatientsNFS->wbc = $request['wbc'];
        $EditOtherResultsPatientsNFS->ly = $request['ly'];
        $EditOtherResultsPatientsNFS->mid = $request['mid'];
        $EditOtherResultsPatientsNFS->gra = $request['gra'];
        $EditOtherResultsPatientsNFS->rbc = $request['rbc'];
        $EditOtherResultsPatientsNFS->hgb = $request['hgb'];
        $EditOtherResultsPatientsNFS->hct = $request['hct'];
        $EditOtherResultsPatientsNFS->plt = $request['plt'];
        $EditOtherResultsPatientsNFS->mvc = $request['mvc'];
        $EditOtherResultsPatientsNFS->mch = $request['mch'];
//        $EditOtherResultsPatientsNFS->ecbu = $request['ecbu'];
//        $EditOtherResultsPatientsNFS->eap = $request['eap'];
//        $EditOtherResultsPatientsNFS->fv_fu = $request['fv_fu'];
        $EditOtherResultsPatientsNFS->patient_id = $request['patient_id'];
        $EditOtherResultsPatientsNFS->patientrecoddateid = $request['patientrecoddateid'];
        $EditOtherResultsPatientsNFS->resultcategory = 'NFS Results';
        $EditOtherResultsPatientsNFS->Labotechid = $user_id;
        $EditOtherResultsPatientsNFS->laboratorytype = $request['laboratorytype'];
        $EditOtherResultsPatientsNFS->laboratorytestname = $request['laboratorytestname'];
        $EditOtherResultsPatientsNFS->save();
        return back();
    }
    public function DeleteOtherResultsPatientsNFS(Request $request){
        $id = $request['id'];
        $deleteotherresults = LaboratoryResultNfs::find($id);
        $deleteotherresults->delete();
        return back()->with('success','you have successfully deleted laboratory results');
    }
    public function LaboratoryResultsPrint(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $patient_id = $getpatientidfromdaterecord;
        $patientinformation = Patient::where('patient.id',$patient_id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
//        $listresutls = LaboratoryOtherResults::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->get();

        $currentMonth = Carbon::now();
        $dt = $currentMonth->toDateString();
       $listresult = LaboratoryOtherResults::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'laboratoryotherresults.laboratorytype')
            ->join('mediservice', 'mediservice.id', '=', 'laboratoryotherresults.laboratorytestname')
            ->select('departmentdata.departmentname','mediservice.medicinename','laboratoryotherresults.*')
            ->get();

        return view('backend.LaboratoryResultsPrint')->with(['listresult'=>$listresult,'patientrecoddateid'=>$patientrecoddateid,'patient_id'=>$patient_id,'patientinformation'=>$patientinformation]);

    }
    public function LaboratoryResultsPrintNFS(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $patient_id = $getpatientidfromdaterecord;

        $patientinformation = Patient::where('patient.id',$patient_id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $currentMonth = Carbon::now();
        $dt = $currentMonth->toDateString();
        $listresult = LaboratoryResultNfs::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->join('departmentdata', 'departmentdata.id', '=', 'laboratoryresultnfs.laboratorytype')
                ->join('mediservice', 'mediservice.id', '=', 'laboratoryresultnfs.laboratorytestname')
                ->select('departmentdata.departmentname','mediservice.medicinename','laboratoryresultnfs.*')
                ->get();

        return view('backend.LaboratoryResultsPrintNFS')->with(['listresult'=>$listresult,'patientrecoddateid'=>$patientrecoddateid,'patient_id'=>$patient_id,'patientinformation'=>$patientinformation]);

    }


    /**
     *
     * END OF  LABORATORY MODULE
     */


    /**
     *
     * START OF  HOSPITALIZATION MODULE
     */

    public function HospitalizationDashboard(){
        return view('backend.HospitalizationDashboard');
    }
    public function Hospitalize(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
//        $listlaboapp = AppointmentLaboratory::join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
//            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
//            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
//            ->select('users.name','users.email','patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name','appointmentlaboratory.lab_tech','appointmentlaboratory.patient_id','appointmentlaboratory.nurse_id','insurancedata.insurance_name')
//            ->get();

        $listlaboapp = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        return view('backend.Hospitalize')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient,'listlaboapp'=>$listlaboapp]);

    }
    public function HospitalizePatientHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        $listlabotest = LaboratoryTest::all();
        return view('backend.HospitalizePatientHistory')->with(['listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }
    public function HospitalizePatient(Request $request){

        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;


        $listusers = User::all();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->join('users', 'users.id', '=', 'patient_doctor_appointment.doctor_id')
            ->select('patient_doctor_appointment.*','users.name','users.email')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('users', 'users.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','users.name','users.email')
            ->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $listroom = RoomPrice::all();
        $listHospitalizePatient = HospitilazePatient::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('roomlevels', 'roomlevels.id', '=', 'hospitilazepatient.patient_roomnumber')
            ->select('roomlevels.roomnumber','hospitilazepatient.*')
            ->get();

        return view('backend.HospitalizePatient')->with(['insurance_id'=>$insurance_id,'patientrecoddateid'=>$patientrecoddateid,'listpatientprice'=>$listpatientprice,'NursePatient'=>$NursePatient,'listHospitalizePatient'=>$listHospitalizePatient,'listroom'=>$listroom,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusers'=>$listusers,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp]);

    }
    public function HospitalizeReport(){
        $listHospitalizePatient = HospitilazePatient::join('patient', 'patient.id', '=', 'hospitilazepatient.patient_id')
            ->select('patient.beneficiary_name','hospitilazepatient.*')
            ->groupBy('created_at')
            ->orderBy('id','desc')
            ->get();
        return view('backend.HospitalizeReport')->with(['listHospitalizePatient'=>$listHospitalizePatient]);

    }
    public function HospitalizeReportFilter(Request $request){

        $roomlevel = $request['roomlevel'];
        $fromdate = $request['fromdate'];
        $fromdate = $request['fromdate'];
        $hospitalizationstatus = $request['hospitalizationstatus'];
        $todate = $request['todate'];

        if($roomlevel == 'All' AND $hospitalizationstatus == 'All'){
            $listHospitalizePatient = HospitilazePatient::whereBetween('hospitilazepatient.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'hospitilazepatient.patient_id')
                ->select('patient.beneficiary_name','hospitilazepatient.*')
                ->groupBy('hospitilazepatient.created_at')
                ->orderBy('id','desc')
                ->get();
            return view('backend.HospitalizeReport')->with(['listHospitalizePatient'=>$listHospitalizePatient]);

        }elseif($roomlevel !== 'All' AND $hospitalizationstatus == 'All'){
            $listHospitalizePatient = HospitilazePatient::whereBetween('hospitilazepatient.created_at', [$fromdate, $todate])
                ->where('patient_roomlevel',$roomlevel)->join('patient', 'patient.id', '=', 'hospitilazepatient.patient_id')
                ->select('patient.beneficiary_name','hospitilazepatient.*')
                ->groupBy('hospitilazepatient.created_at')
                ->orderBy('id','desc')
                ->get();
            return view('backend.HospitalizeReport')->with(['listHospitalizePatient'=>$listHospitalizePatient]);
        }elseif($roomlevel == 'All' AND $hospitalizationstatus !== 'All'){
            $listHospitalizePatient = HospitilazePatient::whereBetween('hospitilazepatient.created_at', [$fromdate, $todate])
                ->where('hospitilazestatus',$hospitalizationstatus)->join('patient', 'patient.id', '=', 'hospitilazepatient.patient_id')
                ->select('patient.beneficiary_name','hospitilazepatient.*')
                ->groupBy('hospitilazepatient.created_at')
                ->orderBy('id','desc')
                ->get();
            return view('backend.HospitalizeReport')->with(['listHospitalizePatient'=>$listHospitalizePatient]);
        }
        else{
            $listHospitalizePatient = HospitilazePatient::where('hospitilazestatus',$hospitalizationstatus)
                ->where('patient_roomlevel',$roomlevel)->whereBetween('hospitilazepatient.created_at', [$fromdate, $todate])->join('patient', 'patient.id', '=', 'hospitilazepatient.patient_id')
                ->select('patient.beneficiary_name','hospitilazepatient.*')
                ->groupBy('hospitilazepatient.created_at')
                ->orderBy('id','desc')
                ->get();
            return view('backend.HospitalizeReport')->with(['listHospitalizePatient'=>$listHospitalizePatient]);
        }

    }


    public function GetRoomLevelAjax(Request $request){
        $roomlevel = $request['roomlevel'];
        $getRoomlevel = RoomLevel::where('roomlevel',$roomlevel)->get();
        echo "<option value=\"\">Select Room Number</option>";
        foreach ($getRoomlevel as $roomlevel){
            echo "<option value='{$roomlevel->id}'>{$roomlevel->roomnumber}</option>";
        }
    }

    public function GetRoomPriceAjax(Request $request){
        $roomnumber = $request['roomnumber'];
        $getRoomlevel = RoomLevel::where('id',$roomnumber)->get();
        $getRoomstatus = RoomLevel::where('id',$roomnumber)->value('roomstatus');

        foreach ($getRoomlevel as $roomprice){
            echo "<option value='{$roomprice->roomprice}'>{$roomprice->roomprice}FRW ($getRoomstatus)</option>";
        }
    }

    public function Rooms(){
        $listroom = RoomLevel::all();
        return view('backend.Rooms')->with(['listroom'=>$listroom]);
    }
    public function Rooms_(Request $request){
        $all = $request->all();
        $addroominfo = new RoomLevel();
        $roomnumber = $request['roomnumber'];
        $checkroom = RoomLevel::where('roomnumber', 'like', '%' . $roomnumber . '%')->value('roomnumber');

        if($checkroom == null){
            $addroominfo-> roomlevel= $request['roomlevel'];
            $addroominfo ->roomnumber = $request['roomnumber'];
            $addroominfo ->roomprice = $request['roomprice'];
            $addroominfo ->roomstatus = 'Available';
            $addroominfo->save();
            return back()->with('success','You have successfully added room information');
        }else{
            return back()->with('success','Room number is already in database');
        }


    }


    public function EditRoomInfo_(Request $request){
        $id = $request['id'];
        $updateroominfo = RoomLevel::find($id);
        $updateroominfo->roomlevel = $request['roomlevel'];
        $updateroominfo->roomnumber = $request['roomnumber'];
        $updateroominfo->roomstatus = $request['roomstatus'];
        $updateroominfo->roomprice = $request['roomprice'];
        $updateroominfo->save();
        return redirect()->route('backend.Rooms')->with('success','You have successful updated Room Information');
    }

    public function DeleteRoomInfo(Request $request){
        $id = $request['id'];
        $deleteroom = RoomLevel::find($id);
        $deleteroom->delete();
        return back()->with('success','you successfully deleted Room');
    }
    public function RoomPrice(){
        $listroom = RoomLevel::all();
        $listinsu= InsuranceData::all();
        $listroomprice = RoomPrice::all();
        return view('backend.RoomPrice')->with(['listroom'=>$listroom,'listinsu'=>$listinsu,'listroomprice'=>$listroomprice]);
    }

    public function RoomPrice_(Request $request){
        $addroomprice  = new RoomPrice();
        $addroomprice->roomlevel = $request['roomlevel'];
        $addroomprice->roomnumber = $request['roomnumber'];
        $addroomprice->roomprice = $request['roomprice'];
        $addroomprice->save();
        return back()->with('success','You have successfully added room price');
    }
    public function EditRoomPrice(Request $request){
        $id = $request['id'];
        $listinsu= InsuranceData::all();
        $listroom = RoomLevel::all();
        $listroomsprice = RoomPrice::where('roomprice.id',$id)
            ->join('insurancedata', 'insurancedata.id', '=', 'roomprice.insurance_medicine')
            ->select('roomprice.roomlevel','roomprice.roomnumber','roomprice.insurance_medicine','roomprice.roomprice','roomprice.created_at','roomprice.id','insurancedata.insurance_name')
            ->get();
        return view('backend.EditRoomPrice')->with(['listroomsprice'=>$listroomsprice,'listinsu'=>$listinsu,'listroom'=>$listroom]);
    }

    public function EditRoomPrice_(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $updateroomprice = RoomPrice::find($id);
        $updateroomprice->roomlevel = $request['roomlevel'];
        $updateroomprice->roomnumber = $request['roomnumber'];
        $updateroomprice->insurance_medicine = $request['insurance_medicine'];
        $updateroomprice->roomprice = $request['roomprice'];
        $updateroomprice->save();
        return redirect()->route('backend.RoomPrice')->with('success','You have successful updated room prices');
    }

    public function DeleteRoomPrices(Request $request){
        $id = $request['id'];
        $deleteroom = RoomPrice::find($id);
        $deleteroom->delete();
        return back()->with('success','you successfully deleted Room price');
    }
    public function AddHospitalizePatient(Request $request){
        $roomid = $request['roomnumber'];
        $user_id = \Auth::user()->id;
        $AddHospitalizePatient = new HospitilazePatient();
        $AddHospitalizePatient->patient_id = $request['patient_id'];
        $AddHospitalizePatient->patient_roomlevel = $request['roomlevel'];
        $AddHospitalizePatient->patient_roomnumber = $request['roomnumber'];
        $AddHospitalizePatient->patient_roomprice = $request['roomprice'];
        $AddHospitalizePatient->patient_id = $request['patient_id'];
        $AddHospitalizePatient->patientrecoddateid = $request['patientrecoddateid'];
        $AddHospitalizePatient->hospitilazestatus = $request['hospitilazestatus'];
        $AddHospitalizePatient->hospitilazeaccount_id = $user_id;
        $AddHospitalizePatient->insurance_id = $request['insurance_id'];
        $AddHospitalizePatient->save();

        $updateroomstatus = RoomLevel::find($roomid);
        $updateroomstatus->roomstatus = 'Not Available';
        $updateroomstatus->save();

        return back()->with('success','you have successfully hospitalized patient');
    }
    public function DeleteHospitalizePatient(Request $request){
        $id = $request['id'];
        $DeleteHospitalizePatient = HospitilazePatient::find($id);
        $DeleteHospitalizePatient->delete();
        return back()->with('success','you have successfully delete hospitalized patient');
    }
    public function CheckOut(Request $request){
        $id = $request['id'];
        $checkout = HospitilazePatient::where('patient_id',$id)->update(['hospitilazestatus' => 'Check Out']);
        $getroomnumber = HospitilazePatient::where('patient_id',$id)->value('patient_roomnumber');
        $updateroomstatus = RoomLevel::where('id',$getroomnumber)->update(['roomstatus' => 'Available']);
        
        return back()->with('success','you have successfully updated hospitalization information');
    }
    /**
     *
     * END OF  HOSPITALIZATION MODULE
     */


    /**
     *
     * START OF  DOCTOR MODULE
     */

    public function DoctorDashboard(){
        $user_id = \Auth::user()->id;
        $accountnumber = User::where('role','Reception')->select(DB::raw('count(id) as accountnumber'))->value('accountnumber');

        $patientnumber =PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)->value('patientnumber');
        $patientpending =PatientDoctorApp::select(DB::raw('count(id) as patientpending'))->where('doctor_id',$user_id)->where('patient_status','pending')->value('patientpending');
        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');

        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');

        $user_id = \Auth::user()->id;
        /**
         *
         * SUMMARY TABLE IN PATIENT
         */

//        $patient_jan =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//
//        $patient_feb =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_marc =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_apr =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$april->format('Y/m/d'),$april->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_may =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//
//        $patient_jun =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_jul =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_aug =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_sept =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//
//        $patient_oct =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//        $patient_nov =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');
//
//
//        $patient_dec =PatientDoctorApp::select(DB::raw('count(id) as patient_status'))
//            ->whereBetween('updated_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
//            ->where('patient_doctor_appointment.doctor_id',$user_id)
//            ->where('patient_status','done')
//            ->groupBy('patientrecoddateid')
//            ->value('patient_status');

        $patient_jan = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_feb = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_marc = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_apr = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_may = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_jun = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('patientnumber');


        $patient_jul = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_aug = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_sept = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_oct = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_nov = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_dec = PatientDoctorApp::select(DB::raw('count(id) as patientnumber'))->where('doctor_id',$user_id)
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('patientnumber');

        return view('backend.DoctorDashboard')->with(['patientpending'=>$patientpending,'patient_dec'=>$patient_dec,'patient_nov'=>$patient_nov,'patient_oct'=>$patient_oct,'patient_sept'=>$patient_sept,'patient_aug'=>$patient_aug,'patient_jul'=>$patient_jul,'patient_jun'=>$patient_jun,'patient_may'=>$patient_may,'patient_apr'=>$patient_apr,'patient_marc'=>$patient_marc,'patient_jan'=>$patient_jan,'patient_feb'=>$patient_feb,'accountnumber'=>$accountnumber,'patientnumber'=>$patientnumber,'insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);
    }
    public function DoctorOperations(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        return view('backend.DoctorOperations')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }
    public function DoctorLaboratoryResults(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlaboapp = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')

            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $appointmentlist = AppointmentLaboratory::where('appointmentlaboratory.patient_status','done')
            ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('patient.*','appointmentlaboratory.created_at','appointmentlaboratory.Laboratorytechnician','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

//        $appointmentlist = PatientCharges::where('activity_name','11')
////            ->where('appointmentlaboratory.patient_status','done')
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
//            ->join('patient', 'patient.id', '=', 'patientcharges.patient_id')
//            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
//            ->select('patient.beneficiary_name','patient.gender','patient.pid','users.name','patient.id','mediservice.medicinename','departmentdata.departmentname')
//            ->groupBy('patient_id')
//            ->orderBy('id', 'desc')
//            ->get();


        return view('backend.DoctorLaboratoryResults')->with(['appointmentlist'=>$appointmentlist,'listinsu'=>$listinsu,'listpatient'=>$listpatient,'listlaboapp'=>$listlaboapp]);
    }
    public function DoctorLaboratoryPatientHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->orderBy('id', 'desc')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.DoctorLaboratoryPatientHistory')->with(['listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }
    public function DoctorLaboratoryTechTest(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;

        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $user_id = \Auth::user()->id;

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();


        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();

        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name','users.name')
            ->get();

        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();

        $listlabotest = LaboratoryTest::all();
        $listdoctorcomment = DoctorLabComment::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'doctorlabcomment.doneby')
            ->select('doctorlabcomment.*','users.name')
            ->get();

        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listsoap = SOAP::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        $listusersdoctors = User::where('role','Doctor')->get();
        $listusers = User::where('role','Doctor')->get();
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();


        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');


        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();
        $listmeasurements= PatientMeasurements::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();

        $listdental = DentalClinic::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        $listdoctorcomment = DoctorLabComment::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'doctorlabcomment.doneby')
            ->select('doctorlabcomment.*','users.name')
            ->get();

        $listlaboratory =AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->where('patient_status','done')
            ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','appointmentlaboratory.*')
            ->groupBy('appointmentlaboratory.patientrecoddateid')
            ->get();

        return view('backend.DoctorLaboratoryTechTest')->with(['listmeasurements'=>$listmeasurements,'listsoap'=>$listsoap,'listlaboratory'=>$listlaboratory,'listdental'=>$listdental,'listdoctorcomment'=>$listdoctorcomment,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function DoctorTestComment(Request $request){
        $all = $request->all();
        $addcomment = new DoctorLabComment();
        $user_id = \Auth::user()->id;
        $addcomment->resultsinterpretation = $request['resultsinterpretation'];
        $addcomment->finalconclusion = $request['finalconclusion'];
        $addcomment->treatment = $request['treatment'];
        $addcomment->patient_id = $request['patient_id'];
        $addcomment->patientrecoddateid = $request['patientrecoddateid'];
        $addcomment->doneby = $user_id;
        $addcomment->save();
        return back()->with('success','You have successfully added a comment');
    }
    public function DoctorTestCommentEdit(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $updatedoctorcomment = DoctorLabComment::find($id);
        $user_id = \Auth::user()->id;
        $updatedoctorcomment->resultsinterpretation = $request['resultsinterpretation'];
        $updatedoctorcomment->finalconclusion = $request['finalconclusion'];
        $updatedoctorcomment->treatment = $request['treatment'];
        $updatedoctorcomment->patient_id = $request['patient_id'];
        $updatedoctorcomment->patientrecoddateid = $request['patientrecoddateid'];
        $updatedoctorcomment->doneby = $user_id;
        $updatedoctorcomment->save();
        return back()->with('success','You have successfully updated a comment');
    }
    public function DoctorTestCommentDelete(Request $request){
        $id = $request['id'];
        $deletecomment = DoctorLabComment::find($id);
        $deletecomment->delete();
        return back()->with('success','You have successfully delete a comment');

    }

    /**
     *
     * END OF  DOCTOR MODULE
     */



    /**
     *
     * START OF  DOCTOR MODULE
     */

    public function DoctorDentistDashboard(){
        $accountnumber = User::where('role','Reception')->select(DB::raw('count(id) as accountnumber'))->value('accountnumber');
        $patientnumber = Patient::select(DB::raw('count(id) as patientnumber'))->value('patientnumber');
        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');

        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         * SUMMARY TABLE IN PATIENT
         */

        $patient_jan = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_feb = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_marc = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_apr = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_may = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_jun = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('patientnumber');


        $patient_jul = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_aug = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_sept = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('patientnumber');
        $patient_oct = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_nov = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('patientnumber');

        $patient_dec = Patient::select(DB::raw('count(id) as patientnumber'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('patientnumber');

        return view('backend.DoctorDentist')->with(['patient_dec'=>$patient_dec,'patient_nov'=>$patient_nov,'patient_oct'=>$patient_oct,'patient_sept'=>$patient_sept,'patient_aug'=>$patient_aug,'patient_jul'=>$patient_jul,'patient_jun'=>$patient_jun,'patient_may'=>$patient_may,'patient_apr'=>$patient_apr,'patient_marc'=>$patient_marc,'patient_jan'=>$patient_jan,'patient_feb'=>$patient_feb,'accountnumber'=>$accountnumber,'patientnumber'=>$patientnumber,'insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);


    }
    public function DentistConsultation(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $appointmentlist = PatientDoctorApp::where('patient_doctor_appointment.doctor_id',$user_id)->where('patient_doctor_appointment.patient_status','pending')->where('patient_doctor_appointment.doctor_id',$user_id)
            ->join('patient', 'patient.id', '=', 'patient_doctor_appointment.patient_id')
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient.*','patient_doctor_appointment.dateconfirm','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();

        $appointmentshift = ConfirmAppointment::where('confirm_appointments.patient_status','pending')->where('confirm_appointments.nurse_id',$user_id)
            ->join('patient', 'patient.id', '=', 'confirm_appointments.patient_id')
            ->join('users', 'users.id', '=', 'confirm_appointments.nurse_id')
            ->select('patient.*','confirm_appointments.appointment_message','users.name')
            ->groupBy('patient_id')
            ->orderBy('id', 'desc')
            ->get();
        return view('backend.DentistConsultation')->with(['appointmentshift'=>$appointmentshift,'appointmentlist'=>$appointmentlist,'listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }
    public function DentistPatientHistoryRecord(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.DentistPatientHistoryRecord')->with(['id'=>$id,'listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }
    public function DentistPatientHistoryConsult(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;

        $listsoap = SOAP::where('patient_id',$id)->get();
        $listusersdoctors = User::where('role','Doctor')->get();
        $listusers = User::where('role','Doctor')->orWhere('role','Doctor Dentist')->get();
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');


//        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
//            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
//            ->select('users.name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
//            ->get();

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();
        $listmeasurements= PatientMeasurements::all();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $listdental = DentalClinic::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();

        return view('backend.DentistPatientHistoryConsult')->with(['patient_amountpayed'=>$patient_amountpayed,'listdental'=>$listdental,'insurance_id'=>$insurance_id,'patientrecoddateid'=>$patientrecoddateid,'listappointdoctor'=>$listappointdoctor,'listsoap'=>$listsoap,'listmeasurements'=>$listmeasurements,'listconslutationcomment'=>$listconslutationcomment,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function DentistOperationPatientRecordHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.DentistOperationPatientRecordHistory')->with(['id'=>$id,'listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }
    public function DentistPatientHistoryOperation(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];
        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;
        $listusers = User::all();
        $listsoap = SOAP::where('patient_id',$id)->get();
        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $listappointdoctor = ConfirmAppointment::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->join('users', 'users.id', '=', 'patientcharges.nurseid')
            ->select('users.name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $listdoctorsapp = PatientDoctorApp::where('doctor_id',$user_id)->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();
        $listlabotest = LaboratoryTest::all();
        return view('backend.DentistPatientHistoryOperation')->with(['patient_amountpayed'=>$patient_amountpayed,'insurance_id'=>$insurance_id,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listpatientprice'=>$listpatientprice,'listappointdoctor'=>$listappointdoctor,'listsoap'=>$listsoap,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusers'=>$listusers,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp]);

    }

//    public function DoctorOperations(){
//        $listinsu= InsuranceData::all();
//        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
//            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
//            ->get();
//        return view('backend.DoctorOperations')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
//    }

    /**
     *
     * END OF  DOCTOR MODULE
     */




    /**
     *
     * START OF  ACCOUNTANT MODULE
     *
     */

    public function AccountantDashboard(){
        return view('backend.AccountantDashboard');
    }

    public function Accountant(){

        $listinsu= InsuranceData::all();

        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name','patient.id')
            ->get();

        $listlaboapp = AppointmentLaboratory::join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name','appointmentlaboratory.lab_tech','appointmentlaboratory.patient_id','appointmentlaboratory.nurse_id','insurancedata.insurance_name')
            ->get();

        $listpatientcharges= PatientCharges::join('insurancedata', 'insurancedata.id', '=', 'patientcharges.insurance_id_')
            ->join('medicine_insurance', 'medicine_insurance.medicinepriceinsurance', '=', 'patientcharges.medicine_service_price')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('patientcharges.*','insurancedata.insurance_name','medicine_insurance.medicinepriceinsurance')
            ->get();

        return view('backend.Accountant')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient,'listlaboapp'=>$listlaboapp,'listpatientcharges'=>$listpatientcharges]);
    }
    public function AccountantPatientPay(){
        $listinsu= InsuranceData::all();
        $listpatient = Patient::join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->limit('30')
            ->get();
        return view('backend.AccountantPatientPay')->with(['listinsu'=>$listinsu,'listpatient'=>$listpatient]);
    }
    public function SearchAccountantPatientPay(Request $request){
        $patientname = $request['patientname'];
        $listpatient = Patient::where('beneficiary_name', 'like', '%' . $patientname . '%')
            ->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('insurancedata.insurance_name','patient.*')
            ->get();


        foreach($listpatient as $patient){
            $record = route('backend.AccountantPatientHistory',['id'=>$patient->id]);
            echo "<tr><td><a href='$record'>{$patient->beneficiary_name}</a></td><td>{$patient->gender}</td></td><td>{$patient->beneficiary_telephone_number}</td><td>{$patient->insurance_name}</td><td>{$patient->pid}</td></tr>";
        }

    }
    public function AccountantPatientHistory(Request $request){
        $id = $request['id'];
        $listmeasurements= PatientMeasurements::all();
        $listpatientdaterecord= PatientNewRecord::where('patient_id',$id)->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listlabotest = LaboratoryTest::all();
        return view('backend.AccountantPatientHistory')->with(['listpatientdaterecord'=>$listpatientdaterecord,'listmeasurements'=>$listmeasurements,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient]);

    }

    public function AccountantPatient(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $id = $getpatientidfromdaterecord;

        $listusersdoctors = User::all();
        $listusers = User::where('role','Doctor')->get();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $user_id = \Auth::user()->id;
        $listinsu= InsuranceData::orderBy('created_at','desc')->get();

        $listpatientcharges= PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $patient_amountpayed = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');
        $remainingbalance = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();

        $listdoctorsapp = PatientDoctorApp::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'patient_doctor_appointment.nurse_id')
            ->select('patient_doctor_appointment.*','users.name')
            ->get();
        $listlaboapp = AppointmentLaboratory::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'appointmentlaboratory.lab_tech')
            ->select('appointmentlaboratory.*','LaboratoryTest.labotest_name')
            ->get();

        $insurance_id = Patient::where('id',$id)->value('insurance_patient_name');
        $listconslutationcomment = PatientConsultation::where('patient_id',$id)->get();
        $departmentdata = DepartmentData::all();
        $invoicedetails = AccountPatientClear::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
            ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
            ->get();

        $listlabotest = LaboratoryTest::all();
        $listHospitalizePatient = HospitilazePatient::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)->get();
        return view('backend.AccountantPatient')->with(['invoicedetails'=>$invoicedetails,'remainingbalance'=>$remainingbalance,'patient_amountpayed'=>$patient_amountpayed,'listHospitalizePatient'=>$listHospitalizePatient,'insurance_id'=>$insurance_id,'listpatientprice'=>$listpatientprice,'patientrecoddateid'=>$patientrecoddateid,'departmentdata'=>$departmentdata,'listlabotest'=>$listlabotest,'NursePatient'=>$NursePatient,'id'=>$id,'user_id'=>$user_id,'listconslutationcomment'=>$listconslutationcomment,'listMedicine'=>$listMedicine,'listinsu'=>$listinsu,'listpatientcharges'=>$listpatientcharges,'listusersdoctors'=>$listusersdoctors,'listdoctorsapp'=>$listdoctorsapp,'listlaboapp'=>$listlaboapp,'listusers'=>$listusers]);

    }
    public function AccountantUpdatePaymentStatus(Request $request){
        $patient_id = $request['patient_id'];
        $all = $request->all();
        $update_id = PatientCharges::find($patient_id);
        $update_id->payment_status = $request['payment_status'];
        $update_id->paymentmethod = $request['paymentmethod'];
        $update_id->save();
        return back();
    }
    public function PayedSales(){

        $insurance = InsuranceData::all();
        $invoicedetails = AccountPatientClear::where('chargesreasons','patientcharges')->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
            ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name','users.name')
            ->get();
        $totalamountpaid = AccountPatientClear::select(DB::raw('SUM(amount_paid) as amount_paid'))
            ->value('amount_paid');
        $totalamountdue = AccountPatientClear::select(DB::raw('SUM(amount_due) as amount_due'))
            ->value('amount_due');
        $departmentdata = DepartmentData::all();

        $totalinsurance_due = AccountPatientClear::select(DB::raw('SUM(insurance_due) as insurance_due'))
            ->value('insurance_due');

        return view('backend.PayedSales')->with(['totalinsurance_due'=>$totalinsurance_due,'departmentdata'=>$departmentdata,'totalamountdue'=>$totalamountdue,'totalamountpaid'=>$totalamountpaid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

    }
    public function AccountantSalesFilter(Request $request){
        $all = $request->all();
        $insuranceid = $request['insurance'];
        $paymentmethod = $request['paymentmethod'];
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $insurance = InsuranceData::all();

        if($insuranceid == 'All' AND $paymentmethod == 'All' ){

            $invoicedetails = AccountPatientClear::whereBetween('accountpatientclear.updated_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
//
            $totalamountpaid = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])
                ->select(DB::raw('SUM(amount_paid) as amount_paid'))
                ->value('amount_paid');

            $totalinsurance_due = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])
                ->select(DB::raw('SUM(insurance_due) as insurance_due'))
                ->value('insurance_due');
//            dd($totalinsurance_due);
//            $totalinsurance_due = PatientCharges::whereBetween('created_at', [$fromdate, $todate])
//                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
//                ->value('insurance_amount');

            $totalamountdue = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])
                ->select(DB::raw('SUM(amount_due) as amount_due'))
                ->value('amount_due');
            return view('backend.AccountantSalesFilter')->with(['totalinsurance_due'=>$totalinsurance_due,'fromdate'=>$fromdate,'todate'=>$todate,'totalamountdue'=>$totalamountdue,'totalamountpaid'=>$totalamountpaid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }

        elseif($insuranceid != 'All' AND $paymentmethod == 'All'){

            $invoicedetails = AccountPatientClear::whereBetween('accountpatientclear.updated_at', [$fromdate, $todate])
                ->where('accountpatientclear.insurance_id',$insuranceid)
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
//            dd($invoicedetails);

            $totalamountpaid = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])->where('accountpatientclear.insurance_id',$insuranceid)
                ->select(DB::raw('SUM(amount_paid) as amount_paid'))
                ->value('amount_paid');
            $totalamountdue = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])->where('accountpatientclear.insurance_id',$insuranceid)
                ->select(DB::raw('SUM(amount_due) as amount_due'))
                ->value('amount_due');
//            $totalinsurance_due = AccountPatientClear::whereBetween('created_at', [$fromdate, $todate])->where('accountpatientclear.insurance_id',$insuranceid)
//                ->select(DB::raw('SUM(insurance_due) as insurance_due'))
//                ->value('insurance_due');

            $totalinsurance_due = PatientCharges::whereBetween('updated_at', [$fromdate, $todate])->where('insurance_id',$insuranceid)
                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                ->value('insurance_amount');


            return view('backend.AccountantSalesFilter')->with(['fromdate'=>$fromdate,'todate'=>$todate,'totalinsurance_due'=>$totalinsurance_due,'totalamountdue'=>$totalamountdue,'totalamountpaid'=>$totalamountpaid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }

        elseif($insuranceid == 'All' AND $paymentmethod != 'All'){

            $invoicedetails = AccountPatientClear::whereBetween('accountpatientclear.updated_at', [$fromdate, $todate])
                ->where('accountpatientclear.paymentmethod',$paymentmethod)
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
            $totalamountpaid = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])->where('paymentmethod',$paymentmethod)
                ->select(DB::raw('SUM(amount_paid) as amount_paid'))
                ->value('amount_paid');
            $totalamountdue = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])->where('paymentmethod',$paymentmethod)
                ->select(DB::raw('SUM(amount_due) as amount_due'))
                ->value('amount_due');
//            $totalinsurance_due = AccountPatientClear::whereBetween('created_at', [$fromdate, $todate])->where('paymentmethod',$paymentmethod)
//                ->select(DB::raw('SUM(insurance_due) as insurance_due'))
//                ->value('insurance_due');
            $totalinsurance_due = PatientCharges::whereBetween('updated_at', [$fromdate, $todate])->where('paymentmethod',$paymentmethod)
                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                ->value('insurance_amount');

            return view('backend.AccountantSalesFilter')->with(['fromdate'=>$fromdate,'todate'=>$todate,'totalinsurance_due'=>$totalinsurance_due,'totalamountdue'=>$totalamountdue,'totalamountpaid'=>$totalamountpaid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }

        elseif($insuranceid !== 'All' AND $paymentmethod !='All'){

            $invoicedetails = AccountPatientClear::whereBetween('accountpatientclear.updated_at', [$fromdate, $todate])
                ->where('accountpatientclear.insurance_id',$insuranceid)
                ->where('accountpatientclear.paymentmethod',$paymentmethod)
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
            $totalamountpaid = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])->where('accountpatientclear.insurance_id',$insuranceid)
                ->where('paymentmethod',$paymentmethod)
                ->select(DB::raw('SUM(amount_paid) as amount_paid'))
                ->value('amount_paid');
            $totalamountdue = AccountPatientClear::whereBetween('updated_at', [$fromdate, $todate])->where('accountpatientclear.insurance_id',$insuranceid)
                ->where('paymentmethod',$paymentmethod)
                ->select(DB::raw('SUM(amount_due) as amount_due'))
                ->value('amount_due');
            $totalinsurance_due = PatientCharges::whereBetween('updated_at', [$fromdate, $todate])
                ->where('insurance_id',$insuranceid)
                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                ->value('insurance_amount');

            return view('backend.AccountantSalesFilter')->with(['fromdate'=>$fromdate,'todate'=>$todate,'totalinsurance_due'=>$totalinsurance_due,'totalamountdue'=>$totalamountdue,'totalamountpaid'=>$totalamountpaid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }
    }
    public function AccountantCashierSales(){
        $todaydate = Carbon::now();
        $tomorrow = Carbon::tomorrow();
        $datenow = $todaydate->toDateString();
        $datetomorrow = $tomorrow->toDateString();


        $insurance = InsuranceData::all();
        $listofaccountant = User::where('role','Accountant')->get();

        $invoicedetails = AccountPatientClear::whereBetween('accountpatientclear.created_at', [$datenow, $datetomorrow])
            ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
            ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
            ->get();

        $totalamountpaid = AccountPatientClear::whereBetween('created_at', [$datenow, $datetomorrow])
            ->selectRaw('SUM(`amount_due`) `amount_due`')
            ->selectRaw('SUM(`amount_paid`) `amount_paid`')
            ->selectRaw('SUM(`insurance_due`) `insurance_due`')
            ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
//            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
//            ->select('accountant_id')
            ->groupBy('accountpatientclear.accountant_id')
            ->get();

        $totalcashier = AccountPatientClear::whereBetween('created_at', [$datenow, $datetomorrow])
            ->selectRaw('SUM(`amount_due`) `amount_due`')
            ->selectRaw('SUM(`amount_paid`) `amount_paid`')
            ->selectRaw('SUM(`insurance_due`) `insurance_due`')
            ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
            ->get();

        return view('backend.AccountantCashierSales')->with(['datetomorrow'=>$datetomorrow,'datenow'=>$datenow,'totalcashier'=>$totalcashier,'listofaccountant'=>$listofaccountant,'totalamountpaid'=>$totalamountpaid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

    }
    public function AccountantCashierSalesFilter(Request $request){
        $listofaccountant = User::where('role','Accountant')->get();
        $cashier_list = $request['cashier_list'];

        $fromdate = $request['fromdate'];
        $todate = $request['todate'];

        if($cashier_list == 'All'){

            $totalamountpaid = AccountPatientClear::whereBetween('created_at', [$fromdate, $todate])
                ->selectRaw('SUM(`amount_due`) `amount_due`')
                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                ->selectRaw('SUM(`insurance_due`) `insurance_due`')
                ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
//            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
//            ->select('accountant_id')
                ->groupBy('accountpatientclear.accountant_id')
                ->get();

            $totalcashier = AccountPatientClear::whereBetween('created_at', [$fromdate, $todate])
                ->selectRaw('SUM(`amount_due`) `amount_due`')
                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                ->selectRaw('SUM(`insurance_due`) `insurance_due`')
                ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
                ->get();

            return view('backend.AccountantCashierSalesFilter')->with(['todate'=>$todate,'fromdate'=>$fromdate,'listofaccountant'=>$listofaccountant,'totalamountpaid'=>$totalamountpaid,'totalcashier'=>$totalcashier]);

        }else {

            $listofaccountant = User::where('role', 'Accountant')->get();
            $cashier_list = $request['cashier_list'];

            $fromdate = $request['fromdate'];
            $todate = $request['todate'];

//            $totalamountpaid = AccountPatientClear::where('accountant_id',$cashier_list)
//                ->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
//                ->selectRaw('SUM(`amount_due`) `amount_due`')
//                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
//                ->selectRaw('SUM(`insurance_due`) `insurance_due`')
//                ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
//                ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
//                ->select('accountpatientclear.*', 'users.name')
//                ->get();


            $totalamountpaid = AccountPatientClear::where('accountant_id',$cashier_list)
                ->whereBetween('created_at', [$fromdate, $todate])
                ->selectRaw('SUM(`amount_due`) `amount_due`')
                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                ->selectRaw('SUM(`insurance_due`) `insurance_due`')
                ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
//            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
//            ->select('accountant_id')
                ->groupBy('accountpatientclear.accountant_id')
                ->get();

//            $totalcashier = AccountPatientClear::where('accountant_id',$cashier_list)
//                ->whereBetween('created_at', [$fromdate, $todate])
//                ->selectRaw('SUM(`amount_due`) `amount_due`')
//                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
//                ->selectRaw('SUM(`insurance_due`) `insurance_due`')
//                ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
//                ->get();


            $totalcashier = AccountPatientClear::where('accountant_id',$cashier_list)
                ->whereBetween('created_at', [$fromdate, $todate])
                ->selectRaw('SUM(`amount_due`) `amount_due`')
                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                ->selectRaw('SUM(`insurance_due`) `insurance_due`')
                ->selectRaw('SUM(`remainingbalance`) `remainingbalance`')
                ->get();

            return view('backend.AccountantCashierSalesFilter')->with(['todate'=>$todate,'fromdate'=>$fromdate,'listofaccountant'=>$listofaccountant,'totalamountpaid' => $totalamountpaid, 'totalcashier' => $totalcashier]);
        }
    }
    public function HospitalizeSales(){
        $insurance = InsuranceData::all();
        $invoicedetails = AccountPatientClear::where('chargesreasons','hospitalization')->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
            ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
            ->get();
        return view('backend.HospitalizeSales')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

    }
    public function HospitalizeAccountantSalesFilter(Request $request){
        $all = $request->all();
        $insuranceid = $request['insurance'];
        $paymentmethod = $request['paymentmethod'];
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $insurance = InsuranceData::all();

        if($insuranceid == 'All' AND $paymentmethod == 'All'){

            $invoicedetails = AccountPatientClear::where('chargesreasons','hospitalization')->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();

            return view('backend.AccountantSalesFilter')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }
        elseif($insuranceid != 'All' AND $paymentmethod == 'All'){

            $invoicedetails = AccountPatientClear::where('chargesreasons','hospitalization')->where('accountpatientclear.insurance_id',$insuranceid)
                ->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
            return view('backend.AccountantSalesFilter')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }
        elseif($insuranceid !== 'All' AND $paymentmethod !='All'){

            $invoicedetails = AccountPatientClear::where('chargesreasons','hospitalization')->where('accountpatientclear.insurance_id',$insuranceid)
                ->where('accountpatientclear.paymentmethod',$paymentmethod)
                ->whereBetween('accountpatientclear.created_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name')
                ->get();
            return view('backend.HospitalizeAccountantSalesFilter')->with(['insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }


    }
    public function DeletePayedSales(Request $request){
        $id = $request['id'];
        $DeletePayedSales = AccountPatientClear::find($id);
        $DeletePayedSales->delete();

        $deleteInvoicenumber = PatientInvoice::where('accountpatientclear_id',$id)->value('id');
        $deleteInvoice = PatientInvoice::find($deleteInvoicenumber);
        $deleteInvoice -> delete();

        return back()->with('success','you have successfully deleted payment');
    }
    public function AccountantSales(){
        $insurance = InsuranceData::all();
//        $allcharges= PatientCharges::join('insurancedata', 'insurancedata.id', '=', 'patientcharges.insurance_id')
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
//            ->select('insurancedata.insurance_name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
//            ->get();
        $insurance = InsuranceData::all();
       $allcharges= PatientCharges::join('insurancedata', 'insurancedata.id', '=', 'patientcharges.insurance_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('insurancedata.insurance_name','medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        return view('backend.AccountantSales')->with(['allcharges'=>$allcharges,'insurance'=>$insurance]);
    }

    public function AccountantViewMore(Request $request){
        $all =$request->all();
        $id = $request['id'];
        $listusers = User::all();
        $NursePatient = Patient::where('patient.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $listpatientcharges= PatientCharges::join('insurancedata', 'insurancedata.id', '=', 'patientcharges.insurance_id_')
            ->join('medicine_insurance', 'medicine_insurance.medicinepriceinsurance', '=', 'patientcharges.medicine_service_price')
            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
            ->select('patientcharges.*','insurancedata.insurance_name','medicine_insurance.medicinepriceinsurance')
            ->get();
        return view('backend.AccountantViewMore')->with(['NursePatient'=>$NursePatient,'listpatientcharges'=>$listpatientcharges]);
    }
    public function AccountPatientClear(Request $request){
        $all = $request->all();
//        dd($all);
        $user_id = \Auth::user()->id;
        $patient_id =$request['patient_id'];
        $patientrecoddateid = $request['patientrecoddateid'];
        $checkpatient_id = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('id');
        $checkpatient_record = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('patientrecoddateid');
        $checkcurrentamountremaining = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_due');
        $checkcurrentinsurance_due = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('insurance_due');
        $updateinsurance_due = $checkcurrentinsurance_due + $request['insurance_due'];
        $newamount_paid = $checkcurrentamountremaining + $request['patient_amount_paid'];


        $newamount = round($request['patient_amount_paid']);

        $newamountdue = round($checkcurrentamountremaining - $newamount);
//        dd($newamountdue);
        $checkinvoice = PatientInvoice::where('accountpatientclear_id',$checkpatient_id)->value('accountpatientclear_id');

        $patient_amount_paid = $request['patient_amount_paid'];
        $patient_amount_due = $request['patient_amount_due'];

        if($patient_amount_paid > $patient_amount_due){
            return back()->with('success','you can not pay more than you are supposed to pay');
        }
        else{

            if($newamountdue < 0 ){
                if($checkinvoice == null AND $checkpatient_record == $patientrecoddateid){
                    $patient_amount_due = $request['patient_amount_due'] - $newamount_paid;
                    $AccountPatientClear = AccountPatientClear::find($checkpatient_id);
                    $AccountPatientClear->patient_id = $request['patient_id'];
                    $AccountPatientClear->patientrecoddateid = $request['patientrecoddateid'];
                    $AccountPatientClear->amount_due = '0';
                    $AccountPatientClear->insurance_due = $request['insurance_due'];
                    $AccountPatientClear->amount_paid = $newamount;
                    $AccountPatientClear->accountant_id = $user_id;
                    $AccountPatientClear->paymentmethod = $request['paymentmethod'];
                    $AccountPatientClear->insurance_id = $request['insurance_id'];
                    $AccountPatientClear->chargesreasons = $request['chargesreasons'];
                    $AccountPatientClear->paymentcomment = $request['paymentcomment'];
                    $AccountPatientClear->remainingbalance = '0';
                    $AccountPatientClear->save();
                    $makeinvoice = new PatientInvoice();
                    $makeinvoice->accountpatientclear_id = $AccountPatientClear->id;
                    $makeinvoice->save();
                    return back()->with('success','you have processed payment for a client');

                }elseif($checkpatient_record == null AND $checkinvoice == null){

                    $patient_amount_due = $request['patient_amount_due'] - $request['patient_amount_paid'];
                    $AccountPatientClear = new AccountPatientClear();
                    $AccountPatientClear->patient_id = $request['patient_id'];
                    $AccountPatientClear->patientrecoddateid = $request['patientrecoddateid'];
                    $AccountPatientClear->amount_due = '0';
                    $AccountPatientClear->insurance_due = $request['insurance_due'];
                    $AccountPatientClear->amount_paid = $newamount;
                    $AccountPatientClear->accountant_id = $user_id;
                    $AccountPatientClear->paymentmethod = $request['paymentmethod'];
                    $AccountPatientClear->insurance_id = $request['insurance_id'];
                    $AccountPatientClear->chargesreasons = $request['chargesreasons'];
                    $AccountPatientClear->paymentcomment = $request['paymentcomment'];
                    $AccountPatientClear->remainingbalance = '0';
                    $AccountPatientClear->save();
                    $makeinvoice = new PatientInvoice();
                    $makeinvoice->accountpatientclear_id = $AccountPatientClear->id;
                    $makeinvoice->save();
                    return back()->with('success','you have processed payment for a client');
                }elseif($checkinvoice == $checkpatient_id AND $checkpatient_record == $patientrecoddateid){
                    return back()->with('success','you can not create a new invoice because there is an invoice already! kindly update invoice');
                }
            }else{
                if($checkinvoice == null AND $checkpatient_record == $patientrecoddateid){
                    $patient_amount_due = $request['patient_amount_due'] - $newamount_paid;
                    $AccountPatientClear = AccountPatientClear::find($checkpatient_id);
                    $AccountPatientClear->patient_id = $request['patient_id'];
                    $AccountPatientClear->patientrecoddateid = $request['patientrecoddateid'];
                    $AccountPatientClear->amount_due = $newamountdue;
                    $AccountPatientClear->insurance_due = $request['insurance_due'];
                    $AccountPatientClear->amount_paid = $newamount;
                    $AccountPatientClear->accountant_id = $user_id;
                    $AccountPatientClear->paymentmethod = $request['paymentmethod'];
                    $AccountPatientClear->insurance_id = $request['insurance_id'];
                    $AccountPatientClear->chargesreasons = $request['chargesreasons'];
                    $AccountPatientClear->paymentcomment = $request['paymentcomment'];
                    $AccountPatientClear->remainingbalance = $newamountdue;
                    $AccountPatientClear->save();
                    $makeinvoice = new PatientInvoice();
                    $makeinvoice->accountpatientclear_id = $AccountPatientClear->id;
                    $makeinvoice->save();
                    return back()->with('success','you have processed payment for a client');

                }elseif($checkpatient_record == null AND $checkinvoice == null){

                    $patient_amount_due = $request['patient_amount_due'] - $request['patient_amount_paid'];
                    $AccountPatientClear = new AccountPatientClear();
                    $AccountPatientClear->patient_id = $request['patient_id'];
                    $AccountPatientClear->patientrecoddateid = $request['patientrecoddateid'];
                    $AccountPatientClear->amount_due = $newamountdue;
                    $AccountPatientClear->insurance_due = $request['insurance_due'];
                    $AccountPatientClear->amount_paid = $newamount;
                    $AccountPatientClear->accountant_id = $user_id;
                    $AccountPatientClear->paymentmethod = $request['paymentmethod'];
                    $AccountPatientClear->insurance_id = $request['insurance_id'];
                    $AccountPatientClear->chargesreasons = $request['chargesreasons'];
                    $AccountPatientClear->paymentcomment = $request['paymentcomment'];
                    $AccountPatientClear->remainingbalance = $newamountdue;
                    $AccountPatientClear->save();
                    $makeinvoice = new PatientInvoice();
                    $makeinvoice->accountpatientclear_id = $AccountPatientClear->id;
                    $makeinvoice->save();
                    return back()->with('success','you have processed payment for a client');
                }elseif($checkinvoice == $checkpatient_id AND $checkpatient_record == $patientrecoddateid){
                    return back()->with('success','you can not create a new invoice because there is an invoice already! kindly update invoice');
                }
            }


        }


    }

    public function UpdateAccountPatientClear(Request $request){
        $all = $request->all();
        $patient_id = $request['patient_id'];
        $patientrecoddateid = $request['patientrecoddateid'];
        $payment_id = $request['payment_id'];
        $remainingbalance = $request['remainingbalance'];
        $patient_amount_paid = $request['patient_amount_paid'];
        $checkcurrentamount = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_paid');
        $checkcurrentamountremaining = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('amount_due');
        $newamount = $checkcurrentamount + $patient_amount_paid;
        $newamountremaing = $checkcurrentamountremaining - $patient_amount_paid;

        if($patient_amount_paid > $remainingbalance){
            return back()->with('success','you can not pay more than you are supposed to pay');
        }else{
            $updatepayment = AccountPatientClear::find($payment_id);
            $updatepayment->amount_due = $newamountremaing;
            $updatepayment->remainingbalance = $newamountremaing;
            $updatepayment->amount_paid = $newamount;
            $updatepayment->paymentcomment = $request['paymentcomment'];
            $updatepayment->save();
            return back()->with('success','you have successfully updated invoice');
        }
    }

    public function InvoiceTobeGenerated(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $patient_id = $getpatientidfromdaterecord;
//        $exclude = '6';
        $patientcharges= PatientCharges::whereNotIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $patientchargesprivate= PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listpatientprice = PatientCharges::whereNotIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listpatientpricetotal = PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $listpatientpriceprivate = PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $listpatientprice__ = PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)->where('patientcharges.medicine_service_price','!=','40000')
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $patientinformation = Patient::where('patient.id',$patient_id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        $invoicedetails = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->where('chargesreasons','patientcharges')
            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
            ->select('users.name','accountpatientclear.*')
            ->get();


        $invoicedetailshospi = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->where('chargesreasons','hospitalization')
            ->join('PatientInvoice', 'PatientInvoice.accountpatientclear_id', '=', 'accountpatientclear.id')
            ->select('PatientInvoice.id','accountpatientclear.paymentmethod','accountpatientclear.amount_paid','accountpatientclear.paymentcomment')
            ->get();

        $currentMonth = Carbon::now();
        $dt = $currentMonth->toDateString();

        $patientinsurance = Patient::where('patient.id',$patient_id)->value('insurance_patient_name');

        return view('backend.InvoiceTobeGenerated')->with(['listpatientpricetotal'=>$listpatientpricetotal,'patientinsurance'=>$patientinsurance,'listpatientpriceprivate'=>$listpatientpriceprivate,'patientchargesprivate'=>$patientchargesprivate,'patientrecoddateid'=>$patientrecoddateid,'patient_id'=>$patient_id,'invoicedetailshospi'=>$invoicedetailshospi,'dt'=>$dt,'patientcharges'=>$patientcharges,'listpatientprice'=>$listpatientprice,'patientinformation'=>$patientinformation,'invoicedetails'=>$invoicedetails]);

    }
    public function InvoiceTobeGeneratedSummary(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $patient_id = $getpatientidfromdaterecord;

        $patientcharges= PatientCharges::whereNotIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();
        $listpatientprice = PatientCharges::whereNotIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
//        $listpatientpriceprivate = PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
//            ->get();

        $listpatientprice__ = PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)->where('patientcharges.medicine_service_price','!=','40000')
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $patientinformation = Patient::where('patient.id',$patient_id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        $invoicedetails = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
            ->select('users.name','accountpatientclear.*')
            ->get();
//        dd($invoicedetails);

        $invoicedetailshospi = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->where('chargesreasons','hospitalization')
            ->join('PatientInvoice', 'PatientInvoice.accountpatientclear_id', '=', 'accountpatientclear.id')
            ->select('PatientInvoice.id','accountpatientclear.paymentmethod','accountpatientclear.amount_paid','accountpatientclear.paymentcomment')
            ->get();

        $currentMonth = Carbon::now();
        $dt = $currentMonth->toDateString();
        return view('backend.InvoiceTobeGeneratedSummary')->with(['patientrecoddateid'=>$patientrecoddateid,'patient_id'=>$patient_id,'invoicedetailshospi'=>$invoicedetailshospi,'dt'=>$dt,'patientcharges'=>$patientcharges,'listpatientprice'=>$listpatientprice,'patientinformation'=>$patientinformation,'invoicedetails'=>$invoicedetails]);

    }
    public function InvoiceTobeGeneratedSummaryPrivate(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $patient_id = $getpatientidfromdaterecord;

        $patientcharges= PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
            ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
            ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
            ->get();

        $listpatientpriceprivate = PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $listpatientprice__ = PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)->where('patientcharges.medicine_service_price','!=','40000')
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();

        $patientinformation = Patient::where('patient.id',$patient_id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();

        $invoicedetails = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('users', 'users.id', '=', 'accountpatientclear.accountant_id')
            ->select('users.name','accountpatientclear.*')
            ->get();
        $invoicedetailshospi = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->where('chargesreasons','hospitalization')
            ->join('PatientInvoice', 'PatientInvoice.accountpatientclear_id', '=', 'accountpatientclear.id')
            ->select('PatientInvoice.id','accountpatientclear.paymentmethod','accountpatientclear.amount_paid','accountpatientclear.paymentcomment')
            ->get();

        $currentMonth = Carbon::now();
        $dt = $currentMonth->toDateString();
        return view('backend.InvoiceTobeGeneratedSummaryPrivate')->with(['listpatientpriceprivate'=>$listpatientpriceprivate,'listpatientpriceprivate'=>$listpatientpriceprivate,'patientrecoddateid'=>$patientrecoddateid,'patient_id'=>$patient_id,'invoicedetailshospi'=>$invoicedetailshospi,'dt'=>$dt,'patientcharges'=>$patientcharges,'patientinformation'=>$patientinformation,'invoicedetails'=>$invoicedetails]);

    }
    public function InvoiceHospitalize(Request $request){
        $patientrecoddateid = $request['patientrecoddateid'];

        $getpatientidfromdaterecord = PatientNewRecord::where('id',$patientrecoddateid)->value('patient_id');
        $patient_id = $getpatientidfromdaterecord;

//        $patientcharges= PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//            ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
//            ->join('medicine_table', 'medicine_table.id', '=', 'patientcharges.Medicine_service_name')
//            ->select('medicine_table.medicinename','patientcharges.*','departmentdata.departmentname')
//            ->get();
        $patientcharges = HospitilazePatient::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->get();


        $listpatientprice = PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
            ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
            ->get();
        $patientinformation = Patient::where('patient.id',$patient_id)->join('insurancedata', 'insurancedata.id', '=', 'patient.insurance_patient_name')
            ->select('patient.patient_names','patient.patient_country_id','patient.insurance_patient_name','patient.insurance_card_number','patient.gender','patient.dateofbirth','patient.patient_telephone_number','patient.copay','patient.copay_relationship','patient.beneficiary_name','patient.beneficiary_telephone_number','patient.insured_location','patient.created_at','patient.id','patient.pid','insurancedata.insurance_name')
            ->get();
        $invoicedetails = AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->where('chargesreasons','patientcharges')
            ->join('PatientInvoice', 'PatientInvoice.accountpatientclear_id', '=', 'accountpatientclear.id')
            ->select('PatientInvoice.id','accountpatientclear.paymentmethod','accountpatientclear.amount_paid','accountpatientclear.paymentcomment')
            ->get();

        $invoicedetailshospi = HospitilazePatient::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
            ->join('roomlevels', 'roomlevels.id', '=', 'hospitilazepatient.patient_roomnumber')
            ->select('roomlevels.roomnumber','hospitilazepatient.*')
            ->get();

//        dd($invoicedetailshospi);

        $currentMonth = Carbon::now();
        $dt = $currentMonth->toDateString();


        return view('backend.InvoiceHospitalize')->with(['invoicedetailshospi'=>$invoicedetailshospi,'dt'=>$dt,'patientcharges'=>$patientcharges,'listpatientprice'=>$listpatientprice,'patientinformation'=>$patientinformation,'invoicedetails'=>$invoicedetails]);

    }
    public function AccountantInsuranceReport(){
        $today = Carbon::now();
        $firstdaydate = $today->toDateString();
        $insurance = InsuranceData::all();
        $invoicedetails = AccountPatientClear::where('accountpatientclear.created_at',$firstdaydate)->whereNotIn('patient.insurance_patient_name',[6])
            ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
            ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name','patient.dateofbirth','patient.gender','patient.patient_names','patient.insurance_card_number')
            ->get();

        $getconsumablesid = DepartmentData::where('departmentname', 'like',  'Consommables%')->value('id');
        $getlaboratoryid = DepartmentData::where('departmentname', 'like',  'Laboratoire%')->value('id');
        $getmedicinesid = DepartmentData::where('departmentname', 'like',  'Médicaments%')->value('id');
        $getconsultationid = DepartmentData::where('departmentname', 'like',  'consultation%')->value('id');

        return view('backend.AccountantInsuranceReport')->with(['getconsultationid'=>$getconsultationid,'getmedicinesid'=>$getmedicinesid,'getlaboratoryid'=>$getlaboratoryid,'getconsumablesid'=>$getconsumablesid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

    }
    public function AccountantInsuranceReportFilter(Request $request){
        $insurance = InsuranceData::all();
        $insuranceid = $request['insurance'];
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];

        if($insuranceid == 'All'){
            $invoicedetails = AccountPatientClear::whereNotIn('insurance_id',[6])->whereBetween('accountpatientclear.updated_at', [$fromdate, $todate])->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name','patient.dateofbirth','patient.gender','patient.patient_names','patient.insurance_card_number')
                ->get();


            $getconsumablesid = DepartmentData::where('departmentname', 'like',  'Consommables%')->value('id');
            $getlaboratoryid = DepartmentData::where('departmentname', 'like',  'Laboratoire%')->value('id');
            $getmedicinesid = DepartmentData::where('departmentname', 'like',  'Médicaments%')->value('id');
            $getconsultationid = DepartmentData::where('departmentname', 'like',  'consultation%')->value('id');

            return view('backend.AccountantInsuranceReportFilter')->with(['fromdate'=>$fromdate,'todate'=>$todate,'getconsultationid'=>$getconsultationid,'getmedicinesid'=>$getmedicinesid,'getlaboratoryid'=>$getlaboratoryid,'getconsumablesid'=>$getconsumablesid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }else{

            $invoicedetails = AccountPatientClear::whereNotIn('insurance_id',[6])->where('insurance_id',$insuranceid)
                ->whereBetween('accountpatientclear.updated_at', [$fromdate, $todate])
                ->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
                ->join('insurancedata', 'insurancedata.id', '=', 'accountpatientclear.insurance_id')
                ->select('insurancedata.insurance_name','accountpatientclear.*','patient.beneficiary_name','patient.dateofbirth','patient.gender','patient.patient_names','patient.insurance_card_number')
                ->get();

            $getconsumablesid = DepartmentData::where('departmentname', 'like',  'Consommables%')->value('id');
            $getlaboratoryid = DepartmentData::where('departmentname', 'like',  'Laboratoire%')->value('id');
            $getmedicinesid = DepartmentData::where('departmentname', 'like',  'Médicaments%')->value('id');
            $getconsultationid = DepartmentData::where('departmentname', 'like',  'consultation%')->value('id');

            return view('backend.AccountantInsuranceReport')->with(['todate'=>$todate,'fromdate'=>$fromdate,'insuranceid'=>$insuranceid,'getconsultationid'=>$getconsultationid,'getmedicinesid'=>$getmedicinesid,'getlaboratoryid'=>$getlaboratoryid,'getconsumablesid'=>$getconsumablesid,'insurance'=>$insurance,'invoicedetails'=>$invoicedetails]);

        }

    }
    public function AccountantCashierDeposit(){
        $listdeposit = AccountantDeposit::join('users', 'users.id', '=', 'depositaccountant.deposited_by')
            ->select('users.name','depositaccountant.*')
            ->orderBy('created_at','desc')
            ->get();
        return view('backend.AccountantCashierDeposit')->with(['listdeposit'=>$listdeposit]);

    }

    public function AccountantCashierDeposit_(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $savedeposit = new AccountantDeposit();
        $savedeposit->deposit_amount = $request['deposit_amount'];
        $savedeposit->deposit_date = $request['deposit_date'];
        $savedeposit->depositcomment = $request['depositcomment'];
        $savedeposit->deposited_by = $user_id;
        $savedeposit->save();
        return back()->with('success','You have successfully saved a deposit');
    }

    public function AccountantCashierDepositEdit(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        $updatedeposit = AccountantDeposit::find($id);
        $updatedeposit->deposit_amount = $request['deposit_amount'];
        $updatedeposit->deposit_date = $request['deposit_date'];
        $updatedeposit->depositcomment = $request['depositcomment'];
        $updatedeposit->deposited_by = $user_id;
        $updatedeposit->save();
        return back()->with('success','You have successfully updated a deposit');

    }
    public function AccountCashierDepositReport(){
        $listdeposit = AccountantDeposit::join('users', 'users.id', '=', 'depositaccountant.deposited_by')
            ->select('users.name','depositaccountant.*')
            ->orderBy('created_at','desc')
            ->get();
        $totaldeposit_amount = AccountantDeposit::select(DB::raw('SUM(deposit_amount) as deposit_amount'))
            ->value('deposit_amount');
        return view('backend.AccountCashierDepositReport')->with(['totaldeposit_amount'=>$totaldeposit_amount,'listdeposit'=>$listdeposit]);

    }
    public function AccountCashierDepositReportFilter(Request $request){
        $all = $request->all();
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $checkdeposit = AccountantDeposit::join('users', 'users.id', '=', 'depositaccountant.deposited_by')
            ->select('users.name','depositaccountant.*')
            ->whereBetween('depositaccountant.created_at', [$fromdate, $todate])
            ->get();
        $deposit_amount = AccountantDeposit::whereBetween('created_at', [$fromdate, $todate])
            ->select(DB::raw('SUM(deposit_amount) as deposit_amount'))
            ->value('deposit_amount');
        return view('backend.AccountCashierDepositReportFilter')->with(['checkdeposit'=>$checkdeposit,'deposit_amount'=>$deposit_amount]);
    }
    public function AccountantExpenses(){
        $listexpense = AccountantExpense::join('users', 'users.id', '=', 'AccountantExpense.accountant_id')
            ->select('users.name','AccountantExpense.*')
            ->get();
        return view('backend.AccountantExpenses')->with(['listexpense'=>$listexpense]);
    }
    public function AddAccountantExpenses(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $addexpense = new AccountantExpense();
        $addexpense->expense_amount = $request['expense_amount'];
        $addexpense->expense_date = $request['expense_date'];
        $addexpense->expense_comment = $request['expense_comment'];
        $addexpense->accountant_id = $user_id;
        $addexpense->save();
        return back()->with('success','You have successfully saved an expense');
    }
    public function EditAccountantExpenses(Request $request){
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        $updateexpense = AccountantExpense::find($id);
        $updateexpense->expense_amount = $request['expense_amount'];
        $updateexpense->expense_date = $request['expense_date'];
        $updateexpense->expense_comment = $request['expense_comment'];
        $updateexpense->accountant_id = $user_id;
        $updateexpense->save();
        return back()->with('success','You have successfully updated an expense');
    }
    public function DeleteAccountantExpenses(Request $request){
        $id = $request['id'];
        $updateexpense = AccountantExpense::find($id);
        $updateexpense->delete();
        return back()->with('success','You have successfully deleted an expense');

    }
    public function AccountantExpensesReport(){
        $listexpense = AccountantExpense::join('users', 'users.id', '=', 'AccountantExpense.accountant_id')
            ->select('users.name','AccountantExpense.*')
            ->get();
        $totalexpense_amount = AccountantExpense::select(DB::raw('SUM(expense_amount) as expense_amount'))
            ->value('expense_amount');

        return view('backend.AccountantExpensesReport')->with(['totalexpense_amount'=>$totalexpense_amount,'listexpense'=>$listexpense]);
    }
    public function AccountantExpensesReportFilter(Request $request){
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $checkexpenses = AccountantExpense::join('users', 'users.id', '=', 'AccountantExpense.accountant_id')
            ->select('users.name','AccountantExpense.*')
            ->whereBetween('AccountantExpense.created_at', [$fromdate, $todate])
            ->get();
        return view('backend.AccountantExpensesReportFilter')->with(['checkexpenses'=>$checkexpenses]);
    }

//    public function ExportExcelOrders(Request $request){
//        $guestid = $request['guestid'];
//
//        $dataexcel = ProductOrders::select('productname','productquantity','producttotal','productunit')
//            ->where('guestid','=',$guestid)
//            ->get()
//            ->toArray();
//
//        return Excel::create('Rename', function($excel) use ($dataexcel) {
//            $excel->sheet('mySheet', function($sheet) use ($dataexcel)
//            {
//                $sheet->fromArray($dataexcel);
//            });
//        })->download('xls');
//
//        $dataexcel = DB::table('productorders')
//            ->join('guest_orders', 'productorders.guestid', '=', 'guest_orders.id')
//            ->select('productorders.productname',DB::raw('SUM(productorders.productquantity) as productquantity,SUM(productorders.producttotal) as producttotal'))
//            ->whereBetween('productorders.created_at',[$request->input('from_date'),$request->input('to_date')])
//            ->where('guest_orders.orderconfirmation','=','Confirmed')
//            ->groupBy('productorders.Cartid')
//            ->get()
//            ->toArray();
//
//    }




    /**
     *
     * END OF  ACCOUNTANT MODULE
     *
     */

    public function AddInsurance(){

        $listinsu= InsuranceData::orderBy('created_at','desc')->get();
        return view('backend.AddInsurance')->with(['listinsu'=>$listinsu]);

    }

    public function AddInsurance_(Request $request){
        $all = $request->all();
        $insurancename = new InsuranceData();
        $insurance = $request['insurancename'];
        $insuid = InsuranceData::where('insurance_name', 'LIKE', '%'.$insurance.'%')->value('insurance_name');

        if (($_POST["insurancename"])==$insuid) {
            return back()->with('success','Insurance is already in Database');
        }
        else{
        $insurancename->insurance_name = $request['insurancename'];
        $insurancename->save();
        return back()->with('success','you successfully uploaded a new Insurance');
        }

    }

    public function DeleteBrand(Request $request){
        $id = $request['id'];
        $deleteinsurance = InsuranceData::find($id);
        $deleteinsurance->delete();
        return back()->with('success','you successfully deleted Insurance');

    }

    public function EditInsurance(Request $request){
        $id=$request['id'];
        $editins = InsuranceData::where('id',$id)->get();
        return view('backend.EditInsurance')->with(['editins'=>$editins]);
    }

    public function EditInsurance_(Request $request){
        $id = $request['id'];
        $update = InsuranceData::find($id);
        $update->insurance_name = $request['insurancename'];
        $update->save();
        return redirect()->route('backend.AddInsurance')->with('success','You have successful updated Insurance');
    }

    public function AddDepartment(){
        $listdepr= DepartmentData::orderBy('created_at','desc')->get();
        return view('backend.AddDepartment')->with(['listdepr'=>$listdepr]);
    }

    public function AddDepartment_(Request $request){
        $all =$request->all();
        $savedepartment= new DepartmentData();
        $departmentname = $request['departmentname'];
        $depart = DepartmentData::where('departmentname', 'LIKE', '%'.$departmentname.'%')->value('departmentname');

        if (($_POST["departmentname"])==$depart) {
            return back()->with('success','Department is already in Database');
        }
        else{
            $savedepartment->departmentname = $request['departmentname'];
            $savedepartment->save();
            return back()->with('success','you successfully uploaded a new Department');
        }
    }

    public function DeleteDepartment(Request $request){
        $id = $request['id'];
        $deletebrand = DepartmentData::find($id);
        $deletebrand->delete();
        return back()->with('success','you successfully deleted Department');

    }
    public function EditDepartment(Request $request){
        $id=$request['id'];
        $editdepr = DepartmentData::where('id',$id)->get();
        return view('backend.EditDepartment')->with(['editdepr'=>$editdepr]);
    }

    public function EditDepartment_(Request $request){
        $id = $request['id'];
        $updatedepartment = DepartmentData::find($id);
        $updatedepartment->departmentname = $request['departmentname'];
        $updatedepartment->save();
        return redirect()->route('backend.AddDepartment')->with('success','You have successful updated Department Name');

    }
    public function AddMedicines(){
        $listinsu= InsuranceData::all();
        $listdepr= DepartmentData::all();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listMedicinesec = MediService::join('insurancedata', 'insurancedata.id', '=', 'mediservice.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'mediservice.department_name_id')
            ->select('mediservice.medicinename','mediservice.medicinequantity','mediservice.insurance_medicine_id','mediservice.department_name_id','mediservice.medicinepriceinsurance','mediservice.created_at','mediservice.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
//        dd($listMedicinesec);

        return view('backend.AddMedicines')->with(['listMedicinesec'=>$listMedicinesec,'listdepr'=>$listdepr,'listinsu'=>$listinsu,'listMedicine'=>$listMedicine]);

    }
    public function ListServices(){
        $listinsu= InsuranceData::all();
        $listdepr= DepartmentData::all();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listMedicinesec = MediService::join('insurancedata', 'insurancedata.id', '=', 'mediservice.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'mediservice.department_name_id')
            ->select('mediservice.medicinename','mediservice.medicinequantity','mediservice.insurance_medicine_id','mediservice.department_name_id','mediservice.medicinepriceinsurance','mediservice.created_at','mediservice.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->limit('10')
            ->get();
//        dd($listMedicinesec);

        return view('backend.ListService')->with(['listMedicinesec'=>$listMedicinesec,'listdepr'=>$listdepr,'listinsu'=>$listinsu,'listMedicine'=>$listMedicine]);

    }
    public function EditMedecine(Request $request){
        $id = $request['id'];
        $listMedicinesec = MediService::where('mediservice.id',$id)->join('insurancedata', 'insurancedata.id', '=', 'mediservice.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'mediservice.department_name_id')
            ->select('mediservice.medicinename','mediservice.medicinequantity','mediservice.insurance_medicine_id','mediservice.department_name_id','mediservice.medicinepriceinsurance','mediservice.created_at','mediservice.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        return view('backend.EditMedecine')->with(['listMedicinesec'=>$listMedicinesec]);

    }
    public function SearchServices(Request $request){
        $servicename = $request['servicename'];
        $listMedicinesec = MediService::where('medicinename', 'like', '%' . $servicename . '%')->join('insurancedata', 'insurancedata.id', '=', 'mediservice.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'mediservice.department_name_id')
            ->select('mediservice.medicinename','mediservice.medicinequantity','mediservice.insurance_medicine_id','mediservice.department_name_id','mediservice.medicinepriceinsurance','mediservice.created_at','mediservice.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();


        foreach($listMedicinesec as $medi){
            $EditMedecine = route('backend.EditMedecine',['id'=>$medi->id]);
            $NewRecordSearch = route('backend.NewRecordSearch',['id'=>$medi->id]);
            echo "<tr><td><a href='$EditMedecine'>{$medi->departmentname}</a></td><td>{$medi->medicinename}</td></td><td>{$medi->medicinecode}</td><td>{$medi->insurance_name}</td><td>{$medi->medicinequantity}</td><td>{$medi->medicinepriceinsurance}</td><td>{$medi->created_at}</td></tr>";
        }

    }
    public function AddService(){
        $listinsu= InsuranceData::all();
        $listdepr= DepartmentData::all();
        $listMedicine = MedicineInsurance::join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();

        $listMedicinesec = MediService::join('insurancedata', 'insurancedata.id', '=', 'mediservice.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'mediservice.department_name_id')
            ->select('mediservice.medicinename','mediservice.medicinequantity','mediservice.insurance_medicine_id','mediservice.department_name_id','mediservice.medicinepriceinsurance','mediservice.created_at','mediservice.id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
//        dd($listMedicinesec);

        return view('backend.AddMedicines')->with(['listMedicinesec'=>$listMedicinesec,'listdepr'=>$listdepr,'listinsu'=>$listinsu,'listMedicine'=>$listMedicine]);

    }
    public function AddMedicineExcelUpdate(Request $request){
        $all  = $request->all();
        $id = $request['id'];
        $updatemed = MediService::find($id);
        $updatemed ->medicinepriceinsurance = $request['medicineprice'];
        $updatemed->save();
        return back()->with('success','you have successfully updated record');
    }

    public function AddMedicines_(Request $request)
    {
        $all = $request->all();
//        dd($all);
        $medicineprice = $request['medicineprice'];

        foreach ($medicineprice as $key => $medicineprice) {

            $addmedicine = new MediService();
            $addmedicine->medicinecode = $request['medicinecode'];
            $addmedicine->medicinecategory = $request['medicinecategory'];
            $addmedicine->medicinename = $request['medicinename'];
            $addmedicine->medicinequantity = $request['medicinequantity'];
            $addmedicine->department_name_id = $request['medicine_department_id'];
            $addmedicine->insurance_medicine_id = $request['insurance_medicine'][$key];
            $addmedicine->medicinepriceinsurance = $request['medicineprice'][$key];
            $addmedicine->save();

        }

//        $insurance_medicine = $request['insurance_medicine'];
//        $labo_department = $request['medicine_department_id'];
//        $medicineprice = $request['medicineprice'];
//
//        $addmedicine  = new MedicineData();
//        $addmedicine->medicinecode = $request['medicinecode'];
//        $addmedicine->medicinename = $request['medicinename'];
//        $addmedicine->medicinequantity = $request['medicinequantity'];
//        $addmedicine->department_name_id = $request['medicine_department_id'];
//        $addmedicine->save();
//
//        $lastid_medicine_id = $addmedicine->id;
//
//        foreach ($medicineprice as $key => $medicineprice) {
//            $addlaboprice  = new MedicineInsurance();
//            $addlaboprice->medicine_id = $lastid_medicine_id;
//            $addlaboprice->department_name_id = $labo_department;
//            $addlaboprice->insurance_medicine_id = $insurance_medicine[$key];
//            $addlaboprice->medicinepriceinsurance = $medicineprice;
//            $addlaboprice->save();
//
//        }

        return back()->with('success','you successfully uploaded a new record');
    }
    public function AddMedicineExcel(Request $request){
        $activityid = $request['medicine_department_id'];
        $insurance_medicine = $request['insurance_medicine'];
        $medicinecategory = $request['medicinecategory'];
        $current_date = Carbon::now();

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();

                if(!empty($data) && $data->count()){
                    foreach ($data as $key => $value) {
                        $insert[] = [
                            'medicinecode' => $value->medicinecode,
                            'medicinename' => $value->medicinename,
                            'department_name_id' => $activityid,
                            'medicinecategory' => $medicinecategory,
                            'medicinequantity' => $value->medicinequantity,
                            'insurance_medicine_id' => $insurance_medicine,
                            'medicinepriceinsurance' => $value->medicineprice,
                            'updated_at' => $current_date,
                            'created_at' => $current_date,
                        ];
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('mediservice')->insert($insert);
                        if ($insertData) {
                            return back()->with('success','you have successfully upload a new record');
                        }
                        else {
                            return back()->with('failed','Ooops something is wrong');
                        }
                    }
                }
            }
        }
    }

    public function EditMedicine(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $listinsu= InsuranceData::all();
        $listdepr= DepartmentData::all();
        $listMedicine = MedicineInsurance::where('medicine_insurance.id',$id)
            ->join('medicine_table', 'medicine_table.id', '=', 'medicine_insurance.medicine_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->join('departmentdata', 'departmentdata.id', '=', 'medicine_insurance.department_name_id')
            ->select('medicine_table.medicinecode','medicine_table.medicinename','medicine_table.medicinequantity','medicine_insurance.insurance_medicine_id','medicine_insurance.department_name_id','medicine_insurance.medicinepriceinsurance','medicine_insurance.created_at','medicine_insurance.id','medicine_insurance.medicine_id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        $getservicesid = MedicineInsurance::where('medicine_insurance.id',$id)->value('medicine_id');
        $getinsuranceandprice = MedicineInsurance::where('medicine_id',$getservicesid)
            ->join('insurancedata', 'insurancedata.id', '=', 'medicine_insurance.insurance_medicine_id')
            ->select('medicine_insurance.*','insurancedata.insurance_name')
            ->get();
//        dd($getinsuranceandprice);

        return view('backend.EditMedicine')->with(['getinsuranceandprice'=>$getinsuranceandprice,'listdepr'=>$listdepr,'listinsu'=>$listinsu,'listMedicine'=>$listMedicine]);

    }
    public function EditMedicine_(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $medicineid = $request['medicine_id'];

//        $updatemedicine  =  MedicineData::find($medicineid);
//        $updatemedicine->medicinecode = $request['medicinecode'];
//        $updatemedicine->medicinename = $request['medicinename'];
//        $updatemedicine->medicinequantity = $request['medicinequantity'];
//        $updatemedicine->save();

        $update = MedicineInsurance::find($id);
//        $update->department_name_id = $request['medicine_department_id'];
//        $update->insurance_medicine_id = $request['insurance_medicine_notprivate'];
        $update->medicinepriceinsurance = $request['medicinepriceinsurance'];
        $update->save();
        return redirect()->route('backend.AddMedicines')->with('success','You have successful updated Medicine');
    }
    public function UpdateNewInsurancePrice(Request $request){
        $all = $request->all();
        $medicineprice = $request['medicineprice'];

        foreach ($medicineprice as $key => $medicineprice) {
            $addlaboprice  = new MedicineInsurance();
            $addlaboprice->medicine_id = $request['medicine_id'];
            $addlaboprice->department_name_id = $request['medicine_id'];
            $addlaboprice->insurance_medicine_id = $request['insurance_medicine'][$key];
            $addlaboprice->medicinepriceinsurance = $medicineprice;
            $addlaboprice->save();

        }

        return back()->with('success','you successfully uploaded a new Medicine');

    }
    public function DeleteMedicine(Request $request){
        $id = $request['id'];
        $deletemedicine = MediService::find($id);
        $deletemedicine->delete();
        return back()->with('success','you successfully deleted Medicine');
    }
    public function StockManagement(){
        $listmedicine = MedicicneStockManagement::orderBy('id','desc')->get();
        return view('backend.StockManagement')->with(['listmedicine'=>$listmedicine]);
    }
    public function StockManagementReport(){
        $listmedicine = MedicicneStockManagement::orderBy('id','desc')->get();
        return view('backend.StockManagementReport')->with(['listmedicine'=>$listmedicine]);
    }
    public function StockManagementFilter(Request $request){
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];

        $listmedicine = MedicicneStockManagement::whereBetween('updated_at',[$fromdate,$todate])->orderBy('id','desc')->get();
        return view('backend.StockManagementReport')->with(['listmedicine'=>$listmedicine]);

    }
    public function StockManagementRequisition(){
        $listmedicine = MedicicneStockManagement::whereRaw('remainingpiece <= 10')->get();
        return view('backend.StockManagementRequisition')->with(['listmedicine'=>$listmedicine]);
    }
    public function StockManagementUpdate(Request $request){
        $id = $request['id'];
        $updatemedicine = MedicineData::find($id);
        $updatemedicine->medicinequantity = $request['medicine_quantity'];
        $updatemedicine->save();
        return back()->with('success','you have successfully update medicine quantity');

    }
    public function AddMedicineStock(Request $request){
        $all = $request->all();
        $addproductstock = new MedicicneStockManagement();
        $addproductstock->productname = $request['productname'];
        $addproductstock->productpackage  = $request['productpackage'];
        $addproductstock->productpiece  = $request['productpiece'];
        $addproductstock->remainingpiece  = $request['productpiece'];
//        $addproductstock->productunitprice  = $request['productunitprice'];
        $addproductstock->shipmentstatus  = '';
        $addproductstock->save();
        return back()->with('success','You have successfully added a new record');

    }
    public function UpdateMedicineStock(Request $request){
        $id = $request['id'];
//        $updatestock = MedicicneStockManagement::find($id);
//        $updatestock->productname = $request['productname'];
//        $updatestock->productpackage  = $request['productpackage'];
//        $updatestock->productpiece  = $request['productpiece'];
////        $updatestock->productunitprice  = $request['productunitprice'];
//        $updatestock->shipmentstatus  = '';

        $getcurrentproduct = MedicicneStockManagement::where('id',$id)->value('productpiece');
        $getremainingpiece = MedicicneStockManagement::where('id',$id)->value('remainingpiece');
        $getproductpackage = MedicicneStockManagement::where('id',$id)->value('productpackage');

        $newproduct = $request['productpiece'] + $getcurrentproduct;
        $newremain= $request['productpiece'] + $getremainingpiece;
        $newproductpackage= $request['productpackage'] + $getproductpackage;

        $updateproduct = MedicicneStockManagement::where('id',$id)->update(['productpackage'=>$newproductpackage,'productpiece'=>$newproduct,'remainingpiece'=>$newremain]);

        return back()->with('success','You have successfully updated current record');
    }
    public function RemoveMedicineStock(Request $request){
        $id = $request['id'];
        $deletestock = MedicicneStockManagement::find($id);
        $deletestock->delete();
        return back()->with('success','You have successfully deleted current record');
    }
    public function MedicineStockShipment(Request $request){
        $all = $request->all();
        $addshipment = new Medicineshipment();
        $addshipment->shipmentdepartment = $request['shipmentdepartment'];
        $addshipment->shipmentproductname = $request['shipmentproductname'];
        $addshipment->shipmentproductpiece = $request['shipmentproductpiece'];
        $addshipment->product_id = $request['product_id'];
        $addshipment->save();
        return back()->with('success','You have successfully added a new record');

    }
    public function RemoveMedicineStockShipment(Request $request){
        $id = $request['id'];
        $deleteshipment = Medicineshipment::find($id);
        $deleteshipment->delete();
        return back()->with('success','You have successfully deletd  a shipment record');

    }
    public function DepartmentStockManagement(){
        $user_id = \Auth::user()->role;
        $user_id_ = \Auth::user()->id;
        $listshipment = Medicineshipment::where('shipmentdepartment',$user_id)->get();
        $liststock = DepartmentStockManagement::where('user_id',$user_id_)->get();
        $listmedicine = MedicicneStockManagement::orderBy('id','desc')->get();
        return view('backend.DepartmentStockManagement')->with(['liststock'=>$liststock,'listshipment'=>$listshipment,'listmedicine'=>$listmedicine]);
    }

    public function AddDepartmentStockManagement(Request $request){
        $all = $request->all();

        $shipmentproductname = $request['shipmentproductname'];
        $shipmentproductpiece = $request['shipmentproductpiece'];
        $shipmentdepartment = $request['shipmentdepartment'];
        $currentshipmentproductpiece = $request['currentshipmentproductpiece'];
        $shipment_id = $request['shipment_id'];
        $product_id = $request['product_id'];

        if($shipmentproductpiece > $currentshipmentproductpiece){
            return back()->with('success','You can not add more than what you have in your stock');
        }else{
//            $viewmore_sum = Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
//                ->where('guestid','=',$id)->get();here('guestid','=',$id)->get();
////            $checktotalofstock = MedicicneStockManagement::
///
//            $getcurrentproductdep = DepartmentStockManagement::where('id',$product_id)->value('shipmentproductpiece');
            $getproductquantity = MedicicneStockManagement::where('id',$product_id)->value('remainingpiece');
            $newquantity = $getproductquantity - $shipmentproductpiece ;

            $updatequantity = MedicicneStockManagement::where('id', $product_id)->update(['remainingpiece' => $newquantity]);

            $user_id_ = \Auth::user()->id;
            $addshipmentrecord = new DepartmentStockManagement();
            $addshipmentrecord->shipmentdepartment = $request['shipmentdepartment'];
            $addshipmentrecord->shipmentproductname = $request['shipmentproductname'];
            $addshipmentrecord->shipmentproductpiece = $request['shipmentproductpiece'];
            $addshipmentrecord->shipmentdepartment = $request['shipmentdepartment'];
            $addshipmentrecord->shipment_id = $request['shipment_id'];
            $addshipmentrecord->user_id = $user_id_;
            $addshipmentrecord->save();

            return back()->with('success','You have successfully updated your stock');
        }
    }

    public function DeleteDepartmentStockManagement(Request $request){
        $id = $request['id'];
        $deletestock = DepartmentStockManagement::find($id);
        $deletestock->delete();
        return back()->with('success','You have successfully deleted an item in your stock');
    }

    public function LaboratoryTest(){
        $listinsu= InsuranceData::all();
        $listdepr= DepartmentData::all();
        $listLabo = LaboratoryTest::all();
//        $listLabo = LaboratoryInsurancePrice::join('LaboratoryTest', 'LaboratoryTest.id', '=', 'Laboratory_insurance_price.labotest_id')
//            ->join('insurancedata', 'insurancedata.id', '=', 'Laboratory_insurance_price.insuranceid')
//            ->join('departmentdata', 'departmentdata.id', '=', 'Laboratory_insurance_price.departmentid')
//            ->select('LaboratoryTest.labotest_name','LaboratoryTest.labotest_type','Laboratory_insurance_price.insuranceid','Laboratory_insurance_price.departmentid','Laboratory_insurance_price.laboratoryprice','Laboratory_insurance_price.created_at','Laboratory_insurance_price.id','insurancedata.insurance_name','departmentdata.departmentname')
//            ->get();
        return view('backend.LaboratoryTest')->with(['listdepr'=>$listdepr,'listinsu'=>$listinsu,'listLabo'=>$listLabo]);
    }
    public function LaboratoryTest_(Request $request){
        $all = $request->all();
//
//        $insurance_medicine = $request['insurance_medicine'];
//        $labo_department = $request['departmentid'];
//        $laboprice = $request['laboprice'];

        $addlabotest  = new LaboratoryTest();
        $addlabotest->labotest_name = $request['labotest_name'];
        $addlabotest->labotest_type = $request['labotest_type'];
        $addlabotest->save();

        return back()->with('success','you successfully added  Laboratory Test');
    }
    public function EditLaboTest(Request $request){
        $id = $request['id'];
        $listdepr= DepartmentData::all();
        $listinsu= InsuranceData::all();
        $listLabo = LaboratoryInsurancePrice::where('Laboratory_insurance_price.id',$id)
            ->join('LaboratoryTest', 'LaboratoryTest.id', '=', 'Laboratory_insurance_price.labotest_id')
            ->join('insurancedata', 'insurancedata.id', '=', 'Laboratory_insurance_price.insuranceid')
            ->join('departmentdata', 'departmentdata.id', '=', 'Laboratory_insurance_price.departmentid')
            ->select('LaboratoryTest.labotest_name','Laboratory_insurance_price.insuranceid','Laboratory_insurance_price.departmentid','Laboratory_insurance_price.laboratoryprice','Laboratory_insurance_price.created_at','Laboratory_insurance_price.id','Laboratory_insurance_price.labotest_id','insurancedata.insurance_name','departmentdata.departmentname')
            ->get();
        return view('backend.EditLaboTest')->with(['listinsu'=>$listinsu,'listdepr'=>$listdepr,'listLabo'=>$listLabo]);
    }
    public function EditLaboTest_(Request $request){
        $laboid = $request['laboid'];

        $updatelabotest = LaboratoryTest::find($laboid);
        $updatelabotest->labotest_name = $request['labotest_name'];
        $updatelabotest->save();


        return redirect()->route('backend.LaboratoryTest')->with('success','You have successful updated Laboratory Test');
    }
    public function DeleteLaboratoryTest(Request $request){
        $id = $request['id'];
        $deletelabo = LaboratoryTest::find($id);
        $deletelabo->delete();
        return back()->with('success','you successfully deleted Laboratory Test');
    }
    public function ListServicesTestExcel(Request $request){
        $labotest_name = $request['labotest_name'];
        $labotest_type = $request['labotest_type'];
        $current_date = Carbon::now();

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();

                if(!empty($data) && $data->count()){
                    foreach ($data as $key => $value) {
                        $insert[] = [
                            'labotest_name' => $value->labotest_name,
                            'labotest_type' => $labotest_type,
                            'updated_at' => $current_date,
                            'created_at' => $current_date,
                        ];
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('LaboratoryTest')->insert($insert);
                        if ($insertData) {
                            return back()->with('success','you have successfully upload a new laboratory Test');
                        }
                        else {
                            return back()->with('failed','Ooops something is wrong');
                        }
                    }
                }
            }
        }
    }
    public function ChangeLaboratoryStatus(Request $request){
//        $all = $request->all();
//        dd($all);
        $patient_status = $request['patient_status'];
        $patient_id = $request['patient_id'];
        $patientrecoddateid = $request['patientrecoddateid'];
        $doctorid = $request['doctorid'];
        $user_id = \Auth::user()->id;
        $getlabtec = User::where('id',$user_id)->value('name');

        $updatelabo = AppointmentLaboratory::where('patient_id',$patient_id)
            ->where('patientrecoddateid',$patientrecoddateid)
            ->update(['patient_status' => $patient_status,'Laboratorytechnician'=>$getlabtec]);

        $getpatient = Patient::where('id',$patient_id)->value('beneficiary_name');
//        $getdoctor = AppointmentLaboratory::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)->value('nurse_id');
//        dd($doctorid);

        User::find($doctorid)->notify(new LaboratoryResults($getpatient,$patientrecoddateid));
//        Notification::to($user_id)->send(new LaboratoryResults($getOrders));
//        Notification::send($user_id,new LaboratoryResults($getOrders));

        return back()->with('success','you have successfully update laboratory status');

    }

    public function LaboratoryReport(){
        $listlaboratory =AppointmentLaboratory::where('patient_status','done')
        ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
        ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
        ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','appointmentlaboratory.*')
        ->groupBy('appointmentlaboratory.patientrecoddateid')
        ->get();
//        dd($listlaboratory);

        $departmentdata = DepartmentData::where('departmentname','like','labo%')->get();

        return view('backend.LaboratoryReport')->with(['departmentdata'=>$departmentdata,'listlaboratory'=>$listlaboratory]);

//        return view('backend.LaboratoryReport')->with(['listlaboratory'=>$listlaboratory]);
    }
    public function GetLaboratoryTestName(Request $request){
        $laboratorytype = $request['laboratorytype'];
        $getlaboratorytestname = MediService::where('department_name_id',$laboratorytype)->get();

        echo "<option value=\"\"></option>";
        foreach ($getlaboratorytestname as $name){
            echo "<option value='{$name->medicinename}'>{$name->medicinename}</option>";
        }
    }
    public function laboratoryReportFilter(Request $request){
        $all = $request->all();
//        dd($all);
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $laboratorytype = $request['laboratorytype'];
        $laboratorytestname = $request['laboratorytestname'];

//        if (stristr($laboratorytestname, 'NFS')) {
//           $testdata = LaboratoryResultNfs::whereBetween('laboratoryresultnfs.created_at',[$fromdate,$todate])
//            ->join('patient', 'patient.id', '=', 'laboratoryresultnfs.patient_id')
//            ->join('users', 'users.id', '=', 'laboratoryresultnfs.Labotechid')
//            ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','laboratoryresultnfs.*')
//            ->groupBy('laboratoryresultnfs.patientrecoddateid')
//            ->get();
//
//        } else {
//            echo "no";
//        }
//
        $listlaboratory =AppointmentLaboratory::whereBetween('appointmentlaboratory.updated_at',[$fromdate,$todate])->where('patient_status','done')
            ->join('patient', 'patient.id', '=', 'appointmentlaboratory.patient_id')
            ->join('users', 'users.id', '=', 'appointmentlaboratory.nurse_id')
            ->select('patient.dateofbirth','patient.beneficiary_name','patient.pid','users.name','appointmentlaboratory.*')
            ->groupBy('appointmentlaboratory.patientrecoddateid')
            ->get();

        return view('backend.laboratoryReportFilter')->with(['listlaboratory'=>$listlaboratory]);

    }
    public function MedicalReport(){
        $todaydate = Carbon::now();
        $tomorrow = Carbon::tomorrow();
        $datenow = $todaydate->toDateString();
        $datetomorrow = $tomorrow->toDateString();


        $insurance = InsuranceData::all();
        $listofaccountant = User::where('role','Accountant')->get();
        $report = AccountPatientClear::whereBetween('accountpatientclear.created_at', [$datenow, $datetomorrow])->join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->select('accountpatientclear.*','patient.beneficiary_name','patient.dateofbirth')
            ->get();
        return view('backend.MedicalReport')->with(['report'=>$report]);
    }
    public function MedicalReportFilter(Request $request){
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $report = AccountPatientClear::join('patient', 'patient.id', '=', 'accountpatientclear.patient_id')
            ->whereBetween('accountpatientclear.created_at',[$fromdate,$todate])
            ->select('accountpatientclear.*','patient.beneficiary_name','patient.dateofbirth')
            ->get();
        return view('backend.MedicalReportFilter')->with(['report'=>$report]);
    }



//    public function PdfView(Request $request){
//        $id = $request['applicationid'];
//        $items = DB::table("Applications")->where('id',$id)->get();
//
//        view()->share('items',$items);
////        if($request->has('download')){
////            $pdf = PDF::loadView('backend.PdfView');
////            return $pdf->download('pdfview.pdf');
////        }
//        return view('backend.PdfView');
//    }
//    public function PdfView_(Request $request){
//        $id = $request['id'];
//        $items = DB::table("Applications")->where('id',$id)->get();
//        $names = DB::table("Applications")->where('id',$id)->value('name');
//        $data2 = 'pdf';
//        $result = $names .'.'. $data2;
//        $pdf = PDF::loadView('backend.PdfView_', compact('items'));
//        return $pdf->download($result);
//
//    }

    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();
        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->role = $request['user_role'];
        $register->phonenumber = $request['phonenumber'];
        $register->password = bcrypt($request['password']);
        $register->save();
        return redirect()->route('backend.AccountList')->with('Success','You have successfully created your account');
    }
    public function CreateAccountUpdate(Request $request){
        $id = $request['id'];
        $password = $request['password'];
        if(empty($password)){
            $updateaccount = User::find($id);
            $updateaccount->name = $request['name'];
            $updateaccount->email = $request['email'];
            $updateaccount->phonenumber = $request['phonenumber'];
            $updateaccount->save();
            return back()->with('success','you have successfully edited account information');
        }else{
            $updateaccount = User::find($id);
            $updateaccount->name = $request['name'];
            $updateaccount->email = $request['email'];
            $updateaccount->phonenumber = $request['phonenumber'];
            $updateaccount->password = bcrypt($request['password']);
            $updateaccount->save();
            return back()->with('success','you have successfully edited account information');
        }

    }
    public function CreateAccountDelete(Request $request){
        $id = $request['id'];
        $deleteaccount = User::find($id);
        $deleteaccount->delete();
        return back()->with('success','you have successfully deleted account information');
    }
    public function ViewMore(Request $request){
        $id =$request['id'];
        $listapplications = ApplyVisaFree::where('id', $id)->get();

        return view('backend.ViewMore')->with(['listapplications'=>$listapplications]);
    }
    public function SetSession(Request $request){
        $request->session()->put('my_name','Virat Gandhi');
        echo "Data has been added to session";
    }
    public function ShowSession(Request $request){
        if($request->session()->has('my_name'))
            echo $request->session()->get('my_name');
        else
            echo 'No data in the session';
    }
    public function deleteSessionData(Request $request){
        $request->session()->forget('my_name');
        echo "Data has been removed from session.";
    }

}
