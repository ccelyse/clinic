<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaboratoryOtherResults extends Model
{
    protected $table = "Laboratoryotherresults";
    protected $fillable = ['id','normalvalues','laboratorytestname','laboratorytype','laboappoint_id','laboratoryresults','patient_id','patientrecoddateid','resultcategory','Labotechid'];
}
