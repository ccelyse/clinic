<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOAP extends Model
{
    protected $table = "SOAP";
    protected $fillable = ['id','subjective','objective','assessment','plan','patient_id','patientrecoddateid','Labotechid'];
}
