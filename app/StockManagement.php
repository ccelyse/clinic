<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockManagement extends Model
{
    protected $table = "stockmanagement";
    protected $fillable = ['id','item_department','item_id','item_used_quantity'];
}

