<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorLabComment extends Model
{
    protected $table = "doctorlabcomment";
    protected $fillable = ['id','resultsinterpretation','treatment','finalconclusion','patient_id','patientrecoddateid','doneby'];
}
