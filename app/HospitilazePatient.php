<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HospitilazePatient extends Model
{
    protected $table = "hospitilazepatient";
    protected $fillable = ['id','patient_id','hospitilazestatus','patient_roomnumber','patient_roomprice','patient_roomlevel','hospitilazeaccount_id','patientrecoddateid','insurance_id'];
}
