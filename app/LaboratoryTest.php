<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaboratoryTest extends Model
{
    protected $table = "laboratorytest";
    protected $fillable = ['id','labotest_name','labotest_type'];
}
