<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicineInsurance extends Model
{
    protected $table = "medicine_insurance";
    protected $fillable = ['id','medicine_id','insurance_medicine'];
}
