<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentStockManagement extends Model
{
    protected $table = "DepartmentStockManagement";
    protected $fillable = ['id','shipmentdepartment','shipmentproductname','shipmentproductpiece','user_id','shipment_id'];
}
