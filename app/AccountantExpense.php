<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountantExpense extends Model
{
    protected $table = "AccountantExpense";
    protected $fillable = ['id','expense_amount','expense_date','expense_comment','accountant_id'];
}
