<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cell extends Model
{
    protected $table = "cells";
    protected $fillable = ['id','codecell','nameCell','sectorcode'];
}
