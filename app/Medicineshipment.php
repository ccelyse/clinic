<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicineshipment extends Model
{
    protected $table = "Medicineshipment";
    protected $fillable = ['id','shipmentdepartment','shipmentproductname','shipmentproductpiece','product_id'];
}
