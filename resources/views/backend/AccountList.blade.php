@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Complex headers with column visibility table -->
                <section id="setting">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">User Account List</h4>
                                    @if (session('Success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('Success') }}
                                        </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered setting-defaults  table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone Number</th>
                                                <th>Account level</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listaccount as $data)
                                            <tr>
                                                <td>{{$data->name}}</td>
                                                <td>{{$data->email}}</td>
                                                <td>{{$data->phonenumber}}</td>
                                                <td>{{$data->role}}</td>
                                                <td>{{$data->created_at}}</td>
                                                <td>

                                                    <button type="button"
                                                            class="btn btn-login btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#edimed{{$data->id}}">
                                                        Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="edimed{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Edit Account </h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('CreateAccountUpdate') }}" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Name</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->name}}"
                                                                                               name="name" required>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                               name="id" hidden>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Email</label>
                                                                                        <input type="email" id="projectinput1" class="form-control" value="{{$data->email}}"
                                                                                               name="email" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Phone number</label><br>
                                                                                        <input type="number" id="projectinput1" class="form-control" value="{{$data->phonenumber}}" name="phonenumber"   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                                               pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Password</label>
                                                                                        <input type="number" id="projectinput1" class="form-control"
                                                                                               name="password">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Update
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </td>
                                                <td><a href="{{ route('backend.CreateAccountDelete',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>

                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
@endsection
