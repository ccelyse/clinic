@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body"><!-- HTML5 export buttons table -->
                <section id="html5">
                    <div class="row">
                        <form
                                method="POST"
                                action="{{ url('AccountantInsuranceReportFilter') }}"
                                enctype="multipart/form-data" style="width: 100%;display: contents;">

                            {{ csrf_field() }}
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="projectinput1">Insurance</label>
                                    <select class="form-control" id="listofmedecinecharge"
                                            name="insurance"
                                    >
                                        <option value="All">All</option>
                                        @foreach($insurance as $datas)
                                            <option value="{{$datas->id}}">{{$datas->insurance_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="date">From Date</label>
                                    <input type="date" class="form-control" id="date" name="fromdate"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="date">To Date</label>
                                    <input type="date" class="form-control" id="date" name="todate"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin-bottom: 10px">
                                    <button type="submit" class="btn btn-login">
                                        <i class="la la-check-square-o"></i> Filter
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive" id="">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Voucher Identification</th>
                                                <th>Beneficiary's Affiliate Number</th>
                                                <th>age</th>
                                                <th>Sex</th>
                                                <th>Beneficiary's Names</th>
                                                <th>Affiliate's Names</th>
                                                <th>Affiliate's Affection</th>
                                                <th>Cost of consultation</th>
                                                <th>Cost of laboratory</th>
                                                <th>Cost of consumables</th>
                                                <th>Cost of medicines</th>
                                                <th>Cost of Mediacl Imaging(Echograph)</th>
                                                <th>Cost of Hospitalization</th>
                                                <th>Cost for Procedures & Materials</th>
                                                <th>Total Amount 100%</th>
                                                <th>Total Amount (insurance Due)</th>
                                                <th>Total Amount(Patient Due)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($invoicedetails as $data)
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $recordid = $data->patientrecoddateid;
                                                        $getrecorddate = \App\PatientNewRecord::where('id',$recordid)->value('patientrecorddate');
                                                        echo $getrecorddate;
                                                        ?>
                                                    </td>
                                                    <td>50540002/ {{$data->id}} / <?php echo $getrecorddate; ?></td>
                                                    <td>{{$data->insurance_card_number}}</td>
                                                    <td>{{$data->dateofbirth}}</td>
                                                    <td>{{$data->gender}}</td>
                                                    <td>{{$data->beneficiary_name}}</td>
                                                    <td>{{$data->patient_names}}</td>
                                                    <td>{{$data->insurance_name}}</td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $consultation = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->where('chargename', 'like',  'consultation%')
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$consultation RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $laboratory = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->where('chargename', 'like',  'Laboratoire%')
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$laboratory RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $consumables = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->where('chargename', 'like',  'Consommables%')
                                                            ->select(DB::raw('SUM(patient_amount) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$consumables RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $medicines = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->where('chargename', 'like',  'Médicaments%')
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$medicines RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $imaging = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->where('chargename', 'like',  'ECHOGRAPHIE%')->orWhere('chargename', 'like',  'Actes Médicaux%')
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$imaging RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $hospitalization = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->where('chargename', 'like',  'Hospitalization%')
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$hospitalization RWF";
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $procedures = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)->whereNotIn('chargename', ['consultation','Laboratoire','Consommables','Médicaments','Actes Médicaux','ECHOGRAPHIE','Hospitalization'])
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$procedures RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $totalunit = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)
                                                            ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                            ->value('medicine_service_price');
                                                        echo "$totalunit RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $insurancedue = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)
                                                            ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                                                            ->value('insurance_amount');
                                                        echo "$insurancedue RWF";
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $idcons = $data->patient_id;
                                                        $patientdue = \App\PatientCharges::where('patientrecoddateid',$recordid)->where('patient_id',$idcons)
                                                            ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                                                            ->value('patient_amount');
                                                        echo "$patientdue RWF";
                                                        ?>
                                                    </td>

                                                </tr>

                                            @endforeach
                                            <tfoot align="right">
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $consultation = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->where('chargename', 'like',  'consultation%')
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$consultation RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $laboratory = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->where('chargename', 'like',  'Laboratoire%')
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$laboratory RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $consumables = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->where('chargename', 'like',  'Consommables%')
                                                        ->select(DB::raw('SUM(patient_amount) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$consumables RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $medicines = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->where('chargename', 'like',  'Médicaments%')
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$medicines RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $imaging = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->where('chargename', 'like',  'ECHOGRAPHIE%')->orWhere('chargename', 'like',  'Actes Médicaux%')
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$imaging RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $hospitalization = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->where('chargename', 'like',  'Hospitalization%')
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$hospitalization RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $procedures = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])->whereNotIn('chargename', ['consultation','Laboratoire','Consommables','Médicaments','Actes Médicaux','ECHOGRAPHIE','Hospitalization'])
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$procedures RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $totalunit = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])
                                                        ->select(DB::raw('SUM(medicine_service_price) as medicine_service_price'))
                                                        ->value('medicine_service_price');
                                                    echo "$totalunit RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $insurancedue = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])
                                                        ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                                                        ->value('insurance_amount');
                                                    echo "$insurancedue RWF";
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    $idcons = $data->patient_id;
                                                    $patientdue = \App\PatientCharges::whereBetween('patientcharges.created_at', [$fromdate, $todate])
                                                        ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                                                        ->value('patient_amount');
                                                    echo "$patientdue RWF";
                                                    ?>
                                                </th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection