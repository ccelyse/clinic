@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

@include('backend.layout.sidemenu')
@include('backend.layout.upmenu')
<link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
<script>
</script>

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body"><!-- HTML5 export buttons table -->
            <section id="html5">
                <div class="row">
                    <form
                            method="POST"
                            action="{{ url('MedicalReportFilter') }}"
                            enctype="multipart/form-data" style="width: 100%;display: contents;">

                        {{ csrf_field() }}

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="date">From Date</label>
                                <input type="date" class="form-control" id="date" name="fromdate"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="date">To Date</label>
                                <input type="date" class="form-control" id="date" name="todate"
                                       required>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="form-group" style="margin-bottom: 10px">
                            <button type="submit" class="btn btn-login">
                                <i class="la la-check-square-o"></i> Filter
                            </button>
                        </div>
                    </div>
                    </form>
                    <div class="col-12">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Patient Names</th>
                                            <th>Age</th>
                                            <th>Doctors Names</th>
                                            <th>Subjective</th>
                                            <th>Objective</th>
                                            <th>Assessment</th>
                                            <th>Plan</th>
                                            <th>Tests</th>
                                            <th>Results interception</th>
                                            <th></th>
                                            <th>Final conclusion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($report as $data)
                                        <tr>
                                        <td>{{$data->created_at}}</td>
                                        <td>{{$data->beneficiary_name}}</td>
                                        <td>{{$data->dateofbirth}}</td>
                                        <td>
                                            <?php
                                            $patient_iddo = $data->patient_id;
                                            $patientrecoddateiddo = $data->patientrecoddateid;
                                            $doctor = \App\SOAP::where('patient_id',$patient_iddo)->where('patientrecoddateid',$patientrecoddateiddo)->value('Labotechid');
                                            $user = \App\User::where('id',$doctor)->value('name');
                                            echo $user;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            $patient_ids = $data->patient_id;
                                            $patientrecoddateids = $data->patientrecoddateid;
                                            $subjective = \App\SOAP::where('patient_id',$patient_ids)->where('patientrecoddateid',$patientrecoddateids)->value('subjective');
                                            echo  "$subjective";
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $patient_ido = $data->patient_id;
                                            $patientrecoddateido = $data->patientrecoddateid;
                                            $objective = \App\SOAP::where('patient_id',$patient_ido)->where('patientrecoddateid',$patientrecoddateido)->value('objective');
                                            echo  "$objective";
                                            ?>
                                        </td>
                                            <td>
                                                <?php
                                                $patient_ido = $data->patient_id;
                                                $patientrecoddateido = $data->patientrecoddateid;
                                                $assessment = \App\SOAP::where('patient_id',$patient_ido)->where('patientrecoddateid',$patientrecoddateido)->value('assessment');
                                                echo  "$assessment";
                                                ?>
                                            </td>
                                        <td>
                                            <?php
                                            $patient_idp = $data->patient_id;
                                            $patientrecoddateidp = $data->patientrecoddateid;
                                            $plan = \App\SOAP::where('patient_id',$patient_idp)->where('patientrecoddateid',$patientrecoddateidp)->value('plan');
                                            echo  "$plan";
                                            ?>
                                        </td>

                                        <td>

                                            <?php
                                            $patient_idla = $data->patient_id;
                                            $patientrecoddateidla = $data->patientrecoddateid;

                                            $listlabochargers= \App\PatientCharges::whereIn('patientcharges.activity_name',['39','40','41','42','43','44','45','46','47','48','49','50','51'])
                                                ->where('patientcharges.patientrecoddateid',$patientrecoddateidla)
                                                ->where('patientcharges.patient_id',$patient_idla)
                                                ->join('departmentdata', 'departmentdata.id', '=', 'patientcharges.activity_name')
                                                ->join('mediservice', 'mediservice.id', '=', 'patientcharges.Medicine_service_name')
                                                ->select('mediservice.medicinename','patientcharges.*','departmentdata.departmentname')
                                                ->get();

                                            foreach ($listlabochargers as $listlabochargers){
                                                echo "$listlabochargers->medicinename, <br><br>";
                                            }
//                                            $labotestname = $data->medicinename;
//                                            echo $patient_id;
//                                            echo $patientrecoddateid;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $patient_idd = $data->patient_id;
                                            $patientrecoddateidd = $data->patientrecoddateid;
                                            $resultsinterpretation = \App\DoctorLabComment::where('patient_id',$patient_idd)->where('patientrecoddateid',$patientrecoddateidd)->value('resultsinterpretation');
                                            echo $resultsinterpretation;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $patient_idf = $data->patient_id;
                                            $patientrecoddateidf = $data->patientrecoddateid;
                                            $treatment = \App\DoctorLabComment::where('patient_id',$patient_idf)->where('patientrecoddateid',$patientrecoddateidf)->value('treatment');
                                            echo $treatment;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $patient_idf = $data->patient_id;
                                            $patientrecoddateidf = $data->patientrecoddateid;
                                            $finalconclusion = \App\DoctorLabComment::where('patient_id',$patient_idf)->where('patientrecoddateid',$patientrecoddateidf)->value('finalconclusion');
                                            echo $finalconclusion;

//                                            foreach ($doctorcomment as $doctorcomment){
//                                                $finalconclusion = $patient_idf;
//                                            }
//                                            echo  "$finalconclusion";
                                            ?>
                                        </td>

                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
type="text/javascript"></script>
<script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
<script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
<script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
<script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
<script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
<script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
<script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
<script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
<script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection