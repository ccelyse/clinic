@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST"
                                              action="{{ url('NewRecordHistory') }}"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row  multi-field">
                                                <div class="col-md-12" hidden>
                                                    <div class="form-group">
                                                        <label for="projectinput1">Patient ID</label>
                                                        <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo "$id"; ?>">
                                                        {{--<input type="text" id="projectinput1"  class="form-control" name="patient_id" value="{{$data->id}}">da--}}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Select Date of service</label>
                                                        <input type="date" class="form-control" id="date" name="newrecorddate">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>

                                            </div>
                                        </form>
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Patient Information</h4>
                                                <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                <div class="heading-elements">
                                                    <ul class="list-inline mb-0">
                                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <ul class="list-group">
                                                        @php
                                                            $listpatientdaterecord= \App\PatientNewRecord::where('patient_id',$id)->get();
                                                        @endphp

                                                        @foreach($listpatientdaterecord as $datepatient)
                                                            <li class="list-group-item"><strong>Record Date:</strong> {{$datepatient->patientrecorddate}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection