@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($NursePatient as $datas)
                                                <li class="list-group-item"><strong>Patient
                                                        Names:</strong> {{$datas->beneficiary_name}}</li>
                                                <li class="list-group-item"><strong>Patient Date of Birth
                                                        Id:</strong> {{$datas->dateofbirth}}</li>

                                                <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                                <?php
                                                $insurancename = $datas->insurance_name;
                                                $pid = $datas->pid;
                                                $beneficiary_telephone_number = $datas->beneficiary_telephone_number;
                                                $copay = $datas->copay;
                                                ?>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Payment Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($listpatientprice as $charges)
                                                <li class="list-group-item"><strong>Insurance
                                                        Name:</strong> <?php echo"$insurancename";?></li>
                                                <li class="list-group-item"><strong>Insurance Due:
                                                    </strong>
                                                    <?php
                                                    $charges =$charges->producttotalsum;
                                                    $sumofinsurance = ($charges * $copay)/100;
                                                    $sumofpatient = $charges - $sumofinsurance ;
                                                    echo $sumofinsurance ;
                                                    ?>
                                                    FRW
                                                </li>
                                                <li class="list-group-item"><strong>Patient Due:</strong> <?php echo $sumofpatient; ?> FRW</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="form-grouppop">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#notifypatient">
                                            Notify Patient
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="notifypatient" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Notify Patient</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('ResultNotifyPatient') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}

                                                            <div class="row  multi-field">
                                                                <div class='col-md-12'>
                                                                    <script language="javascript" type="text/javascript">
                                                                        function limitText(limitField, limitCount, limitNum) {
                                                                            if (limitField.value.length > limitNum) {
                                                                                limitField.value = limitField.value.substring(0, limitNum);
                                                                            } else {
                                                                                limitCount.value = limitNum - limitField.value.length;
                                                                            }
                                                                        }
                                                                    </script>

                                                                    <textarea class='form-control' name="limitedtextarea" onKeyDown="limitText(this.form.limitedtextarea,this.form.countdown,160);"
                                                                              onKeyUp="limitText(this.form.limitedtextarea,this.form.countdown,160);" rows="5" required></textarea><br>
                                                                    <font size="1">(Maximum characters: 160)<br>
                                                                </div>

                                                                You have <input readonly type="text" name="countdown" size="3" value="160"> characters left.</font>
                                                                <input type="text" name="phonenumber" value="<?php echo "$beneficiary_telephone_number"; ?>" hidden>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Send
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-grouppop">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#notifydoctor">
                                            Notify Doctor
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="notifydoctor" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Notify Doctor</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('LabNotifyDoctor') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}

                                                            <div class="row  multi-field">
                                                                <div class="col-md-12" style="margin-bottom: 10px;">
                                                                    <label for="projectinput1">Doctors List</label>
                                                                    <select class="form-control" name="phonenumber" required>
                                                                        @foreach($listusers as $users)
                                                                            <option value="{{$users->phonenumber}}">{{$users->name}} ({{$users->role}})</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>

                                                                <div class='col-md-12'>
                                                                    <script language="javascript" type="text/javascript">
                                                                        function limitText(limitField, limitCount, limitNum) {
                                                                            if (limitField.value.length > limitNum) {
                                                                                limitField.value = limitField.value.substring(0, limitNum);
                                                                            } else {
                                                                                limitCount.value = limitNum - limitField.value.length;
                                                                            }
                                                                        }
                                                                    </script>
                                                                    <?php
                                                                    $chechrecorddate = \App\PatientNewRecord::where('id',$patientrecoddateid)->value('patientrecorddate');
                                                                    ?>
                                                                    <textarea class='form-control' name="limitedtextarea" onKeyDown="limitText(this.form.limitedtextarea,this.form.countdown,160);"
                                                                              onKeyUp="limitText(this.form.limitedtextarea,this.form.countdown,160);" rows="5" required><?php echo"Lab Results of patient with PID:$pid on this record $chechrecorddate is available"; ?></textarea><br>
                                                                    <font size="1">(Maximum characters: 160)<br>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Send
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ route('backend.LaboratoryResultsPrint',['patientrecoddateid'=> $patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Print Other Results</a>
                                    <a href="{{ route('backend.LaboratoryResultsPrintNFS',['patientrecoddateid'=> $patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Print NFS Results</a>
                                    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                            data-toggle="modal"
                                            data-target="#laboratorystatus">
                                        Laboratory Test Status
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade text-left" id="laboratorystatus" tabindex="-1"
                                         role="dialog" aria-labelledby="myModalLabel1"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel1">Laboratory Test Status</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="form-horizontal form-simple" method="POST"
                                                          action="{{ url('ChangeLaboratoryStatus') }}"
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                                                        <div class="row  multi-field">
                                                            <div class="col-md-12" style="margin-bottom: 10px;">
                                                                <label for="projectinput1">Doctors List</label>
                                                                <select class="form-control" name="doctorid" required>
                                                                    @foreach($listusers as $users)
                                                                        <option value="{{$users->id}}">{{$users->name}} ({{$users->role}})</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="date">Laboratory Test Status</label>
                                                                    <select class="form-control" name="patient_status" required>
                                                                        <option value=""></option>
                                                                        <option value="pending">pending</option>
                                                                        <option value="done">done</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <input type="text" id="patient_id"
                                                                   class="form-control"
                                                                   name="patient_id" value="<?php echo $id;?>" hidden>
                                                            <input type="text" id="patientrecoddateid"
                                                                   class="form-control"
                                                                   name="patientrecoddateid"
                                                                   value="<?php echo $patientrecoddateid;?>" hidden>
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn btn-login">
                                                                    <i class="la la-check-square-o"></i> Save
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Laboratory Test</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Laboratory Type</th>
                                                <th>Laboratory Test Name</th>
                                                <th>Date Created</th>
                                                <th>Appointed By</th>
                                                <th>Results</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listlabochar as $data)
                                                <tr>
                                                    <td>{{$data->departmentname}}</td>
                                                    <td>{{$data->medicinename}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>{{$data->name}}</td>
                                                    <?php
                                                        $labotestname = $data->medicinename;


                                                    if(preg_match('/NFS/', $labotestname)){

                                                        $patientrecoddateidnfs =  route('backend.LaboratoryNFS',['id'=> $data->id]);
                                                        echo "<td><a href='$patientrecoddateidnfs' class='btn btn-login btn-min-width mr-1 mb-1'>$labotestname Results</a></td>";
                                                    }else{
                                                        $patientrecoddateidother =  route('backend.LaboOtherResults',['id'=> $data->id]);
                                                        echo "<td><a href='$patientrecoddateidother' class='btn btn-login btn-min-width mr-1 mb-1'>$labotestname Results</a></td>";
                                                    }
                                                    ?>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
