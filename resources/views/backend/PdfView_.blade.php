<link rel="apple-touch-icon" href="backend/app-assets/images/VFALogo.png">
<link rel="shortcut icon" type="image/x-icon" href="backend/app-assets/images/VFALogo.png">
{{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700"--}}
{{--rel="stylesheet">--}}
{{--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}
{{--<link rel="stylesheet" type="text/css" href="backend/app-assets/css/vendors.min.css">--}}
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="backend/app-assets/css/app.min.css">
<link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
<link rel="stylesheet" type="text/css" href="backend/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/colors/palette-gradient.min.css">

<style>
    .content-header{
        margin-top: 40px;
        background: #000;
    }
    .content-header-title{
        color: #fff;
        border-bottom: 4px solid #0088cc;
        position: absolute;
        margin-top: 3px;
        line-height: 3;
    }
    .btn-visa {
        border-color: #d9534f !important;
        background-color: #d9534f !important;
        color: #FFF;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .card-title{
        color: #0088cc;
    }
    .btn-visablue {
        border-color: #0088cc !important;
        background-color: #0088cc !important;
        color: #FFF;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .header-navbar {
        padding: 0;
        min-height: 8rem !important;
        font-family: Quicksand,Georgia,'Times New Roman',Times,serif;
        transition: .3s ease all;
    }
    .header-navbar .navbar-container ul.nav li>a.nav-link:hover {
        color: #fff !important;
        background: #0088cc;
    }
    a{
        color: #fff !important;
    }
</style>

@foreach ($items as $key => $item)
    <div class="col-md-7">
        <div id="with-header" class="card">
            <div class="card-content collapse show">
                <div class="card-body border-top-blue-grey border-top-lighten-5 ">
                    <h4 class="card-title">Applicant Information</h4>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->name}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->email}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Phone Number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->phonenumber}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Date of Birth</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->dateofbirth}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Gender</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->gender}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Country</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->country}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>City</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->city}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Passport ID</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->passportid}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Passport Picture</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><img src="passportpictures/{{$item->passportpicture}}" style="width: 400px"></td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered setting-defaults">
                        <thead>
                        <tr>
                            <th>Your story</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$item->message}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endforeach

