@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Complex headers with column visibility table -->
                <div id="crypto-stats-3" class="row">
                    <div class="col-xl-4 col-12">
                        <div class="card crypto-card-3 pull-up">
                            <div class="card-content">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-2">
                                            <h1><i class="fas fa-users warning font-large-1" title="BTC"></i></h1>
                                        </div>
                                        <div class="col-10 pl-2">
                                            <h4>SMS Sent This Month</h4>
                                        </div>
                                        <div class="col-10 text-right">
                                            <h4><?php echo $smssentthism; ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <canvas id="btc-chartjs" class="height-75"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-12">
                        <div class="card crypto-card-3 pull-up">
                            <div class="card-content">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-2">
                                            <h1><i class="fas fa-users warning font-large-1" title="BTC"></i></h1>
                                        </div>
                                        <div class="col-10 pl-2">
                                            <h4>SMS Sent This Last Month</h4>
                                        </div>
                                        <div class="col-10 text-right">
                                            <h4><?php echo $smssentlastm; ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <canvas id="btc-chartjs" class="height-75"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="setting">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Message List</h4>
                                    @if (session('Success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('Success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered setting-defaults  table-responsive">
                                            <thead>
                                            <tr>
                                                <th>From</th>
                                                <th>to</th>
                                                <th>Message</th>
                                                <th>Delivery Status</th>
                                                <th>Sent Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listsms as $data)
                                                <tr>
                                                    <td>{{$data->from}}</td>
                                                    <td>{{$data->to}}</td>
                                                    <td>{{$data->message}}</td>
                                                    <td>{{$data->deliverystatus}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
@endsection
