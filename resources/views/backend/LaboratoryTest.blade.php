@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function () {
            $("#chkPassport").click(function () {
                if ($(this).is(":checked")) {
                    $("#dvPassport").show();
                    $("#AddPassport").hide();
                } else {
                    $("#dvPassport").hide();
                    $("#AddPassport").show();
                }
            });
        });
    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('LaboratoryTest_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Laboratory Test Type</label>
                                                            <select class="form-control" name="labotest_type" required>
                                                                   <option value="Hématologie">Hématologie</option>
                                                                   <option value="Biochimie">Biochimie</option>
                                                                   <option value="immuno sérologie">immuno sérologie</option>
                                                                   <option value="Parasitologie">Parasitologie</option>
                                                                   <option value="Bactériologie">Bactériologie</option>
                                                                   <option value="Echographie">Echographie</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Test Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Test Name"
                                                                   name="labotest_name" id="labotest_name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-login">
                                                            <i class="la la-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Department Name</label>--}}
                                                            {{--<select class="select2 form-control" multiple="multiple" id="id_h5_multi" name="departmentid" required>--}}
                                                                {{--@foreach($listdepr as $data)--}}
                                                                    {{--<option value="{{$data->id}}">{{$data->departmentname}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                                <label for="chkPassport">
                                                    <input type="checkbox" id="chkPassport" />
                                                    Upload data with excel
                                                </label>
                                                <hr />
                                                <div class="multi-field-wrapper" id="dvPassport" style="display: none">
                                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('AddLaboratoryTestExcel') }}" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Laboratory Test Type</label>
                                                                        <select class="form-control" name="labotest_type" required>
                                                                            <option value="Hématologie">Hématologie</option>
                                                                            <option value="Biochimie">Biochimie</option>
                                                                            <option value="immuno sérologie">immuno sérologie</option>
                                                                            <option value="Parasitologie">Parasitologie</option>
                                                                            <option value="Bactériologie">Bactériologie</option>
                                                                            <option value="Echographie">Echographie</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Upload Excel</label>
                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                               name="file">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <button type="submit" class="btn btn-login">
                                                                    <i class="la la-check-square-o"></i> Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                {{--<script type="text/javascript">--}}
                                                    {{--$(document).ready(function()--}}
                                                    {{--{--}}
                                                        {{--$('.multi-field-wrapper').each(function() {--}}
                                                            {{--var $wrapper = $('.multi-fields', this);--}}
                                                            {{--$(".add-field", $(this)).click(function(e) {--}}
                                                                {{--$('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();--}}
                                                            {{--});--}}
                                                            {{--$('.multi-field .remove-field', $wrapper).click(function() {--}}
                                                                {{--if ($('.multi-field', $wrapper).length > 1)--}}
                                                                    {{--$(this).parent('.multi-field').remove();--}}
                                                            {{--});--}}
                                                        {{--});--}}
                                                    {{--});--}}
                                                {{--</script>--}}
                                                {{--<div class="multi-field-wrapper">--}}
                                                    {{--<div class="multi-fields">--}}
                                                          {{--<div class="row  multi-field">--}}
                                                              {{--<div class="col-md-6">--}}
                                                                  {{--<div class="form-group">--}}
                                                                      {{--<label for="projectinput1">Insurance  Name</label>--}}
                                                                      {{--<select class="form-control"  id="basicSelect" name="insurance_medicine[]" required>--}}
                                                                          {{--@foreach($listinsu as $data)--}}
                                                                              {{--<option value="{{$data->id}}">{{$data->insurance_name}}</option>--}}
                                                                          {{--@endforeach--}}
                                                                      {{--</select>--}}

                                                                      {{--<input type="text" class="form-control" placeholder="Test Name" name="stuff[]">--}}

                                                                  {{--</div>--}}
                                                              {{--</div>--}}
                                                              {{--<div class="col-md-6">--}}
                                                                  {{--<div class="form-group">--}}
                                                                      {{--<label for="projectinput1">Laboratory  Price</label>--}}
                                                                      {{--<input type="text" id="projectinput1" class="form-control" placeholder="Laboratory  Price"--}}
                                                                      {{--name="laboprice[]">--}}
                                                                  {{--</div>--}}
                                                              {{--</div>--}}
                                                              {{--<button type="button" class="remove-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>--}}
                                                              {{--<button type="button" class="add-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>--}}
                                                          {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic form layout section end -->
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Laboratory Test</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Laboratory Test Type</th>
                                                <th>Laboratory Test Name</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listLabo as $data)
                                                <tr>
                                                    <td>{{$data->labotest_type}}</td>
                                                    <td>{{$data->labotest_name}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                            <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#clearchargers{{$data->id}}">
                                                                Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="clearchargers{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Edit Laboratory</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST"
                                                                                  action="{{ url('EditLaboTest_') }}"
                                                                                  enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row  multi-field">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Laboratory Test Type</label>
                                                                                            <select class="form-control" name="labotest_type" required>
                                                                                                <option value="{{$data->labotest_type}}">{{$data->labotest_type}}</option>
                                                                                                <option value="Hématologie">Hématologie</option>
                                                                                                <option value="Biochimie">Biochimie</option>
                                                                                                <option value="immuno sérologie">immuno sérologie</option>
                                                                                                <option value="Parasitologie">Parasitologie</option>
                                                                                                <option value="Bactériologie">Bactériologie</option>
                                                                                                <option value="Echographie">Echographie</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Test Name</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->labotest_name}}"
                                                                                                   name="labotest_name" id="labotest_name" required>
                                                                                            <input type="text" value="{{$data->id}}" name="laboid" hidden>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <button type="submit" class="btn btn-login">
                                                                                            <i class="la la-check-square-o"></i> Save
                                                                                        </button>
                                                                                    </div>

                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </td>

                                                    <td><a href="{{ route('backend.DeleteLaboratoryTest',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="{!! asset('js/append.js') !!}" type="text/javascript"></script>


   <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>


@endsection