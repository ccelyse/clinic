@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">

                <section id="complex-header">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <h4 class="card-title">Laboratory  Results</h4>
                                <form class="form-horizontal form-simple" method="POST"
                                      action="{{ url('AddOtherResultsPatients') }}"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12" hidden>
                                                <div class="form-group">
                                                    <label for="projectinput1">Patient
                                                        ID</label>
                                                    <input type="text" id="projectinput1"
                                                           class="form-control"
                                                           name="patient_id" value="<?php echo $id;?>">
                                                    <input type="text" id="projectinput1"
                                                           class="form-control"
                                                           name="patientrecoddateid"
                                                           value="<?php echo $patientrecoddateid;?>">
                                                    <input type="text" id="projectinput1"
                                                           class="form-control"
                                                           name="laboappoint_id"
                                                           value="<?php echo $laboappid;?>">
                                                    <input type="text" id="projectinput1"
                                                           class="form-control"
                                                           name="laboratorytype"
                                                           value="<?php echo $laboratorytype;?>" >

                                                    <input type="text" id="projectinput1"
                                                           class="form-control"
                                                           name="laboratorytestname"
                                                           value="<?php echo $laboratorytestname;?>" >

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for='projectinput1'>Laboratory Test Results</label>
                                                    <textarea name="laboratoryresults"
                                                              rows="15"
                                                              class="form-control" required></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for='projectinput1'>Normal Values</label>
                                                    <textarea name="normalvalues"
                                                              rows="5"
                                                              class="form-control"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-login">
                                            <i class="la la-check-square-o"></i> Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Labo Result</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Result</th>
                                                <th>Normal Values</th>
                                                <th>Date Created</th>
                                                <th>Done By</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listresult as $data)
                                                <tr>
                                                    <td>{{$data->laboratoryresults}}</td>
                                                    <td>{{$data->normalvalues}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>{{$data->name}}</td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#default{{$data->id}}">
                                                            Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="default{{$data->id}}"
                                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">
                                                                            Consultation Comment</h4>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('EditOtherResultsPatients') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="form-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12" hidden>
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Patient
                                                                                                ID</label>
                                                                                            <input type="text" id="projectinput1"
                                                                                                   class="form-control"
                                                                                                   name="id" value="{{$data->id}}">
                                                                                            <input type="text" id="projectinput1"
                                                                                                   class="form-control"
                                                                                                   name="patient_id" value="<?php echo $id;?>">
                                                                                            <input type="text" id="projectinput1"
                                                                                                   class="form-control"
                                                                                                   name="patientrecoddateid"
                                                                                                   value="<?php echo $patientrecoddateid;?>">
                                                                                            <input type="text" id="projectinput1"
                                                                                                   class="form-control"
                                                                                                   name="laboratorytype"
                                                                                                   value="<?php echo $laboratorytype;?>" hidden>

                                                                                            <input type="text" id="projectinput1"
                                                                                                   class="form-control"
                                                                                                   name="laboratorytestname"
                                                                                                   value="<?php echo $laboratorytestname;?>" hidden>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <textarea name="laboratoryresults"
                                                                                                      rows="15"
                                                                                                      class="form-control">{{$data->laboratoryresults}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for='projectinput1'>Normal Values</label>
                                                                                            <textarea name="normalvalues"
                                                                                                      rows="5"
                                                                                                      class="form-control">{{$data->normalvalues}}</textarea>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Save
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--<a href="{{ route('backend.EditPatient',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Edit</a>--}}
                                                    </td>

                                                    <td>
                                                        <a href="{{ route('backend.DeleteOtherResultsPatients',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
