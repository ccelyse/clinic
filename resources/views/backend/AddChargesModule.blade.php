<style>
    .select2{
        width: 100% !important;
    }
    #labodata{
        max-width: 900px !important;
    }
</style>
<div class="form-grouppop">
    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
            data-toggle="modal"
            data-target="#defaultcharges">
        Add Services
    </button>
    <!-- Modal -->
    <div class="modal fade text-left" id="defaultcharges"
         role="dialog" aria-labelledby="myModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 600px; !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Add Charges </h4>
                    <button type="button" class="close"  onclick="location.reload();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--<form class="form-horizontal form-simple" method="POST"--}}
                    {{--action="{{ url('AddPatientCharges') }}"--}}
                    {{--enctype="multipart/form-data" style="margin-bottom:15px;">--}}
                    {{--{{ csrf_field() }}--}}
                    <?php
                    $getpatientidcopay = \App\Patient::where('id',$id)->value('copay');
                    ?>
                    <div class="row  multi-field" style="margin-bottom:15px;">
                        <script type="text/javascript">

//                            function getLaboTestType(val) {
//                                $.ajax({
//                                    headers: {
//                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                                    },
//                                    type: "POST",
//                                    url: "PatientChargesNurse",
//                                    data: 'activitynameid=' + val,
//                                    success: function (data) {
//                                        $("#listofmedecinecharge").html(data);
//                                    }
//                                })
////
//                            }
                            $(document).on('change', '#country-list', function() {

                                var Insuranceidprice =$('#insurance_choice').val();
                                var activitynameid = $('#country-list').val();

//                               alert(activitynameid);

                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: "POST",
                                    url: "PatientChargesNurse",
                                    data: {
                                        'activitynameid': activitynameid,
                                        'Insuranceidprice': Insuranceidprice,

                                    },
                                    success: function (data) {
                                        $("#listofmedecinecharge").html(data);
                                    }
                                });
                            });

                            $(document).on('change', '#listofmedecinecharge', function() {

                                var Insuranceidprice =$('#insurance_choice').val();
                                var Medicine_Service = $('#listofmedecinecharge').val();
                                var activityname = $('#country-list').val();

//                                alert(Insuranceidprice);
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: "POST",
                                    url: "PatientChargeSelectMedicine",
                                    data: {
                                        'Medicine_Service': Medicine_Service,
                                        'Insuranceidprice': Insuranceidprice,
                                        'activityname': activityname,
                                    },
                                    success: function (data) {
                                        $("#fees").html(data);
                                    }
                                });
                            });
                            $(document).on('click', '#SaveCharges', function() {
                                var chargess = $('#charges').val();
                                if ( chargess == 'noprice'){
                                    alert('you can not add this medicine/service');
                                }else{
                                    var x=confirm( "Are you sure you want to add a new service?!");
                                    if(x){
                                        var activity_name = $('#country-list').val();
                                        var medecineservices = $('#listofmedecinecharge').val();
                                        var charges = $('#charges').val();
                                        var patient_id = $('#patient_id').val();
                                        var patientrecoddateid = $('#patientrecoddateid').val();
                                        var insurance_id = $('#insurance_choice').val();
                                        var copay = $('#copay').val();
                                        var numberofitems = $('#numberofitems').val();
                                        var stockitem = $('#stockitem').val();

                                        $.ajax({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            type: "POST",
                                            url: "AddPatientCharges",
                                            data: {
                                                'activity_name': activity_name,
                                                'medecineservices': medecineservices,
                                                'charges': charges,
                                                'patient_id': patient_id,
                                                'patientrecoddateid': patientrecoddateid,
                                                'insurance_id': insurance_id,
                                                'copay': copay,
                                                'numberofitems': numberofitems,
                                                'stockitem': stockitem

                                            },
                                            success: function (data) {
                                                $("#pricestables").html(data);
                                            }
                                        });
                                    }

                                }


                            });
                            //
                        </script>

                        <div class="col-md-12" hidden>
                            <div class="form-group">
                                <label for="projectinput1">Patient ID</label>
                                <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput1">Choose Insurance</label>

                                <select class="form-control" id="insurance_choice" name="insurance_choice" required>
                                    <option value=""></option>
                                    <option value="6">Private</option>
                                    <option value="<?php echo $insurance_id;?>">Current Insurance</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput1">Activity
                                    Name</label>

                                <select class="form-control" id="country-list" name="labotest_type" required>
                                    <option value=""></option>
                                    @foreach($departmentdata as $departmentdatas)
                                        <option value="{{$departmentdatas->id}}">{{$departmentdatas->departmentname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectinput1">Medicine/Service/Material
                                    Name</label><br>
                                <select class="form-control" id="listofmedecinecharge"
                                        name="Medicine_service_name"
                                        required>
                                    <option value="">Medicine/Service/Material</option>
                                </select>
                            </div>
                        </div>



                        <div id="fees" style="width: 100%;display: inline-flex;">

                        </div>
                        <input type="text" id="copay"  class="form-control" name="copay" value="<?php echo $getpatientidcopay;?>" hidden>
                        <input type="text" id="patient_id"
                               class="form-control"
                               name="patient_id" value="<?php echo $id;?>" hidden>

                        <input type="text" id="patientrecoddateid"
                               class="form-control"
                               name="patientrecoddateid"
                               value="<?php echo $patientrecoddateid;?>" hidden>


                        <div class="col-md-12">
                            <button class="btn btn-login" id="SaveCharges"><i class="la la-check-square-o"></i> Save</button>
                        </div>

                    </div>
                    {{--</form>--}}


                    <table class="table table-striped table-bordered zero-configuration table-responsive">
                        <thead>
                        <tr>
                            <th>Activity Name</th>
                            <th>Medicine/Service Name</th>
                            <th>Medicine/Service Price</th>

                        </tr>
                        </thead>
                        <tbody id="pricestables">
                        </tbody>



                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
<div class="form-grouppop">
    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
            data-toggle="modal"
            data-target="#clearchargers">
        Clear Chargers
    </button>
    <!-- Modal -->
    <div class="modal fade text-left" id="clearchargers" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Clear Chargers</h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-simple" method="POST"
                          action="{{ url('ClearAllCharges') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row  multi-field">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="date">Clear all Chargers</label>
                                    <select class="form-control" name="patient_status" required>
                                        <option value=""></option>
                                        <option value="not sure">not sure</option>
                                        <option value="clear">clear all charges</option>
                                    </select>
                                </div>
                            </div>


                            <input type="text" id="patient_id"
                                   class="form-control"
                                   name="patient_id" value="<?php echo $id;?>" hidden>
                            <input type="text" id="patientrecoddateid"
                                   class="form-control"
                                   name="patientrecoddateid"
                                   value="<?php echo $patientrecoddateid;?>" hidden>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-login">
                                    <i class="la la-check-square-o"></i> Save
                                </button>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

