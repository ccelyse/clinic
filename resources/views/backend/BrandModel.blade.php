@extends('backend.layout.master')

@section('title', 'Otobox')

@section('content')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/ui/jquery-ui.min.css">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('BrandModel_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Brand Name</label>

                                                            <select class="select2 form-control" name="brandname_id">
                                                                @foreach($listbrand as $data)
                                                                    <option value="{{$data->id}}">{{$data->brandname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Brand Model</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Brand Name"
                                                                   name="brandmodel">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Year  Manufactured</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="2018" name="brandmodelmanufactured" value="{{ old('dateofbirth') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group{{ $errors->has('fileToUpload') ? ' has-error' : '' }}">
                                                            <label for="projectinput8">Brand Model Picture</label>
                                                            <input type="file" class="form-control-file" id="exampleInputFile" name="fileToUpload" value="{{ old('fileToUpload') }}"  required>
                                                            @if ($errors->has('fileToUpload'))
                                                                <span class="help-block">
                                                                  <strong>{{ $errors->first('fileToUpload') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection