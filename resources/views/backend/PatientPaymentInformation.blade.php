<section id="complex-header" style="width: 100%">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Patient Information</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($NursePatient as $datas)

                                <li class="list-group-item"><strong>Patient
                                        Names:</strong> {{$datas->beneficiary_name}}</li>
                                <li class="list-group-item"><strong>Patient Date of Birth
                                        Id:</strong> {{$datas->dateofbirth}}</li>

                                <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                <li class="list-group-item"><strong>Patient
                                        Number :</strong> {{$datas->beneficiary_telephone_number}}</li>
                                <?php
                                $insurancename = $datas->insurance_name;
                                $insuranceid = $datas->insurance_patient_name;
                                $copay = $datas->copay;
                                ?>

                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Payment Information</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body">
                        <ul class="list-group">
                            <?php
                            $insurancedue = \App\PatientCharges::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                                ->value('insurance_amount');
                            $insurancedueprivate = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                                ->value('insurance_amount');

                            $patientdue = \App\PatientCharges::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                                ->value('patient_amount');

                            $patientdueprivate = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                                ->value('patient_amount');

                            $listpatientprice = \App\PatientCharges::whereNotIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                                ->value('producttotalsum');

                            $listpatientpriceprivate = \App\PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                                ->value('producttotalsum');

                            $getcopay = \App\Patient::where('id',$id)->value('copay');
                            $calcopay = 100 - $getcopay;

                            $secinsurancedue = ($listpatientprice * $getcopay)/100;
                            $secpatientdue = ($listpatientprice * $calcopay)/100;

                            $secRemainingrprivate = \App\PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                                ->value('producttotalsum');

                            $amount_paid = \App\AccountPatientClear::whereIn('insurance_id',[6])->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                ->value('amount_paid');

                            $secremtotal =  $secRemainingrprivate + $secpatientdue;

                            ?>

                            @if($insuranceid == '6')
                                <li class="list-group-item"><strong>Insurance Due:</strong> <?php echo "$insurancedueprivate"; ?> FRW</li>
                                <li class="list-group-item"><strong>Patient Due:</strong> <?php echo "$patientdueprivate"; ?> FRW</li>
                                <li class="list-group-item"><strong>Paid Amount:</strong> <?php echo "$amount_paid";?> FRW</li>
                            @else
                                <li class="list-group-item"><strong>Insurance Due:</strong> <?php echo "$secinsurancedue"; ?> FRW</li>
                                <li class="list-group-item"><strong>Patient Due:</strong> <?php echo "$secremtotal"; ?> FRW</li>
                                <li class="list-group-item"><strong>Paid Amount:</strong> <?php echo "$patient_amountpayed";?> FRW</li>
                            @endif

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>