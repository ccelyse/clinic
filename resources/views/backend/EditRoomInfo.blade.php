@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditRoomInfo_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($listroom as $data)
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Level</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->roomlevel}}"
                                                                   name="roomlevel">
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                   name="id" hidden>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Number</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->roomnumber}}"
                                                                   name="roomnumber">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                            @endforeach
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection