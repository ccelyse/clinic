@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .warning{
            color: #103e5f !important;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script>

        $(document).ready(function () {
            var patientjan = <?php echo $patient_jan; ?>;
            var patientfeb = <?php echo $patient_feb; ?>;
            var patientmarch = <?php echo $patient_marc; ?>;
            var patientapril = <?php echo $patient_apr; ?>;
            var patientmay = <?php echo $patient_may; ?>;
            var patientjun = <?php echo $patient_jun; ?>;
            var patientjuly = <?php echo $patient_jul; ?>;
            var patientaugust = <?php echo $patient_aug; ?>;
            var patientseptember = <?php echo $patient_sept; ?>;
            var patientoctober = <?php echo $patient_oct; ?>;
            var patientnov = <?php echo $patient_nov; ?>;
            var patientdec = <?php echo $patient_dec; ?>;


            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Average Patient'
                },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Patient Number'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Monthly Average Patient',
                    data: [patientjan, patientfeb, patientmarch,patientapril,patientmay,patientjun,patientjuly,patientaugust,patientseptember,patientoctober,patientnov,patientdec]
                }]
            });
        });
    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div id="crypto-stats-3" class="row">
                    <div class="col-xl-4 col-12">
                        <div class="card crypto-card-3 pull-up">
                            <div class="card-content">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-2">
                                            <h1><i class="fas fa-thumbs-up warning font-large-1" title="BTC"></i></h1>
                                        </div>
                                        <div class="col-10 pl-2">
                                            <h4>Appointed Patient Number</h4>
                                        </div>
                                        <div class="col-10 text-right">
                                            <?php echo $patientnumber;?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <canvas id="btc-chartjs" class="height-75"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-12">
                        <div class="card crypto-card-3 pull-up">
                            <div class="card-content">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-2">
                                            <h1><i class="fas fa-book warning font-large-1" title="BTC"></i></h1>
                                        </div>
                                        <div class="col-10 pl-2">
                                            <h4>Pending Appointment</h4>
                                        </div>
                                        <div class="col-10 text-right">
                                            <?php echo $patientpending; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <canvas id="btc-chartjs" class="height-75"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="container" style="width:100%; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection