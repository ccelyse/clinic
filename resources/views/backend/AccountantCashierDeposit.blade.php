@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        function FillBilling(f) {
            if(f.billingtoo.selected == 'Private') {
                f.billingname.value = f.shippingname.value;
                f.billingcity.value = f.shippingcity.value;
            }
        }
    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="basic-form-layouts">
                        <div class="row match-height">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">

                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('AccountantCashierDeposit_') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="form-body">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Amount</label>
                                                                <input id="projectinput1" class="form-control"
                                                                       name="deposit_amount" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                       type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Date of Deposit</label>
                                                                <input type="date" class="form-control" id="date" name="deposit_date"  required>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">

                                                        <div class='col-md-6'>
                                                            <div class='form-group'>
                                                                <label for='projectinput1'>Deposit Comment</label>
                                                                <textarea name='depositcomment'
                                                                          rows='5'
                                                                          class='form-control'></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-login">
                                                            <i class="la la-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>
                    <!-- // Basic form layout section end -->
                    <section id="complex-header">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Deposit List</h4>
                                        <a class="heading-elements-toggle"><i
                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Deposit Amount</th>
                                                    <th>Deposit Date</th>
                                                    <th>Deposit Comment</th>
                                                    <th>Deposited By</th>
                                                    <th>Edit</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listdeposit as $data)
                                                    <tr>
                                                        <td>{{$data->deposit_amount}}</td>
                                                        <td>{{$data->deposit_date}}</td>
                                                        <td>{{$data->depositcomment}}</td>
                                                        <td>{{$data->name}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#{{$data->id}}editdeposit">
                                                                Edit Deposit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="{{$data->id}}editdeposit" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document" style="max-width: 600px; !important;">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Edit Deposit</h4>
                                                                            <button type="button" class="close"  onclick="location.reload();">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>

                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST"
                                                                            action="{{ url('AccountantCashierDepositEdit') }}"
                                                                            enctype="multipart/form-data" style="margin-bottom:15px;">
                                                                            {{ csrf_field() }}
                                                                            <div class="row">

                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Amount</label>
                                                                                        <input id="projectinput1" class="form-control" value="{{$data->deposit_amount}}"
                                                                                               name="deposit_amount" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                                               type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Date of Deposit</label>
                                                                                        <input type="date" class="form-control" id="date" name="deposit_date" value="{{$data->deposit_date}}"  required>
                                                                                        <input type="text" class="form-control" id="id" name="id"  value="{{$data->id}}" required hidden>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row">

                                                                                <div class='col-md-12'>
                                                                                    <div class='form-group'>
                                                                                        <label for='projectinput1'>Deposit Comment</label>
                                                                                        <textarea name='depositcomment'
                                                                                                  rows='5'
                                                                                                  class='form-control'>{{$data->depositcomment}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <button class="btn btn-login" id="SaveCharges"><i class="la la-check-square-o"></i> Save</button>
                                                                                </div>
                                                                            </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="content-body">

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
