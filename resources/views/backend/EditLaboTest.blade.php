@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditLaboTest_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($listLabo as $datas)
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Test Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->labotest_name}}"
                                                                   name="labotest_name" required>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->labotest_id}}"
                                                                   name="laboid" required hidden>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Department Name</label>
                                                            <select class="select2 form-control" multiple="multiple" id="id_h5_multi" name="departmentid" required>
                                                                <option value="{{$datas->departmentid}}" selected>{{$datas->departmentname}}</option>
                                                                @foreach($listdepr as $data)
                                                                    <option value="{{$data->id}}">{{$data->departmentname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Insurance  Name</label>
                                                            <select class="select2 form-control" multiple="multiple" id="id_h5_multi" name="insurance_labo">
                                                                <option value="{{$datas->insuranceid}}" selected>{{$datas->insurance_name}}</option>
                                                                @foreach($listinsu as $data)
                                                                    <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Laboratory  Price</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->laboratoryprice}}"
                                                                   name="laboprice">
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->id}}"
                                                                   name="laboprice_id" hidden>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            @endforeach
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection