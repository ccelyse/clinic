@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }

        .main-menu.menu-light .navigation>li.open>a {
            color: #545766;
            background: #f5f5f5;
            border-right: 4px solid #6b442b !important;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">

                <section id="complex-header">
                    <div class="row">
                        <form
                                method="POST"
                                action="{{ url('StockManagementFilter') }}"
                                enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date">From Date</label>
                                        <input type="date" class="form-control" id="date" name="fromdate"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date">To Date</label>
                                        <input type="date" class="form-control" id="date" name="todate"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: 10px">
                                        <button type="submit" class="btn btn-login">
                                            <i class="la la-check-square-o"></i> Filter
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Stock Management</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Product Package</th>
                                                <th>Product Piece/Test</th>
                                                <th>Remaining Product Piece/Test in Stock</th>
                                                <th>Remaining in Labo</th>
                                                <th>Used Piece/Test</th>
                                                <th>Total Remaining Stock Piece/Test</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listmedicine as $data)
                                                <tr>

                                                    <td>{{$data->productname}}</td>
                                                    <td>{{$data->productpackage}}</td>
                                                    <td>{{$data->productpiece}}</td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        $ogpiece = $data->productpiece;
                                                        $remainingpiece = $ogpiece - $viewmore_sum;
                                                        echo $remainingpiece;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('shipmentdepartment','Laboratory')
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        echo $viewmore_sum;
                                                        ?>

                                                    </td>

                                                    <td>
                                                       <?php
                                                        $ogpiece = $data->productpiece;
                                                        $remainingog = $data->remainingpiece;

                                                        $ogrem = $ogpiece - $remainingog;

                                                        echo $ogrem;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        @if($data->remainingpiece < '10')
                                                            <span class="alert bg-danger">
                                                          {{$data->remainingpiece}}
                                                             </span>
                                                        @else
                                                            {{$data->remainingpiece}}
                                                        @endif

                                                    </td>
                                                    <td>{{$data->created_at}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>

@endsection
