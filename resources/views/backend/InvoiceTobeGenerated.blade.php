<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

    <title></title>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style>

        * {
            margin: 0;
            padding: 0;
        }

        body {
            font: 14px/1.4 Helvetica, Arial, sans-serif;
        }

        #page-wrap {
            width: 600px;
            margin: 0 auto;
            padding-bottom: 15px;
        }

        textarea {
            border: 0;
            font: 14px Helvetica, Arial, sans-serif;
            overflow: hidden;
            resize: none;
        }

        table {
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid black;
            padding: 5px;
        }

        #header {
            height: 15px;
            width: 100%;
            margin: 20px 0;
            background: #222;
            text-align: center;
            color: white;
            font: bold 15px Helvetica, Sans-Serif;
            text-decoration: uppercase;
            letter-spacing: 20px;
            padding: 8px 0px;
        }

        #address {
            width: 250px;
            height: 150px;
            float: left;
        }

        #customer {
            overflow: hidden;
        }

        #logo {
            text-align: right;
            float: right;
            position: relative;
            margin-top: 25px;
            border: 1px solid #fff;
            max-width: 540px;
            overflow: hidden;
        }

        #customer-title {
            font-size: 20px;
            font-weight: bold;
            float: left;
        }

        #meta {
            margin-top: 1px;
            width: 100%;
            float: right;
        }

        #meta td {
            text-align: right;
        }

        #meta td.meta-head {
            text-align: left;
            background: #eee;
        }

        #meta td textarea {
            width: 100%;
            height: 20px;
            text-align: right;
        }

        #items {
            clear: both;
            width: 100%;
            margin: 30px 0 0 0;
            border: 1px solid black;
        }

        #items th {
            background: #eee;
        }

        #items textarea {
            width: 80px;
            height: 50px;
        }

        #items tr.item-row td {
            vertical-align: top;
        }

        #items td.description {
            width: 300px;
        }

        #items td.item-name {
            width: 175px;
        }

        #items td.description textarea, #items td.item-name textarea {
            width: 100%;
        }

        #items td.total-line {
            border-right: 0;
            text-align: left;
        }

        #items td.total-value {
            border-left: 0;
            padding: 10px;
        }

        #items td.total-value textarea {
            height: 20px;
            background: none;
        }

        #items td.balance {
            background: #eee;
        }

        #items td.blank {
            border: 0;
        }

        #terms {
            text-align: center;
            margin: 20px 0 0 0;
        }

        #terms h5 {
            text-transform: uppercase;
            font: 13px Helvetica, Sans-Serif;
            letter-spacing: 10px;
            border-bottom: 1px solid black;
            padding: 0 0 8px 0;
            margin: 0 0 8px 0;
        }

        #terms textarea {
            width: 100%;
            text-align: center;
        }

        .delete-wpr {
            position: relative;
        }

        .delete {
            display: block;
            color: #000;
            text-decoration: none;
            position: absolute;
            background: #EEEEEE;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: -6px;
            left: -22px;
            font-family: Verdana;
            font-size: 12px;
        }
        .text-center{
            text-align: center;
        }
        .text-success{
            color:
                    #f7921a ;
        }
        .borderleftnone{
            border-right: none;
            border-left: none;
        }
    </style>

</head>

<body>

<div id="page-wrap">

    <table width="100%">
        <tr>
            <td style="border: 0;  text-align: left" width="62%">
                <span style="font-size: 18px; color: #2f4f4f"><strong></strong></span><br><p><br><p>
                    <img src="backend/app-assets/images/logoclinic.png" alt="Logo" width="10%"><br><br>
                    <strong>Ndengera Clinic</strong><br>
                    <strong>+250788422169</strong><br><br>
                    @foreach($patientinformation  as $PI)
                    Beneficiary Name: {{$PI->beneficiary_name}} <br>
                    PID: {{$PI->pid}} <br>
                    Telephone: {{$PI->patient_telephone_number}}  <br>
                    <?php
                    $insurancename = $PI->insurance_name;
                    $copay = $PI->copay;
                    ?><br><br>
                    @endforeach
                    Invoice Date: <?php  echo $dt; ?><br>
                    @foreach($invoicedetails as $invoicedetail)
                        Payment Method: {{$invoicedetail->paymentmethod}} <br>
                        Amount Paid: {{$invoicedetail->amount_paid}} FRW <br>
                        Payment Status: {{$invoicedetail->paymentcomment}} <br>
                        Cashier: {{$invoicedetail->name}}
                    @endforeach

        </tr>

    </table>

    <div style="clear:both"></div>
    @if($patientinsurance == '6')
        <table id="items">
            <tr>
                <th>Activity Type</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                {{--<th>Insurance Due</th>--}}
                {{--<th>Patient Due</th>--}}
                <th>Total</th>

            </tr>

            @foreach($patientchargesprivate as $items)
                <tr class="item-row">
                    <td>{{$items->departmentname}}</td>
                    <td>{{$items->medicinename}}</td>
                    <td>{{$items->numberofitems}}</td>
                    <td>{{$items->unitprice}} FRW</td>
                    {{--<td>{{$items->insurance_amount}} FRW</td>--}}
                    {{--<td>{{$items->patient_amount}} FRW</td>--}}
                    <td>
                        <?php
                        $quant = $items->numberofitems;
                        $unit = $items->unitprice;
                        $tot = $quant * $unit;
                        echo $tot;
                        ?>FRW</td>
                </tr>
            @endforeach


            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-line">Subtotal</td>
                <td class="blank"></td>
                <td class="blank"></td>
                <td class="blank"></td>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-value">
                    <div id="subtotal">
                        @foreach($listpatientpriceprivate as $subtotal)
                            {{$subtotal->producttotalsum}} RWF
                            <?php $subtotal = $subtotal->producttotalsum;?>
                        @endforeach
                    </div>
                </td>
            </tr>
            <?php
            $insurancedue = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                ->value('insurance_amount');
//
            $patientdue = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                ->value('patient_amount');

            $secRemainingrprivate = \App\PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                ->value('producttotalsum');

            $secremtotal =  $secRemainingrprivate;

            $amount_paid = \App\AccountPatientClear::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_paid');
            ?>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-line">Insurance Due</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div id="subtotal">
                        <?php echo "$insurancedue RWF";
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank text-center text-success">--}}
                {{--<h1>@foreach($invoicedetails as $paymentcomment){{$paymentcomment->paymentcomment}}@endforeach</h1>--}}
                {{--</td>--}}
                <td class="total-line">Patient Due</td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div id="total"><?php echo  "$patientdue FRW" ?></div>
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td  class="total-line">Amount Paid</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div class="due"><?php echo  "$amount_paid" ?> FRW</div>
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td  class="total-line">Remaining Balance</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div class="due"><?php echo  "$secRemainingrprivate" ?> FRW</div>
                </td>
            </tr>


        </table>
        <div style="clear:both; padding: 10px;"></div>
    @else
        <table id="items">
            <h5>Insurance</h5>
            <tr>
                <th>Activity Type</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                {{--<th>Insurance Due</th>--}}
                {{--<th>Patient Due</th>--}}
                <th>Total</th>

            </tr>

            @foreach($patientcharges as $items)
                <tr class="item-row">
                    <td>{{$items->departmentname}}</td>
                    <td>{{$items->medicinename}}</td>
                    <td>{{$items->numberofitems}}</td>
                    <td>{{$items->unitprice}} FRW</td>
                    {{--<td>{{$items->insurance_amount}} FRW</td>--}}
                    {{--<td>{{$items->patient_amount}} FRW</td>--}}
                    <td>
                        <?php
                        $quant = $items->numberofitems;
                        $unit = $items->unitprice;
                        $tot = $quant * $unit;
                        echo $tot;
                        ?>FRW</td>
                </tr>
            @endforeach


            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-line">Subtotal</td>
                <td class="blank"></td>
                <td class="blank"></td>
                <td class="blank"></td>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-value">
                    <div id="subtotal">
                        @foreach($listpatientprice as $subtotal)
                            {{$subtotal->producttotalsum}} RWF
                            <?php $subtotal = $subtotal->producttotalsum;?>
                        @endforeach
                    </div>
                </td>
            </tr>
            <?php
            $insurancedue = \App\PatientCharges::whereNotIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                ->value('insurance_amount');
            $getcopay = \App\Patient::where('id',$patient_id)->value('copay');
            $calcopay = 100 - $getcopay;

            $secinsurancedue = ($subtotal * $getcopay)/100;
            $secpatientdue = ($subtotal * $calcopay)/100;


            $patientdue = \App\PatientCharges::whereNotIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                ->value('patient_amount');

            $Remaining = \App\AccountPatientClear::whereNotIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_due');

            $secRemainingrprivate = \App\PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                ->value('producttotalsum');

            $secremtotal =  $secRemainingrprivate + $secpatientdue;

            $amount_paid = \App\AccountPatientClear::whereNotIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_paid');
            ?>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-line">Insurance Due</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div id="subtotal">
                        <?php echo "$secinsurancedue RWF";
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank text-center text-success">--}}
                {{--<h1>@foreach($invoicedetails as $paymentcomment){{$paymentcomment->paymentcomment}}@endforeach</h1>--}}
                {{--</td>--}}
                <td class="total-line">Patient Due</td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div id="total"><?php echo  "$secpatientdue FRW" ?></div>
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td  class="total-line">Amount Paid</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div class="due"><?php echo  "$amount_paid" ?> FRW</div>
                </td>
            </tr>



        </table>
        <div style="clear:both; padding: 10px;"></div>

        <table id="items">
            <h5>Private</h5>
            <tr>
                <th>Activity Type</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                {{--<th>Insurance Due</th>--}}
                {{--<th>Patient Due</th>--}}
                <th>Total</th>
            </tr>


            @foreach($patientchargesprivate as $items)
                <tr class="item-row">
                    <td>{{$items->departmentname}}</td>
                    <td>{{$items->medicinename}}</td>
                    <td>{{$items->numberofitems}}</td>
                    <td>{{$items->unitprice}} FRW</td>
                    {{--<td>{{$items->insurance_amount}} FRW</td>--}}
                    {{--<td>{{$items->patient_amount}} FRW</td>--}}
                    <td>
                        <?php
                        $quant = $items->numberofitems;
                        $unit = $items->unitprice;
                        $tot = $quant * $unit;
                        echo $tot;
                        ?>FRW</td>
                </tr>
            @endforeach


            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="total-line">Subtotal</td>
                <td class="blank"></td>
                <td class="blank"></td>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td class="blank"></td>
                <td class="total-value">
                    <div id="subtotal">
                        @foreach($listpatientpriceprivate as $subtotal)
                            {{$subtotal->producttotalsum}} RWF
                            <?php $subtotal = $subtotal->producttotalsum;?>
                        @endforeach
                    </div>
                </td>
            </tr>
            <?php
            $insurancedue = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                ->value('insurance_amount');
            $patientdue = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                ->value('patient_amount');

            $Remaining = \App\AccountPatientClear::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_due');
            $amount_paid = \App\AccountPatientClear::whereIn('insurance_id',[6])->where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_paid');
            ?>

            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank text-center text-success">--}}
                {{--<h1>@foreach($invoicedetails as $paymentcomment){{$paymentcomment->paymentcomment}}@endforeach</h1>--}}
                {{--</td>--}}
                <td class="total-line">Patient Due</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="total-value">
                    <div id="total"><?php echo  "$patientdue FRW" ?></div>
                </td>
            </tr>
        </table>
        <table id="items">
            <h5 style="padding-top: 15px">Total </h5>
            <?php

//            $totalRemainingrprivate = \App\PatientCharges::where('patientcharges.patient_id',$patient_id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
//                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
//                ->value('producttotalsum');
            $totalRemainingrprivate = \App\AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_due');

            $totalamount_paid = \App\AccountPatientClear::where('patient_id',$patient_id)->where('patientrecoddateid',$patientrecoddateid)
                ->value('amount_paid');
            ?>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td  class="total-line">Subtotal</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div class="due" style="float: right;">
                        @foreach($listpatientpricetotal as $subtotal)
                            {{$subtotal->producttotalsum}} RWF
                            <?php $subtotal = $subtotal->producttotalsum;?>
                        @endforeach
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td  class="total-line">Amount Paid</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div class="due" style="float: right;"><?php echo  "$totalamount_paid" ?> FRW</div>
                </td>
            </tr>
            <tr>
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                {{--<td class="blank"></td>--}}
                <td  class="total-line">Remaining Balance</td>
                <td class="borderleftnone"></td>
                <td class="borderleftnone"></td>
                {{--<td class="borderleftnone"></td>--}}
                {{--<td class="borderleftnone"></td>--}}
                <td class="borderleftnone"></td>
                <td class="total-value">
                    <div class="due" style="float: right;"><?php echo  "$totalRemainingrprivate" ?> FRW</div>
                </td>
            </tr>


        </table>
        <div style="clear:both; padding: 10px;"></div>
        <table width="100%">
            <tr>
                <td style="border: 0;  text-align: center; font-size: 18px;" width="62%">
                    Thank you / Murakoze!
                </td>
            </tr>

        </table>
    @endif



    {{--@if($inv->note!='')--}}
        {{--<div id="terms">--}}
            {{--<h5>{{language_data('Invoice Note')}}</h5>--}}
            {{--{{$inv->note}}--}}
        {{--</div>--}}
    {{--@endif--}}


</div>

</body>

</html>