@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>
    </script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body"><!-- HTML5 export buttons table -->
                <section id="html5">
                    <div class="row">

                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Product Package</th>
                                                <th>Product Piece/Test</th>
                                                <th>Remaining Product Piece/Test in Stock</th>
                                                <th>Remaining in Labo</th>
                                                <th>Used Piece/Test</th>
                                                <th>Total Remaining Stock Piece/Test</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listmedicine as $data)
                                                <tr>
                                                    <td>{{$data->productname}}</td>
                                                    <td>{{$data->productpackage}}</td>
                                                    <td>{{$data->productpiece}}</td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        $ogpiece = $data->productpiece;
                                                        $remainingpiece = $ogpiece - $viewmore_sum;
                                                        echo $remainingpiece;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('shipmentdepartment','Laboratory')
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        echo $viewmore_sum;
                                                        ?>

                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ogpiece = $data->productpiece;
                                                        $remainingog = $data->remainingpiece;

                                                        $ogrem = $ogpiece - $remainingog;

                                                        echo $ogrem;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        @if($data->remainingpiece < '10')
                                                            <span class="alert bg-danger">
                                                          {{$data->remainingpiece}}
                                                             </span>
                                                        @else
                                                            {{$data->remainingpiece}}
                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection