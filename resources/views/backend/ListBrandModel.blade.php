@extends('backend.layout.master')

@section('title', 'Otobox')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Complex headers with column visibility table -->

                <section id="setting">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Brands List</h4>
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered setting-defaults">
                                                <thead>
                                                <tr>
                                                    <th>Brand Name</th>
                                                    <th>Brand Logo</th>
                                                    <th>Brand Model</th>
                                                    <th>Brand Date Manufactured</th>
                                                    <th>Brand Model Picture</th>
                                                    <th>Date Added</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listmodel as $data)
                                                    <tr>
                                                        <td>{{$data->brandname}}</td>
                                                        <td><img src="images/{{$data->brandlogo}}" style="width:70%"></td>
                                                        <td>{{$data->brandmodel}}</td>
                                                        <td>{{$data->brandmodelmanufactured}}</td>
                                                        <td><img src="images/{{$data->brandmodelpic}}" style="width:70%"></td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td><a href="{{ route('backend.EditBrandModel',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i></i> Edit</a></td>
                                                        <td><a href="{{ route('backend.DeleteBrandModel',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
@endsection
