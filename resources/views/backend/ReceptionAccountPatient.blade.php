@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($NursePatient as $datas)
                                                <li class="list-group-item"><strong>Patient
                                                        Names:</strong> {{$datas->patient_names}}</li>
                                                <li class="list-group-item"><strong>Patient Country
                                                        Id:</strong> {{$datas->patient_country_id}}</li>
                                                <li class="list-group-item"><strong>Patient Insurance Name
                                                        :</strong> {{$datas->insurance_name}}</li>
                                                <li class="list-group-item"><strong>Patient Gender
                                                        :</strong> {{$datas->gender}}</li>
                                                <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                                <li class="list-group-item"><strong>COPAY :</strong> {{$datas->copay}}%</li>
                                                <?php
                                                $copay = $datas->copay;
                                                ?>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Charges</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">

                                    @foreach($listpatientcharges as $datas)
                                        <div class="card-body">
                                            <?php
                                            $status = $datas->payment_status;
                                            if ($status == "Pending"){
                                                echo "<button type='button' class='btn btn-danger btn-min-width btn-glow mr-1 mb-1'>Payment status: {$datas->payment_status}</button>";
                                            }else{
                                                echo "<button type='button' class='btn btn-success btn-min-width btn-glow mr-1 mb-1'>Payment status: {$datas->payment_status}</button>";
                                            }
                                            ?>
                                        </div>
                                        <ul class="list-group">

                                            <li class="list-group-item"><strong>Amount:</strong> {{$datas->medicinepriceinsurance}} FRW</li>
                                            <li class="list-group-item"><strong>Amount with Insurance:</strong>
                                                <?php
                                                $amount  =  $datas->medicinepriceinsurance;
                                                $amountcopay = 100-$copay;
                                                $amountcopay_with = $amount*$amountcopay;
                                                $amountcopaypercentage = $amountcopay_with/100;
                                                echo "$amountcopaypercentage FRW"
                                                ?>
                                            </li>
                                            {{--<li class="list-group-item"><strong>Patient Country--}}
                                            {{--Id:</strong> {{$datas->patient_country_id}}</li>--}}
                                            {{--<li class="list-group-item"><strong>Patient Insurance Name--}}
                                            {{--:</strong> {{$datas->insurance_name}}</li>--}}
                                            {{--<li class="list-group-item"><strong>Patient Gender--}}
                                            {{--:</strong> {{$datas->gender}}</li>--}}
                                            {{--<li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>--}}

                                        </ul>

                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Charges</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Insurance Name</th>
                                                <th>Medicine/Service Price</th>
                                                <th>Date Created</th>
                                                <th>Service/Medicine/Laboratory Test</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listpatientcharges as $data)
                                                <tr>
                                                    <td>{{$data->insurance_name}}</td>
                                                    <td>{{$data->medicinepriceinsurance}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>{{$data->medicinename}}</td></td>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
