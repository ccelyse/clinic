@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">

                <section id="complex-header">
                    <div class="row">
                        <form
                                method="POST"
                                action="{{ url('HospitalizeReportFilter') }}"
                                enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="projectinput1">Room Level</label>
                                        <select class="form-control" id="roomlevel"
                                                name="roomlevel">
                                            <option value="All">All</option>
                                            <option value="Vip">Vip</option>
                                            <option value="Normal">Normal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="projectinput1">Hospitalization Status</label>
                                        <select class="form-control" id="roomlevel"
                                                name="hospitalizationstatus">
                                            <option value="All">All</option>
                                            <option value="Check In">Check In</option>
                                            <option value="Check Out">Check Out</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date">From Date</label>
                                        <input type="date" class="form-control" id="date" name="fromdate"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date">To Date</label>
                                        <input type="date" class="form-control" id="date" name="todate"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: 10px">
                                        <button type="submit" class="btn btn-login">
                                            <i class="la la-check-square-o"></i> Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Hospitalization</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Patient Name</th>
                                                <th>Hospitalization Status</th>
                                                <th>Days</th>
                                                <th>Room Number</th>
                                                <th>Room Level</th>
                                                <th>Room Price</th>
                                                <th>Total</th>
                                                <th>Date Created</th>
                                                {{--<th>Delete</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listHospitalizePatient as $data)
                                                <tr>
                                                    <td>{{$data->beneficiary_name}}</td>
                                                    <td>{{$data->hospitilazestatus}}</td>
                                                    <td>
                                                        <?php
                                                        $datein = strtotime($data->created_at);
                                                        $dateout = strtotime($data->updated_at);
                                                        $datediff = $dateout - $datein;

                                                        echo round($datediff / (60 * 60 * 24));
                                                        ?>
                                                    </td>
                                                    <td>{{$data->patient_roomnumber}}</td>
                                                    <td>{{$data->patient_roomlevel}}</td>
                                                    <td>{{$data->patient_roomprice}}</td>
                                                    <td>
                                                        <?php
                                                        $datein = strtotime($data->created_at);
                                                        $dateout = strtotime($data->updated_at);
                                                        $datediff = $dateout - $datein;
                                                        $price = $data->patient_roomprice;

                                                        if($datediff == 0){
                                                            $total = 1 * $price;
                                                            echo $total;
                                                        }else{
                                                            $total_ = round($datediff / (60 * 60 * 24)) * $price;
                                                            echo $total_ ;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>{{$data->created_at}}</td>
                                                    {{--<td>--}}
                                                        {{--<a href="{{ route('backend.DeleteHospitalizePatient',['id'=> $data->id])}}"--}}
                                                           {{--class="btn btn-login btn-min-width mr-1 mb-1"><i--}}
                                                                    {{--class="fas fa-trash"></i> Remove</a></td>--}}
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
