<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <?php
            $user_id = \Auth::user()->id;
            $Userrole = \App\User::where('id',$user_id)->value('role');
            $recurldash = url('ReceptionDashboard');
            $recurl = url('Reception');
            $insuranceverification = url('InsuranceVerification');

            $adminurl = url('Dashboard');
            $accountant = url('Accountant');
            $accountantsales = url('AccountantSales');
            $CreateAccount = url('CreateAccount');
            $AccountList = url('AccountList');
            $AddInsurance = url('AddInsurance');
            $AddDepartment = url('AddDepartment');
            $AddMedicines = url('AddMedicines');
            $AddService = url('AddService');
            $LaboratoryTest = url('LaboratoryTest');
            $LaboratoryAllPatient = url('LaboratoryAllPatient');
            $Rooms = url('Rooms');
            $RoomPrice = url('RoomPrice');

            $Nurse = url('Nurse');
            $NurseDashboard = url('NurseDashboard');

            $NurseConsultation = url('NurseConsultation');
            $Operations = url('PatientOperations');


            $LaboratoryDashboard = url('LaboratoryDashboard');
            $LaboratoryTech = url('LaboratoryTech');
            $HospitalizationDashboard = url('HospitalizationDashboard');
            $Hospitalize = url('Hospitalize');

            $DoctorDashboard = url('DoctorDashboard');
            $DoctorOperations = url('DoctorOperations');
            $Doctor = url('Doctor');
            $DoctorConsultation = url('DoctorConsultation');

            $DentistConsultation = url('DentistConsultation');

            $accountant = url('ReceptionAccountant');
            $AccountantPatientPay = url('AccountantPatientPay');
            $accountantpatientlist = url('Accountant');
            $AccountantDashboard = url('AccountantDashboard');
            $accountantsales = url('AccountantSales');
            $HospitalizeSales = url('HospitalizeSales');
            $PayedSales = url('PayedSales');
            $HospitalizeReport = url('HospitalizeReport');

            $StockManagement = url('StockManagement');
            $Appointment = url('ReceptionAppointment');
            $InsuranceReport = url('AccountantInsuranceReport');
            $AccountantCashierSales = url('AccountantCashierSales');
            $CommunicationSendSms = url('CommunicationSendSms');
            $SmsList = url('SmsList');
            $AccountantCashierDeposit = url('AccountantCashierDeposit');
            $AccountCashierDepositReport = url('AccountCashierDepositReport');
            $DoctorLaboratoryResults = url('DoctorLaboratoryResults');
            $LaboratoryReport = url('LaboratoryReport');
            $DoctorsReport = url('DoctorsReport');
            $ClinicPatientProfile = url('ClinicPatientProfile');
            $AccountantExpenses = url('AccountantExpenses');
            $AccountantExpensesReport = url('AccountantExpensesReport');
            $ClinicPatientProfile = url('ClinicPatientProfile');
            $DepartmentStockManagement = url('DepartmentStockManagement');
            $MedicalReport = url('MedicalReport');
            $StockManagementReport = url('StockManagementReport');
            $StockManagementRequisition = url('StockManagementRequisition');
            $ListServices = url('ListServices');
            $ListReception = url('ListReception');
            $ListReceptionReport = url('ListReceptionReport');


            switch ($Userrole) {
                case "Admin":
                    echo "<li class='nav-item'><a href='$adminurl'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo"<li class=' nav-item'><a href='#'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.templates.main'>Users</span></a><ul class='menu-content'><li class=' nav-item'><a href='$CreateAccount'><i class='fas fa-users'></i><span class='menu-title'>Create Account</span></a></li><li class=' nav-item'><a href='$AccountList'><i class='fas fa-user-circle'></i><span class='menu-title'>Account List</span></a></li></ul></li><li class=' nav-item'><a href='$AddInsurance'><i class='fas fa-thumbs-up'></i><span class='menu-title'>Insurance</span></a></li><li class=' nav-item'><a href='$AddDepartment'><i class='fas fa-book'></i><span class='menu-title'>Activity</span></a></li><li class=' nav-item'><a href='$LaboratoryTest'><i class='fas fa-thermometer'></i><span class='menu-title'>Laboratory Test</span></a></li><li class=' nav-item'><a href='$Rooms'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Hospitalization</span></a></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-medkit'></i><span class='menu-title' data-i18n='nav.templates.main'>Medicines/Service</span></a><ul class='menu-content'><li class=' nav-item'><a href='$AddService'><i class='fas fa-medkit'></i><span class='menu-title'>Add Medicines/Service</span></a></li><li class=' nav-item'><a href='$ListServices'><i class='fas fa-list-ul'></i><span class='menu-title'>List Medicines/Service</span></a></li></ul></li>";
                    echo "<li class='nav-item'><a href='$MedicalReport'><i class='fas fa-store-alt'></i><span class='menu-title'>Medical Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-comments'></i><span class='menu-title' data-i18n='nav.templates.main'>Stock Management</span></a><ul class='menu-content'><li class=' nav-item'><a href='$StockManagement'><i class='fas fa-file-chart-pie'></i><span class='menu-title'>Medicine Stock</span></a></li><li class=' nav-item'><a href='$StockManagementReport'><i class='fas fa-store-alt'></i><span class='menu-title'>Stock Report</span></a></li><li class=' nav-item'><a href='$StockManagementRequisition'><i class='fas fa-store-alt'></i><span class='menu-title'>Requisition Report</span></a></li></ul></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-comments'></i><span class='menu-title' data-i18n='nav.templates.main'>Communication</span></a><ul class='menu-content'><li class=' nav-item'><a href='$CommunicationSendSms'><i class='fas fa-comment'></i><span class='menu-title'>Send SMS</span></a></li><li class=' nav-item'><a href='$SmsList'><i class='fas fa-list-ul'></i><span class='menu-title'>SMS History</span></a></li></ul></li>";
                    break;

                case "SuperAdmin":
                    echo "<li class='nav-item'><a href='$adminurl'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo"<li class=' nav-item'><a href='$ListReceptionReport'><i class='fas fa-thumbs-up'></i><span class='menu-title'>Reception Report</span></a></li>";
                    echo "<li class='nav-item'><a href='$MedicalReport'><i class='fas fa-store-alt'></i><span class='menu-title'>Medical Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorsReport'><i class='fas fa-file-chart-pie'></i><span class='menu-title'>Doctor Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='$ClinicPatientProfile'><i class='fas fa-users'></i><span class='menu-title'>Patient Profile Menu</span></a></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-briefcase'></i><span class='menu-title' data-i18n='nav.templates.main'>Accountant</span></a><ul class='menu-content'><li class=' nav-item'><a href='$PayedSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Cash Sales</span></a></li><li class=' nav-item'><a href='$AccountantCashierSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'> Daily Cashier Sales</span></a></li><li class=' nav-item'><a href='$AccountCashierDepositReport'><i class='fas fa-briefcase'></i><span class='menu-title'>Cashier Deposit report</span></a></li><li class=' nav-item'><a href='$AccountantExpensesReport'><i class='fas fa-briefcase'></i><span class='menu-title'> Expenses Report</span></a></li></ul></li>";
                    echo "<li class=' nav-item'><a href='$InsuranceReport'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Insurance Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='$SmsList'><i class='fas fa-comments'></i><span class='menu-title' data-i18n='nav.templates.main'>SMS History</span></a></li>";
                    break;

                case "Doctor":
                    echo "<li class=' nav-item'><a href='$DoctorDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Doctor'><i class='fas fa-user-md'></i><span class='menu-title'>Doctor</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorConsultation'><i class='fas fa-thermometer'></i><span class='menu-title'>Consultation</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorLaboratoryResults'><i class='fas fa-vials'></i><span class='menu-title'>Laboratory Results</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorsReport'><i class='fas fa-file-chart-pie'></i><span class='menu-title'>Doctor Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='$ClinicPatientProfile'><i class='fas fa-users'></i><span class='menu-title'>Patient Profile Menu</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$DepartmentStockManagement'><i class='fas fa-users'></i><span class='menu-title'>Stock Management</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$DoctorOperations'><i class='fas fa-user-injured'></i><span class='menu-title'>Operations</span></a></li>";
                    break;

                case "Doctor Dentist":
                    echo "<li class=' nav-item'><a href='$DoctorDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Doctor'><i class='fas fa-user-md'></i><span class='menu-title'>Doctor</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DentistConsultation'><i class='fas fa-thermometer'></i><span class='menu-title'>Consultation</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorLaboratoryResults'><i class='fas fa-vials'></i><span class='menu-title'>Laboratory Results</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorsReport'><i class='fas fa-file-chart-pie'></i><span class='menu-title'>Doctor Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='$ClinicPatientProfile'><i class='fas fa-users'></i><span class='menu-title'>Patient Profile Menu</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$DoctorOperations'><i class='fas fa-user-injured'></i><span class='menu-title'>Operations</span></a></li>";
                    break;

                case "Laboratory":
                    echo "<li class=' nav-item'><a href='$LaboratoryDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo "<li class=' nav-item'><a href='$LaboratoryTech'><i class='fas fa-user-md'></i><span class='menu-title'>Recent Patients</span></a></li>";
                    echo "<li class=' nav-item'><a href='$LaboratoryAllPatient'><i class='fas fa-user-md'></i><span class='menu-title'>All Patients</span></a></li>";
                    echo "<li class=' nav-item'><a href='$LaboratoryReport'><i class='fas fa-file-chart-pie'></i><span class='menu-title'>Laboratory Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DepartmentStockManagement'><i class='fas fa-users'></i><span class='menu-title'>Stock</span></a></li>";
                    break;

                case "Accountant":
                    echo "<li class=' nav-item'><a href='$AccountantDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo "<li class=' nav-item'><a href='$AccountantPatientPay'><i class='fas fa-user-md'></i><span class='menu-title'>Payment</span></a></li>";
                    echo "<li class=' nav-item'><a href='$PayedSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Cash Sales</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$HospitalizeSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Hospitalization Sales</span></a></li>";
                    echo "<li class=' nav-item'><a href='$InsuranceReport'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Insurance Report</span></a></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-briefcase'></i><span class='menu-title' data-i18n='nav.templates.main'>Cashier Sales</span></a><ul class='menu-content'><li class=' nav-item'><a href='$AccountantCashierSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'> Daily Cashier Sales</span></a></li><li class=' nav-item'><a href='$AccountantCashierDeposit'><i class='fas fa-briefcase'></i><span class='menu-title'>Cashier Deposit</span></a></li><li class=' nav-item'><a href='$AccountCashierDepositReport'><i class='fas fa-briefcase'></i><span class='menu-title'>Cashier Deposit report</span></a></li></ul></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-briefcase'></i><span class='menu-title' data-i18n='nav.templates.main'>Expenses</span></a><ul class='menu-content'><li class=' nav-item'><a href='$AccountantExpenses'><i class='fas fa-money-bill-alt'></i><span class='menu-title'> Expenses</span></a></li><li class=' nav-item'><a href='$AccountantExpensesReport'><i class='fas fa-briefcase'></i><span class='menu-title'> Expenses Report</span></a></li></ul></li>";
//                    echo "<li class=' nav-item'><a href='$DepartmentStockManagement'><i class='fas fa-users'></i><span class='menu-title'>Stock Management</span></a></li>";
                    break;

                case "Reception":

                    echo "<li class='nav-item'><a href='$recurldash'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-table'></i><span class='menu-title' data-i18n='nav.templates.main'>Reception</span></a><ul class='menu-content'><li class=' nav-item'><a href='$recurl'><i class='fas fa-medkit'></i><span class='menu-title'>Add New Patient</span></a></li><li class=' nav-item'><a href='$ListReception'><i class='fas fa-list-ul'></i><span class='menu-title'>List Patient</span></a></li></ul></li>";
                    echo "<li class='nav-item'><a href='$ListReceptionReport'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Reception Report</span></a></li>";
                    echo "<li class='nav-item'><a href='$accountant'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Accountant</span></a></li>";
                    echo "<li class='nav-item'><a href='$Appointment'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Appointment</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$DepartmentStockManagement'><i class='fas fa-users'></i><span class='menu-title'>Stock Management</span></a></li>";

                    break;

                case "Insurance Verification":

                    echo "<li class='nav-item'><a href='$insuranceverification'><i class='fas fa-users'></i><span class='menu-title'>Patients</span></a></li>";

                    break;

                case "Hospitalization":

                    echo "<li class='nav-item'><a href='$HospitalizationDashboard'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo "<li class='nav-item'><a href='$Hospitalize'><i class='fas fa-bed'></i><span class='menu-title'>Hospitalize Patient</span></a></li>";
                    echo "<li class='nav-item'><a href='$HospitalizeReport'><i class='fas fa-poll'></i><span class='menu-title'>Hospitalize Report</span></a></li>";
                    echo "<li class='nav-item'><a href='$HospitalizeReport'><i class='fas fa-table'></i><span class='menu-title'>Manage Hospitalization</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$DepartmentStockManagement'><i class='fas fa-users'></i><span class='menu-title'>Stock Management</span></a></li>";
//                    echo "<li class='nav-item'><a href='$accountant'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Accountant</span></a></li>";
                    break;

                case "Nurse":
                    echo "<li class=' nav-item'><a href='$NurseDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Nurse'><i class='fas fa-user-md'></i><span class='menu-title'>Nurse</span></a></li>";
                    echo "<li class=' nav-item'><a href='$NurseConsultation'><i class='fas fa-thermometer'></i><span class='menu-title'>Consultation</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DoctorLaboratoryResults'><i class='fas fa-vials'></i><span class='menu-title'>Laboratory Results</span></a></li>";
                    echo "<li class=' nav-item'><a href='$ClinicPatientProfile'><i class='fas fa-users'></i><span class='menu-title'>Patient Profile Menu</span></a></li>";
                    echo "<li class=' nav-item'><a href='$DepartmentStockManagement'><i class='fas fa-users'></i><span class='menu-title'>Stock</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Operations'><i class='fas fa-user-injured'></i><span class='menu-title'>Operations</span></a></li>";
                    break;

                default:
                    return redirect()->back();
                    break;
            }
            ?>

        </ul>
    </div>
</div>


