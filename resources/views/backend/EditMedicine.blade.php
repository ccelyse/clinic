@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        @foreach($listMedicine as $datas)
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditMedicine_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Medicine Code</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$datas->medicinecode}}"--}}
                                                                   {{--name="medicinecode">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Medicine Name</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$datas->medicinename}}"--}}
                                                                   {{--name="medicinename">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6" hidden>--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Medicine ID</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$datas->medicine_id}}"--}}
                                                                   {{--name="medicine_id">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Medicine Quantity(not mandatory for service)</label>--}}
                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$datas->medicinequantity}}"--}}
                                                                   {{--name="medicinequantity">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Department Name</label>--}}
                                                            {{--<select class="select2 form-control" multiple="multiple" id="id_h5_multi" name="medicine_department_id">--}}

                                                                {{--<option value="{{$datas->department_name_id}}"selected>{{$datas->departmentname}}</option>--}}
                                                                {{--@foreach($listdepr as $data)--}}
                                                                    {{--<option value="{{$data->id}}">{{$data->departmentname}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                <div class="row">
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Insurance Name</label>--}}
                                                            {{--<select class="form-control" name="insurance_medicine_notprivate">--}}
                                                                {{--<option value="{{$datas->insurance_medicine_id}}"selected>{{$datas->insurance_name}}</option>--}}
                                                                {{--@foreach($listinsu as $data)--}}
                                                                    {{--<option value="{{$data->id}}">{{$data->insurance_name}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Insurance Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->insurance_name}}"
                                                                   name="medicinepriceinsurance" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Medicine Price</label>
                                                            <input type="number" id="projectinput1" class="form-control" value="{{$datas->medicinepriceinsurance}}"
                                                                   name="medicinepriceinsurance">
                                                        </div>
                                                    </div>
                                                    <h1 style="font-size: 20px; padding: 15px;">Current Insurance Attached to this Services/Medicine</h1>
                                                    @foreach($getinsuranceandprice as $info)
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Insurance Name</label>
                                                                <input type="text" id="projectinput1" class="form-control" value="{{$info->insurance_name}}"
                                                                       name="medicinepriceinsurance" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Medicine Price</label>
                                                                <input type="number" id="projectinput1" class="form-control" value="{{$info->medicinepriceinsurance}}"
                                                                       name="medicinepriceinsurance" disabled>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                    <div class="col-md-6" hidden>
                                                        <div class="form-group">
                                                            <label for="projectinput1">id</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->id}}"
                                                                   name="id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Update
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateNewInsurancePrice') }}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                    $(function () {
                                                        $("#chkPassport").click(function () {
                                                            if ($(this).is(":checked")) {
                                                                $("#dvPassport").show();
                                                                $("#AddPassport").hide();
                                                            } else {
                                                                $("#dvPassport").hide();
                                                                $("#AddPassport").show();
                                                            }
                                                        });
                                                    });
                                                </script>
                                                <label for="chkPassport">
                                                    <input type="checkbox" id="chkPassport" />
                                                    Add New Insurance and Price
                                                </label>
                                                <hr />
                                                <div class="multi-field-wrapper" id="dvPassport" style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Medicine Code</label>
                                                                <input type="text" id="projectinput1" class="form-control" value="{{$datas->medicinecode}}"
                                                                       name="medicinecode">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Medicine Name</label>
                                                                <input type="text" id="projectinput1" class="form-control" value="{{$datas->medicinename}}"
                                                                       name="medicinename">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" hidden>
                                                            <div class="form-group">
                                                                <label for="projectinput1">Medicine ID</label>
                                                                <input type="text" id="projectinput1" class="form-control" value="{{$datas->medicine_id}}"
                                                                       name="medicine_id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Medicine Quantity(not mandatory for service)</label>
                                                                <input type="text" id="projectinput1" class="form-control" value="{{$datas->medicinequantity}}"
                                                                       name="medicinequantity">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Activity Type</label>
                                                                <select class="form-control" name="medicine_department_id">
                                                                <option value="{{$datas->department_name_id}}">{{$datas->departmentname}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Insurance  Name</label>
                                                                    <select class="form-control"  id="basicSelect" name="insurance_medicine[]" required>
                                                                        @foreach($listinsu as $data)
                                                                            <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Medicine  Price</label>
                                                                    <input type="number" id="projectinput1" class="form-control" placeholder="Medicine  Price"
                                                                           name="medicineprice[]">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-login">
                                                            <i class="la la-check-square-o"></i> Save New
                                                        </button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>

                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection