
@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')
    <style>
        body.vertical-layout.vertical-menu.menu-expanded .content, body.vertical-layout.vertical-menu.menu-expanded .footer, body.vertical-layout.vertical-menu.menu-expanded .navbar .navbar-container {
            margin-left:0;
        }

        body.vertical-layout.vertical-menu.menu-expanded .content, body.vertical-layout.vertical-menu.menu-expanded .footer, body.vertical-layout.vertical-menu.menu-expanded .navbar .navbar-container {
            margin-left: 0px !important;
        }
        .btn-login {
            border-color: #0088cc !important;
            background-color: #0088cc !important;
            color: #FFF;
        }
        .help-block{
            color:red !important;
        }
        .btn-login:hover{
            color:#fff;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/login-register.min.css">


    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <div class="p-1">
                                            <img src="backend/app-assets/images/logoclinic.png" alt="" style="width: 200px;">
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                                        <span>Create Account</span>
                                    </h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('CreateAccount_') }}">
                                            {{ csrf_field() }}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                         <strong>{{ $errors->first('name') }}</strong>
                                                     </span>
                                            @endif
                                            <fieldset class="form-group{{ $errors->has('name') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
                                                <input type="text" class="form-control form-control-lg input-lg" id="user-name" name="name" value="{{ old('name') }}"  placeholder="names" autofocus>

                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                         <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                            @endif
                                            <fieldset class="form-group{{ $errors->has('email') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
                                                <input type="email" class="form-control form-control-lg input-lg" id="user-email" name="email" name="email" value="{{ old('email') }}"  placeholder="email" required>


                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left mb-1">
                                                <input  id="projectinput1" class="form-control"
                                                        name="phonenumber" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                        type = "number" value="{{ old('phonenumber') }}"  pattern="[+]{1}[0-9]{12}" maxlength="16" required>

                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                         <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                            @endif
                                            <fieldset class="form-group{{ $errors->has('password') ? ' has-error' : '' }} position-relative has-icon-left">
                                                <input type="password" class="form-control form-control-lg input-lg" id="user-password"
                                                       placeholder="Enter Password" name="password"  required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control form-control-lg input-lg" id="user-password"
                                                       placeholder="Confirm password" name="password_confirmation"  required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <select class="form-control"  id="basicSelect" name="user_role" required>
                                                        <option value="{{ old('user_role') }}">{{ old('user_role') }}</option>
                                                        <option value="Doctor">Doctor</option>
                                                        <option value="Doctor Dentist">Doctor Dentist</option>
                                                        <option value="Nurse">Nurse</option>
                                                        <option value="Laboratory">Laboratory</option>
                                                        <option value="Accountant">Accountant</option>
                                                        <option value="Reception">Reception</option>
                                                        <option value="Insurance Verification">Insurance Verification</option>
                                                        <option value="Admin">Admin</option>
                                                        <option value="SuperAdmin">Super Admin</option>
                                                        <option value="Hospitalization">Hospitalization</option>
                                                </select>
                                            </fieldset>

                                            <button type="submit" class="btn btn-login btn-lg btn-block"><i class="ft-unlock"></i> Register</button>
                                            <a href="{{ URL::previous() }}" class="btn btn-login btn-lg btn-block"><i class="fas fa-hand-point-left"></i> Back</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
@endsection