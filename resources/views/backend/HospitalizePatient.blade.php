@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($NursePatient as $datas)
                                                <li class="list-group-item"><strong>Patient
                                                        Names:</strong> {{$datas->patient_names}}</li>
                                                <li class="list-group-item"><strong>Patient Date of Birth
                                                        Id:</strong> {{$datas->dateofbirth}}</li>

                                                <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                                <?php
                                                $insurancename = $datas->insurance_name;
                                                $copay = $datas->copay;
                                                ?>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Payment Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($listpatientprice as $charges)
                                                <li class="list-group-item"><strong>Insurance
                                                        Name:</strong> <?php echo"$insurancename";?></li>
                                                <li class="list-group-item"><strong>Insurance Due:
                                                    </strong>
                                                    <?php
                                                    $charges =$charges->producttotalsum;
                                                    $sumofinsurance = ($charges * $copay)/100;
                                                    $sumofpatient = $charges - $sumofinsurance ;
                                                    echo $sumofinsurance ;
                                                    ?>
                                                    FRW
                                                </li>
                                                <li class="list-group-item"><strong>Patient Due:</strong> <?php echo $sumofpatient; ?> FRW</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Payment Hospitalization Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($listHospitalizePatient as $roomsprices)
                                                <li class="list-group-item"><strong>Insurance
                                                        Name:</strong> <?php echo"$insurancename";?></li>
                                                <?php
                                                $roomlevelprice = $roomsprices->roomprice;
                                                $roomlevel = $roomsprices->roomlevel;

                                                $sumofinsurance = ($roomlevelprice * $copay)/100;
                                                $sumofpatient = $roomlevelprice - $sumofinsurance ;

                                                if($roomlevel == 'Vip'){
                                                    $sumofinsurance_ = (15000 * $copay)/100;
                                                    $suminsu =
                                                    $newprice = $roomlevelprice - $sumofinsurance_;
                                                    echo "<li class='list-group-item'><strong>Insurance Due: $sumofinsurance_</strong></li>";
                                                    echo "<li class='list-group-item'><strong>Patient Due: $newprice</strong></li>";
                                                }else{
                                                    echo "<li class='list-group-item'><strong>Insurance Due: $sumofinsurance</strong></li>";
                                                    echo "<li class='list-group-item'><strong>Patient Due: $sumofpatient</strong></li>";
                                                }
                                                ?>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <form class="form-horizontal form-simple" method="POST"
                                          action="{{ url('AddHospitalizePatient') }}"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row  multi-field">
                                            <script type="text/javascript">

                                                function getRoomLevel(val) {
                                                    $.ajax({
                                                        headers: {
                                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                        },
                                                        type: "POST",
                                                        url: "GetRoomLevelAjax",
                                                        data: 'roomlevel=' + val,
                                                        success: function (data) {
                                                            $("#roomnumber").html(data);
//
                                                        }
                                                    })
//
                                                }
                                                function getRoomPrice(val) {
                                                    $.ajax({
                                                        headers: {
                                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                        },
                                                        type: "POST",
                                                        url: "GetRoomPriceAjax",
                                                        data: 'roomnumber=' + val,
                                                        success: function (data) {
                                                            $("#fees").html(data);
                                                        }
                                                    });
                                                }

                                            </script>
                                            <div class="col-md-12" hidden>
                                                <div class="form-group">
                                                    <label for="projectinput1">Patient ID</label>
                                                    <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectinput1">Room Level</label>
                                                    <select class="form-control" id="country-list"
                                                            name="roomlevel"
                                                            onChange="getRoomLevel(this.value);"
                                                            required>
                                                        <option value="" selected>Select
                                                            room level
                                                        </option>
                                                        <option value="Normal">Normal</option>
                                                        <option value="Vip">Vip</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectinput1">Room Number</label>
                                                    <select class="form-control" id="roomnumber"
                                                            name="roomnumber"
                                                            onChange="getRoomPrice(this.value);"
                                                            required>
                                                        {{--<option value="">Select Room Number</option>--}}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectinput1">Room Price
                                                        Price</label>
                                                    <select class="form-control" id="fees"
                                                            name="roomprice" required>
                                                        {{--<option value="">Select Price</option>--}}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectinput1">Hospitalization Status</label>
                                                    <select class="form-control" id="hospitilazestatus"
                                                            name="hospitilazestatus" required>
                                                        <option value="Check In" selected>Check In</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="text" id="patient_id"
                                                   class="form-control"
                                                   name="patient_id" value="<?php echo $id;?>" hidden>
                                            <input type="text" id="patientrecoddateid"
                                                   class="form-control"
                                                   name="patientrecoddateid"
                                                   value="<?php echo $patientrecoddateid;?>" hidden>

                                            <input type="text" id="insurance_id"
                                                   class="form-control"
                                                   name="insurance_id"
                                                   value="<?php echo $insurance_id;?>" hidden>

                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-login">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>

                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Hospitalization</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Room Level</th>
                                                <th>Room Number</th>
                                                <th>Room Price</th>
                                                <th>Hospitalization Status</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listHospitalizePatient as $data)
                                                <tr>
                                                    <td>{{$data->patient_roomlevel}}</td>
                                                    <td>{{$data->roomnumber}}</td>
                                                    <td>{{$data->patient_roomprice}}</td>
                                                    <td>{{$data->hospitilazestatus}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.DeleteHospitalizePatient',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
