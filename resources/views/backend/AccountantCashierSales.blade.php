@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <form
                                method="POST"
                                action="{{ url('AccountantCashierSalesFilter') }}"
                                enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="projectinput1">Cashier Lists</label>
                                        <select class="form-control" id="cashier_list"
                                                name="cashier_list"
                                        >
                                            <option value="All">All</option>
                                            @foreach($listofaccountant as $data)
                                                <option value="{{$data->id}}">{{$data->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date">From Date</label>
                                        <input type="date" class="form-control" id="date" name="fromdate"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date">To Date</label>
                                        <input type="date" class="form-control" id="date" name="todate"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: 10px">
                                        <button type="submit" class="btn btn-login">
                                            <i class="la la-check-square-o"></i> Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Sales Report</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Insurance Due</th>
                                                <th>Patient Due</th>
                                                <th>Patient Paid</th>
                                                <th>Patient Balance</th>
                                                <th>Education</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $todaydate = \Carbon\Carbon::now();
                                            $datenow = $todaydate->toDateString();
                                            $date = $todaydate->toDateString();

                                            $totalcashierEducation = \App\AccountPatientClear::selectRaw('SUM(`amount_paid`) `amount_paid`')
                                                ->whereBetween('created_at', [$datenow, $datetomorrow])
                                                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                                                ->where('chargesreasons','Education')
                                                ->value('amount_paid');

                                            $totalcashierndengera = \App\AccountPatientClear::selectRaw('SUM(`amount_due`) `amount_due`')
                                                ->whereBetween('created_at', [$datenow, $datetomorrow])
                                                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                                                ->where('chargesreasons','Ndengera Clinic')
                                                ->value('amount_paid');
                                            ?>
                                            <?php
                                            $totalcashierPatientPaid = \App\AccountPatientClear::selectRaw('SUM(`amount_due`) `amount_due`')
                                                ->whereBetween('created_at', [$datenow, $datetomorrow])
                                                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                                                ->where('chargesreasons','Patient')
                                                ->value('amount_paid');
                                            $totaltobepaid = \App\PatientCharges::selectRaw('SUM(`patient_amount`) `patient_amount`')
                                                ->whereBetween('created_at', [$datenow, $datetomorrow])
                                                ->value('patient_amount');
                                            ?>

                                            @foreach($totalamountpaid as $data)
                                                <tr>
                                                    <td>{{$data->insurance_due}} RWF</td>
                                                    <td><?php echo "$totalcashierPatientPaid";?> RWF</td>
                                                    <td><?php echo "$totalcashierPatientPaid";?> RWF</td>
                                                    <td>{{$data->remainingbalance}} RWF</td>
                                                    <td><?php echo "$totalcashierEducation";?> RWF</td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <thead>
                                            <?php
                                            $totalcashierPatientPaid = \App\AccountPatientClear::selectRaw('SUM(`amount_due`) `amount_due`')
                                                ->whereBetween('created_at', [$datenow, $datetomorrow])
                                                ->selectRaw('SUM(`amount_paid`) `amount_paid`')
                                                ->where('chargesreasons','Patient')
                                                ->value('amount_paid');
                                            ?>

                                            @foreach($totalcashier as $cashiers)
                                            <tr>


                                                <th>Total Insurance Due</th>
                                                <th>Total Patient Due</th>
                                                <th>Total Patient Paid</th>
                                                <th>Total Patient Balance</th>
                                                <th>Total Education</th>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            <tbody>


                                            <tr>
                                                <th>{{$cashiers->insurance_due}} FRW</th>
                                                <th>{{$cashiers->amount_due}} FRW</th>
                                                <th><?php echo "$totalcashierPatientPaid"; ?> FRW</th>
                                                <th>{{$cashiers->remainingbalance}} FRW</th>
                                                <td><?php echo "$totalcashierEducation";?>FRW</td>

                                                {{--<td><a href="{{ route('backend.DeletePayedSales',['id'=> $data->id])}}"--}}
                                                {{--class="btn btn-login btn-min-width mr-1 mb-1"><i--}}
                                                {{--class="fas fa-trash"></i> Remove</a></td>--}}
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
