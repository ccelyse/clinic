@extends('backend.layout.master')

@section('title', 'Otobox')

@section('content')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/ui/jquery-ui.min.css">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditBrandModel_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                @foreach($editbrandmodel as $datas)
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Brand Name</label>

                                                            <select class="select2 form-control" name="brandname_id">
                                                                <option value="{{$datas->brandname_id}}">{{$datas->brandname}}</option>
                                                                @foreach($listbrand as $data)
                                                                    <option value="{{$data->id}}">{{$data->brandname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Brand Model</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Brand Name"
                                                                   name="brandmodel" value="{{$datas->brandmodel}}">
                                                        </div>
                                                        <div class="form-group" hidden>
                                                            <label for="projectinput1">Id</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Brand Name"
                                                                   name="id" value="{{$datas->id}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="projectinput4">Date Manufactured</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                                </div>
                                                                <input type="text" class="form-control dp-month-year" name="brandmodelmanufactured" value="{{ old('brandmodelmanufactured') }}"  value="{{$datas->brandmodelmanufactured}}" required/>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group{{ $errors->has('fileToUpload') ? ' has-error' : '' }}">
                                                            <label for="projectinput8">Brand Logo</label>
                                                            <input type="file" class="form-control-file" id="exampleInputFile" name="fileToUpload" value="{{ old('fileToUpload') }}">
                                                            @if ($errors->has('fileToUpload'))
                                                                <span class="help-block">
                                                                  <strong>{{ $errors->first('fileToUpload') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <img src="images/{{$datas->brandmodelpic}}" style="width: 100%">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection