@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }

        .main-menu.menu-light .navigation>li.open>a {
            color: #545766;
            background: #f5f5f5;
            border-right: 4px solid #6b442b !important;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="number-tabs">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    {{--<h4 class="card-title">Form wizard with number tabs</h4>--}}
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="number-tab-steps wizard-circle" method="POST" action="{{ url('AddDepartmentStockManagement') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                            <fieldset>
                                                <div class="row">
                                                    @foreach($listshipment as $shipment)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Product Name</label>
                                                            <select class="form-control"  id="basicSelect" name="shipmentproductname" required>

                                                                <option value="{{$shipment->shipmentproductname}}">{{$shipment->shipmentproductname}}</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Product Piece/Test</label>
                                                            <input id="projectinput1" class="form-control"
                                                                   name="shipmentproductpiece" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                   type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                            <input id="projectinput1" class="form-control" value="{{$shipment->shipmentdepartment}}"
                                                                   name="shipmentdepartment" type= "text" hidden>
                                                            <input id="projectinput1" class="form-control" value="{{$shipment->id}}"
                                                                   name="shipment_id" type= "text" hidden>
                                                            <input id="projectinput1" class="form-control" value="{{$shipment->product_id}}"
                                                                   name="product_id" type= "text" hidden>
                                                            <input id="projectinput1" class="form-control" value="{{$shipment->shipmentproductpiece}}"
                                                                   name="currentshipmentproductpiece" type= "text" hidden>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                <i class="la la-check-square-o"></i> Save
                                                </button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Shipped Materials</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Department Name</th>
                                                <th>Product Name</th>
                                                <th>Product Piece</th>
                                                <th>Date Created</th>
                                                {{--<th>Delete</th>--}}

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listshipment as $shipments)
                                                <tr>
                                                    <td>{{$shipments->shipmentdepartment}}</td>
                                                    <td>{{$shipments->shipmentproductname}}</td>
                                                    <td>{{$shipments->shipmentproductpiece}}</td>
                                                    <td>{{$shipments->created_at}}</td>
                                                    {{--<td><a href="{{ route('backend.RemoveMedicineStockShipment',['id'=> $shipment->id])}}"--}}
                                                           {{--class="btn btn-login btn-min-width mr-1 mb-1"><i--}}
                                                                    {{--class="fas fa-trash"></i> Remove</a></td>--}}
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Used Products</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Department Name</th>
                                                <th>Product Name</th>
                                                <th>Product Piece</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($liststock as $shipment)
                                                <tr>
                                                    <td>{{$shipment->shipmentdepartment}}</td>
                                                    <td>{{$shipment->shipmentproductname}}</td>
                                                    <td>{{$shipment->shipmentproductpiece}}</td>
                                                    <td>{{$shipment->created_at}}</td>
                                                    <td><a href="{{ route('backend.DeleteDepartmentStockManagement',['id'=> $shipment->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>

@endsection
