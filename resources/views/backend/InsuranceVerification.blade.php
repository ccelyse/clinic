@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="complex-header">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Patient List</h4>
                                        <a class="heading-elements-toggle"><i
                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Verify</th>
                                                    <th>Beneficiary Name</th>
                                                    <th>Beneficiary Name Country ID</th>
                                                    <th>Insurance Name</th>
                                                    <th>Insurance Card Number</th>
                                                    <th>Gender</th>
                                                    <th>Date of Birth</th>
                                                    <th>Patient Telephone number</th>
                                                    <th>Copay</th>
                                                    <th>Copay Relationship</th>
                                                    <th>Affiliate Name</th>
                                                    <th>Affiliate Telephone number</th>
                                                    <th>Insured Location</th>
                                                    <th>PID</th>
                                                    <th>Date Created</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listpatient as $data)
                                                    <tr>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-login btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#appointment{{$data->id}}">
                                                               Verify
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="appointment{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">New Record</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <form class="form-horizontal form-simple" method="POST"
                                                                                  action="{{ url('VerifyPatient') }}"
                                                                                  enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row  multi-field">
                                                                                    <div class="col-md-12" hidden>
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Patient ID</label>
                                                                                            <input type="text" id="patient_id"  class="form-control" name="patient_id" value="{{$data->id}}">
                                                                                            {{--<input type="text" id="projectinput1"  class="form-control" name="patient_id" value="{{$data->id}}">da--}}
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Insurance Status</label>
                                                                                            <select class="form-control"  id="insurance_status" name="insurance_status" required>
                                                                                                <option value="Verified">Verified</option>
                                                                                                <option value="Not Verified">Not Verified</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <button class="btn btn-login" id="SaveCharges"><i class="la la-check-square-o"></i> Save</button>
                                                                                    </div>


                                                                                </div>
                                                                            </form>

                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    <h4 class="card-title">Insurance Status</h4>
                                                                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                                                    <div class="heading-elements">
                                                                                        <ul class="list-inline mb-0">
                                                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="card-content collapse show">
                                                                                    <div class="card-body">
                                                                                        <ul class="list-group">
                                                                                            @php
                                                                                                $id = $data->id;
                                                                                                $listpatientdaterecord= \App\InsuranceVerification::where('patient_id',$id)->get();
                                                                                            @endphp

                                                                                            @foreach($listpatientdaterecord as $datepatient)
                                                                                                <li class="list-group-item"><strong>Status:</strong> {{$datepatient->status}}     <a href="{{ route('backend.DeleteVerifyPatient',['id'=> $datepatient->id])}}" class="btn btn-login btn-min-width mr-1 mb-1" style="float: right;"><i class="fas fa-edit"></i> Remove</a>
                                                                                                </li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td>{{$data->beneficiary_name}}</td>
                                                        <td>{{$data->patient_country_id}}</td>
                                                        <td>{{$data->insurance_name}}</td>
                                                        <td>{{$data->insurance_card_number}}</td>
                                                        <td>{{$data->gender}}</td>
                                                        <td>{{$data->dateofbirth}}</td>
                                                        <td>{{$data->beneficiary_telephone_number}}</td>
                                                        <td>{{$data->copay}}</td>
                                                        <td>{{$data->copay_relationship}}</td>
                                                        <td>{{$data->patient_names}}</td>
                                                        <td>{{$data->patient_telephone_number}}</td>
                                                        <td>{{$data->insured_location}}</td>
                                                        <td>{{$data->pid}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="content-body">

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
