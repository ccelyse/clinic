@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="basic-form-layouts">
                        <div class="row match-height">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">

                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('Reception_') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="form-body">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Beneficiary Name</label>
                                                                <input type="text" id="projectinput1" class="form-control" placeholder="Beneficiary Name"
                                                                       name="beneficiary_name" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Beneficiary Telephone number</label>
                                                                <input id="projectinput1" class="form-control" placeholder="Beneficiary Telephone number"
                                                                       name="beneficiary_telephone_number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                       type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Gender</label>
                                                                <select class="form-control"  id="basicSelect" name="gender" required>
                                                                    <option value="male">male</option>
                                                                    <option value="female">female</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Date of Birth</label>
                                                                <input type="date" class="form-control" id="date" name="dateofbirth" value="2011-08-19" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Beneficiary Country ID</label>
                                                                <input  id="projectinput1" class="form-control"
                                                                        name="patient_country_id" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                        type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="16" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Insurance  Name</label>
                                                                <select class="form-control"  id="basicSelect" name="insurance_patient_name" required>
                                                                    @foreach($listinsu as $data)
                                                                        <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Affiliate Relationship</label>
                                                                <select class="form-control"  id="basicSelect" name="copay_relationship" required>
                                                                    <option value="Self">Self</option>
                                                                    <option value="Child">Child</option>
                                                                    <option value="Spouse">Spouse</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Insurance Card Number</label>
                                                                <input type="text" id="projectinput1" class="form-control" placeholder="Insurance Card Number"
                                                                       name="insurance_card_number" required>
                                                            </div>
                                                        </div>

                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Affiliate Names</label>
                                                                <input type="text" id="projectinput1" class="form-control" placeholder="Affiliate Names"
                                                                       name="patient_names" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Affiliate Telephone number</label>
                                                                <input  id="projectinput1" class="form-control" placeholder="Patient Telephone number"
                                                                        name="patient_telephone_number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                        type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Copay (%)</label>
                                                                <input type="text" id="projectinput1" class="form-control" placeholder="Copay"
                                                                       name="copay" required>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Insured Location</label>
                                                                <input type="text" id="projectinput1" class="form-control" placeholder="Insured Location"
                                                                       name="insured_location" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-login">
                                                            <i class="la la-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>
                    <!-- // Basic form layout section end -->
                    <section id="complex-header">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Patient List</h4>
                                        <a class="heading-elements-toggle"><i
                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>New Record</th>
                                                    <th>Beneficiary Name</th>
                                                    <th>Beneficiary Name Country ID</th>
                                                    <th>Insurance Name</th>
                                                    <th>Insurance Card Number</th>
                                                    <th>Gender</th>
                                                    <th>Date of Birth</th>
                                                    <th>Patient Telephone number</th>
                                                    <th>Copay</th>
                                                    <th>Copay Relationship</th>
                                                    <th>Affiliate Name</th>
                                                    <th>Affiliate Telephone number</th>
                                                    <th>Insured Location</th>
                                                    <th>PID</th>
                                                    <th>Date Created</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listpatient as $data)
                                                    <tr>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-login btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#appointment{{$data->id}}">
                                                                New Record
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="appointment{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">New Record</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST"
                                                                                  action="{{ url('NewRecordHistory') }}"
                                                                                  enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row  multi-field">
                                                                                    <div class="col-md-12" hidden>
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Patient ID</label>
                                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="{{$data->id}}">
                                                                                            {{--<input type="text" id="projectinput1"  class="form-control" name="patient_id" value="{{$data->id}}">da--}}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Date of Birth</label>
                                                                                            <input type="date" class="form-control" id="date" name="newrecorddate" value="2018-10-19">
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <button type="submit" class="btn btn-login">
                                                                                            <i class="la la-check-square-o"></i> Save
                                                                                        </button>
                                                                                    </div>

                                                                                </div>
                                                                            </form>

                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    <h4 class="card-title">Patient Information</h4>
                                                                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                                                    <div class="heading-elements">
                                                                                        <ul class="list-inline mb-0">
                                                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="card-content collapse show">
                                                                                    <div class="card-body">
                                                                                        <ul class="list-group">
                                                                                            @php
                                                                                                $id = $data->id;
                                                                                                $listpatientdaterecord= \App\PatientNewRecord::where('patient_id',$id)->get();
                                                                                            @endphp

                                                                                            @foreach($listpatientdaterecord as $datepatient)
                                                                                                <li class="list-group-item"><strong>Record Date:</strong> {{$datepatient->patientrecorddate}}</li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td><a href="{{ route('backend.EditPatient',['id'=> $data->id])}}">{{$data->beneficiary_name}}</a></td>
                                                        <td>{{$data->patient_country_id}}</td>
                                                        <td>{{$data->insurance_name}}</td>
                                                        <td>{{$data->insurance_card_number}}</td>
                                                        <td>{{$data->gender}}</td>
                                                        <td>{{$data->dateofbirth}}</td>
                                                        <td>{{$data->beneficiary_telephone_number}}</td>
                                                        <td>{{$data->copay}}</td>
                                                        <td>{{$data->copay_relationship}}</td>
                                                        <td>{{$data->patient_names}}</td>
                                                        <td>{{$data->patient_telephone_number}}</td>
                                                        <td>{{$data->insured_location}}</td>
                                                        <td>{{$data->pid}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <a href="{{ route('backend.EditPatient',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Edit</a>
                                                        </td>

                                                        <td><a href="{{ route('backend.DeletePatient',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>

                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="content-body">

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
