@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditRoomPrice_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($listroomsprice as $datas)
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Level</label>
                                                            <select class="form-control"  id="basicSelect" name="roomlevel">
                                                                <option value="{{$datas->roomlevel}}">{{$datas->roomlevel}}</option>
                                                                @foreach($listroom as $data)
                                                                    <option value="{{$data->roomlevel}}">{{$data->roomlevel}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Number</label>
                                                            <select class="form-control" id="basicSelect" name="roomnumber">
                                                                <option value="{{$datas->roomnumber}}">{{$datas->roomnumber}}</option>
                                                                @foreach($listroom as $data)
                                                                    <option value="{{$data->roomnumber}}">{{$data->roomnumber}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Insurance  Name</label>
                                                                    <select class="form-control"  id="basicSelect" name="insurance_medicine" required>
                                                                        <option value="{{$datas->insurance_medicine}}">{{$datas->insurance_name}}</option>
                                                                        @foreach($listinsu as $data)
                                                                            <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Room  Price</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value="{{$datas->roomprice}}"
                                                                           name="roomprice" >
                                                                    <input type="text" id="projectinput1" class="form-control" value="{{$datas->id}}"
                                                                           name="id" hidden>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                                @endforeach
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic form layout section end -->

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection