        @extends('backend.layout.master')

        @section('title', 'Ndengera Clinic')

        @section('content')

            @include('backend.layout.sidemenu')
            @include('backend.layout.upmenu')
            <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
            <script
                    src="https://code.jquery.com/jquery-3.3.1.js"
                    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
                    crossorigin="anonymous"></script>
            <style>
                .form-grouppop{
                    margin-bottom: -10px;
                    position: relative;
                    display: inline-block;
                }
            </style>

            <div class="app-content content">
                <div class="content-wrapper">
                    @if (session('addedconsultation'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('addedconsultation') }}
                        </div>
                    @endif
                    @if (session('addedconsultation_'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('addedconsultation_') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif

                    <div class="content-body">
                        <section id="complex-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Patient Information</h4>
                                            <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body">
                                                <ul class="list-group">
                                                    @foreach($NursePatient as $datas)
                                                        <li class="list-group-item"><strong>Patient
                                                                Names:</strong> {{$datas->beneficiary_name}}</li>
                                                        <li class="list-group-item"><strong>Patient Date of Birth
                                                                Id:</strong> {{$datas->dateofbirth}}</li>

                                                        <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                                        <?php
                                                        $insurancename = $datas->insurance_name;
                                                        $pid = $datas->pid;
                                                        $beneficiary_telephone_number = $datas->beneficiary_telephone_number;
                                                        $copay = $datas->copay;
                                                        ?>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Payment Information</h4>
                                            <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="card-content collapse show">
                                            <div class="card-body">
                                                <ul class="list-group">
                                                    @foreach($listpatientprice as $charges)
                                                        <li class="list-group-item"><strong>Insurance
                                                                Name:</strong> <?php echo"$insurancename";?></li>
                                                        <li class="list-group-item"><strong>Insurance Due:
                                                            </strong>
                                                            <?php
                                                            $charges =$charges->producttotalsum;
                                                            $sumofinsurance = ($charges * $copay)/100;
                                                            $sumofpatient = $charges - $sumofinsurance ;
                                                            echo $sumofinsurance ;
                                                            ?>
                                                            FRW
                                                        </li>
                                                        <li class="list-group-item"><strong>Patient Due:</strong> <?php echo $sumofpatient; ?> FRW</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </section>
                        <section id="complex-header">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <a href="{{ route('backend.LaboratoryResultsPrint',['patientrecoddateid'=> $patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> View Other Results</a>
                                            <a href="{{ route('backend.LaboratoryResultsPrintNFS',['patientrecoddateid'=> $patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> View NFS Results</a>
                                            <div class="form-grouppop">
                                                <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                        data-toggle="modal"
                                                        data-target="#defaultcharges">
                                                    Doctor Comment
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade text-left" id="defaultcharges"
                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document" style="max-width: 600px; !important;">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel1">Doctor Comment</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form-horizontal form-simple" method="POST"
                                                                action="{{ url('DoctorTestComment') }}"
                                                                enctype="multipart/form-data" style="margin-bottom:15px;">
                                                                {{ csrf_field() }}

                                                                <div class="row  multi-field" style="margin-bottom:15px;">
                                                                    <div class="col-md-12">
                                                                        <div class='form-group'>
                                                                            <label for='projectinput1'>Results interpretation</label>
                                                                            <textarea name='resultsinterpretation'
                                                                                      rows='5'
                                                                                      class='form-control' required></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class='form-group'>
                                                                            <label for='projectinput1'>Final Conclusion</label>
                                                                            <textarea name='finalconclusion'
                                                                                      rows='5'
                                                                                      class='form-control' required></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class='form-group'>
                                                                            <label for='projectinput1'>Treatment</label>
                                                                            <textarea name='treatment'
                                                                                      rows='5'
                                                                                      class='form-control' required></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" id="patient_id"
                                                                           class="form-control"
                                                                           name="patient_id" value="<?php echo $id;?>" hidden>

                                                                    <input type="text" id="patientrecoddateid"
                                                                           class="form-control"
                                                                           name="patientrecoddateid"
                                                                           value="<?php echo $patientrecoddateid;?>" hidden>


                                                                    <div class="col-md-12">
                                                                        <button class="btn btn-login" id="SaveCharges"><i class="la la-check-square-o"></i> Save</button>
                                                                    </div>

                                                                </div>
                                                                </form>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </section>
                        <section id="complex-header">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Doctor's comment</h4>
                                            <a class="heading-elements-toggle"><i
                                                        class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body card-dashboard">
                                                <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>Results interpretation</th>
                                                        <th>Final Conclusion</th>
                                                        <th>Treatment</th>
                                                        <th>Doctor</th>
                                                        <th>Date Created</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($listdoctorcomment as $data)
                                                        <tr>
                                                            <td>{{$data->resultsinterpretation}}</td>
                                                            <td>{{$data->finalconclusion}}</td>
                                                            <td>{{$data->treatment}}</td>
                                                            <td>{{$data->name}}</td>
                                                            <td>{{$data->created_at}}</td>
                                                            <td>
                                                                <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                        data-toggle="modal"
                                                                        data-target="#editdoctorcomment">
                                                                    Edit
                                                                </button>
                                                                <!-- Modal -->
                                                                <div class="modal fade text-left" id="editdoctorcomment"
                                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                                     aria-hidden="true">
                                                                    <div class="modal-dialog" role="document" style="max-width: 600px; !important;">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" id="myModalLabel1">Doctor Comment</h4>
                                                                                <button type="button" class="close" data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form class="form-horizontal form-simple" method="POST"
                                                                                      action="{{ url('DoctorTestCommentEdit') }}"
                                                                                      enctype="multipart/form-data" style="margin-bottom:15px;">
                                                                                    {{ csrf_field() }}

                                                                                    <div class="row  multi-field" style="margin-bottom:15px;">
                                                                                        <div class="col-md-12">
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Results interpretation</label>
                                                                                                <textarea name='resultsinterpretation'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->resultsinterpretation}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Final Conclusion</label>
                                                                                                <textarea name='finalconclusion'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->finalconclusion}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Treatment</label>
                                                                                                <textarea name='treatment'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->treatment}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input type="text" id="patient_id"
                                                                                               class="form-control"
                                                                                               name="id" value="{{$data->id}}" hidden>
                                                                                        <input type="text" id="id"
                                                                                               class="form-control"
                                                                                               name="patient_id" value="<?php echo $id;?>" hidden>

                                                                                        <input type="text" id="patientrecoddateid"
                                                                                               class="form-control"
                                                                                               name="patientrecoddateid"
                                                                                               value="<?php echo $patientrecoddateid;?>" hidden>
                                                                                        <div class="col-md-12">
                                                                                            <button class="btn btn-login" id="SaveCharges"><i class="la la-check-square-o"></i> Save</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </form>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td><a href="{{ route('backend.DoctorTestCommentDelete',['id'=> $data->id])}}"
                                                            class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                            class="fas fa-trash"></i> Remove</a></td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="complex-header">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Patient Measurements</h4>
                                            <a class="heading-elements-toggle"><i
                                                        class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body card-dashboard">
                                                <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                    <thead>
                                                    <tr>

                                                        <th>Delete</th>
                                                        <th>Done by</th>
                                                        <th>Weight</th>
                                                        <th>BP Diastolic (mm Hg)</th>
                                                        <th>Head Circumference (cm)</th>
                                                        <th>Pulse (min)</th>
                                                        <th>BP Systoric (mm Hg)</th>
                                                        <th>Temp location</th>
                                                        <th>Respiration (min)</th>
                                                        <th>Oxygen Saturation (%)</th>
                                                        <th>BMI (Kg/m2)</th>
                                                        <th>Temperature (℃)</th>
                                                        <th>BMI status (Type)</th>
                                                        <th>Date Created</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listmeasurements as $data)
                                                        <tr>


                                                            <td>
                                                            <a href="{{ route('backend.DeletePatientMeasurement',['id'=> $data->id])}}"
                                                            class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                            class="fas fa-trash"></i> Remove</a></td>
                                                            <td>{{$data->name}}</td>
                                                            <td>{{$data->patientWeight}}</td>
                                                            <td>{{$data->patient_Diastolic}}</td>
                                                            <td>{{$data->patient_Circumference}}</td>
                                                            <td>{{$data->patient_Pulse}}</td>
                                                            <td>{{$data->patient_Systoric}}</td>
                                                            <td>{{$data->patient_Templocation}}</td>
                                                            <td>{{$data->patient_Respiration}}</td>
                                                            <td>{{$data->patient_OxygenSaturation}}</td>
                                                            <td>{{$data->patient_BMI}}</td>
                                                            <td>{{$data->patient_Temperature}}</td>
                                                            <td>{{$data->patient_BMIstatus}}</td>
                                                            <td>{{$data->created_at}}</td>

                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section id="complex-header">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Nurse's Comments</h4>
                                            <a class="heading-elements-toggle"><i
                                                        class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body card-dashboard">
                                                <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>Consultation Comment</th>
                                                        <th>Date Created</th>
                                                        <th>Done By</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listconslutationcomment as $data)
                                                        <tr>
                                                            <td>{{$data->consultation_comment}}</td>
                                                            <td>{{$data->created_at}}</td>
                                                            <td>{{$data->name}}</td>

                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id='complex-header'>
                            <div class='row'>
                                <div class='col-12'>
                                    <div class='card'>
                                        <div class='card-header'>
                                            <h4 class='card-title'>SOAP</h4>
                                            <a class='heading-elements-toggle'><i
                                                        class='la la-ellipsis-v font-medium-3'></i></a>
                                            <div class='heading-elements'>
                                                <ul class='list-inline mb-0'>
                                                    <li><a data-action='collapse'><i class='ft-minus'></i></a></li>
                                                    <li><a data-action='reload'><i class='ft-rotate-cw'></i></a></li>
                                                    <li><a data-action='expand'><i class='ft-maximize'></i></a></li>
                                                    <li><a data-action='close'><i class='ft-x'></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class='card-content collapse show'>
                                            <div class='card-body card-dashboard'>
                                                <table class='table table-striped table-bordered zero-configuration table-responsive'>
                                                    <thead>
                                                    <tr>
                                                        <th>subjective</th>
                                                        <th>objective</th>
                                                        <th>assessment</th>
                                                        <th>plan</th>
                                                        <th>Date Created</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listsoap as $data)
                                                        <tr>
                                                            <td>{{$data->subjective}}</td>
                                                            <td>{{$data->objective}}</td>
                                                            <td>{{$data->assessment}}</td>
                                                            <td>{{$data->plan}}</td>
                                                            <td>{{$data->created_at}}</td>
                                                            <td>

                                                                <button type='button'
                                                                        class='btn btn-login btn-min-width mr-1 mb-1'
                                                                        data-toggle='modal'
                                                                        data-target='#SOAP{{$data->id}}'>
                                                                    Edit
                                                                </button>
                                                                <!-- Modal -->
                                                                <div class='modal fade text-left' id='SOAP{{$data->id}}' tabindex='-1'
                                                                     role='dialog' aria-labelledby='myModalLabel1'
                                                                     aria-hidden='true'>
                                                                    <div class='modal-dialog' role='document'>
                                                                        <div class='modal-content'>
                                                                            <div class='modal-header'>
                                                                                <h4 class='modal-title' id='myModalLabel1'>Edit SOAP </h4>
                                                                                <button type='button' class='close' data-dismiss='modal'
                                                                                        aria-label='Close'>
                                                                                    <span aria-hidden='true'>&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class='modal-body'>
                                                                                <form class='form-horizontal form-simple' method='POST'
                                                                                      action='{{ url('SOAPEdit') }}'
                                                                                      enctype='multipart/form-data'>
                                                                                    {{ csrf_field() }}
                                                                                    @foreach($listsoap as $editsoap)
                                                                                        <div class='row  multi-field'>
                                                                                            <div class='col-md-12'>
                                                                                                <div class='form-group'>
                                                                                                    <label for='projectinput1'>Subjective</label>
                                                                                                    <textarea name='subjective'
                                                                                                              rows='5'
                                                                                                              class='form-control'>{{$editsoap->subjective}}</textarea>
                                                                                                    <input type='text' name='id' value='{{$data->id}}' hidden>
                                                                                                    <input type='text' name='patient_id' value='<?php echo $id;?>' hidden>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class='col-md-12'>
                                                                                                <div class='form-group'>
                                                                                                    <label for='projectinput1'>Objective</label>
                                                                                                    <textarea name='objective'
                                                                                                              rows='5'
                                                                                                              class='form-control'>{{$editsoap->objective}}</textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class='col-md-12'>
                                                                                                <div class='form-group'>
                                                                                                    <label for='projectinput1'>Assessment</label>
                                                                                                    <textarea name='assessment'
                                                                                                              rows='5'
                                                                                                              class='form-control'>{{$editsoap->assessment}}</textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class='col-md-12'>
                                                                                                <div class='form-group'>
                                                                                                    <label for='projectinput1'>Plan</label>
                                                                                                    <textarea name='plan'
                                                                                                              rows='5'
                                                                                                              class='form-control'>{{$editsoap->plan}}</textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                            <input type='text' id='patient_id'
                                                                                                   class='form-control'
                                                                                                   name='patient_id' value='<?php echo $id;?>' hidden>
                                                                                            <input type='text' id='patientrecoddateid'
                                                                                                   class='form-control'
                                                                                                   name='patientrecoddateid'
                                                                                                   value='<?php echo $patientrecoddateid;?>' hidden>
                                                                                            <div class='col-md-12'>
                                                                                                <button type='submit' class='btn btn-login'>
                                                                                                    <i class='la la-check-square-o'></i> Save
                                                                                                </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </form>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </td>

                                                            <td>
                                                                <a href='{{ route('backend.DeleteAddAppointmentLabo',['id'=> $data->id])}}'
                                                                   class='btn btn-login btn-min-width mr-1 mb-1'><i
                                                                            class='fas fa-trash'></i> Remove</a></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <section id='complex-header'>
                            <div class='row'>
                                <div class='col-12'>
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">laboratory result</h4>
                                            <a class="heading-elements-toggle"><i
                                                        class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body card-dashboard">
                                                <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>Patient Name</th>
                                                        <th>Patient ID</th>
                                                        <th>Age</th>
                                                        <th>Result</th>
                                                        <th>Time Tested</th>
                                                        <th>Lab User</th>
                                                        <th>Doctor</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listlaboratory as $data)
                                                        <tr>
                                                            <td>{{$data->beneficiary_name}}</td>
                                                            <td>{{$data->pid}}</td>
                                                            <td>{{$data->dateofbirth}}</td>
                                                            <td>
                                                                <a href="{{ route('backend.LaboratoryResultsPrint',['patientrecoddateid'=> $data->patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> View Other Results</a>
                                                                <a href="{{ route('backend.LaboratoryResultsPrintNFS',['patientrecoddateid'=> $data->patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> View NFS Results</a>
                                                            </td>
                                                            <td>{{$data->updated_at}}</td>
                                                            <td>{{$data->Laboratorytechnician}}</td>
                                                            <td>{{$data->name}}</td>

                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section id='complex-header'>
                            <div class='row'>
                                <div class='col-12'>
                                    <div class='card'>
                                        <div class='card-header'>
                                            <h4 class='card-title'>Dental</h4>
                                            <a class='heading-elements-toggle'><i
                                                        class='la la-ellipsis-v font-medium-3'></i></a>
                                            <div class='heading-elements'>
                                                <ul class='list-inline mb-0'>
                                                    <li><a data-action='collapse'><i class='ft-minus'></i></a></li>
                                                    <li><a data-action='reload'><i class='ft-rotate-cw'></i></a></li>
                                                    <li><a data-action='expand'><i class='ft-maximize'></i></a></li>
                                                    <li><a data-action='close'><i class='ft-x'></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class='card-content collapse show'>
                                            <div class='card-body card-dashboard'>
                                                <table class='table table-striped table-bordered zero-configuration table-responsive'>
                                                    <thead>
                                                    <tr>
                                                        <th>Consultation</th>
                                                        <th>History of present illness</th>
                                                        <th>Past Medical history</th>
                                                        <th>Past dental history</th>
                                                        <th>Extra-oral Exam</th>
                                                        <th>Intra-oral Exam</th>
                                                        <th>Examination of teeth</th>
                                                        <th>clinical finding</th>
                                                        <th>Investigation</th>
                                                        <th>Diagnosis</th>
                                                        <th>Treatment</th>
                                                        <th>Date Created</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listdental as $data)
                                                        <tr>
                                                            <td>{{$data->consultation}}</td>
                                                            <td>{{$data->Historyofpresentillness}}</td>
                                                            <td>{{$data->PastMedicalhistory}}</td>
                                                            <td>{{$data->Pastdentalhistory}}</td>
                                                            <td>{{$data->ExtraoralExam}}</td>
                                                            <td>{{$data->IntraoralExam}}</td>
                                                            <td>{{$data->Examinationofteeth}}</td>
                                                            <td>{{$data->clinicalfinding}}</td>
                                                            <td>{{$data->Investigation}}</td>
                                                            <td>{{$data->Diagnosis}}</td>
                                                            <td>{{$data->Treatment}}</td>
                                                            <td>{{$data->created_at}}</td>
                                                            <td>

                                                                <button type='button'
                                                                        class='btn btn-login btn-min-width mr-1 mb-1'
                                                                        data-toggle='modal'
                                                                        data-target='#dental{{$data->id}}'>
                                                                    Edit
                                                                </button>
                                                                <!-- Modal -->
                                                                <div class='modal fade text-left' id='dental{{$data->id}}' tabindex='-1'
                                                                     role='dialog' aria-labelledby='myModalLabel1'
                                                                     aria-hidden='true'>
                                                                    <div class='modal-dialog' role='document'>
                                                                        <div class='modal-content'>
                                                                            <div class='modal-header'>
                                                                                <h4 class='modal-title' id='myModalLabel1'>Edit SOAP </h4>
                                                                                <button type='button' class='close' data-dismiss='modal'
                                                                                        aria-label='Close'>
                                                                                    <span aria-hidden='true'>&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class='modal-body'>
                                                                                <form class='form-horizontal form-simple' method='POST'
                                                                                      action='{{ url('EditDoctorDentist') }}'
                                                                                      enctype='multipart/form-data'>
                                                                                    {{ csrf_field() }}

                                                                                    <div class='row  multi-field'>
                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Consultation</label>
                                                                                                <textarea name='consultation'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->consultation}}</textarea>
                                                                                                <input type='text' name='id' value='{{$data->id}}' hidden>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>History of present illness</label>
                                                                                                <textarea name='Historyofpresentillness'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->Historyofpresentillness}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Past Medical history</label>
                                                                                                <textarea name='PastMedicalhistory'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->PastMedicalhistory}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Past dental history</label>
                                                                                                <textarea name='Pastdentalhistory'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->Pastdentalhistory}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Extra-oral Exam</label>
                                                                                                <textarea name='ExtraoralExam'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->ExtraoralExam}}</textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Intra-oral Exam</label>
                                                                                                <textarea name='IntraoralExam'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->IntraoralExam}}</textarea>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Examination of teeth</label>
                                                                                                <textarea name='Examinationofteeth'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->Examinationofteeth}}</textarea>
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>clinical finding</label>
                                                                                                <textarea name='clinicalfinding'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->clinicalfinding}}</textarea>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Investigation</label>
                                                                                                <textarea name='Investigation'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->Investigation}}</textarea>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Diagnosis</label>
                                                                                                <textarea name='Diagnosis'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->Diagnosis}}</textarea>
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class='col-md-6'>
                                                                                            <div class='form-group'>
                                                                                                <label for='projectinput1'>Treatment</label>
                                                                                                <textarea name='Treatment'
                                                                                                          rows='5'
                                                                                                          class='form-control'>{{$data->Treatment}}</textarea>
                                                                                            </div>
                                                                                        </div>

                                                                                        <input type='text' id='patient_id'
                                                                                               class='form-control'
                                                                                               name='patient_id' value='<?php echo $id;?>' hidden>
                                                                                        <input type='text' id='patientrecoddateid'
                                                                                               class='form-control'
                                                                                               name='patientrecoddateid'
                                                                                               value='<?php echo $patientrecoddateid;?>' hidden>
                                                                                        <div class='col-md-12'>
                                                                                            <button type='submit' class='btn btn-login'>
                                                                                                <i class='la la-check-square-o'></i> Save
                                                                                            </button>
                                                                                        </div>

                                                                                    </div>
                                                                                </form>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </td>

                                                            <td>
                                                                <a href='{{ route('backend.DeleteDoctorDentist',['id'=> $data->id])}}'
                                                                   class='btn btn-login btn-min-width mr-1 mb-1'><i
                                                                            class='fas fa-trash'></i> Remove</a></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
                    type="text/javascript"></script>
            <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
            <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
                    type="text/javascript"></script>

        @endsection
