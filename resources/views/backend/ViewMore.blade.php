@extends('backend.layout.master')

@section('title', 'Visa Free Africa')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <section class="row" style="padding: 0 70px">
                    @foreach ($listapplications as $data)
                        <div class="col-md-6">
                            <div id="with-header" class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body border-top-blue-grey border-top-lighten-5 ">

                                        <h4 class="card-title">Applicant Information</h4>
                                        <p class="card-text"><strong>Name:</strong>   {{$data->name}}</p>
                                        <p class="card-text"><strong>Email:</strong>   {{$data->email}}</p>
                                        <p class="card-text"><strong>Phone Number:</strong>   {{$data->phonenumber}}</p>
                                        <p class="card-text"><strong>Date of Birth:</strong>  {{$data->dateofbirth}}</p>
                                        <p class="card-text"><strong>Gender:</strong>   {{$data->gender}}</p>
                                        <p class="card-text"><strong>Country:</strong>  {{$data->country}}</p>
                                        <p class="card-text"><strong>City:</strong>  {{$data->city}}</p>
                                        <p class="card-text"><strong>Passport/ID:</strong>  {{$data->passportid}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div id="with-header" class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body border-top-blue-grey border-top-lighten-5 ">
                                        <h4 class="card-title">Passport Picture</h4>
                                        <p class="card-text">
                                            <img src="passportpictures/{{$data->passportpicture}}" style="    width: 100%;">
                                        </p>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div id="with-header" class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body border-top-blue-grey border-top-lighten-5 ">
                                        <h4 class="card-title">Your Story</h4>
                                        <p class="card-text"> {{$data->message}}</p>

                                </div>
                            </div>
                        </div>
                        <a href="{{ route('backend.PdfView_',['id'=> $data->id])}}" class="btn btn-secondary btn-min-width mr-1 mb-1"><i class="fas fa-file"></i> Export to Pdf</a>


                    @endforeach
                    {{--<div class="btn-group" role="group" aria-label="Button group with nested dropdown">--}}
                    {{--<button class="btn btn-visablue dropdown-toggle dropdown-menu-right box-shadow-2 px-2"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="{{route('backend.PdfView',['download'=>'pdf'])}}" >Download pdf</a></button>--}}
                    {{--</div>--}}

                </section>


            </div>
        </div>
    </div>
    @endsection