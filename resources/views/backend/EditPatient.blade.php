@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">


        $(document).on('change', '#province', function() {

            var province =$('#province').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetDistrict",
                data: {
                    'province': province,
                },
                success: function (data) {
                    $("#district").html(data);
                }
            });
        });

        $(document).on('change', '#district', function() {

            var district =$('#district').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetSector",
                data: {
                    'district': district,
                },
                success: function (data) {
                    $("#sector").html(data);
                }
            });
        });

        $(document).on('change', '#sector', function() {

            var sector =$('#sector').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetCell",
                data: {
                    'sector': sector,
                },
                success: function (data) {
                    $("#cell").html(data);
                }
            });
        });

        $(document).on('change', '#cell', function() {

            var cell =$('#cell').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetVillage",
                data: {
                    'cell': cell,
                },
                success: function (data) {
                    $("#village").html(data);
                }
            });
        });
    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditPatient_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($editPatient as $datas)
                                            <div class="form-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Beneficiary Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->beneficiary_name}}"
                                                                   name="beneficiary_name">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Patient Country ID</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->patient_country_id}}"
                                                                   name="patient_country_id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Insurance  Name</label>
                                                            <select class="form-control"  id="basicSelect" name="insurance_patient_name" required>
                                                                <option value="{{$datas->insurance_patient_name}}">{{$datas->insurance_name}}</option>
                                                                @foreach($listinsu as $data)
                                                                    <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Insurance Card Number</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->insurance_card_number}}"
                                                                   name="insurance_card_number">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Gender</label>
                                                            <select class="form-control"  id="basicSelect" name="gender" required>
                                                                <option value="{{$datas->gender}}">{{$datas->gender}}</option>
                                                                <option value="male">male</option>
                                                                <option value="female">female</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Age</label>
                                                            <input type="text" class="form-control" id="date" name="dateofbirth" value="{{$datas->dateofbirth}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Beneficiary Telephone number</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->beneficiary_telephone_number}}"
                                                                   name="beneficiary_telephone_number">
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Copay %</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->copay}}"
                                                                   name="copay">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Province</label>
                                                            <select class="form-control"  id="province" name="beneficiary_province" required>
                                                                <option value="{{$datas->beneficiary_province}}">{{$datas->provincename}}</option>
                                                                @foreach($province as $data)
                                                                    <option value="{{$data->provincecode}}" >{{$data->provincename}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">District</label>
                                                            <select class="form-control"  id="district" name="beneficiary_district" required>
                                                                <option value="{{$datas->beneficiary_district}}">{{$datas->namedistrict}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Sector</label>
                                                            <select class="form-control"  id="sector" name="beneficiary_sector" required>
                                                                <option value="{{$datas->beneficiary_sector}}">{{$datas->namesector}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Cell</label>
                                                            <select class="form-control"  id="cell" name="beneficiary_cell" required>
                                                                <option value="{{$datas->beneficiary_sector}}">{{$datas->nameCell}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Viilage</label>
                                                            <select class="form-control"  id="village" name="beneficiary_village" required>
                                                                <option value="{{$datas->beneficiary_village}}">{{$datas->VillageName}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Copay Relationship</label>
                                                            <select class="form-control"  id="basicSelect" name="copay_relationship" required>
                                                                <option value="{{$datas->copay_relationship}}">{{$datas->copay_relationship}}</option>
                                                                <option value="Self">Self</option>
                                                                <option value="Child">Child</option>
                                                                <option value="Spouse">Spouse</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Affiliate Names</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->patient_names}}"
                                                                   name="patient_names">
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->id}}"
                                                                   name="id" hidden>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Affiliate Telephone number</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->patient_telephone_number}}"
                                                                   name="patient_telephone_number">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Insured Location</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{$datas->insured_location}}"
                                                                   name="insured_location">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                                @endforeach
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection