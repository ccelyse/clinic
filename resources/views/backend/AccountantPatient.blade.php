@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($NursePatient as $datas)
                                                <li class="list-group-item"><strong>Patient
                                                        Names:</strong> {{$datas->beneficiary_name}}</li>
                                                <li class="list-group-item"><strong>Patient Date of Birth
                                                        Id:</strong> {{$datas->dateofbirth}}</li>

                                                <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                                <?php
                                                $insurancename = $datas->insurance_name;
                                                $insuranceid = $datas->insurance_patient_name;
                                                $copay = $datas->copay;
                                                ?>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Payment Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            <?php
                                            $insurancedue = \App\PatientCharges::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                                                ->value('insurance_amount');
                                            $insurancedueprivate = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(insurance_amount) as insurance_amount'))
                                                ->value('insurance_amount');

                                            $patientdue = \App\PatientCharges::where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                                                ->value('patient_amount');

                                            $patientdueprivate = \App\PatientCharges::whereIn('insurance_id',[6])->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(patient_amount) as patient_amount'))
                                                ->value('patient_amount');

                                            $listpatientprice = \App\PatientCharges::whereNotIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                                                ->value('producttotalsum');

                                            $listpatientpriceprivate = \App\PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                                                ->value('producttotalsum');

                                            $getcopay = \App\Patient::where('id',$id)->value('copay');
                                            $calcopay = 100 - $getcopay;

                                            $secinsurancedue = ($listpatientprice * $getcopay)/100;
                                            $secpatientdue = ($listpatientprice * $calcopay)/100;

                                            $secRemainingrprivate = \App\PatientCharges::whereIn('patientcharges.insurance_id',[6])->where('patientcharges.patient_id',$id)->where('patientcharges.patientrecoddateid',$patientrecoddateid)
                                                ->select(DB::raw('SUM(medicine_service_price) as producttotalsum'))
                                                ->value('producttotalsum');

                                            $amount_paid = \App\AccountPatientClear::whereIn('insurance_id',[6])->where('patient_id',$id)->where('patientrecoddateid',$patientrecoddateid)
                                                ->value('amount_paid');

                                            $secremtotal =  $secRemainingrprivate + $secpatientdue;

                                            ?>

                                            @if($insuranceid == '6')
                                                    <li class="list-group-item"><strong>Insurance Due:</strong> <?php echo "$insurancedueprivate"; ?> FRW</li>
                                                    <li class="list-group-item"><strong>Patient Due:</strong> <?php echo "$patientdueprivate"; ?> FRW</li>
                                                    <li class="list-group-item"><strong>Paid Amount:</strong> <?php echo "$amount_paid";?> FRW</li>
                                            @else
                                                    <li class="list-group-item"><strong>Insurance Due:</strong> <?php echo "$secinsurancedue"; ?> FRW</li>
                                                    <li class="list-group-item"><strong>Patient Due:</strong> <?php echo "$secremtotal"; ?> FRW</li>
                                                    <li class="list-group-item"><strong>Paid Amount:</strong> <?php echo "$patient_amountpayed";?> FRW</li>
                                            @endif

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    @include('backend.AddChargesModule')
                                    <div class="form-grouppop">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#appointtodoctors">
                                            <i class="fas fa-money-bill-alt"></i> Process Payment
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="appointtodoctors" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            @if($insuranceid == '6')
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel1">Amount Due: <?php echo $listpatientpriceprivate; ?> FRW </h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="form-horizontal form-simple" method="POST"
                                                                  action="{{ url('AccountPatientClear') }}"
                                                                  enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <div class="row  multi-field">
                                                                    <div class="col-md-12" >
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Patient ID</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>" hidden>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12" hidden>
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Amount Due</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_amount_due" value="<?php echo $listpatientpriceprivate; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12" hidden>
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Insurance Due</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="insurance_due" value="0">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Paid Amount</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_amount_paid" >
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Payment Method</label>
                                                                            <select class="form-control" id="listofmedecinecharge"
                                                                                    name="paymentmethod"
                                                                                    required>
                                                                                <option value="{{$datas->paymentmethod}}">{{$datas->paymentmethod}}</option>
                                                                                <option value="Cash">Cash</option>
                                                                                <option value="POS">POS</option>
                                                                                <option value="Check">Check</option>
                                                                                <option value="None">None</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Payment Status</label>
                                                                            <select class="form-control" id="listofmedecinecharge"
                                                                                    name="chargesreasons"
                                                                                    required>
                                                                                <option value="Education">Education</option>
                                                                                {{--<option value="Ndengera Clinic">Ndengera Clinic</option>--}}
                                                                                <option value="Patient">Patient</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Payment Comment</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="paymentcomment" >
                                                                        </div>
                                                                    </div>
                                                                    {{--<input type="text" id="patient_id"--}}
                                                                    {{--class="form-control"--}}
                                                                    {{--name="chargesreasons" value="patientcharges" hidden>--}}
                                                                    <input type="text" id="patient_id"
                                                                           class="form-control"
                                                                           name="patient_id" value="<?php echo $id;?>" hidden>
                                                                    <input type="text" id="patientrecoddateid"
                                                                           class="form-control"
                                                                           name="patientrecoddateid"
                                                                           value="<?php echo $patientrecoddateid;?>" hidden>
                                                                    <input type="text" id="insurance_id"
                                                                           class="form-control"
                                                                           name="insurance_id"
                                                                           value="<?php echo $insurance_id;?>" hidden>
                                                                    <div class="col-md-12">
                                                                        <button type="submit" class="btn btn-login">
                                                                            <i class="la la-check-square-o"></i> Save
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel1">Amount Due: <?php echo $secremtotal; ?> FRW </h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="form-horizontal form-simple" method="POST"
                                                                  action="{{ url('AccountPatientClear') }}"
                                                                  enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <div class="row  multi-field">
                                                                    <div class="col-md-12" >
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Patient ID</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>" hidden>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12" hidden>
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Amount Due</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_amount_due" value="<?php echo $secremtotal; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12" hidden>
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Insurance Due</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="insurance_due" value="<?php echo $secinsurancedue; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Paid Amount</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="patient_amount_paid" >
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Payment Method</label>
                                                                            <select class="form-control" id="listofmedecinecharge"
                                                                                    name="paymentmethod"
                                                                                    required>
                                                                                <option value="{{$datas->paymentmethod}}">{{$datas->paymentmethod}}</option>
                                                                                <option value="Cash">Cash</option>
                                                                                <option value="POS">POS</option>
                                                                                <option value="Check">Check</option>
                                                                                <option value="None">None</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Payment Status</label>
                                                                            <select class="form-control" id="listofmedecinecharge"
                                                                                    name="chargesreasons"
                                                                                    required>
                                                                                <option value="Education">Education</option>
                                                                                {{--<option value="Ndengera Clinic">Ndengera Clinic</option>--}}
                                                                                <option value="Patient">Patient</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="projectinput1">Payment Comment</label>
                                                                            <input type="text" id="projectinput1"  class="form-control" name="paymentcomment" >
                                                                        </div>
                                                                    </div>
                                                                    {{--<input type="text" id="patient_id"--}}
                                                                    {{--class="form-control"--}}
                                                                    {{--name="chargesreasons" value="patientcharges" hidden>--}}

                                                                    <input type="text" id="patient_id"
                                                                           class="form-control"
                                                                           name="patient_id" value="<?php echo $id;?>" hidden>
                                                                    <input type="text" id="patientrecoddateid"
                                                                           class="form-control"
                                                                           name="patientrecoddateid"
                                                                           value="<?php echo $patientrecoddateid;?>" hidden>
                                                                    <input type="text" id="insurance_id"
                                                                           class="form-control"
                                                                           name="insurance_id"
                                                                           value="<?php echo $insurance_id;?>" hidden>
                                                                    <div class="col-md-12">
                                                                        <button type="submit" class="btn btn-login">
                                                                            <i class="la la-check-square-o"></i> Save
                                                                        </button>
                                                                    </div>

                                                                </div>
                                                            </form>

                                                        </div>

                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-grouppop">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#Remaining">
                                            <i class="fas fa-money-bill-alt"></i> Update Payment
                                        </button>
                                        <!-- Modal -->
                                        @foreach($remainingbalance as $remainingbalance_)
                                        <div class="modal fade text-left" id="Remaining" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Remaining Balance: {{$remainingbalance_->amount_due}} FRW </h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('UpdateAccountPatientClear') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="row  multi-field">
                                                                <div class="col-md-12" hidden>
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Patient ID</label>
                                                                        <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>">
                                                                        <input type="text" id="projectinput1"  class="form-control" name="payment_id" value="{{$remainingbalance_->id}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" hidden>
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Amount Due</label>
                                                                        <input type="number" id="projectinput1"  class="form-control" name="remainingbalance" value="{{$remainingbalance_->amount_due}}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" hidden>
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Insurance Due</label>
                                                                        <input type="text" id="projectinput1"  class="form-control" name="insurance_due" value="<?php echo $insurancedue; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Paid Amount</label>
                                                                        <input type="number" id="projectinput1"  class="form-control" step=".01" name="patient_amount_paid" value="{{$remainingbalance_->amount_paid}}" required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Payment Method</label>
                                                                        <select class="form-control" id="listofmedecinecharge"
                                                                                name="paymentmethod"
                                                                                required>
                                                                            <option value="{{$datas->paymentmethod}}">{{$datas->paymentmethod}}</option>
                                                                            <option value="Cash">Cash</option>
                                                                            <option value="POS">POS</option>
                                                                            <option value="Check">Check</option>
                                                                            <option value="None">None</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Payment Comment</label>
                                                                        <input type="text" id="projectinput1"  class="form-control" name="paymentcomment" >
                                                                    </div>
                                                                </div>
                                                                <input type="text" id="patient_id"
                                                                       class="form-control"
                                                                       name="chargesreasons" value="patientcharges" hidden>

                                                                <input type="text" id="patient_id"
                                                                       class="form-control"
                                                                       name="patient_id" value="<?php echo $id;?>" hidden>
                                                                <input type="text" id="patientrecoddateid"
                                                                       class="form-control"
                                                                       name="patientrecoddateid"
                                                                       value="<?php echo $patientrecoddateid;?>" hidden>
                                                                <input type="text" id="insurance_id"
                                                                       class="form-control"
                                                                       name="insurance_id"
                                                                       value="<?php echo $insurance_id;?>" hidden>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Save
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                            @endforeach
                                    </div>
                                    <a href="{{ route('backend.InvoiceTobeGenerated',['patientrecoddateid'=> $patientrecoddateid])}}"
                                       class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-file-invoice"></i>Invoice Details</a>


                                </div>
                            </div>
                        </div>

                    </div>

                </section>

                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Invoice</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Invoice Insurance</th>
                                                <th>Invoice Private</th>
                                                <th>Patient Names</th>
                                                <th>Insurance Name</th>
                                                <th>Amount Due</th>
                                                <th>Amount Paid</th>
                                                <th>Payment Method</th>
                                                <th>Date Created</th>
                                                <th>Remove</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($invoicedetails as $data)
                                                <tr>
                                                    <td><a href="{{ route('backend.InvoiceTobeGeneratedSummary',['patientrecoddateid'=> $data->patientrecoddateid])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"> Invoice Insurance</a></td>
                                                    <td><a href="{{ route('backend.InvoiceTobeGeneratedSummaryPrivate',['patientrecoddateid'=> $data->patientrecoddateid])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"> Invoice Private</a></td>
                                                    <td>{{$data->beneficiary_name}}</td>
                                                    <td>{{$data->insurance_name}}</td>
                                                    <td>RWF {{$data->amount_due}}</td>
                                                    <td>RWF {{$data->amount_paid}}</td>
                                                    <td>RWF {{$data->paymentmethod}}</td>
                                                    <td>{{$data->created_at}}</td>

                                                    <td><a href="{{ route('backend.DeletePayedSales',['id'=> $data->id])}}"
                                                    class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
