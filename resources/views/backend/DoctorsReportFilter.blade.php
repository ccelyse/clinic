@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <form
                                method="POST"
                                action="{{ url('DoctorsReportFilter') }}"
                                enctype="multipart/form-data" style="width: 100%;display: inline-flex;;">

                            {{ csrf_field() }}

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="date">From Date</label>
                                    <input type="date" class="form-control" id="date" name="fromdate"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="date">To Date</label>
                                    <input type="date" class="form-control" id="date" name="todate"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-12" style="    padding-top: 25px;
">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-login">
                                        <i class="la la-check-square-o"></i> Filter
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Waiting  Patients</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Patient Name</th>
                                                <th>Patient ID</th>
                                                <th>Age</th>
                                                <th>Result</th>
                                                <th>Time Tested</th>
                                                <th>Doctor</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listpatient as $data)
                                                <tr>
                                                    <td>{{$data->beneficiary_name}}</td>
                                                    <td>{{$data->pid}}</td>
                                                    <td>{{$data->dateofbirth}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.LaboratoryResultsPrint',['patientrecoddateid'=> $data->patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> View Other Results</a>
                                                        <a href="{{ route('backend.LaboratoryResultsPrintNFS',['patientrecoddateid'=> $data->patientrecoddateid])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> View NFS Results</a>
                                                    </td>
                                                    <td>{{$data->updated_at}}</td>
                                                    <td>{{$data->name}}</td>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
