@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/plugins/forms/wizard.css">
    <style>
        /*.app-content .wizard > .actions {*/
            /*position: relative;*/
            /*display: block;*/
            /*text-align: right;*/
            /*padding: 20px;*/
            /*padding-top: 0;*/
            /*display: none;*/
        /*}*/
        .app-content .wizard > .actions > ul > a[href='#finish'] {
            display: none !important;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        function FillBilling(f) {
            if(f.billingtoo.selected == 'Private') {
                f.billingname.value = f.shippingname.value;
                f.billingcity.value = f.shippingcity.value;
            }
        }
    </script>
    <script type="text/javascript">


        $(document).on('change', '#province', function() {

            var province =$('#province').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetDistrict",
                data: {
                    'province': province,
                },
                success: function (data) {
                    $("#district").html(data);
                }
            });
        });

        $(document).on('change', '#district', function() {

            var district =$('#district').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetSector",
                data: {
                    'district': district,
                },
                success: function (data) {
                    $("#sector").html(data);
                }
            });
        });

        $(document).on('change', '#sector', function() {

            var sector =$('#sector').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetCell",
                data: {
                    'sector': sector,
                },
                success: function (data) {
                    $("#cell").html(data);
                }
            });
        });

        $(document).on('change', '#cell', function() {

            var cell =$('#cell').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "GetVillage",
                data: {
                    'cell': cell,
                },
                success: function (data) {
                    $("#village").html(data);
                }
            });
        });
    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">
                        <section id="number-tabs">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            {{--<h4 class="card-title">Form wizard with number tabs</h4>--}}
                                            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body">
                                                    <form class="number-tab-steps wizard-circle" method="POST" action="{{ url('Reception_') }}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <!-- Step 1 -->
                                                    <h6>Beneficiary Information</h6>
                                                    <fieldset>
                                                        <div class="row">

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Beneficiary Name</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value="{{ old('beneficiary_name') }}" placeholder="Beneficiary Name"
                                                                           name="beneficiary_name" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Beneficiary Telephone number</label>
                                                                    <input id="projectinput1" class="form-control" placeholder="Beneficiary Telephone number"
                                                                           name="beneficiary_telephone_number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                           type = "number"  value="{{ old('beneficiary_telephone_number') }}" pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Gender</label>
                                                                    <select class="form-control"  id="basicSelect" name="gender" required>
                                                                        <option value="{{ old('gender') }}">{{ old('gender') }}</option>
                                                                        <option value="male">male</option>
                                                                        <option value="female">female</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Age</label>
                                                                    <input type="number" class="form-control" id="date" value="{{ old('dateofbirth') }}" name="dateofbirth" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Beneficiary Country ID</label>
                                                                    <input  id="projectinput1" class="form-control"
                                                                            name="patient_country_id" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                            type = "number" value="{{ old('patient_country_id') }}" pattern="[+]{1}[0-9]{12}" maxlength="16" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <!-- Step 2 -->
                                                    <h6>Beneficiary Location</h6>
                                                    <fieldset>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Province</label>
                                                                    <select class="form-control"  id="province" name="beneficiary_province" required>
                                                                        <option value="{{ old('email') }}">{{ old('beneficiary_province') }}</option>
                                                                        <option value=""></option>
                                                                        @foreach($province as $data)
                                                                            <option value="{{$data->provincecode}}" >{{$data->provincename}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">District</label>
                                                                    <select class="form-control"  id="district" name="beneficiary_district">

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Sector</label>
                                                                    <select class="form-control"  id="sector" name="beneficiary_sector">
                                                                        <option value=""></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Cell</label>
                                                                    <select class="form-control"  id="cell" name="beneficiary_cell">
                                                                        <option value=""></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Village</label>
                                                                    <select class="form-control"  id="village" name="beneficiary_village" >
                                                                        <option value=""></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <!-- Step 3 -->
                                                    <h6>Affiliate Information</h6>
                                                    <fieldset>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Affiliate Relationship</label>
                                                                    <select class="form-control"  id="basicSelect" name="copay_relationship" required>
                                                                        <option value="{{ old('copay_relationship') }}">{{ old('copay_relationship') }}</option>
                                                                        <option value="Self">Self</option>
                                                                        <option value="Child">Child</option>
                                                                        <option value="Spouse">Spouse</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Insurance  Name</label>
                                                                    <select class="form-control"  id="basicSelect" name="insurance_patient_name" onclick="FillBilling(this.form)" required>
                                                                        <option value="{{ old('insurance_patient_name') }}">{{ old('insurance_patient_name') }}</option>
                                                                        @foreach($listinsu as $data)
                                                                            <option value="{{$data->id}}" >{{$data->insurance_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Insurance Card Number</label>
                                                                    <input type="text" id="projectinput1" class="form-control" placeholder="Insurance Card Number"
                                                                           name="insurance_card_number" value="{{ old('insurance_card_number') }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Affiliate Names</label>
                                                                    <input type="text" id="projectinput1" class="form-control" placeholder="Affiliate Names"
                                                                           name="patient_names" value="{{ old('patient_names') }}" value="{{ old('email') }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Affiliate Telephone number</label>
                                                                    <input  id="projectinput1" class="form-control" placeholder="Patient Telephone number"
                                                                            name="patient_telephone_number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                            type = "number" value="{{ old('patient_telephone_number') }}" pattern="[+]{1}[0-9]{12}" maxlength="10" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Copay (%)</label>
                                                                    <input type="number" id="projectinput1" class="form-control" placeholder="Copay"
                                                                           name="copay" value="{{ old('copay') }}" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Insured Location</label>
                                                                    <input type="text" id="projectinput1" class="form-control" placeholder="Insured Location"
                                                                           name="insured_location" value="{{ old('insured_location') }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <button type="submit" class="btn btn-login">
                                                                <i class="la la-check-square-o"></i> Save
                                                            </button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            <div class="content-body">

            </div>
        </div>
    </div>


    <script src="backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>

@endsection
