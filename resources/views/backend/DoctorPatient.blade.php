@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            @foreach($NursePatient as $datas)
                                                <li class="list-group-item"><strong>Patient
                                                        Names:</strong> {{$datas->patient_names}}</li>
                                                <li class="list-group-item"><strong>Patient Country
                                                        Id:</strong> {{$datas->patient_country_id}}</li>
                                                <li class="list-group-item"><strong>Patient Insurance Name
                                                        :</strong> {{$datas->insurance_name}}</li>
                                                <li class="list-group-item"><strong>Patient Gender
                                                        :</strong> {{$datas->gender}}</li>
                                                <li class="list-group-item"><strong>PID :</strong> {{$datas->pid}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#defaultcharges">
                                            Add Charges
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="defaultcharges" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Add Charges </h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('AddPatientCharges') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="row  multi-field">
                                                                <script type="text/javascript">
                                                                    $(document).ready(function () {
                                                                        $('.multi-field-wrapper').each(function () {
                                                                            var $wrapper = $('.multi-fields', this);
                                                                            $(".add-field", $(this)).click(function (e) {
                                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                                            });
                                                                            $('.multi-field .remove-field', $wrapper).click(function () {
                                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                                    $(this).parent('.multi-field').remove();
                                                                            });
                                                                        });
                                                                    });

                                                                    function getInsurenceIdCharge(val) {
                                                                        $.ajax({
                                                                            headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                            },
                                                                            type: "POST",
                                                                            url: "PatientChargesNurse",
                                                                            data: 'insurenceidcharge=' + val,
                                                                            success: function (data) {
                                                                                $("#listofmedecinecharge").html(data);
                                                                                SelectMedicine();
                                                                            }
                                                                        })
//
                                                                    }

                                                                    function SelectMedicine(val) {
                                                                        $.ajax({
                                                                            headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                            },
                                                                            type: "POST",
                                                                            url: "PatientChargeSelectMedicine",
                                                                            data: 'Medicine_Service=' + val,
                                                                            success: function (data) {
                                                                                $("#fees").html(data);
                                                                            }
                                                                        });
                                                                    }

                                                                </script>

                                                                <div class="col-md-12" hidden>
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Patient ID</label>
                                                                        <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Insurance
                                                                            Name</label>

                                                                        <select class="form-control" id="country-list"
                                                                                name="insurance_id_"
                                                                                onChange="getInsurenceIdCharge(this.value);"
                                                                                required>
                                                                            <option value disabled selected>Select
                                                                                Insurance Name
                                                                            </option>
                                                                            @foreach($listinsu as $insurance)
                                                                                <option value="{{$insurance->id}}">{{$insurance->insurance_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Medicine/Service
                                                                            Name</label>
                                                                        <select class="form-control" id="listofmedecinecharge"
                                                                                name="Medicine_service_name"
                                                                                onChange="SelectMedicine(this.value);"
                                                                                required>
                                                                            <option value="">Select Medicine</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Medicine
                                                                            Price</label>
                                                                        <select class="form-control" id="fees"
                                                                                name="medicine_service_price" required>
                                                                            <option value="">Select Price</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Save
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                        {{--<form class="form-horizontal form-simple" method="POST" action="#" enctype="multipart/form-data">--}}
                                                        {{--{{ csrf_field() }}--}}

                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="submit" class="btn btn-login">--}}
                                                        {{--<i class="la la-check-square-o"></i> Save--}}
                                                        {{--</button>--}}
                                                        {{--<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>--}}
                                                        {{--</div>--}}
                                                        {{--</form>--}}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#appointtodoctors">
                                            Appoint to Doctor
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="appointtodoctors" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Appoint to Doctor </h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('AddAppointmentDoctors') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="row  multi-field">
                                                                <div class="col-md-12" hidden>
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Patient ID</label>
                                                                        <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Doctors List</label>

                                                                        <select class="form-control" id="country-list" name="doctor_id" onChange="getState(this.value);" required>
                                                                            @foreach($listusers as $users)
                                                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Save
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                        {{--<form class="form-horizontal form-simple" method="POST" action="#" enctype="multipart/form-data">--}}
                                                        {{--{{ csrf_field() }}--}}

                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="submit" class="btn btn-login">--}}
                                                        {{--<i class="la la-check-square-o"></i> Save--}}
                                                        {{--</button>--}}
                                                        {{--<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>--}}
                                                        {{--</div>--}}
                                                        {{--</form>--}}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#appointlabo">
                                            Add  Laboratory Test
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="appointlabo" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Add Laboratory Test</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('AddAppointmentLabo') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="row  multi-field">
                                                                <div class="col-md-12" hidden>
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Patient ID</label>
                                                                        <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="<?php echo $id;?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Lab Test</label>

                                                                        <select class="form-control" id="country-list" name="lab_tech" onChange="getState(this.value);" required>
                                                                            @foreach($listlabotest as $labotest)
                                                                                <option value="{{$labotest->id}}">{{$labotest->labotest_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Save
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                        {{--<form class="form-horizontal form-simple" method="POST" action="#" enctype="multipart/form-data">--}}
                                                        {{--{{ csrf_field() }}--}}

                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="submit" class="btn btn-login">--}}
                                                        {{--<i class="la la-check-square-o"></i> Save--}}
                                                        {{--</button>--}}
                                                        {{--<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>--}}
                                                        {{--</div>--}}
                                                        {{--</form>--}}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                data-toggle="modal"
                                                data-target="#confirmappointment">
                                            Confirm Appointment
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="confirmappointment" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel1"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel1">Confirm Appointment</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-simple" method="POST"
                                                              action="{{ url('ConfirmAppointment') }}"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}

                                                            <div class="row  multi-field">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="date">Date</label>

                                                                        <input type="date" class="form-control" id="date" name="dateconfirm"
                                                                        >
                                                                    </div>
                                                                    <input type="text" name="patient_id" value="<?php echo $id;?>">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="projectinput1">Time</label>
                                                                        <input type="time" class="form-control" id="time" name="timeconfirm" value="13:45:00">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-login">
                                                                        <i class="la la-check-square-o"></i> Save
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <h4 class="card-title">SOAP</h4>
                                <form class="form-horizontal form-simple" method="POST"
                                      action="{{ url('SOAP') }}"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="row  multi-field">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Subjective</label>
                                                <textarea name="subjective"
                                                          rows="5"
                                                          class="form-control"></textarea>
                                                <input type="text" name="patient_id" value="<?php echo $id;?>" hidden>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Objective</label>
                                                <textarea name="objective"
                                                          rows="5"
                                                          class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Assessment</label>
                                                <textarea name="assessment"
                                                          rows="5"
                                                          class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Plan</label>
                                                <textarea name="plan"
                                                          rows="5"
                                                          class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-login">
                                                <i class="la la-check-square-o"></i> Save
                                            </button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">SOAP</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>subjective</th>
                                                <th>objective</th>
                                                <th>assessment</th>
                                                <th>plan</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listsoap as $data)
                                                <tr>
                                                    <td>{{$data->subjective}}</td>
                                                    <td>{{$data->objective}}</td>
                                                    <td>{{$data->assessment}}</td>
                                                    <td>{{$data->plan}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>

                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#SOAP{{$data->id}}">
                                                            Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="SOAP{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Edit SOAP </h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('SOAPEdit') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            @foreach($listsoap as $editsoap)
                                                                                <div class="row  multi-field">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Subjective</label>
                                                                                            <textarea name="subjective"
                                                                                                      rows="5"
                                                                                                      class="form-control">{{$editsoap->subjective}}</textarea>
                                                                                            <input type="text" name="id" value="{{$data->id}}" hidden>
                                                                                            <input type="text" name="patient_id" value="<?php echo $id;?>" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Objective</label>
                                                                                            <textarea name="objective"
                                                                                                      rows="5"
                                                                                                      class="form-control">{{$editsoap->objective}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Assessment</label>
                                                                                            <textarea name="assessment"
                                                                                                      rows="5"
                                                                                                      class="form-control">{{$editsoap->assessment}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Plan</label>
                                                                                            <textarea name="plan"
                                                                                                      rows="5"
                                                                                                      class="form-control">{{$editsoap->plan}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <button type="submit" class="btn btn-login">
                                                                                            <i class="la la-check-square-o"></i> Save
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </td>

                                                    <td>
                                                        <a href="{{ route('backend.DeleteAddAppointmentLabo',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient to Doctor Appointments</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Doctor Name</th>
                                                <th>Email</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listdoctorsapp as $data)
                                                <tr>
                                                    <td>{{$data->name}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.DeleteAddAppointmentDoctors',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Doctor Appointments Confirmation</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Appointment Date</th>
                                                <th>Appointment Time</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listappointdoctor as $data)
                                                <tr>
                                                    <td>{{$data->dateconfirm}}</td>
                                                    <td>{{$data->timeconfirm}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#appointdoc{{$data->id}}">
                                                            Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="appointdoc{{$data->id}}"
                                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">
                                                                           Update Appointment</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('ConfirmAppointmentEdit') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="date">Date</label>

                                                                                        <input type="date" class="form-control" id="date" value="{{$data->dateconfirm}}" name="dateconfirm"
                                                                                        >
                                                                                    </div>
                                                                                    <input type="text" name="patient_id" value="<?php echo $id;?>" hidden>
                                                                                    <input type="text" name="id" value="{{$data->id}}">
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Time</label>
                                                                                        <input type="time" class="form-control" id="time" name="timeconfirm" value="{{$data->timeconfirm}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Update
                                                                                    </button>
                                                                                </div>

                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--<a href="{{ route('backend.EditPatient',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Edit</a>--}}
                                                    </td>

                                                    <td>
                                                        <a href="{{ route('backend.ConfirmAppointmentDelete',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Charges</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Insurance Name</th>
                                                <th>Medicine/Service Price</th>
                                                <th>Payment Status</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listpatientcharges as $data)
                                                <tr>
                                                    <td>{{$data->insurance_name}}</td>
                                                    <td>{{$data->medicinepriceinsurance}}</td>
                                                    <td><button type="button" class="btn btn-danger btn-min-width btn-glow mr-1 mb-1">{{$data->payment_status}}</button></td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.DeletePatientCharges',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient Laboratory Test</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Laboratory Test</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listlaboapp as $data)
                                                <tr>
                                                    <td>{{$data->labotest_name}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.DeleteAddAppointmentLabo',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
