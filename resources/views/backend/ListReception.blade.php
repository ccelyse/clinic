@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/plugins/forms/wizard.css">
    <style>
        /*.app-content .wizard > .actions {*/
            /*position: relative;*/
            /*display: block;*/
            /*text-align: right;*/
            /*padding: 20px;*/
            /*padding-top: 0;*/
            /*display: none;*/
        /*}*/
        .app-content .wizard > .actions > ul > a[href='#finish'] {
            display: none !important;
        }
        /*.pagination li{*/
            /*color: #1E9FF2;*/
            /*border: 1px solid #BABFC7;*/
            /*padding: 5px 15px;*/
        /*}*/
        /*.active{*/
            /*color: #FFF !important;*/
            /*background-color: #1E9FF2;*/
            /*border-color: #1E9FF2;*/
        /*}*/
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $('.search-box input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputVal = $(this).val();
                var resultDropdown = $(this).siblings(".result");

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchReception",
                    data: {
                        'patientname': inputVal,

                    },
                    success: function (data) {
                        $("#display").html(data);
                    }
                });
            });
        });


    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">
                        <section id="complex-header">
                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-lg-12">
                                                <div class="search-box">
                                                    <fieldset class="form-group position-relative mb-0">
                                                        <input type="text" class="form-control form-control-xl input-xl" id="searchpatient" placeholder="Search Patient">
                                                    </fieldset>
                                                </div>
                                                <table class="table table-striped table-bordered  table-responsive" style="margin-top: 10px">
                                                    <thead>
                                                    <tr>
                                                        <th>Beneficiary New Record</th>
                                                        <th>Beneficiary Name</th>
                                                        <th>Beneficiary Gender</th>
                                                        <th>Beneficiary Phone number</th>
                                                        <th>Beneficiary Insurance</th>
                                                        <th>Beneficiary PID</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody id="display">
                                                    </tbody>



                                                </table>
                                            </div>
                                            {{--<div class="col-lg-12" style="padding-top: 10px">--}}
                                            {{--<fieldset class="form-group position-relative mb-0">--}}
                                            {{--<button type="submit" class="btn btn-login">--}}
                                            {{--<i class="la la-check-square-o"></i> search--}}
                                            {{--</button>--}}
                                            {{--</fieldset>--}}
                                            {{--</div>--}}

                                            {{--<div id="display"></div>--}}

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                        <section id="complex-header">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Patient List</h4>
                                            <a class="heading-elements-toggle"><i
                                                        class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body card-dashboard">
                                                <table id="receptiontable" class="table table-striped table-bordered zero-configuration table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>New Record</th>
                                                        <th>Beneficiary Name</th>
                                                        <th>Beneficiary Name Country ID</th>
                                                        <th>Insurance Name</th>
                                                        <th>Insurance Card Number</th>
                                                        <th>Gender</th>
                                                        <th>Date of Birth</th>
                                                        <th>Patient Telephone number</th>
                                                        <th>Copay</th>
                                                        <th>Copay Relationship</th>
                                                        <th>Affiliate Name</th>
                                                        <th>Affiliate Telephone number</th>
                                                        <th>Insured Location</th>
                                                        <th>PID</th>
                                                        <th>Date Created</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($listpatient as $data)
                                                        <tr>
                                                            <td>
                                                                <button type="button"
                                                                        class="btn btn-login btn-min-width mr-1 mb-1"
                                                                        data-toggle="modal"
                                                                        data-target="#appointment{{$data->id}}">
                                                                    New Record
                                                                </button>
                                                                <!-- Modal -->
                                                                <div class="modal fade text-left" id="appointment{{$data->id}}" tabindex="-1"
                                                                     role="dialog" aria-labelledby="myModalLabel1"
                                                                     aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" id="myModalLabel1">New Record</h4>
                                                                                <button type="button" class="close" data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form class="form-horizontal form-simple" method="POST"
                                                                                      action="{{ url('NewRecordHistory') }}"
                                                                                      enctype="multipart/form-data">
                                                                                    {{ csrf_field() }}
                                                                                    <div class="row  multi-field">
                                                                                        <div class="col-md-12" hidden>
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Patient ID</label>
                                                                                                <input type="text" id="projectinput1"  class="form-control" name="patient_id" value="{{$data->id}}">
                                                                                                {{--<input type="text" id="projectinput1"  class="form-control" name="patient_id" value="{{$data->id}}">da--}}
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Select Date of service</label>
                                                                                                <input type="date" class="form-control" id="date" name="newrecorddate">
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-12">
                                                                                            <button type="submit" class="btn btn-login">
                                                                                                <i class="la la-check-square-o"></i> Save
                                                                                            </button>
                                                                                        </div>

                                                                                    </div>
                                                                                </form>

                                                                                    <div class="card">
                                                                                        <div class="card-header">
                                                                                            <h4 class="card-title">Patient Information</h4>
                                                                                            <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                                                            <div class="heading-elements">
                                                                                                <ul class="list-inline mb-0">
                                                                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                                                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="card-content collapse show">
                                                                                            <div class="card-body">
                                                                                                <ul class="list-group">
                                                                                                    @php
                                                                                                        $id = $data->id;
                                                                                                        $listpatientdaterecord= \App\PatientNewRecord::where('patient_id',$id)->get();
                                                                                                    @endphp

                                                                                                    @foreach($listpatientdaterecord as $datepatient)
                                                                                                        <li class="list-group-item"><strong>Record Date:</strong> {{$datepatient->patientrecorddate}}</li>
                                                                                                    @endforeach
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>


                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                            <td><a href="{{ route('backend.EditPatient',['id'=> $data->id])}}">{{$data->beneficiary_name}}</a></td>
                                                            <td>{{$data->patient_country_id}}</td>
                                                            <td>{{$data->insurance_name}}</td>
                                                            <td>{{$data->insurance_card_number}}</td>
                                                            <td>{{$data->gender}}</td>
                                                            <td>{{$data->dateofbirth}}</td>
                                                            <td>{{$data->beneficiary_telephone_number}}</td>
                                                            <td>{{$data->copay}}</td>
                                                            <td>{{$data->copay_relationship}}</td>
                                                            <td>{{$data->patient_names}}</td>
                                                            <td>{{$data->patient_telephone_number}}</td>
                                                            <td>{{$data->insured_location}}</td>
                                                            <td>{{$data->pid}}</td>
                                                            <td>{{$data->created_at}}</td>
                                                            <td>
                                                                <a href="{{ route('backend.EditPatient',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Edit</a>
                                                            </td>

                                                            <td><a href="{{ route('backend.DeletePatient',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>

                                                        </tr>
                                                    @endforeach
                                                </table>
                                                {{--{{ $listpatient->links() }}--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            <div class="content-body">

            </div>
        </div>
    </div>


    <script src="backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/forms/wizard-steps.js"></script>
    <script src="backend/app-assets/vendors/js/extensions/jquery.steps.min.js"></script>

@endsection
