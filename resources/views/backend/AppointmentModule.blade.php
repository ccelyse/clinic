<div class="form-grouppop">
    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
            data-toggle="modal"
            data-target="#addlaboratory">
        Add lab tests
    </button>
    <!-- Modal -->
    <div class="modal fade text-left" id="addlaboratory" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="" id="labodata">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Add Laboratory</h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-simple" method="POST"
                          action="{{ url('AddLaboratoryService') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row  multi-field">
                            <div class="col-md-12">
                                <h3>BIOCHIMIE</h3>
                                <?php
                                    $medicinecategory = 'BIOCHIMIE';
                                $getpatientlabos = \App\MediService::where('medicinecategory', 'like', '%' . $medicinecategory . '%')->where('department_name_id','11')->where('insurance_medicine_id',$insurance_id)->orderBy('medicinename')->get();
                                ?>
                                @foreach($getpatientlabos as $labodata)
                                    <div class="form-group" style="display:contents">
                                        <div class='custom-control custom-checkbox' style='display: inline-block;margin: 5px;'>
                                            <input type='checkbox' class='custom-control-input' name='labodata[]' value='{{$labodata->id}}' id='{{$labodata->id}}'>
                                            <label class='custom-control-label' for='{{$labodata->id}}'>{{$labodata->medicinename}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="col-md-12">
                                <h3>HEMATOLOGIE</h3>
                                <?php
                                $medicinecategory = 'HEMATOLOGIE';
                                $getpatientlabos = \App\MediService::where('medicinecategory', 'like', '%' . $medicinecategory . '%')->where('department_name_id','11')->where('insurance_medicine_id',$insurance_id)->orderBy('medicinename')->get();
                                ?>
                                @foreach($getpatientlabos as $labodata)
                                    <div class="form-group" style="display:contents">
                                        <div class='custom-control custom-checkbox' style='display: inline-block;margin: 5px;'>
                                            <input type='checkbox' class='custom-control-input' name='labodata[]' value='{{$labodata->id}}' id='{{$labodata->id}}'>
                                            <label class='custom-control-label' for='{{$labodata->id}}'>{{$labodata->medicinename}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="col-md-12">
                                <h3>PARASITOLOGIE</h3>
                                <?php
                                $medicinecategory = 'PARASITOLOGIE';
                                $getpatientlabos = \App\MediService::where('medicinecategory', 'like', '%' . $medicinecategory . '%')->where('department_name_id','11')->where('insurance_medicine_id',$insurance_id)->orderBy('medicinename')->get();
                                ?>
                                @foreach($getpatientlabos as $labodata)
                                    <div class="form-group" style="display:contents">
                                        <div class='custom-control custom-checkbox' style='display: inline-block;margin: 5px;'>
                                            <input type='checkbox' class='custom-control-input' name='labodata[]' value='{{$labodata->id}}' id='{{$labodata->id}}'>
                                            <label class='custom-control-label' for='{{$labodata->id}}'>{{$labodata->medicinename}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            <div class="col-md-12">
                                <h3>RADIOLOGIE</h3>
                                <?php
                                $medicinecategory = 'RADIOLOGIE';
                                $getpatientlabos = \App\MediService::where('medicinecategory', 'like', '%' . $medicinecategory . '%')->where('department_name_id','11')->where('insurance_medicine_id',$insurance_id)->orderBy('medicinename')->get();
                                ?>
                                @foreach($getpatientlabos as $labodata)
                                    <div class="form-group" style="display:contents">
                                        <div class='custom-control custom-checkbox' style='display: inline-block;margin: 5px;'>
                                            <input type='checkbox' class='custom-control-input' name='labodata[]' value='{{$labodata->id}}' id='{{$labodata->id}}'>
                                            <label class='custom-control-label' for='{{$labodata->id}}'>{{$labodata->medicinename}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            <div class="col-md-12">
                                <h3>SEROLOGIE</h3>
                                <?php
                                $medicinecategory = 'SEROLOGIE';
                                $getpatientlabos = \App\MediService::where('medicinecategory', 'like', '%' . $medicinecategory . '%')->where('department_name_id','11')->where('insurance_medicine_id',$insurance_id)->orderBy('medicinename')->get();
                                ?>
                                @foreach($getpatientlabos as $labodata)
                                    <div class="form-group" style="display:contents">
                                        <div class='custom-control custom-checkbox' style='display: inline-block;margin: 5px;'>
                                            <input type='checkbox' class='custom-control-input' name='labodata[]' value='{{$labodata->id}}' id='{{$labodata->id}}'>
                                            <label class='custom-control-label' for='{{$labodata->id}}'>{{$labodata->medicinename}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="col-md-12">
                                <h3>BACTERIOLOGIE</h3>
                                <?php
                                $medicinecategory = 'BACTERIOLOGIE';
                                $getpatientlabos = \App\MediService::where('medicinecategory', 'like', '%' . $medicinecategory . '%')->where('department_name_id','11')->where('insurance_medicine_id',$insurance_id)->orderBy('medicinename')->get();
                                ?>
                                @foreach($getpatientlabos as $labodata)
                                    <div class="form-group" style="display:contents">
                                        <div class='custom-control custom-checkbox' style='display: inline-block;margin: 5px;'>
                                            <input type='checkbox' class='custom-control-input' name='labodata[]' value='{{$labodata->id}}' id='{{$labodata->id}}'>
                                            <label class='custom-control-label' for='{{$labodata->id}}'>{{$labodata->medicinename}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            <input type="text" id="patient_id"
                                   class="form-control"
                                   name="patient_id" value="<?php echo $id;?>" hidden>
                            <input type="text" id="patientrecoddateid"
                                   class="form-control"
                                   name="patientrecoddateid"
                                   value="<?php echo $patientrecoddateid;?>" hidden>
                            <input type="text" id="patientrecoddateid"
                                   class="form-control"
                                   name="insurance_id"
                                   value="<?php echo $insurance_id;?>" hidden>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-login">
                                    <i class="la la-check-square-o"></i> Save
                                </button>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="form-grouppop">
    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
            data-toggle="modal"
            data-target="#appointtodoctors">
        Appoint to Doctor
    </button>
    <!-- Modal -->
    <div class="modal fade text-left" id="appointtodoctors" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Appoint to Doctor </h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-simple" method="POST"
                          action="{{ url('AddAppointmentDoctors') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row  multi-field">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput1">Doctors List</label>

                                    <select class="form-control" name="doctor_id"  required>
                                        @foreach($listusers as $users)
                                            <option value="{{$users->id}}">{{$users->name}} ({{$users->role}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">Date</label>

                                    <input type="date" class="form-control" id="date" name="dateconfirm"
                                     required>
                                </div>
                                <input type="text" id="patient_id"
                                       class="form-control"
                                       name="patient_id" value="<?php echo $id;?>" hidden>
                                <input type="text" id="patientrecoddateid"
                                       class="form-control"
                                       name="patientrecoddateid"
                                       value="<?php echo $patientrecoddateid;?>" hidden>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput1">Time</label>
                                    <input type="time" class="form-control" id="time" name="timeconfirm" value="13:45:00" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-login">
                                    <i class="la la-check-square-o"></i> Save
                                </button>
                            </div>

                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>


