@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('Rooms_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Level</label>
                                                            <select class="form-control" id="listofmedecinecharge"
                                                                    name="roomlevel" required>
                                                                <option value="Normal">Normal</option>
                                                                <option value="Vip">Vip</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Number</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Room Number"
                                                                   name="roomnumber">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room  Price</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Room  Price"
                                                                   name="roomprice">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- // Basic form layout section end -->
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Rooms Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Room Level</th>
                                                <th>Room Number</th>
                                                <th>Room Status</th>
                                                <th>Room Price</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listroom as $data)
                                                <tr>
                                                    <td>{{$data->roomlevel}}</td>
                                                    <td>{{$data->roomnumber}}</td>
                                                    <td>
                                                        <?php

                                                        $roomstatus = $data->roomstatus;

                                                        if ($roomstatus == "Not Available"){
                                                            echo "<button type='button' class='btn btn-danger btn-min-width btn-glow mr-1 mb-1'>$roomstatus</button>";
                                                        }else{
                                                            echo "<button type='button' class='btn btn-success btn-min-width btn-glow mr-1 mb-1'>$roomstatus</button>";
                                                        }
                                                        ?>

                                                    </td>
                                                    <td>{{$data->roomprice}} FRW</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#appointlabo{{$data->id}}">
                                                            <i class="fas fa-edit"></i> Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="appointlabo{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Edit Room Information</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('EditRoomInfo_') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Room Level</label>
                                                                                            <select class="form-control" id="listofmedecinecharge"
                                                                                                    name="roomlevel" required>
                                                                                                <option value="{{$data->roomlevel}}">{{$data->roomlevel}}</option>
                                                                                                <option value="Normal">Normal</option>
                                                                                                <option value="Vip">Vip</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Room Number</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->roomnumber}}"
                                                                                                   name="roomnumber">
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                   name="id" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Room Price</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->roomprice}}"
                                                                                               name="roomprice">

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Room Availability</label>
                                                                                        <select class="form-control" id="listofmedecinecharge"
                                                                                                name="roomstatus" required>
                                                                                            <option value="{{$data->roomstatus}}">{{$data->roomstatus}}</option>
                                                                                            <option value="Available">Available</option>
                                                                                            <option value="Not Available">Not Available</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        {{--@foreach($listlabotest as $labotest)--}}
                                                                                            {{--<div class="custom-control custom-checkbox">--}}
                                                                                                {{--<input type="checkbox" class="custom-control-input"  name="lab_tech[]" value="{{$labotest->id}}" id="{{$labotest->id}}">--}}
                                                                                                {{--<label class="custom-control-label" for="{{$labotest->id}}">{{$labotest->labotest_name}}</label>--}}
                                                                                            {{--</div>--}}
                                                                                        {{--@endforeach--}}
                                                                                        {{--<label for="projectinput1">Lab Test</label>--}}

                                                                                        {{--<select class="form-control" id="country-list" name="lab_tech" onChange="getState(this.value);" required>--}}
                                                                                        {{--@foreach($listlabotest as $labotest)--}}
                                                                                        {{--<option value="{{$labotest->id}}">{{$labotest->labotest_name}}</option>--}}
                                                                                        {{--@endforeach--}}
                                                                                        {{--</select>--}}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>

                                                                            </div>
                                                                        </form>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><a href="{{ route('backend.DeleteRoomInfo',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
    {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection