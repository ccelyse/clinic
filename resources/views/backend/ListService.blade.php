@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }

        .main-menu.menu-light .navigation>li.open>a {
            color: #545766;
            background: #f5f5f5;
            border-right: 4px solid #6b442b !important;
        }
        form label {
            color: #2B335E;
            font-size: 12px;
        }
        .modal-dialog {
            max-width: 700px !important;
        }
        /*.pagination li{*/
            /*color: #1E9FF2;*/
            /*border: 1px solid #BABFC7;*/
            /*padding: 5px 15px;*/
        /*}*/
        /*.active{*/
            /*color: #FFF !important;*/
            /*background-color: #1E9FF2;*/
            /*border-color: #1E9FF2;*/
        /*}*/
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $('.search-box input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputVal = $(this).val();
                var resultDropdown = $(this).siblings(".result");

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchServices",
                    data: {
                        'servicename': inputVal,

                    },
                    success: function (data) {
                        $("#display").html(data);
                    }
                });
            });
        });


    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <!-- // Basic form layout section end -->
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="card">
                                <div class="card-header">
                                    <div class="col-lg-12">
                                        <div class="search-box">
                                            <fieldset class="form-group position-relative mb-0">
                                                <input type="text" class="form-control form-control-xl input-xl" id="searchpatient" placeholder="Search Patient">
                                            </fieldset>
                                        </div>
                                        <table class="table table-striped table-bordered  table-responsive" style="margin-top: 10px">
                                            <thead>
                                            <tr>
                                                <th>Activity Name</th>
                                                <th>Medicine Name</th>
                                                <th>Medicine Code</th>
                                                <th>Insurance Name</th>
                                                <th>Quantity</th>
                                                <th>Price Name</th>
                                                <th>Date Created</th>
                                            </tr>
                                            </thead>
                                            <tbody id="display">
                                            </tbody>



                                        </table>
                                    </div>
                                    {{--<div class="col-lg-12" style="padding-top: 10px">--}}
                                    {{--<fieldset class="form-group position-relative mb-0">--}}
                                    {{--<button type="submit" class="btn btn-login">--}}
                                    {{--<i class="la la-check-square-o"></i> search--}}
                                    {{--</button>--}}
                                    {{--</fieldset>--}}
                                    {{--</div>--}}

                                    {{--<div id="display"></div>--}}

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List of Activities</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table id="servicetable" class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Activity Name</th>
                                                <th>Medicine Name</th>
                                                <th>Medicine Code</th>
                                                <th>Insurance Name</th>
                                                <th>Quantity</th>
                                                <th>Price Name</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listMedicinesec as $data)
                                                <tr>
                                                    <td>{{$data->departmentname}}</td>
                                                    <td>{{$data->medicinename}}</td>
                                                    <td>{{$data->medicinecode}}</td>
                                                    <td>{{$data->insurance_name}}</td>
                                                    <td>{{$data->medicinequantity}}</td>
                                                    <td>{{$data->medicinepriceinsurance}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>

                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#edimed{{$data->id}}">
                                                            Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="edimed{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Edit Medicine/Service/Laboratory </h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddMedicineExcelUpdate') }}" enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="form-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Medicine Batch number/Service Code</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->medicinecode}}"
                                                                                                   name="medicinecode">
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                   name="id" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Medicine/Service Name</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->medicinename}}"
                                                                                                   name="medicinename" required disabled>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Medicine Quantity(not mandatory for service)</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->medicinequantity}}"
                                                                                                   name="medicinequantity" disabled>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Activity Name</label><br>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->departmentname}}"
                                                                                                   name="medicine_department_id" disabled>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Insurance  Name</label>
                                                                                            <select class="form-control"  id="basicSelect" name="insurance_medicine" required disabled>
                                                                                                <option value="{{$data->insurance_medicine_id}}">{{$data->insurance_name}}</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Medicine  Price</label>
                                                                                            <input type="number" id="projectinput1" class="form-control" value="{{$data->medicinepriceinsurance}}"
                                                                                                   name="medicineprice">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-actions">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Update
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </td>

                                                    <td><a href="{{ route('backend.DeleteMedicine',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>

                                                </tr>
                                            @endforeach
                                        </table>
                                        {{--{{ $listMedicinesec->links() }}--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {{--<section id="complex-header">--}}
                {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                {{--<div class="card">--}}
                {{--<div class="card-header">--}}
                {{--<h4 class="card-title">List of Activities</h4>--}}
                {{--<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>--}}
                {{--<div class="heading-elements">--}}
                {{--<ul class="list-inline mb-0">--}}
                {{--<li><a data-action="collapse"><i class="ft-minus"></i></a></li>--}}
                {{--<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>--}}
                {{--<li><a data-action="expand"><i class="ft-maximize"></i></a></li>--}}
                {{--<li><a data-action="close"><i class="ft-x"></i></a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="card-content collapse show">--}}
                {{--<div class="card-body card-dashboard">--}}
                {{--<table class="table table-striped table-bordered zero-configuration table-responsive">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                {{--<th>Activity Name</th>--}}
                {{--<th>Medicine Name</th>--}}
                {{--<th>Medicine Code</th>--}}

                {{--<th>Insurance Name</th>--}}
                {{--<th>Quantity</th>--}}
                {{--<th>Price Name</th>--}}
                {{--<th>Date Created</th>--}}
                {{--<th>Edit</th>--}}
                {{--<th>Delete</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach($listMedicine as $data)--}}
                {{--<tr>--}}
                {{--<td>{{$data->departmentname}}</td>--}}
                {{--<td>{{$data->medicinename}}</td>--}}
                {{--<td>{{$data->medicinecode}}</td>--}}
                {{--<td>{{$data->insurance_name}}</td>--}}
                {{--<td>{{$data->medicinequantity}}</td>--}}
                {{--<td>{{$data->medicinepriceinsurance}}</td>--}}
                {{--<td>{{$data->created_at}}</td>--}}

                {{--<td>--}}
                {{--<a href="{{ route('backend.EditMedicine',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Edit</a>--}}
                {{--</td>--}}

                {{--<td><a href="{{ route('backend.DeleteMedicine',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-trash"></i> Remove</a></td>--}}
                {{--</tr>--}}
                {{--@endforeach--}}
                {{--</table>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</section>--}}
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>

@endsection
