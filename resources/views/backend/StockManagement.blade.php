@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }

        .main-menu.menu-light .navigation>li.open>a {
            color: #545766;
            background: #f5f5f5;
            border-right: 4px solid #6b442b !important;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="number-tabs">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    {{--<h4 class="card-title">Form wizard with number tabs</h4>--}}
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="number-tab-steps wizard-circle" method="POST" action="{{ url('AddMedicineStock') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Product Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Product Name"
                                                                   name="productname" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Product Package</label>
                                                            <input id="projectinput1" class="form-control"
                                                                   name="productpackage" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                   type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Product Piece/Test</label>
                                                            <input id="projectinput1" class="form-control"
                                                                   name="productpiece" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                   type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Product Unit Price</label>--}}
                                                            {{--<input id="projectinput1" class="form-control"--}}
                                                                   {{--name="productunitprice" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"--}}
                                                                   {{--type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                                </div>
                                                <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                <i class="la la-check-square-o"></i> Save
                                                </button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Stock Management</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Edit</th>
                                                <th>Ship Product</th>
                                                <th>Ship Report</th>
                                                <th>Product Name</th>
                                                <th>Product Package</th>
                                                <th>Product Piece/Test</th>
                                                <th>Remaining Stock Piece/Test</th>
                                                <th>Remaining in Labo</th>
                                                <th>Remaining in Nurse</th>
                                                <th>Total Remaining Stock Piece/Test</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listmedicine as $data)
                                                <tr>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#medicine{{$data->id}}">
                                                            Update
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="medicine{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> </h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('UpdateMedicineStock') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Product Name</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->productname}}"
                                                                                               name="productname" required disabled>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                               name="id" required hidden>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Product Package</label>
                                                                                        <input id="projectinput1" class="form-control" value="{{$data->productpackage}}"
                                                                                               name="productpackage" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                                               type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Product Piece/Test</label>
                                                                                        <input id="projectinput1" class="form-control" value="{{$data->productpiece}}"
                                                                                               name="productpiece" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                                               type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                                                    </div>
                                                                                </div>
                                                                                {{--<div class="col-md-6">--}}
                                                                                    {{--<div class="form-group">--}}
                                                                                        {{--<label for="projectinput1">Product Unit Price</label>--}}
                                                                                        {{--<input id="projectinput1" class="form-control" value="{{$data->productunitprice}}"--}}
                                                                                               {{--name="productunitprice" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"--}}
                                                                                               {{--type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#shipment{{$data->id}}">
                                                            Product Shipment
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="shipment{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> </h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('MedicineStockShipment') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Departmant</label>
                                                                                        <select class="form-control"  id="basicSelect" name="shipmentdepartment" required>
                                                                                            <option value="Doctor">Doctor</option>
                                                                                            {{--<option value="Doctor Dentist">Doctor Dentist</option>--}}
                                                                                            <option value="Nurse">Nurse</option>
                                                                                            <option value="Laboratory">Laboratory</option>
                                                                                            {{--<option value="Accountant">Accountant</option>--}}
                                                                                            {{--<option value="Reception">Reception</option>--}}
                                                                                            {{--<option value="Admin">Admin</option>--}}

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Product Name</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->productname}}"
                                                                                               name="shipmentproductname" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Product Piece</label>
                                                                                        <input id="projectinput1" class="form-control"
                                                                                               name="shipmentproductpiece" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                                               type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10">
                                                                                        <input id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                               name="product_id" type= "text" hidden>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#shipmentreport{{$data->id}}">
                                                             Shipment Report
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="shipmentreport{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document" style="max-width: 800px !important;">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> </h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="card-content collapse show">
                                                                            <div class="card-body card-dashboard">
                                                                                <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Department Name</th>
                                                                                        <th>Product Name</th>
                                                                                        <th>Product Piece</th>
                                                                                        <th>Date Created</th>
                                                                                        <th>Delete</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <?php
                                                                                    $listshipment = \App\Medicineshipment::where('product_id',$data->id)->get();
                                                                                    ?>
                                                                                    @foreach($listshipment as $shipment)
                                                                                        <tr>
                                                                                            <td>{{$shipment->shipmentdepartment}}</td>
                                                                                            <td>{{$shipment->shipmentproductname}}</td>
                                                                                            <td>{{$shipment->shipmentproductpiece}}</td>
                                                                                            <td>{{$shipment->created_at}}</td>
                                                                                            <td><a href="{{ route('backend.RemoveMedicineStockShipment',['id'=> $shipment->id])}}"
                                                                                                   class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                                                            class="fas fa-trash"></i> Remove</a></td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{$data->productname}}</td>
                                                    <td>{{$data->productpackage}}</td>
                                                    <td>{{$data->productpiece}}</td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        $ogpiece = $data->productpiece;
                                                        $remainingpiece = $ogpiece - $viewmore_sum;
                                                        echo $remainingpiece;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('shipmentdepartment','Laboratory')
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        echo $viewmore_sum;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        $viewmore_sum = \App\Medicineshipment::select('shipmentproductpiece', DB::raw('SUM(shipmentproductpiece) as shipmentproductpiece'))
                                                            ->where('shipmentdepartment','Nurse')
                                                            ->where('product_id',$data->id)
                                                            ->value('shipmentproductpiece');
                                                        echo $viewmore_sum;
                                                        ?>

                                                    </td>
                                                    <td>
                                                        @if($data->remainingpiece < '10')
                                                            <span class="alert bg-danger">
                                                          {{$data->remainingpiece}}
                                                             </span>
                                                        @else
                                                            {{$data->remainingpiece}}
                                                        @endif

                                                    </td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td><a href="{{ route('backend.RemoveMedicineStock',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>



                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>

@endsection
