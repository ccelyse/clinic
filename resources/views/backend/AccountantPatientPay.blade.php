@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $('.search-box input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputVal = $(this).val();
                var resultDropdown = $(this).siblings(".result");

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchAccountantPatientPay",
                    data: {
                        'patientname': inputVal,

                    },
                    success: function (data) {
                        $("#display").html(data);
                    }
                });
            });
        });


    </script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-12">


                            <div class="card">
                                <div class="card-header">
                                    <div class="col-lg-12">
                                        <div class="search-box">
                                            <fieldset class="form-group position-relative mb-0">
                                                <input type="text" class="form-control form-control-xl input-xl" id="searchpatient" placeholder="Search Patient">
                                            </fieldset>
                                        </div>
                                        <table class="table table-striped table-bordered  table-responsive" style="margin-top: 10px">
                                            <thead>
                                            <tr>
                                                <th>Patient Name</th>
                                                <th>Patient Gender</th>
                                                <th>Patient Phone number</th>
                                                <th>Patient Insurance</th>
                                                <th>Patient PID</th>

                                            </tr>
                                            </thead>
                                            <tbody id="display">
                                            </tbody>



                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient List</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Beneficiary Name</th>
                                                <th>Beneficiary Name Country ID</th>
                                                <th>Insurance Name</th>
                                                <th>Insurance Card Number</th>
                                                <th>Gender</th>
                                                <th>Date of Birth</th>
                                                <th>Patient Telephone number</th>
                                                <th>Copay</th>
                                                <th>Copay Relationship</th>
                                                <th>Affiliate Name</th>
                                                <th>Affiliate Telephone number</th>
                                                <th>Insured Location</th>
                                                <th>PID</th>
                                                <th>Date Created</th>
                                                <th>Operate</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listpatient as $data)
                                                <tr>
                                                    <td><a href="{{ route('backend.AccountantPatientHistory',['id'=> $data->id])}}">{{$data->beneficiary_name}}</a></td>
                                                    <td>{{$data->patient_country_id}}</td>
                                                    <td>{{$data->insurance_name}}</td>
                                                    <td>{{$data->insurance_card_number}}</td>
                                                    <td>{{$data->gender}}</td>
                                                    <td>{{$data->dateofbirth}}</td>
                                                    <td>{{$data->beneficiary_telephone_number}}</td>
                                                    <td>{{$data->copay}}</td>
                                                    <td>{{$data->copay_relationship}}</td>
                                                    <td>{{$data->patient_names}}</td>
                                                    <td>{{$data->patient_telephone_number}}</td>
                                                    <td>{{$data->insured_location}}</td>
                                                    <td>{{$data->pid}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.AccountantPatientHistory',['id'=> $data->id])}}" class="btn btn-login btn-min-width mr-1 mb-1"><i class="fas fa-edit"></i> Operate</a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
