@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    {{--@include('backend.layout.sidemenu')--}}
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }

        .main-menu.menu-light .navigation>li.open>a {
            color: #545766;
            background: #f5f5f5;
            border-right: 4px solid #6b442b !important;
        }
        .modal-dialog {
            max-width: 700px !important;
        }
        #laboapp{
            display: none;
        }
    </style>
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                <?php
                $user_id = \Auth::user()->id;
                $Userrole = \App\User::where('id',$user_id)->value('role');
                $recurldash = url('ReceptionDashboard');
                $recurl = url('Reception');
                $adminurl = url('Dashboard');
                $accountant = url('Accountant');
                $accountantsales = url('AccountantSales');
                $CreateAccount = url('CreateAccount');
                $AccountList = url('AccountList');
                $AddInsurance = url('AddInsurance');
                $AddDepartment = url('AddDepartment');
                $AddMedicines = url('AddMedicines');
                $LaboratoryTest = url('LaboratoryTest');
                $Rooms = url('Rooms');
                $RoomPrice = url('RoomPrice');

                $Nurse = url('Nurse');
                $NurseDashboard = url('NurseDashboard');


                $NurseConsultation = url('NurseConsultation');

                $NurseConsultation_ = route('backend.PatientHistoryRecord',['id'=> $id]);
                $Operations = route('backend.NurseOperationPatientRecordHistory',['id'=>$id]);


                $LaboratoryDashboard = url('LaboratoryDashboard');
                $LaboratoryTech = url('LaboratoryTech');
                $HospitalizationDashboard = url('HospitalizationDashboard');
                $Hospitalize = url('Hospitalize');

                $DoctorDashboard = url('DoctorDashboard');
                $DoctorOperations = url('DoctorOperations');
                $DoctorOperations_ = route('backend.DoctorOperationPatientRecordHistory',['id'=> $id]);
                $Doctor = url('Doctor');
                $DoctorConsultation = url('DoctorConsultation');
                $DoctorConsultation_ = route('backend.DoctorPatientHistoryRecord',['id'=> $id]);

                $accountant = url('ReceptionAccountant');
                $AccountantPatientPay = url('AccountantPatientPay');
                $accountantpatientlist = url('Accountant');
                $AccountantDashboard = url('AccountantDashboard');
                $accountantsales = url('AccountantSales');
                $HospitalizeSales = url('HospitalizeSales');
                $PayedSales = url('PayedSales');
                $HospitalizeReport = url('HospitalizeReport');


                switch ($Userrole) {
                    case "Admin":
                        echo "<li class='nav-item'><a href='$adminurl'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                        echo"<li class=' nav-item'><a href='#'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.templates.main'>Account</span></a><ul class='menu-content'><li class=' nav-item'><a href='$CreateAccount'><i class='fas fa-users'></i><span class='menu-title'>Create Account</span></a></li><li class=' nav-item'><a href='$AccountList'><i class='fas fa-user-circle'></i><span class='menu-title'>Account List</span></a></li></ul></li><li class=' nav-item'><a href='$AddInsurance'><i class='fas fa-thumbs-up'></i><span class='menu-title'>Insurance</span></a></li><li class=' nav-item'><a href='$AddDepartment'><i class='fas fa-book'></i><span class='menu-title'>Activity</span></a></li><li class=' nav-item'><a href='$AddMedicines'><i class='fas fa-medkit'></i><span class='menu-title'>Medicines/Service</span></a></li><li class=' nav-item'><a href='$LaboratoryTest'><i class='fas fa-thermometer'></i><span class='menu-title'>Laboratory Test</span></a></li><li class=' nav-item'><a href='#'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Hospitalization</span></a><ul class='menu-content'><li class='nav-item'><a href='$Rooms'><i class='fas fa-hospital-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Rooms</span></a></li><li class=' nav-item'><a href='$RoomPrice'><i class='fas fa-hospital'></i><span class='menu-title'>Room Prices</span></a></li></ul></li>";

                        break;

                    case "Doctor":
                        echo "<li class=' nav-item'><a href='$DoctorDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Doctor'><i class='fas fa-user-md'></i><span class='menu-title'>Doctor</span></a></li>";
                        echo "<li class=' nav-item'><a href='$DoctorConsultation_'><i class='fas fa-thermometer'></i><span class='menu-title'>Consultation</span></a></li>";
                        echo "<li class=' nav-item'><a href='$DoctorOperations_'><i class='fas fa-user-injured'></i><span class='menu-title'>Operations</span></a></li>";
                        break;

                    case "Laboratory":
                        echo "<li class=' nav-item'><a href='$LaboratoryDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
                        echo "<li class=' nav-item'><a href='$LaboratoryTech'><i class='fas fa-user-md'></i><span class='menu-title'>Patients</span></a></li>";
                        break;

                    case "Accountant":
                        echo "<li class=' nav-item'><a href='$AccountantDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
                        echo "<li class=' nav-item'><a href='$AccountantPatientPay'><i class='fas fa-user-md'></i><span class='menu-title'>Patients</span></a></li>";
                        echo "<li class=' nav-item'><a href='$PayedSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Sales</span></a></li>";
                        echo "<li class=' nav-item'><a href='$HospitalizeSales'><i class='fas fa-money-bill-alt'></i><span class='menu-title'>Hospitalization Sales</span></a></li>";
                        break;

                    case "Reception":

                        echo "<li class='nav-item'><a href='$recurldash'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                        echo "<li class='nav-item'><a href='$recurl'><i class='fas fa-table'></i><span class='menu-title'>Reception</span></a></li>";
                        echo "<li class='nav-item'><a href='$accountant'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Accountant</span></a></li>";

                        break;

                    case "Hospitalization":

                        echo "<li class='nav-item'><a href='$HospitalizationDashboard'><i class='fas fa-chart-pie'></i><span class='menu-title'>Dashboard</span></a></li>";
                        echo "<li class='nav-item'><a href='$Hospitalize'><i class='fas fa-table'></i><span class='menu-title'>Hospitalize</span></a></li>";
                        echo "<li class='nav-item'><a href='$HospitalizeReport'><i class='fas fa-table'></i><span class='menu-title'>Hospitalize Report</span></a></li>";
//                    echo "<li class='nav-item'><a href='$accountant'><i class='fas fa-hospital-alt'></i><span class='menu-title'>Accountant</span></a></li>";
                        break;

                    case "Nurse":
                        echo "<li class=' nav-item'><a href='$NurseDashboard'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
//                    echo "<li class=' nav-item'><a href='$Nurse'><i class='fas fa-user-md'></i><span class='menu-title'>Nurse</span></a></li>";
                        echo "<li class=' nav-item'><a href='$NurseConsultation_'><i class='fas fa-thermometer'></i><span class='menu-title'>Consultation</span></a></li>";
                        echo "<li class=' nav-item'><a href='$Operations'><i class='fas fa-user-injured'></i><span class='menu-title'>Operations</span></a></li>";
                        break;

                    default:
                        return redirect()->back();
                        break;
                }
                ?>

            </ul>
        </div>
    </div>

    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    @include('backend.AppointmentModule')
                                </div>
                            </div>
                        </div>
                        @include('backend.PatientPaymentInformation')
                    </div>

                </section>


                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Patient to Doctor Appointments</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                {{--<th>Doctor Name</th>--}}
                                                <th>Doctor Date</th>
                                                <th>Doctor time</th>
                                                <th>Done by</th>
                                                <th>Date Created</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listdoctorsapp as $data)
                                                <tr>
                                                    {{--<td>{{$data->doctor_id}}</td>--}}
                                                    <td>{{$data->dateconfirm}}</td>
                                                    <td>{{$data->timeconfirm}}</td>
                                                    <td>{{$data->name}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.DeleteAddAppointmentDoctors',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
