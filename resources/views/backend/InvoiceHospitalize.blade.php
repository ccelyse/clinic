<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

    <title></title>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <style>

        * {
            margin: 0;
            padding: 0;
        }

        body {
            font: 14px/1.4 Helvetica, Arial, sans-serif;
        }

        #page-wrap {
            width: 800px;
            margin: 0 auto;
        }

        textarea {
            border: 0;
            font: 14px Helvetica, Arial, sans-serif;
            overflow: hidden;
            resize: none;
        }

        table {
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid black;
            padding: 5px;
        }

        #header {
            height: 15px;
            width: 100%;
            margin: 20px 0;
            background: #222;
            text-align: center;
            color: white;
            font: bold 15px Helvetica, Sans-Serif;
            text-decoration: uppercase;
            letter-spacing: 20px;
            padding: 8px 0px;
        }

        #address {
            width: 250px;
            height: 150px;
            float: left;
        }

        #customer {
            overflow: hidden;
        }

        #logo {
            text-align: right;
            float: right;
            position: relative;
            margin-top: 25px;
            border: 1px solid #fff;
            max-width: 540px;
            overflow: hidden;
        }

        #customer-title {
            font-size: 20px;
            font-weight: bold;
            float: left;
        }

        #meta {
            margin-top: 1px;
            width: 100%;
            float: right;
        }

        #meta td {
            text-align: right;
        }

        #meta td.meta-head {
            text-align: left;
            background: #eee;
        }

        #meta td textarea {
            width: 100%;
            height: 20px;
            text-align: right;
        }

        #items {
            clear: both;
            width: 100%;
            margin: 30px 0 0 0;
            border: 1px solid black;
        }

        #items th {
            background: #eee;
        }

        #items textarea {
            width: 80px;
            height: 50px;
        }

        #items tr.item-row td {
            vertical-align: top;
        }

        #items td.description {
            width: 300px;
        }

        #items td.item-name {
            width: 175px;
        }

        #items td.description textarea, #items td.item-name textarea {
            width: 100%;
        }

        #items td.total-line {
            border-right: 0;
            text-align: right;
        }

        #items td.total-value {
            border-left: 0;
            padding: 10px;
        }

        #items td.total-value textarea {
            height: 20px;
            background: none;
        }

        #items td.balance {
            background: #eee;
        }

        #items td.blank {
            border: 0;
        }

        #terms {
            text-align: center;
            margin: 20px 0 0 0;
        }

        #terms h5 {
            text-transform: uppercase;
            font: 13px Helvetica, Sans-Serif;
            letter-spacing: 10px;
            border-bottom: 1px solid black;
            padding: 0 0 8px 0;
            margin: 0 0 8px 0;
        }

        #terms textarea {
            width: 100%;
            text-align: center;
        }

        .delete-wpr {
            position: relative;
        }

        .delete {
            display: block;
            color: #000;
            text-decoration: none;
            position: absolute;
            background: #EEEEEE;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: -6px;
            left: -22px;
            font-family: Verdana;
            font-size: 12px;
        }
        .text-center{
            text-align: center;
        }
        .text-success{
            color:
                    #f7921a ;
        }
    </style>

</head>

<body>

<div id="page-wrap">

    <table width="100%">
        <tr>
            <td style="border: 0;  text-align: left" width="62%">
                <span style="font-size: 18px; color: #2f4f4f"><strong></strong></span><br><p><br><p>
                    <img src="backend/app-assets/images/logoclinic.png" alt="Logo" width="10%"><br><br>
                    @foreach($patientinformation  as $PI)
                        Patient Names: {{$PI->beneficiary_name}} <br>
                        PID: {{$PI->pid}} <br>
                        Telephone: {{$PI->patient_telephone_number}}  <br>
                <?php
                $insurancename = $PI->insurance_name;
                $copay = $PI->copay;
                ?>
                @endforeach
            </td>

            <td style="border: 0;  text-align: right" width="62%">
                <div id="logo">



                </div>
                Ndengera Clinic<br>
                Telecom House <br>
                8 KG 7 Ave, <br>
                Kigali <br>
                Rwanda <br>

            </td>
        </tr>

    </table>

    <div style="clear:both"></div>

    <div id="customer">

        <table id="meta">
            <tr>
                <td rowspan="5" style="border: 1px solid white; border-right: 1px solid black; text-align: left" width="62%">
                    <br>

                </td>

            </tr>
            @foreach($invoicedetailshospi as $invoicedetail)
                <tr>
                    <td class="meta-head">Payment Method</td>
                    <td>{{$invoicedetail->paymentmethod}}</td>
                </tr>
                <tr>
                    <td class="meta-head">Invoice Date </td>
                    <td><?php  echo $dt; ?></td>
                </tr>
                <tr>
                    <td class="meta-head">Amount Paid</td>
                    <td>
                        <div class="due">RWF {{$invoicedetail->amount_paid}}</div>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

    <table id="items">

        <tr>
            <th width="35%">Activity Name</th>
            <th width="30%">Item</th>
            {{--<th align="right">Quantity</th>--}}
            <th align="right">Unit Price</th>
            <th align="right">Total</th>

        </tr>


        @foreach($invoicedetailshospi as $items)
            <tr class="item-row">
                <td class="description">Room: {{$items->patient_roomlevel}}</td>
                <td class="description">Room Number: {{$items->roomnumber}}</td>
                {{--<td align="right">{{$items->medicine_service_price}}</td>--}}
                <td align="right">{{$items->patient_roomprice}}</td>
                <td align="right">{{$items->patient_roomprice}}</td>
            </tr>

        <tr>
            {{--<td class="blank"></td>--}}
            <td class="blank"></td>
            <td colspan="2" class="total-line">Subtotal</td>
            <td class="total-value">
                <div id="subtotal">
                    {{$items->patient_roomprice}}
                    <?php $subtotal = $items->roomprice;$roomlevels = $items->roomlevel;?>
                </div>
            </td>
        </tr>
        @endforeach

        <tr>
            {{--<td class="blank"></td>--}}
            <td class="blank"></td>
            <td colspan="2" class="total-line">Insurance Due</td>
            <td class="total-value">
                <div id="subtotal">
                    <?php
                    $roomlevelprice = $subtotal;
                    $roomlevel = $roomlevels;

                    $sumofinsurance = ($roomlevelprice * $copay)/100;
                    $sumofpatient = $roomlevelprice - $sumofinsurance ;

                    if($roomlevel == 'Vip'){

                    $sumofinsurance_ = (15000 * $copay)/100;
                    $suminsu =
                    $newprice = $roomlevelprice - $sumofinsurance_;

                    $sumofinsurance = ($subtotal * $copay)/100;
                    $sumofpatient = $subtotal - $sumofinsurance ;

                    echo "RWF $sumofinsurance_";

                    }else{
                        echo "$sumofinsurance";
                    }
                    ?>

                </div>
            </td>
        </tr>
        <tr>
            {{--<td class="blank"></td>--}}
            <td class="blank text-center text-success"> <h1>@foreach($invoicedetailshospi as $paymentcomment){{$paymentcomment->paymentcomment}}@endforeach</h1></td>
            <td colspan="2" class="total-line">Amount Due</td>
            <td class="total-value">
                <div id="total">
                    <?php
                    $roomlevelprice = $subtotal;
                    $roomlevel = $roomlevels;

                    $sumofinsurance = ($roomlevelprice * $copay)/100;
                    $sumofpatient = $roomlevelprice - $sumofinsurance ;

                    if($roomlevel == 'Vip'){

                        $sumofinsurance_ = (15000 * $copay)/100;
                        $suminsu =
                        $newprice = $roomlevelprice - $sumofinsurance_;

                        $sumofinsurance = ($subtotal * $copay)/100;
                        $sumofpatient = $subtotal - $sumofinsurance ;

                        echo "RWF $newprice";

                    }else{
                        echo "$sumofpatient";
                    }
                    ?>
                </div>
            </td>
        </tr>


        @foreach($invoicedetailshospi as $paid)
            <tr>
                <td class="blank"></td>
                {{--<td class="blank"></td>--}}
                <td colspan="2" class="total-line balance">Amount Paid</td>
                <td class="total-value balance">
                    <div class="due">RWF {{$paid->amount_paid}}</div>
                </td>
            </tr>
        @endforeach

    </table>

    {{--@if($inv->note!='')--}}
    {{--<div id="terms">--}}
    {{--<h5>{{language_data('Invoice Note')}}</h5>--}}
    {{--{{$inv->note}}--}}
    {{--</div>--}}
    {{--@endif--}}


</div>

</body>

</html>