@extends('backend.layout.master')

@section('title', 'Ndengera Clinic')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <style>
        .form-grouppop{
            margin-bottom: -10px;
            position: relative;
            display: inline-block;
        }

        .main-menu.menu-light .navigation>li.open>a {
            color: #545766;
            background: #f5f5f5;
            border-right: 4px solid #6b442b !important;
        }
        form label {
            color: #2B335E;
            font-size: 12px;
        }
        .modal-dialog {
            max-width: 700px !important;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function () {
            $("#chkPassport").click(function () {
                if ($(this).is(":checked")) {
                    $("#dvPassport").show();
                    $("#AddPassport").hide();
                } else {
                    $("#dvPassport").hide();
                    $("#AddPassport").show();
                }
            });
        });
    </script>
    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('addedconsultation'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation') }}
                </div>
            @endif
            @if (session('addedconsultation_'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('addedconsultation_') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif

            <div class="content-body">
                <section id="complex-header">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <form class="form-horizontal form-simple" method="POST" action="{{ url('AddMedicines_') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Medicine Batch number/Service Code</label>
                                                        <input type="text" id="projectinput1" class="form-control" placeholder=""
                                                               name="medicinecode">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Medicine/Service Name</label>
                                                        <input type="text" id="projectinput1" class="form-control" placeholder=""
                                                               name="medicinename" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Medicine Category (<strong>Not Mandatory</strong>)</label>
                                                        <input type="text" id="projectinput1" class="form-control" placeholder=""
                                                               name="medicinecategory">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Medicine Quantity(not mandatory for service)</label>
                                                        <input type="text" id="projectinput1" class="form-control" placeholder="Medicine Quantity"
                                                               name="medicinequantity">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Activity Name</label><br>
                                                        <select class="form-control"  name="medicine_department_id">
                                                            @foreach($listdepr as $data)
                                                                <option value="{{$data->id}}">{{$data->departmentname}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(document).ready(function()
                                                {
                                                    $('.multi-field-wrapper').each(function() {
                                                        var $wrapper = $('.multi-fields', this);
                                                        $(".add-field", $(this)).click(function(e) {
                                                            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                        });
                                                        $('.multi-field .remove-field', $wrapper).click(function() {
                                                            if ($('.multi-field', $wrapper).length > 1)
                                                                $(this).parent('.multi-field').remove();
                                                        });
                                                    });
                                                });
                                            </script>
                                            <div class="multi-field-wrapper">
                                                <div class="multi-fields">
                                                    <div class="row  multi-field">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Insurance  Name</label>
                                                                <select class="form-control"  id="basicSelect" name="insurance_medicine[]" required>
                                                                    @foreach($listinsu as $data)
                                                                        <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Medicine  Price</label>
                                                                <input type="number" id="projectinput1" class="form-control" placeholder="Medicine  Price"
                                                                       name="medicineprice[]">
                                                            </div>
                                                        </div>
                                                        <button type="button" class="remove-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                        <button type="button" class="add-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <label for="chkPassport">
                                        <input type="checkbox" id="chkPassport" />
                                        Upload data with excel
                                    </label>
                                    <hr />
                                    <div class="multi-field-wrapper" id="dvPassport" style="display: none">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddMedicineExcel') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Activity Name</label><br>
                                                            <select class="form-control"  name="medicine_department_id">
                                                                @foreach($listdepr as $data)
                                                                    <option value="{{$data->id}}">{{$data->departmentname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Insurance  Name</label>
                                                            <select class="form-control"  id="basicSelect" name="insurance_medicine" required>
                                                                @foreach($listinsu as $data)
                                                                    <option value="{{$data->id}}">{{$data->insurance_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Medicine Category (<strong>Not Mandatory</strong>)</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder=""
                                                                   name="medicinecategory">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Upload Excel</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="file">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>

@endsection
