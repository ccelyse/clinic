@extends('layouts.master')

@section('title', 'Visa Free Africa')

@section('content')

@include('layouts.topmenu')
<style>
    .content-header{
        margin-top: 40px;
        background: #000;
    }
    .content-header-title{
        color: #fff;
        border-bottom: 4px solid #0088cc;
        position: absolute;
        margin-top: 3px;
        line-height: 3;
    }
    .btn-visa {
        border-color: #d9534f !important;
        background-color: #d9534f !important;
        color: #FFF;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .card-title{
        color: #0088cc;
    }
    .btn-visablue {
        border-color: #0088cc !important;
        background-color: #0088cc !important;
        color: #FFF;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .header-navbar {
        padding: 0;
        min-height: 8rem !important;
        font-family: Quicksand,Georgia,'Times New Roman',Times,serif;
        transition: .3s ease all;
    }
    .header-navbar .navbar-container ul.nav li>a.nav-link:hover {
        color: #fff !important;
        background: #0088cc;
    }
    a{
        color: #fff !important;
    }
</style>
<div class="content-header row">

    <div class="content-header-left col-md-4 col-12 mb-2 offset-md-1">
        <h3 class="content-header-title">55 Voices</h3>
    </div>

    <div class="content-header-right col-md-6 col-12">
        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
            <button class="btn btn-visa dropdown-toggle dropdown-menu-right box-shadow-2 px-2"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="{{url('Apply')}}" >Apply for the competition</a></button>
        </div>
    </div>
</div>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <section class="row" style="padding: 0 70px">
                <div class="col-md-12">
                    <div id="with-header" class="card">
                        <div class="card-content collapse show">
                            <div class="card-body border-top-blue-grey border-top-lighten-5 ">
                                <h4 class="card-title">55 VOICES FOR A #VISAFREEAFRICA WRITING COMPETITION</h4>
                                <p class="card-text">The Global Shapers Community is an initiative from the World Economic Forum. The global shapers are a
                                    network of Hubs developed and led by young people who are exceptional in their potential, their
                                    achievements and their drive to make a contribution to their communities.</p>
                                <p class="card-text">During the World Economic Forum - Africa Forum in 2016, The Kigali Global Shapers launched the
                                    #VisaFreeAfrica Petition to call for free movement. The petition serves to request;
                                <ol>
                                    <li>A clear plan with clear deadlines to when we will have a #VisaFreeAfrica</li>
                                    <li>All African countries to grant to Africans AT LEAST a 30 day visa on arrival</li>
                                </ol>
                                </p>
                                <p class="card-text">The petition targeted to have at least 1000 Signature by 18th July 2016, last day of AU heads of states summit
                                    and 2000 signatures by 18 July 2017.</p>


                                <h4 class="card-title">Why Visa Free Africa</h4>
                                <p class="card-text">For the past 30 years, Africa has attempted to address free movement. A continental strategy document and more recently the Continental Free trade Agreement were signed
                                    but we are yet to enjoy the result of these agreements. Many a story are told of missed opportunities due to rigorous Visa requirements and processing before entering other
                                    countries on the continent. The lack of free mobility for Africans within african countries has significantly reduced intra- continental trade and increased the cost of transport
                                    across the continent.</p>
                                <p class="card-text">Today, Africans require a visa to enter into 40 of the 54 countries on the continent. As more and more countries open up to allow free mobility, we believe that 2063 is too far
                                    off. Africa needs to act now in order to leverage all the benefits that come with the AFCFTA.
                                </p>


                                <h4 class="card-title">Writing Competition</h4>
                                <p class="card-text">The writing competition is a campaign aimed at creating awareness on the issue of free mobility for Africans travelling across the continent. Young people across Africa face
                                    challenges that range from missed opportunities for work or study when traveling across the continent. In this writing contest, we invite you to share your experience while
                                    travelling through Africa. </p>

                                <p class="card-text">The writing competition is a campaign aimed at creating awareness on the issue of free mobility for Africans travelling across the continent. Young people across Africa face
                                    challenges that range from missed opportunities for work or study when traveling across the continent. In this writing contest, we invite you to share your experience while
                                    travelling through Africa. </p>

                                <h4 class="card-title">Eligibility Criteria</h4>
                                <p class="card-text"><strong>Age:</strong> 18 -35 Years</p>
                                <p class="card-text"><strong>Eligibility:</strong> Open only to ALL African nationals based in an African country.</p>
                                <p class="card-text"><strong>Genre:</strong> Narrative Nonfiction</p>
                                <p class="card-text"><strong>Language:</strong> English or French</p>
                                <p class="card-text"><strong>Content:</strong> The story should be a narrative based on a real life experience you had while traveling from and to an African Country in not more than 1000 words. The story
should be from a time when you were denied a Visa or the process involved in getting the Visa cost you an opportunity for study or work.</p>
                                <p class="card-text"><strong>Deadline:</strong> 15th January,2019</p>



                                <h4 class="card-title">Judges</h4>
                                <p class="card-text">These are prominent individuals that are championing efforts towards a Visa Free Africa and others that have a literary background. Here are the proposed names.</p>
                                <p class="card-text">
                                <ul>
                                    <li>Jackson Biko - Award winning Blogger, Kenya</li>
                                    <li>Michaella Rugwizangoga - CEO, Volkswagen Rwanda and Global Shaper, Rwanda</li>
                                    <li>Hassan El Houry - CEO, National Aviation Services</li>
                                </ul>
                                </p>

                                <h4 class="card-title">Prizes</h4>
                                <p class="card-text">
                                <ul>
                                    <li>Winning Story: $1000, Story published in Forbes Africa Magazine</li>
                                    <li>2nd Runner Up: $500</li>
                                    <li>3rd Runner Up: $300</li>
                                </ul>
                                </p>
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <button class="btn btn-visablue dropdown-toggle dropdown-menu-right box-shadow-2 px-2"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="{{url('Apply')}}" >Apply for the competition</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>
@include('layouts.footer')
@endsection
