<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentalClinicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DentalClinic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('consultation');
            $table->string('Historyofpresentillness');
            $table->string('PastMedicalhistory');
            $table->string('Pastdentalhistory');
            $table->string('ExtraoralExam');
            $table->string('IntraoralExam');
            $table->string('Examinationofteeth');
            $table->string('clinicalfinding');
            $table->string('Investigation');
            $table->string('Diagnosis');
            $table->string('Treatment');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('savedby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DentalClinic');
    }
}
