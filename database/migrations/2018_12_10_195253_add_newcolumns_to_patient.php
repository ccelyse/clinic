<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewcolumnsToPatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patient', function($table) {
            $table->string('beneficiary_province');
            $table->string('beneficiary_district');
            $table->string('beneficiary_sector');
            $table->string('beneficiary_cell');
            $table->string('beneficiary_village');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patient', function($table) {
            $table->string('beneficiary_province');
            $table->string('beneficiary_district');
            $table->string('beneficiary_sector');
            $table->string('beneficiary_cell');
            $table->string('beneficiary_village');
        });
    }
}
