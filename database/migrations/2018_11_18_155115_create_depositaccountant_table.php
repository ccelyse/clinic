<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositaccountantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depositaccountant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deposit_amount');
            $table->string('deposit_date');
            $table->string('depositcomment');
            $table->string('deposited_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depositaccountant');
    }
}
