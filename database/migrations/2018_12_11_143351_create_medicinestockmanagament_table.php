<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinestockmanagamentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicinestockmanagement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productname');
            $table->string('productpackage');
            $table->string('productpiece');
            $table->string('productunitprice');
            $table->string('shipmentstatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicinestockmanagement');
    }
}
