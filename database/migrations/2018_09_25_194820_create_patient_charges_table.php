<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patientcharges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('activity_name');
            $table->string('Medicine_service_name');
            $table->string('medicine_service_price');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('nurseid');
            $table->string('insurance_amount');
            $table->string('patient_amount');
            $table->string('insurance_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patientcharges');
    }
}
