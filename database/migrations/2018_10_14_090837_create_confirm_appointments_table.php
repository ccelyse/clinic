<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirm_appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dateconfirm');
            $table->string('timeconfirm');
            $table->string('appointment_message');
            $table->string('patient_id');
            $table->string('nurse_id');
            $table->string('patientrecoddateid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirm_appointments');
    }
}
