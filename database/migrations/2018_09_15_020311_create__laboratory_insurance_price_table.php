<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratoryInsurancePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Laboratory_insurance_price', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('labotest_id');
            $table->foreign('labotest_id')->references('id')->on('LaboratoryTest');
            $table->string('departmentid');
            $table->string('insuranceid');
            $table->string('laboratoryprice');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Laboratory_insurance_price');
    }
}
