<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mediservice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('medicinecode');
            $table->string('medicinename');
            $table->string('department_name_id');
            $table->string('medicinequantity');
            $table->string('insurance_medicine_id');
            $table->string('medicinepriceinsurance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediservice');
    }
}
