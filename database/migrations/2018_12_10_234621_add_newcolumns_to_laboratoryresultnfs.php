<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewcolumnsToLaboratoryresultnfs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laboratoryresultnfs', function($table) {
            $table->string('laboratorytype');
            $table->string('laboratorytestname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laboratoryresultnfs', function($table) {
            $table->string('laboratorytype');
            $table->string('laboratorytestname');
        });
    }
}
