<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_names');
            $table->string('patient_country_id');
            $table->unsignedInteger('insurance_patient_name');
            $table->foreign('insurance_patient_name')->references('id')->on('insurancedata');
            $table->string('insurance_card_number');
            $table->string('gender');
            $table->string('dateofbirth');
            $table->string('patient_telephone_number');
            $table->string('copay');
            $table->string('copay_relationship');
            $table->string('beneficiary_name');
            $table->string('beneficiary_telephone_number');
            $table->string('insured_location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
