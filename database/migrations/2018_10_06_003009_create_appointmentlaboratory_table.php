<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentlaboratoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointmentlaboratory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lab_tech');
            $table->string('nurse_id');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('Laboratorytechnician');
            $table->string('patient_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointmentlaboratory');
    }
}
