<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicineInsuranceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_insurance', function (Blueprint $table) {
            $table->increments('id');
            $table->string('medicine_id');
            $table->string('insurance_medicine_id');
            $table->string('department_name_id');
            $table->string('medicinepriceinsurance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_insurance');
    }
}
