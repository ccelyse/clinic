<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNfsimageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfsimage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nfsimage');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('Laboratorytechnician');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfsimage');
    }
}
