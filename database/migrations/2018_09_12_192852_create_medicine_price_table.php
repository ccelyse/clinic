<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_price', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medicine_id');
            $table->foreign('medicine_id')->references('id')->on('medicine_table');
            $table->unsignedInteger('medicine_department_id');
            $table->foreign('medicine_department_id')->references('id')->on('medicine_department_');
            $table->unsignedInteger('medicine_insurance_id');
            $table->foreign('medicine_insurance_id')->references('id')->on('medicine_insurance');
            $table->string('medicinepriceinsurance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_price');
    }
}
