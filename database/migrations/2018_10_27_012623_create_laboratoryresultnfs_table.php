<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratoryresultnfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laboratoryresultnfs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wbc');
            $table->string('ly');
            $table->string('mid');
            $table->string('gra');
            $table->string('rbc');
            $table->string('hgb');
            $table->string('hct');
            $table->string('plt');
            $table->string('ecbu');
            $table->string('eap');
            $table->string('fv_fu');
            $table->string('mvc');
            $table->string('mch');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('resultcategory');
            $table->string('Labotechid');
            $table->string('laboappoint_id');
            $table->string('laboratorytype');
            $table->string('laboratorytestname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratoryresultnfs');
    }
}
