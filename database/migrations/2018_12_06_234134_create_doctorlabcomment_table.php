<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorlabcommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctorlabcomment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resultsinterpretation');
            $table->string('finalconclusion');
            $table->string('treatment');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('doneby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctorlabcomment');
    }
}
