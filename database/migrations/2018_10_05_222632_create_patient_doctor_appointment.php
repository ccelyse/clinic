<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDoctorAppointment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_doctor_appointment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('doctor_id');
            $table->string('timeconfirm');
            $table->string('dateconfirm');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('nurse_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_doctor_appointment');
    }
}
