<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSOAPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SOAP', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subjective');
            $table->string('objective');
            $table->string('assessment');
            $table->string('plan');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('Labotechid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SOAP');
    }
}
