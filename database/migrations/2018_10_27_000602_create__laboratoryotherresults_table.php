<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratoryotherresultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Laboratoryotherresults', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('laboratoryresults');
            $table->longText('normalvalues');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('resultcategory');
            $table->string('Labotechid');
            $table->string('laboappoint_id');
            $table->string('laboratorytype');
            $table->string('laboratorytestname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Laboratoryotherresults');
    }
}
