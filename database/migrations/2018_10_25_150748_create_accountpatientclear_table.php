<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountpatientclearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountpatientclear', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount_due');
            $table->string('insurance_due');
            $table->string('amount_paid');
            $table->string('accountant_id');
            $table->string('patientrecoddateid');
            $table->string('patient_id');
            $table->string('paymentmethod');
            $table->string('insurance_id');
            $table->string('chargesreasons');
            $table->string('paymentcomment');
            $table->string('remainingbalance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountpatientclear');
    }
}
