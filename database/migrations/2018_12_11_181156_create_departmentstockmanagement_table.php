<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentstockmanagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DepartmentStockManagement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shipmentdepartment');
            $table->string('shipmentproductname');
            $table->string('shipmentproductpiece');
            $table->string('user_id');
            $table->string('shipment_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DepartmentStockManagement');
    }
}
