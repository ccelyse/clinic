<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patientWeight');
            $table->string('patient_Diastolic');
            $table->string('patient_Circumference');
            $table->string('patient_Pulse');
            $table->string('patient_Templocation');
            $table->string('patient_Respiration');
            $table->string('patient_OxygenSaturation');
            $table->string('patient_BMI');
            $table->string('patient_Temperature');
            $table->string('patient_BMIstatus');
            $table->string('patient_id');
            $table->string('patientrecoddateid');
            $table->string('nurseid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_measurements');
    }
}
