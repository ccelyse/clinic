<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/mail', function () {
//    \Mail::to(User::find(68))->send(new OrderEmail($getOrders,$getguestPhonenumberbold));
//});

Route::get('/',['as'=>'welcome','uses'=>'FrontendController@Home']);
Route::get('/Apply',['as'=>'Apply','uses'=>'FrontendController@Apply']);
Route::post('/Apply_',['as'=>'Apply_','uses'=>'FrontendController@Apply_']);


Route::get('/ContactUs', ['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/Test', ['as'=>'backend.Login','uses'=>'FrontendController@Login']);
Route::get('/LoginApp',['as'=>'login','uses'=>'FrontendController@Home']);
Route::post('/Login',['as'=>'login','uses'=>'BackendController@SignIn_']);

//Auth::routes();
Route::group(['middleware' => ['auth']], function () {
//        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::post('/getLogout', ['as' => 'backend.getLogout', 'uses' => 'BackendController@getLogout']);
//        Route::get('/AddaBrand', ['as' => 'backend.AddaBrand', 'uses' => 'BackendController@AddaBrand']);
        Route::get('/AddInsurance', ['as' => 'backend.AddInsurance', 'uses' => 'BackendController@AddInsurance']);
        Route::post('/AddInsurance_', ['as' => 'backend.AddInsurance_', 'uses' => 'BackendController@AddInsurance_']);
        Route::get('/ListInsurance', ['as' => 'backend.ListInsurance', 'uses' => 'BackendController@ListInsurance']);
        Route::get('/DeleteBrand', ['as' => 'backend.DeleteBrand', 'uses' => 'BackendController@DeleteBrand']);
        Route::get('/EditInsurance', ['as' => 'backend.EditInsurance', 'uses' => 'BackendController@EditInsurance']);
        Route::post('/EditInsurance_', ['as' => 'backend.EditInsurance_', 'uses' => 'BackendController@EditInsurance_']);
        Route::get('/CommunicationSendSms', ['as' => 'backend.CommunicationSendSms', 'uses' => 'BackendController@CommunicationSendSms']);
//        Route::post('/SendDatesSMS', ['as' => 'backend.SendDatesSMS', 'uses' => 'BackendController@SendDatesSMS']);
        Route::post('/CommunicationSendSmsFilter', ['as' => 'backend.CommunicationSendSmsFilter', 'uses' => 'BackendController@CommunicationSendSmsFilter']);
        Route::post('/FilterSendSMS', ['as' => 'backend.FilterSendSMS', 'uses' => 'BackendController@FilterSendSMS']);
        Route::get('/SmsList', ['as' => 'backend.SmsList', 'uses' => 'BackendController@SmsList']);

        Route::get('/AddDepartment', ['as' => 'backend.AddDepartment', 'uses' => 'BackendController@AddDepartment']);
        Route::post('/AddDepartment_', ['as' => 'backend.AddDepartment_', 'uses' => 'BackendController@AddDepartment_']);
        Route::get('/DeleteDepartment', ['as' => 'backend.DeleteDepartment', 'uses' => 'BackendController@DeleteDepartment']);
        Route::get('/EditDepartment', ['as' => 'backend.EditDepartment', 'uses' => 'BackendController@EditDepartment']);
        Route::post('/EditDepartment_', ['as' => 'backend.EditDepartment_', 'uses' => 'BackendController@EditDepartment_']);

        Route::get('/MedicalReport', ['as' => 'backend.MedicalReport', 'uses' => 'BackendController@MedicalReport']);
        Route::post('/MedicalReportFilter', ['as' => 'backend.MedicalReportFilter', 'uses' => 'BackendController@MedicalReportFilter']);


        Route::get('/AddMedicines', ['as' => 'backend.AddMedicines', 'uses' => 'BackendController@AddMedicines']);
        Route::get('/AddService', ['as' => 'backend.AddService', 'uses' => 'BackendController@AddService']);
        Route::get('/ListServices', ['as' => 'backend.ListService', 'uses' => 'BackendController@ListServices']);
        Route::post('/SearchServices', ['as' => 'backend.SearchServices', 'uses' => 'BackendController@SearchServices']);
        Route::get('/EditMedecine', ['as' => 'backend.EditMedecine', 'uses' => 'BackendController@EditMedecine']);
        Route::post('/AddMedicines_', ['as' => 'backend.AddMedicines_', 'uses' => 'BackendController@AddMedicines_']);
        Route::post('/AddMedicineExcel', ['as' => 'backend.AddMedicineExcel', 'uses' => 'BackendController@AddMedicineExcel']);
        Route::post('/AddMedicineExcelUpdate', ['as' => 'backend.AddMedicineExcelUpdate', 'uses' => 'BackendController@AddMedicineExcelUpdate']);
        Route::get('/EditMedicine', ['as' => 'backend.EditMedicine', 'uses' => 'BackendController@EditMedicine']);
        Route::post('/EditMedicine_', ['as' => 'backend.EditMedicine_', 'uses' => 'BackendController@EditMedicine_']);
        Route::post('/UpdateNewInsurancePrice', ['as' => 'backend.UpdateNewInsurancePrice', 'uses' => 'BackendController@UpdateNewInsurancePrice']);
        Route::get('/DeleteMedicine', ['as' => 'backend.DeleteMedicine', 'uses' => 'BackendController@DeleteMedicine']);

        Route::get('/StockManagement', ['as' => 'backend.StockManagement', 'uses' => 'BackendController@StockManagement']);
        Route::get('/StockManagementReport', ['as' => 'backend.StockManagementReport', 'uses' => 'BackendController@StockManagementReport']);
        Route::post('/StockManagementFilter', ['as' => 'backend.StockManagementFilter', 'uses' => 'BackendController@StockManagementFilter']);
        Route::get('/StockManagementRequisition', ['as' => 'backend.StockManagementRequisition', 'uses' => 'BackendController@StockManagementRequisition']);
        Route::post('/StockManagementUpdate', ['as' => 'backend.StockManagementUpdate', 'uses' => 'BackendController@StockManagementUpdate']);
        Route::post('/AddMedicineStock', ['as' => 'backend.AddMedicineStock', 'uses' => 'BackendController@AddMedicineStock']);
        Route::post('/UpdateMedicineStock', ['as' => 'backend.UpdateMedicineStock', 'uses' => 'BackendController@UpdateMedicineStock']);
        Route::get('/RemoveMedicineStock', ['as' => 'backend.RemoveMedicineStock', 'uses' => 'BackendController@RemoveMedicineStock']);
        Route::post('/MedicineStockShipment', ['as' => 'backend.MedicineStockShipment', 'uses' => 'BackendController@MedicineStockShipment']);
        Route::get('/RemoveMedicineStockShipment', ['as' => 'backend.RemoveMedicineStockShipment', 'uses' => 'BackendController@RemoveMedicineStockShipment']);
        Route::get('/DepartmentStockManagement', ['as' => 'backend.DepartmentStockManagement', 'uses' => 'BackendController@DepartmentStockManagement']);
        Route::post('/AddDepartmentStockManagement', ['as' => 'backend.AddDepartmentStockManagement', 'uses' => 'BackendController@AddDepartmentStockManagement']);
        Route::get('/DeleteDepartmentStockManagement', ['as' => 'backend.DeleteDepartmentStockManagement', 'uses' => 'BackendController@DeleteDepartmentStockManagement']);


        Route::get('/LaboratoryTest', ['as' => 'backend.LaboratoryTest', 'uses' => 'BackendController@LaboratoryTest']);
        Route::post('/LaboratoryTest_', ['as' => 'backend.LaboratoryTest_', 'uses' => 'BackendController@LaboratoryTest_']);
        Route::get('/EditLaboTest', ['as' => 'backend.EditLaboTest', 'uses' => 'BackendController@EditLaboTest']);
        Route::post('/EditLaboTest_', ['as' => 'backend.EditLaboTest_', 'uses' => 'BackendController@EditLaboTest_']);
        Route::get('/DeleteLaboratoryTest', ['as' => 'backend.DeleteLaboratoryTest', 'uses' => 'BackendController@DeleteLaboratoryTest']);
        Route::post('/AddLaboratoryTestExcel', ['as' => 'backend.AddLaboratoryTestExcel', 'uses' => 'BackendController@AddLaboratoryTestExcel']);

        Route::get('/LaboratoryPatientHistory', ['as' => 'backend.LaboratoryPatientHistory', 'uses' => 'BackendController@LaboratoryPatientHistory']);
        Route::get('/LaboOtherResults', ['as' => 'backend.LaboOtherResults', 'uses' => 'BackendController@LaboOtherResults']);
        Route::post('/AddOtherResultsPatients', ['as' => 'backend.AddOtherResultsPatients', 'uses' => 'BackendController@AddOtherResultsPatients']);
        Route::get('/DeleteOtherResultsPatients', ['as' => 'backend.DeleteOtherResultsPatients', 'uses' => 'BackendController@DeleteOtherResultsPatients']);
        Route::post('/EditOtherResultsPatients', ['as' => 'backend.EditOtherResultsPatients', 'uses' => 'BackendController@EditOtherResultsPatients']);
        Route::get('/LaboratoryNFS', ['as' => 'backend.LaboratoryNFS', 'uses' => 'BackendController@LaboratoryNFS']);
        Route::post('/AddOtherResultsPatientsNFS', ['as' => 'backend.AddOtherResultsPatientsNFS', 'uses' => 'BackendController@AddOtherResultsPatientsNFS']);
        Route::post('/EditOtherResultsPatientsNFS', ['as' => 'backend.EditOtherResultsPatientsNFS', 'uses' => 'BackendController@EditOtherResultsPatientsNFS']);
        Route::get('/DeleteOtherResultsPatientsNFS', ['as' => 'backend.DeleteOtherResultsPatientsNFS', 'uses' => 'BackendController@DeleteOtherResultsPatientsNFS']);
        Route::post('/AddNfsImage', ['as' => 'backend.AddNfsImage', 'uses' => 'BackendController@AddNfsImage']);


        Route::get('/LaboratoryTech', ['as' => 'backend.Laboratory', 'uses' => 'BackendController@LaboratoryTech']);
        Route::get('/LaboratoryAllPatient', ['as' => 'backend.LaboratoryAllPatient', 'uses' => 'BackendController@LaboratoryAllPatient']);
        Route::get('/LaboratoryTechTest', ['as' => 'backend.LaboratoryTechTest', 'uses' => 'BackendController@LaboratoryTechTest']);
        Route::post('/ResultNotifyPatient', ['as' => 'backend.ResultNotifyPatient', 'uses' => 'BackendController@ResultNotifyPatient']);
        Route::post('/LabNotifyDoctor', ['as' => 'backend.LabNotifyDoctor', 'uses' => 'BackendController@LabNotifyDoctor']);
        Route::get('/LaboratoryResultsPrint', ['as' => 'backend.LaboratoryResultsPrint', 'uses' => 'BackendController@LaboratoryResultsPrint']);
        Route::get('/LaboratoryResultsPrintNFS', ['as' => 'backend.LaboratoryResultsPrintNFS', 'uses' => 'BackendController@LaboratoryResultsPrintNFS']);
        Route::get('/LaboratoryReport', ['as' => 'backend.LaboratoryReport', 'uses' => 'BackendController@LaboratoryReport']);
        Route::post('/ChangeLaboratoryStatus', ['as' => 'backend.ChangeLaboratoryStatus', 'uses' => 'BackendController@ChangeLaboratoryStatus']);
        Route::post('/laboratoryReportFilter', ['as' => 'backend.laboratoryReportFilter', 'uses' => 'BackendController@laboratoryReportFilter']);
        Route::post('/GetLaboratoryTestName', ['as' => 'backend.GetLaboratoryTestName', 'uses' => 'BackendController@GetLaboratoryTestName']);

        Route::get('/Rooms', ['as' => 'backend.Rooms', 'uses' => 'BackendController@Rooms']);
        Route::post('/Rooms_', ['as' => 'backend.Rooms_', 'uses' => 'BackendController@Rooms_']);
        Route::get('/EditRoomInfo', ['as' => 'backend.EditRoomInfo', 'uses' => 'BackendController@EditRoomInfo']);
        Route::post('/EditRoomInfo_', ['as' => 'backend.EditRoomInfo_', 'uses' => 'BackendController@EditRoomInfo_']);
        Route::get('/DeleteRoomInfo', ['as' => 'backend.DeleteRoomInfo', 'uses' => 'BackendController@DeleteRoomInfo']);
        Route::get('/RoomPrice', ['as' => 'backend.RoomPrice', 'uses' => 'BackendController@RoomPrice']);
        Route::post('/RoomPrice_', ['as' => 'backend.RoomPrice_', 'uses' => 'BackendController@RoomPrice_']);
        Route::get('/EditRoomPrice', ['as' => 'backend.EditRoomPrice', 'uses' => 'BackendController@EditRoomPrice']);
        Route::post('/EditRoomPrice_', ['as' => 'backend.EditRoomPrice_', 'uses' => 'BackendController@EditRoomPrice_']);
        Route::get('/DeleteRoomPrices', ['as' => 'backend.DeleteRoomPrices', 'uses' => 'BackendController@DeleteRoomPrices']);

        Route::get('/Reception', ['as' => 'backend.Reception', 'uses' => 'BackendController@Reception']);
        Route::post('/Reception_', ['as' => 'backend.Reception_', 'uses' => 'BackendController@Reception_']);
        Route::get('/ListReception', ['as' => 'backend.ListReception', 'uses' => 'BackendController@ListReception']);
        Route::get('/ListReceptionReport', ['as' => 'backend.ListReceptionReport', 'uses' => 'BackendController@ListReceptionReport']);
        Route::post('/ListReceptionReportFilter', ['as' => 'backend.ListReceptionReportFilter', 'uses' => 'BackendController@ListReceptionReportFilter']);
        Route::get('/NewRecordSearch', ['as' => 'backend.NewRecordSearch', 'uses' => 'BackendController@NewRecordSearch']);
        Route::post('/SearchReception', ['as' => 'backend.SearchReception', 'uses' => 'BackendController@SearchReception']);
        Route::get('/EditPatient', ['as' => 'backend.EditPatient', 'uses' => 'BackendController@EditPatient']);
        Route::post('/EditPatient_', ['as' => 'backend.EditPatient_', 'uses' => 'BackendController@EditPatient_']);
        Route::get('/DeletePatient', ['as' => 'backend.DeletePatient', 'uses' => 'BackendController@DeletePatient']);
        Route::get('/ReceptionDashboard', ['as' => 'backend.ReceptionDashboard', 'uses' => 'BackendController@ReceptionDashboard']);
        Route::get('/ReceptionAccountant', ['as' => 'backend.ReceptionAccountant', 'uses' => 'BackendController@ReceptionAccountant']);
        Route::get('/ReceptionAccountPatient', ['as' => 'backend.ReceptionAccountPatient', 'uses' => 'BackendController@ReceptionAccountPatient']);
        Route::post('/NewRecordHistory', ['as' => 'backend.NewRecordHistory', 'uses' => 'BackendController@NewRecordHistory']);
        Route::get('/DoctorPatientHistoryConsult', ['as' => 'backend.DoctorPatientHistoryConsult', 'uses' => 'BackendController@DoctorPatientHistoryConsult']);
        Route::get('/DoctorOperationPatientRecordHistory', ['as' => 'backend.DoctorOperationPatientRecordHistory', 'uses' => 'BackendController@DoctorOperationPatientRecordHistory']);
        Route::get('/DoctorPatientHistoryOperation', ['as' => 'backend.DoctorPatientHistoryOperation', 'uses' => 'BackendController@DoctorPatientHistoryOperation']);
        Route::post('/ReceptionAccountantFilter', ['as' => 'backend.ReceptionAccountantFilter', 'uses' => 'BackendController@ReceptionAccountantFilter']);
        Route::get('/ReceptionAppointment', ['as' => 'backend.ReceptionAppointment', 'uses' => 'BackendController@ReceptionAppointment']);
        Route::get('/ReceptionAppPatientHistory', ['as' => 'backend.ReceptionAppPatientHistory', 'uses' => 'BackendController@ReceptionAppPatientHistory']);
        Route::get('/ReceptionDoctorAppointment', ['as' => 'backend.ReceptionDoctorAppointment', 'uses' => 'BackendController@ReceptionDoctorAppointment']);
        Route::post('/GetDistrict', ['as' => 'backend.GetDistrict', 'uses' => 'BackendController@GetDistrict']);
        Route::post('/GetSector', ['as' => 'backend.GetSector', 'uses' => 'BackendController@GetSector']);
        Route::post('/GetCell', ['as' => 'backend.GetCell', 'uses' => 'BackendController@GetCell']);
        Route::post('/GetVillage', ['as' => 'backend.GetVillage', 'uses' => 'BackendController@GetVillage']);


        Route::get('/InsuranceVerification', ['as' => 'backend.InsuranceVerification', 'uses' => 'BackendController@InsuranceVerification']);
        Route::post('/VerifyPatient', ['as' => 'backend.InsuranceVerification', 'uses' => 'BackendController@VerifyPatient']);
        Route::get('/DeleteVerifyPatient', ['as' => 'backend.DeleteVerifyPatient', 'uses' => 'BackendController@DeleteVerifyPatient']);
        Route::post('/DoctorDentist', ['as' => 'backend.DoctorDentist', 'uses' => 'BackendController@DoctorDentist']);


        Route::get('/NurseDashboard', ['as' => 'backend.NurseDashboard', 'uses' => 'BackendController@NurseDashboard']);
        Route::get('/NurseConsultation', ['as' => 'backend.NurseConsultation', 'uses' => 'BackendController@NurseConsultation']);
        Route::get('/PatientHistoryRecord', ['as' => 'backend.PatientHistoryRecord', 'uses' => 'BackendController@PatientHistoryRecord']);
        Route::get('/NurseOperationPatientRecordHistory', ['as' => 'backend.NurseOperationPatientRecordHistory', 'uses' => 'BackendController@NurseOperationPatientRecordHistory']);
        Route::get('/NurseConsultationPatient', ['as' => 'backend.NurseConsultationPatient', 'uses' => 'BackendController@NurseConsultationPatient']);
        Route::post('/AddPatientMeasurement', ['as' => 'backend.AddPatientMeasurement', 'uses' => 'BackendController@AddPatientMeasurement']);
        Route::get('/PatientOperations', ['as' => 'backend.PatientOperations', 'uses' => 'BackendController@PatientOperations']);
        Route::post('/EditPatientMeasurement', ['as' => 'backend.EditPatientMeasurement', 'uses' => 'BackendController@EditPatientMeasurement']);
        Route::get('/DeletePatientMeasurement', ['as' => 'backend.DeletePatientMeasurement', 'uses' => 'BackendController@DeletePatientMeasurement']);
        Route::get('/LaboratoryDashboard', ['as' => 'backend.LaboratoryDashboard', 'uses' => 'BackendController@LaboratoryDashboard']);

        Route::get('/Nurse', ['as' => 'backend.Nurse', 'uses' => 'BackendController@Nurse']);
        Route::get('/NursePatient', ['as' => 'backend.NursePatient', 'uses' => 'BackendController@NursePatient']);
        Route::get('/PatientConsultaton', ['as' => 'backend.PatientConsultaton', 'uses' => 'BackendController@PatientConsultaton']);
        Route::post('/PatientConsultaton_', ['as' => 'backend.PatientConsultaton_', 'uses' => 'BackendController@PatientConsultaton_']);
        Route::get('/DeletePatientConsultaton', ['as' => 'backend.DeletePatientConsultaton', 'uses' => 'BackendController@DeletePatientConsultaton']);
        Route::post('/EditPatientConsultaton_', ['as' => 'backend.EditPatientConsultaton_', 'uses' => 'BackendController@EditPatientConsultaton_']);
        Route::post('/PatientCharges', ['as' => 'backend.PatientCharges', 'uses' => 'BackendController@PatientCharges']);
        Route::post('/PatientChargesNurse', ['as' => 'backend.PatientChargesNurse', 'uses' => 'BackendController@PatientChargesNurse']);
        Route::post('/PatientChargeSelectMedicine', ['as' => 'backend.PatientChargeSelectMedicine', 'uses' => 'BackendController@PatientChargeSelectMedicine']);
        Route::post('/PatientCharges_', ['as' => 'backend.PatientCharges_', 'uses' => 'BackendController@PatientCharges_']);
        Route::post('/AddPatientCharges', ['as' => 'backend.AddPatientCharges', 'uses' => 'BackendController@AddPatientCharges']);
        Route::post('/AddAppointmentDoctors', ['as' => 'backend.AddAppointmentDoctors', 'uses' => 'BackendController@AddAppointmentDoctors']);
        Route::post('/EditAddAppointmentDoctors', ['as' => 'backend.EditAddAppointmentDoctors', 'uses' => 'BackendController@EditAddAppointmentDoctors']);
        Route::get('/DeleteAddAppointmentDoctors', ['as' => 'backend.DeleteAddAppointmentDoctors', 'uses' => 'BackendController@DeleteAddAppointmentDoctors']);
        Route::post('/AddAppointmentLabo', ['as' => 'backend.AddAppointmentLabo', 'uses' => 'BackendController@AddAppointmentLabo']);
        Route::post('/EditAppointmentLabo', ['as' => 'backend.EditAppointmentLabo', 'uses' => 'BackendController@EditAppointmentLabo']);
        Route::get('/DeleteAddAppointmentLabo', ['as' => 'backend.DeleteAddAppointmentLabo', 'uses' => 'BackendController@DeleteAddAppointmentLabo']);
//        Route::get('/EditPatientCharges', ['as' => 'backend.EditPatientCharges', 'uses' => 'BackendController@EditPatientCharges']);
        Route::get('/DeletePatientCharges', ['as' => 'backend.DeletePatientCharges', 'uses' => 'BackendController@DeletePatientCharges']);
        Route::post('/UpdateAccountPatientClear', ['as' => 'backend.UpdateAccountPatientClear', 'uses' => 'BackendController@UpdateAccountPatientClear']);
        Route::get('/CheckOut', ['as' => 'backend.CheckOut', 'uses' => 'BackendController@CheckOut']);
        Route::post('/ClearAllCharges', ['as' => 'backend.ClearAllCharges', 'uses' => 'BackendController@ClearAllCharges']);
        Route::post('/AddLaboratoryService', ['as' => 'backend.AddLaboratoryService', 'uses' => 'BackendController@AddLaboratoryService']);




        Route::get('/Accountant', ['as' => 'backend.Accountant', 'uses' => 'BackendController@Accountant']);
        Route::get('/AccountantPatientPay', ['as' => 'backend.AccountantPatientPay', 'uses' => 'BackendController@AccountantPatientPay']);
        Route::post('/SearchAccountantPatientPay', ['as' => 'backend.SearchAccountantPatientPay', 'uses' => 'BackendController@SearchAccountantPatientPay']);
        Route::get('/AccountantPatientHistory', ['as' => 'backend.AccountantPatientHistory', 'uses' => 'BackendController@AccountantPatientHistory']);
        Route::get('/AccountantPatient', ['as' => 'backend.AccountantPatient', 'uses' => 'BackendController@AccountantPatient']);
        Route::get('/AccountantViewMore', ['as' => 'backend.AccountantViewMore', 'uses' => 'BackendController@AccountantViewMore']);
        Route::post('/AccountPatientClear', ['as' => 'backend.AccountPatientClear', 'uses' => 'BackendController@AccountPatientClear']);
        Route::post('/AccountantUpdatePaymentStatus', ['as' => 'backend.AccountantUpdatePaymentStatus', 'uses' => 'BackendController@AccountantUpdatePaymentStatus']);
        Route::get('/AccountantSales', ['as' => 'backend.AccountantSales', 'uses' => 'BackendController@AccountantSales']);
        Route::post('/AccountantSalesFilter', ['as' => 'backend.AccountantSalesFilter', 'uses' => 'BackendController@AccountantSalesFilter']);
        Route::get('/HospitalizeSales', ['as' => 'backend.HospitalizeSales', 'uses' => 'BackendController@HospitalizeSales']);
        Route::post('/HospitalizeAccountantSalesFilter', ['as' => 'backend.HospitalizeAccountantSalesFilter', 'uses' => 'BackendController@HospitalizeAccountantSalesFilter']);

        Route::get('/AccountantInsuranceReport', ['as' => 'backend.AccountantInsuranceReport', 'uses' => 'BackendController@AccountantInsuranceReport']);
        Route::get('/AccountantCashierSales', ['as' => 'backend.AccountantCashierSales', 'uses' => 'BackendController@AccountantCashierSales']);
        Route::post('/AccountantCashierSalesFilter', ['as' => 'backend.AccountantCashierSalesFilter', 'uses' => 'BackendController@AccountantCashierSalesFilter']);
        Route::post('/AccountantInsuranceReportFilter', ['as' => 'backend.AccountantInsuranceReportFilter', 'uses' => 'BackendController@AccountantInsuranceReportFilter']);
        Route::get('/AccountantCashierDeposit', ['as' => 'backend.AccountantCashierDeposit', 'uses' => 'BackendController@AccountantCashierDeposit']);
        Route::post('/AccountantCashierDeposit_', ['as' => 'backend.AccountantCashierDeposit_', 'uses' => 'BackendController@AccountantCashierDeposit_']);
        Route::post('/AccountantCashierDepositEdit', ['as' => 'backend.AccountantCashierDepositEdit', 'uses' => 'BackendController@AccountantCashierDepositEdit']);
        Route::get('/AccountCashierDepositReport', ['as' => 'backend.AccountCashierDepositReport', 'uses' => 'BackendController@AccountCashierDepositReport']);
        Route::post('/AccountCashierDepositReportFilter', ['as' => 'backend.AccountCashierDepositReportFilter', 'uses' => 'BackendController@AccountCashierDepositReportFilter']);


        Route::get('/HospitalizationDashboard', ['as' => 'backend.HospitalizationDashboard', 'uses' => 'BackendController@HospitalizationDashboard']);
        Route::get('/HospitalizePatientHistory', ['as' => 'backend.HospitalizePatientHistory', 'uses' => 'BackendController@HospitalizePatientHistory']);
        Route::get('/DeleteHospitalizePatient', ['as' => 'backend.DeleteHospitalizePatient', 'uses' => 'BackendController@DeleteHospitalizePatient']);
        Route::get('/Hospitalize', ['as' => 'backend.Hospitalize', 'uses' => 'BackendController@Hospitalize']);
        Route::get('/HospitalizePatient', ['as' => 'backend.HospitalizePatient', 'uses' => 'BackendController@HospitalizePatient']);
        Route::post('/GetRoomLevelAjax', ['as' => 'backend.GetRoomLevelAjax', 'uses' => 'BackendController@GetRoomLevelAjax']);
        Route::post('/GetRoomPriceAjax', ['as' => 'backend.GetRoomPriceAjax', 'uses' => 'BackendController@GetRoomPriceAjax']);
        Route::post('/AddHospitalizePatient', ['as' => 'backend.AddHospitalizePatient', 'uses' => 'BackendController@AddHospitalizePatient']);
        Route::get('/HospitalizeReport', ['as' => 'backend.HospitalizeReport', 'uses' => 'BackendController@HospitalizeReport']);
        Route::post('/HospitalizeReportFilter', ['as' => 'backend.HospitalizeReportFilter', 'uses' => 'BackendController@HospitalizeReportFilter']);


        Route::get('/MarkAsRead', ['as' => 'backend.MarkAsRead', 'uses' => 'BackendController@MarkAsRead']);
        Route::get('/DoctorDashboard', ['as' => 'backend.DoctorDashboard', 'uses' => 'BackendController@DoctorDashboard']);
        Route::get('/Doctor', ['as' => 'backend.Doctor', 'uses' => 'BackendController@Doctor']);
        Route::get('/DoctorConsultation', ['as' => 'backend.Doctor', 'uses' => 'BackendController@DoctorConsultation']);
        Route::post('/SearchPatientData', ['as' => 'backend.SearchPatientData', 'uses' => 'BackendController@SearchPatientData']);
        Route::get('/DoctorPatientHistoryRecord', ['as' => 'backend.DoctorPatientHistoryRecord', 'uses' => 'BackendController@DoctorPatientHistoryRecord']);
        Route::get('/DoctorOperations', ['as' => 'backend.Doctor', 'uses' => 'BackendController@DoctorOperations']);
        Route::get('/DoctorPatient', ['as' => 'backend.DoctorPatient', 'uses' => 'BackendController@DoctorPatient']);
        Route::post('/ConfirmPatientStatus', ['as' => 'backend.ConfirmPatientStatus', 'uses' => 'BackendController@ConfirmPatientStatus']);
        Route::get('/DoctorLaboratoryResults', ['as' => 'backend.DoctorLaboratoryResults', 'uses' => 'BackendController@DoctorLaboratoryResults']);
        Route::get('/DoctorLaboratoryPatientHistory', ['as' => 'backend.DoctorLaboratoryPatientHistory', 'uses' => 'BackendController@DoctorLaboratoryPatientHistory']);
        Route::get('/DoctorLaboratoryTechTest', ['as' => 'backend.DoctorLaboratoryTechTest', 'uses' => 'BackendController@DoctorLaboratoryTechTest']);
        Route::get('/DoctorsReport', ['as' => 'backend.DoctorsReport', 'uses' => 'BackendController@DoctorsReport']);
        Route::post('/DoctorsReportFilter', ['as' => 'backend.DoctorsReportFilter', 'uses' => 'BackendController@DoctorsReportFilter']);
        Route::get('/ClinicPatientProfile', ['as' => 'backend.ClinicPatientProfile', 'uses' => 'BackendController@ClinicPatientProfile']);
        Route::get('/ClinicPatientProfileRecord', ['as' => 'backend.ClinicPatientProfileRecord', 'uses' => 'BackendController@ClinicPatientProfileRecord']);
        Route::get('/ClinicPatientProfileInfo', ['as' => 'backend.ClinicPatientProfileInfo', 'uses' => 'BackendController@ClinicPatientProfileInfo']);
        Route::post('/DoctorTestComment', ['as' => 'backend.DoctorTestComment', 'uses' => 'BackendController@DoctorTestComment']);
        Route::post('/DoctorTestCommentEdit', ['as' => 'backend.DoctorTestCommentEdit', 'uses' => 'BackendController@DoctorTestCommentEdit']);
        Route::get('/DoctorTestCommentDelete', ['as' => 'backend.DoctorTestCommentDelete', 'uses' => 'BackendController@DoctorTestCommentDelete']);
        Route::get('/AccountantExpenses', ['as' => 'backend.AccountantExpenses', 'uses' => 'BackendController@AccountantExpenses']);
        Route::post('/AddAccountantExpenses', ['as' => 'backend.AddAccountantExpenses', 'uses' => 'BackendController@AddAccountantExpenses']);
        Route::post('/EditAccountantExpenses', ['as' => 'backend.EditAccountantExpenses', 'uses' => 'BackendController@EditAccountantExpenses']);
        Route::get('/DeleteAccountantExpenses', ['as' => 'backend.DeleteAccountantExpenses', 'uses' => 'BackendController@DeleteAccountantExpenses']);
        Route::get('/AccountantExpensesReport', ['as' => 'backend.AccountantExpensesReport', 'uses' => 'BackendController@AccountantExpensesReport']);
        Route::post('/AccountantExpensesReportFilter', ['as' => 'backend.AccountantExpensesReportFilter', 'uses' => 'BackendController@AccountantExpensesReportFilter']);


        Route::get('/DentistConsultation', ['as' => 'backend.DentistConsultation', 'uses' => 'BackendController@DentistConsultation']);
        Route::get('/DentistPatientHistoryRecord', ['as' => 'backend.DentistPatientHistoryRecord', 'uses' => 'BackendController@DentistPatientHistoryRecord']);
        Route::get('/DentistPatientHistoryConsult', ['as' => 'backend.DentistPatientHistoryConsult', 'uses' => 'BackendController@DentistPatientHistoryConsult']);
        Route::get('/DentistPatientHistoryConsult', ['as' => 'backend.DentistPatientHistoryConsult', 'uses' => 'BackendController@DentistPatientHistoryConsult']);

        Route::get('/DentistOperationPatientRecordHistory', ['as' => 'backend.DentistOperationPatientRecordHistory', 'uses' => 'BackendController@DentistOperationPatientRecordHistory']);
        Route::get('/DentistPatientHistoryOperation', ['as' => 'backend.DentistPatientHistoryOperation', 'uses' => 'BackendController@DentistPatientHistoryOperation']);
        Route::get('/DoctorDentistDashboard', ['as' => 'backend.DoctorDentist', 'uses' => 'BackendController@DoctorDentistDashboard']);
        Route::post('/SOAP', ['as' => 'backend.SOAP', 'uses' => 'BackendController@SOAP']);
        Route::post('/EditDoctorDentist', ['as' => 'backend.EditDoctorDentist', 'uses' => 'BackendController@EditDoctorDentist']);
        Route::get('/DeleteDoctorDentist', ['as' => 'backend.DeleteDoctorDentist', 'uses' => 'BackendController@DeleteDoctorDentist']);
        Route::post('/SOAPEdit', ['as' => 'backend.SOAPEdit', 'uses' => 'BackendController@SOAPEdit']);
        Route::post('/ConfirmAppointment', ['as' => 'backend.ConfirmAppointment', 'uses' => 'BackendController@ConfirmAppointment']);
        Route::post('/ConfirmAppointmentEdit', ['as' => 'backend.ConfirmAppointmentEdit', 'uses' => 'BackendController@ConfirmAppointmentEdit']);
        Route::get('/ConfirmAppointmentDelete', ['as' => 'backend.ConfirmAppointmentDelete', 'uses' => 'BackendController@ConfirmAppointmentDelete']);


        Route::get('/AccountantDashboard', ['as' => 'backend.AccountantDashboard', 'uses' => 'BackendController@AccountantDashboard']);
        Route::get('/Accountant', ['as' => 'backend.Accountant', 'uses' => 'BackendController@Accountant']);
        Route::get('/InvoiceTobeGenerated', ['as' => 'backend.InvoiceTobeGenerated', 'uses' => 'BackendController@InvoiceTobeGenerated']);
        Route::get('/InvoiceTobeGeneratedSummary', ['as' => 'backend.InvoiceTobeGeneratedSummary', 'uses' => 'BackendController@InvoiceTobeGeneratedSummary']);
        Route::get('/InvoiceTobeGeneratedSummaryPrivate', ['as' => 'backend.InvoiceTobeGeneratedSummaryPrivate', 'uses' => 'BackendController@InvoiceTobeGeneratedSummaryPrivate']);
        Route::get('/InvoiceHospitalize', ['as' => 'backend.InvoiceHospitalize', 'uses' => 'BackendController@InvoiceHospitalize']);
        Route::get('/PayedSales', ['as' => 'backend.PayedSales', 'uses' => 'BackendController@PayedSales']);
        Route::get('/DeletePayedSales', ['as' => 'backend.DeletePayedSales', 'uses' => 'BackendController@DeletePayedSales']);
        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
        Route::post('/CreateAccountUpdate', ['as' => 'backend.CreateAccountUpdate', 'uses' => 'BackendController@CreateAccountUpdate']);
        Route::get('/CreateAccountDelete', ['as' => 'backend.CreateAccountDelete', 'uses' => 'BackendController@CreateAccountDelete']);

        Route::get('/ShowSession', ['as' => 'backend.ShowSession', 'uses' => 'BackendController@ShowSession']);
        Route::get('/SetSession', ['as' => 'backend.SetSession', 'uses' => 'BackendController@SetSession']);
        Route::get('/deleteSessionData', ['as' => 'backend.deleteSessionData', 'uses' => 'BackendController@deleteSessionData']);
//        Route::get('PdfView',array('as'=>'backend.PdfView','uses'=>'BackendController@PdfView'));
//        Route::get('/PdfView_', ['as' => 'backend.PdfView_', 'uses' => 'BackendController@PdfView_']);
//        Route::get('/ViewMore', ['as' => 'backend.ViewMore', 'uses' => 'BackendController@ViewMore']);
////        Route::get('/PdfView','BackendController@export_pdf');
//        Route::post('/AddAttractions_', ['as' => 'backend.AddAttractions_', 'uses' => 'BackendController@AddAttractions_']);
});





